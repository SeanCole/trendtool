﻿using System;

namespace TechneauxUtility
{
    public class TimePeriod
    {
        public TimePeriod(DateTime newStart, DateTime newEnd)
        {
            Start = newStart;
            End = newEnd;
        }

        public DateTime Start { get; }
        public DateTime End { get; }
    }
}