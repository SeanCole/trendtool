﻿using System.Collections.Generic;

namespace TechneauxUtility
{
    public interface IValueSet
    {

        Dictionary<string, IConvertibleValue> CellValues { get; set; }

    }
}