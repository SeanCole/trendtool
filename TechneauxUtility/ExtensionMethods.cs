﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechneauxUtility
{
    public static class ExtensionMethods
    {
        public static bool IsLikePattern(this string srcStr, string compStrPattern)
        {
            return LikeOperator.LikeString(srcStr, compStrPattern, Microsoft.VisualBasic.CompareMethod.Text);
        }

        public static bool IsLikePatterns(this string srcStr, IEnumerable<string> patternList)
        {
            return patternList.Any(srcStr.IsLikePattern);
        }

        public static bool IsPatternListIn(this IEnumerable<string> patternList, IEnumerable<string> compList)
        {
            return compList.Any(comp => comp.IsLikePatterns(patternList));
        }

        public static bool IsAny<T>(this IEnumerable<T> srcEnum)
        {
            return (srcEnum != null && srcEnum.Any());
        }
    }

    public static class DictionaryExtensions
    {
        public static bool IsEqualTo(
            this IDictionary<string, IConvertibleValue> srcDict,
            IDictionary<string, IConvertibleValue> compDict)
        {
            // Check if keys match

            var srcKeySet = new HashSet<string>(srcDict.Keys);

            if (!srcKeySet.SetEquals(compDict.Keys))
                return false;

            var mismatchedValues = srcKeySet.Where(key => !srcDict[key].Equals(compDict[key]));

            return !mismatchedValues.Any();
        }

        //public static IDictionary<string, IValueSet> IsNotIn(
        //    this IDictionary<string, IValueSet> srcDict,
        //    IDictionary<string, IValueSet> compDict)
        //{


        //}
    }
}
