﻿using System;
using static System.Convert;

namespace TechneauxUtility
{
    public class ConvertibleValue : IConvertibleValue
    {
        public ConvertibleValue(object newVal) => RawValue = newVal;

        public object RawValue { get; }

        public string StringValue => ToString();

        public bool TryGetBool(out bool value)
        {
            switch (RawValue)
            {
                case null:
                    value = false;
                    return false;
                case bool b:
                    value = b;
                    return true;
                default:
                    switch (RawValue.ToString())
                    {
                        case "Y":
                            value = true;
                            return true;
                        case "N":
                            value = false;
                            return true;
                        default:
                            return bool.TryParse(RawValue.ToString(), out value);
                    }
            }
        }

        public bool TryGetDateTime(out DateTime value)
        {
            switch (RawValue)
            {
                case null:
                    value = DateTime.Now;
                    return false;
                case DateTime dt:
                    value = dt;
                    return true;
                default:
                    return DateTime.TryParse(RawValue.ToString(), out value);
            }
        }

        public bool TryGetInt(out int value)
        {
            switch (RawValue)
            {
                case null:
                    value = 0;
                    return false;
                case double d:
                    value = Convert.ToInt32(d);
                    return true;
                case int i:
                    value = i;
                    return true;
                default:
                    return int.TryParse(RawValue.ToString(), out value);
            }
        }

        public bool TryGetDouble(out double value)
        {
            switch (RawValue)
            {
                case null:
                    value = 0;
                    return false;
                case double d:
                    value = d;
                    return true;
                case int i:
                    value = Convert.ToDouble(i);
                    return true;
                default:
                    return double.TryParse(RawValue.ToString(), out value);
            }
        }

        public override string ToString() => RawValue?.ToString() ?? "";

        public TypeCode GetTypeCode() => Convert.GetTypeCode(RawValue);

        public bool ToBoolean(IFormatProvider provider) => Convert.ToBoolean(RawValue, provider);

        public char ToChar(IFormatProvider provider) => Convert.ToChar(RawValue, provider);

        public sbyte ToSByte(IFormatProvider provider) => Convert.ToSByte(RawValue, provider);

        public byte ToByte(IFormatProvider provider) => Convert.ToByte(RawValue, provider);

        public short ToInt16(IFormatProvider provider) => Convert.ToInt16(RawValue, provider);

        public ushort ToUInt16(IFormatProvider provider) => Convert.ToUInt16(RawValue, provider);

        public int ToInt32(IFormatProvider provider) => Convert.ToInt32(RawValue, provider);

        public uint ToUInt32(IFormatProvider provider) => Convert.ToUInt32(RawValue, provider);

        public long ToInt64(IFormatProvider provider) => Convert.ToInt64(RawValue, provider);

        public ulong ToUInt64(IFormatProvider provider) => Convert.ToUInt64(RawValue, provider);

        public float ToSingle(IFormatProvider provider) => Convert.ToSingle(RawValue, provider);

        public double ToDouble(IFormatProvider provider) => Convert.ToDouble(RawValue);

        public decimal ToDecimal(IFormatProvider provider) => Convert.ToDecimal(RawValue, provider);

        public DateTime ToDateTime(IFormatProvider provider) => Convert.ToDateTime(RawValue);

        public string ToString(IFormatProvider provider) => RawValue?.ToString() ?? "";

        public object ToType(Type conversionType, IFormatProvider provider) => ChangeType(RawValue, conversionType, provider);

        public override int GetHashCode() => RawValue == null ? base.GetHashCode() : RawValue.GetHashCode();

        public override bool Equals(object obj)
        {
            if (!(obj is IConvertibleValue ic)) return false;
            
            var myTypeCode = GetTypeCode();
            var compTypeCode = ic.GetTypeCode();

            myTypeCode = myTypeCode == TypeCode.DBNull ? TypeCode.Empty : myTypeCode;
            compTypeCode = compTypeCode == TypeCode.DBNull ? TypeCode.Empty : compTypeCode;

            if (myTypeCode == compTypeCode)
            {
                return myTypeCode == TypeCode.Empty 
                       || RawValue.Equals(ic.RawValue)
                       || RawValue.ToString().Trim() == ic.RawValue.ToString().Trim();
            }
            
            if (RawValue is char || ic.RawValue is char)
            {
                return RawValue.ToString() == ic.RawValue.ToString();
            }

            if (RawValue is DateTime dt && DateTime.TryParse(ic.RawValue.ToString().Trim(), out DateTime compDt))
            {
                return dt == compDt;
            }

            if (double.TryParse(RawValue.ToString().Trim(), out double myDbl) && !double.IsNaN(myDbl))
            {
                if (double.TryParse(ic.RawValue.ToString().Trim(), out double compDbl) && !double.IsNaN(compDbl))
                {
                    return Math.Abs(myDbl - compDbl) < .00000001;
                }
            }
            
            return false;
        }
    }

}