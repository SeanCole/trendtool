﻿using System;

namespace TechneauxUtility
{

    public interface IConvertibleValue : IConvertible
    {
        object RawValue { get; }

        string StringValue { get; }

        string ToString();
        bool TryGetBool(out bool value);
        bool TryGetDateTime(out DateTime value);
        bool TryGetInt(out int value);
    }

}