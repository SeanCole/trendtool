using System.Xml.Serialization;
using XmlDataModelUtility;

namespace TechneauxUtility
{
    public class IdDescKey : NotifyCopyDataModel, IValidatedRule
    {
        public IdDescKey() : this("", "") { }

        public IdDescKey(string id) : this(id, "") { }

        public IdDescKey(string id, string desc)
        {
            Id = id;
            Desc = desc;
        }

        [XmlAttribute]
        public string Id
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string Desc
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public bool IsRuleValid => CheckIfValid.isValid;

        public string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid => string.IsNullOrWhiteSpace(Id) ? (false, "Id is blank") : (true, "No Error");
    }
}
