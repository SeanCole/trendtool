﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechneauxHistorySynchronization.SqlServerHistory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechneauxHistorySynchronization.Models;
using Microsoft.SqlServer.Management.Smo;

namespace TechneauxHistorySynchronization.SqlServerHistory.Tests
{
    [TestClass()]
    public class SqlRowResolverTest
    {  
        [TestMethod()]
        public void GetCroppedStringTest()
        {

            //test#1 
            SimpleSqlColumnInfo srcCol = new SimpleSqlColumnInfo();
            string destStr = "abcdef";
            srcCol.DataType = DataType.Char(6);
            string newString = SqlRowResolver.GetCroppedString( srcCol, destStr);
            Assert.IsTrue(srcCol.DataType.MaximumLength >= newString.Length);

            //test#2
            srcCol.DataType = DataType.Char(5);
            newString = SqlRowResolver.GetCroppedString(srcCol, destStr);
            Assert.IsTrue(srcCol.DataType.MaximumLength >= newString.Length);
            Assert.IsTrue(newString == "ab...");
            
            //test#3
            srcCol.DataType = DataType.Char(3);
            newString = SqlRowResolver.GetCroppedString(srcCol, destStr);
            Assert.IsTrue(srcCol.DataType.MaximumLength >= newString.Length);
            Assert.IsTrue(newString == "...");

            //test#4
            srcCol.DataType = DataType.VarChar(1);
            newString = SqlRowResolver.GetCroppedString(srcCol, "");
            Assert.IsTrue(srcCol.DataType.MaximumLength >= newString.Length);
            Assert.IsTrue(newString == "");

            //test#5
            srcCol.DataType = DataType.VarChar(0);
            newString = SqlRowResolver.GetCroppedString(srcCol, "1");
            Assert.IsTrue(srcCol.DataType.MaximumLength >= newString.Length);
            Assert.IsTrue(newString == "");
        }
    }
}