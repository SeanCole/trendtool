﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CygNetRuleModel.Resolvers.History;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using static CygNetRuleModel.Resolvers.History.HistoryNormalization;

namespace CygNetRuleModel.Resolvers.History.Tests
{
    [TestClass()]
    public class NormalizedHistoryEntryTest
    {
        [TestMethod()]
        public void NormalizedHistoryEntryConstructorTest()
        {
            //test #1 beforeafter 
            double windowIntervalHours = .10;
            DateTime target = new DateTime(2018, 1, 30, 3, 25, 00, DateTimeKind.Utc);
            List<CygNetHistoryEntry> srcRawEntriesInWindow = new List<CygNetHistoryEntry>();
            CygNetHistoryEntry temp1 = new CygNetHistoryEntry()
            {
                RawValue = 0,

                RawTimestamp = new DateTime(2018, 1, 30, 3, 15, 00, DateTimeKind.Utc),
                ServerUtcOffset = TimeSpan.FromHours(0)
            };
            CygNetHistoryEntry temp2 = new CygNetHistoryEntry()
            {
                RawValue = 0,

                RawTimestamp = new DateTime(2018, 1, 30, 3, 20, 00, DateTimeKind.Utc), 
                ServerUtcOffset = TimeSpan.FromHours(0)
            };
            CygNetHistoryEntry temp3 = new CygNetHistoryEntry()
            {
                RawValue = 0,

                RawTimestamp = new DateTime(2018, 1, 30, 3, 24,00, DateTimeKind.Utc),
                ServerUtcOffset = TimeSpan.FromHours(0)
            };
            srcRawEntriesInWindow.Add(temp1);
            srcRawEntriesInWindow.Add(temp2);
            srcRawEntriesInWindow.Add(temp3);
            
            History.HistoryNormalization.NormalizedHistoryEntry testEntry = new NormalizedHistoryEntry(
    target, srcRawEntriesInWindow, false, PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before_After, windowIntervalHours);
            DateTime checkCritical = target.Add(target - temp3.AdjustedTimestamp);
            Assert.IsTrue(testEntry.CriticalTimestamp == checkCritical);

            //test # 2 before
            HistoryNormalization.NormalizedHistoryEntry testEntry2 = new NormalizedHistoryEntry(target, srcRawEntriesInWindow, false, PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before, windowIntervalHours);
            Assert.IsTrue(testEntry2.CriticalTimestamp == target);

            //test # 3 simple
            CygNetHistoryEntry temp4 = new CygNetHistoryEntry()
            {
                RawValue = 0,
                
                RawTimestamp = new DateTime(2018, 1, 30, 3, 27, 00, DateTimeKind.Utc ),
                ServerUtcOffset = TimeSpan.FromHours(0)
            };
            srcRawEntriesInWindow.Add(temp4);
            HistoryNormalization.NormalizedHistoryEntry testEntry3 = new NormalizedHistoryEntry(target, srcRawEntriesInWindow, false, PointHistoryNormalizationOptions.SamplingType.Simple_Average, windowIntervalHours);
            Assert.IsTrue(testEntry3.CriticalTimestamp == testEntry3.NormalizedTimestamp.AddHours(windowIntervalHours));

            //test # 4 wa
            HistoryNormalization.NormalizedHistoryEntry testEntry4 = new NormalizedHistoryEntry(target, srcRawEntriesInWindow, false, PointHistoryNormalizationOptions.SamplingType.Weighted_Average, windowIntervalHours);
            Assert.IsTrue(testEntry4.CriticalTimestamp == testEntry4.NormalizedTimestamp.AddHours(windowIntervalHours));
            //test # 5 linear interp.
            CygNetHistoryEntry temp5 = new CygNetHistoryEntry()
            {
                RawValue = 0,
                RawTimestamp = new DateTime(2018, 1, 30, 3, 35, 00, DateTimeKind.Utc),
                ServerUtcOffset = TimeSpan.FromHours(0)
            };
            srcRawEntriesInWindow.Remove(temp4);
            srcRawEntriesInWindow.Add(temp5);
            HistoryNormalization.NormalizedHistoryEntry testEntry5 = new NormalizedHistoryEntry(target, srcRawEntriesInWindow, false, PointHistoryNormalizationOptions.SamplingType.Linear_Interpolation, windowIntervalHours);
            Assert.IsTrue(testEntry5.CriticalTimestamp == temp5.AdjustedTimestamp);
        }

        [TestMethod()]
        public void TryGetNearestBeforeTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void TryGetNearestAfterTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void TryGetNearestBeforeOrAfterTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void TryGetSimpleAverageTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void TryGetWeightedAverageTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void TryGetInterpolatedTest()
        {
            Assert.Fail();
        }
    }
}