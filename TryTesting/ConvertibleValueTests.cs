﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechneauxUtility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechneauxUtility.Tests
{
    [TestClass()]
    public class ConvertibleValueTests
    {
        //[TestMethod()]
        //public void ConvertibleValueTest()
        //{
        //    throw new NotImplementedException();
        //}

        [TestMethod()]
        public void TryGetBoolTest()
        {
            var x = new ConvertibleValue("Y");
            Assert.IsTrue(x.TryGetBool(out bool b));
            Assert.IsTrue(b);

            var x2 = new ConvertibleValue("N");
            Assert.IsTrue(x2.TryGetBool(out bool b2));
            Assert.IsFalse(b2);
        }

        //[TestMethod()]
        //public void TryGetDateTimeTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void TryGetIntTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void TryGetDoubleTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void ToStringTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void GetTypeCodeTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void ToBooleanTest()
        //{
        //    Assert.Fail();
        //}

        [TestMethod()]
        public void ToCharTest()
        {
            var val = new ConvertibleValue("A");
            Assert.AreEqual(val.ToChar(CultureInfo.CurrentCulture), 'A');

            var val2 = new ConvertibleValue('A');
            Assert.AreEqual(val, val2);
        }

        //[TestMethod()]
        //public void ToSByteTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void ToByteTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void ToInt16Test()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void ToUInt16Test()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void ToInt32Test()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void ToUInt32Test()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void ToInt64Test()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void ToUInt64Test()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void ToSingleTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void ToDoubleTest()
        //{
        //    Assert.Fail();
        //}

        [TestMethod()]
        public void ToDecimalTest()
        {
            var dec = (Decimal)112341.61214235;
            var val = new ConvertibleValue(dec);

            Assert.AreEqual(val.ToDecimal(CultureInfo.InvariantCulture), dec);

            var flt = (double)112341.61214235;
            var val2 = new ConvertibleValue(flt);

            Assert.AreEqual(val2.ToDecimal(CultureInfo.InvariantCulture), dec);
        }

        //[TestMethod()]
        //public void ToDateTimeTest()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void ToStringTest1()
        //{
        //    Assert.Fail();
        //}

        //[TestMethod()]
        //public void ToTypeTest()
        //{
        //    Assert.Fail();
        //}

        [TestMethod()]
        public void GetHashCodeTest()
        {
            var val = new ConvertibleValue(DateTime.Now);
            val.GetHashCode();

            val = new ConvertibleValue("Testing it");
            val.GetHashCode();

            val = new ConvertibleValue(null);
            val.GetHashCode();

            val = new ConvertibleValue(DBNull.Value);
            val.GetHashCode();

            val = new ConvertibleValue('a');
            val.GetHashCode();

            val = new ConvertibleValue(1235.51464);
            val.GetHashCode();

            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void EqualsTest()
        {
            var val1 = new ConvertibleValue("Y");
            var val2 = new ConvertibleValue('Y');
            Assert.AreEqual(val1, val2);

            val1 = new ConvertibleValue("3.2");
            val2 = new ConvertibleValue(3.2);
            Assert.AreEqual(val1, val2);
            Assert.IsTrue(val1.Equals(val2));

            val1 = new ConvertibleValue("3.21");
            val2 = new ConvertibleValue(3.2);
            Assert.AreNotEqual(val1, val2);

            val1 = new ConvertibleValue(null);
            val2 = new ConvertibleValue(null);
            Assert.AreEqual(val1, val2);

            val1 = new ConvertibleValue(null);
            val2 = new ConvertibleValue(System.DBNull.Value);
            Assert.AreEqual(val1, val2);

            val1 = new ConvertibleValue(new DateTime(2017, 5, 1, 7, 59, 1));
            val2 = new ConvertibleValue("2017-5-1 7:59:01");
            Assert.AreEqual(val1, val2);

            val1 = new ConvertibleValue(new DateTime(2017, 5, 1, 7, 59, 1));
            val2 = new ConvertibleValue(@"5/1/2017 7:59:01");
            Assert.AreEqual(val1, val2);

            val1 = new ConvertibleValue(new DateTime(2017, 5, 1, 7, 59, 1));
            val2 = new ConvertibleValue(@"5/1/2017 7:59:00");
            Assert.AreNotEqual(val1, val2);

            val1 = new ConvertibleValue((float)4.511);
            val2 = new ConvertibleValue((decimal)4.511);
            Assert.AreEqual(val1, val2);

            val1 = new ConvertibleValue("padded string  ");
            val2 = new ConvertibleValue("padded string     ");
            Assert.AreEqual(val1, val2);
        }
    }
}