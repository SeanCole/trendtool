﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechneauxDataSyncService.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechneauxUtility;

namespace TechneauxDataSyncService.Helper.Tests
{
    [TestClass()]
    public class DictionaryExtensionsTests
    {
        [TestMethod()]
        public void IsEqualToTest()
        {
            var dict1 = GetDict1();

            var dict2 = GetDict2();
            
            Assert.IsTrue(dict1.IsEqualTo(dict2));

            dict1.Add("bad val", new ConvertibleValue("extra val"));
            Assert.IsFalse(dict1.IsEqualTo(dict2));

            dict1.Remove("bad val");
            Assert.IsTrue(dict1.IsEqualTo(dict2));

            dict1["string1"] = new ConvertibleValue("different now");
            Assert.IsFalse(dict1.IsEqualTo(dict2));
        }

        private Dictionary<string, IConvertibleValue> GetDict1()
        {
            var dict1 = new Dictionary<string, IConvertibleValue>
            {
                {"string1", new ConvertibleValue("Test1")},
                {"string2", new ConvertibleValue("Test2        ")},
                {"num1", new ConvertibleValue(1.2)}
            };
            
            return dict1;
        }

        private Dictionary<string, IConvertibleValue> GetDict2()
        {
            var dict2 = new Dictionary<string, IConvertibleValue>
            {
                {"string1", new ConvertibleValue("Test1")},
                {"string2", new ConvertibleValue("Test2        ")},
                {"num1", new ConvertibleValue("1.2")}
            };
            
            return dict2;
        }
    }
}