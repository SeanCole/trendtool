﻿using GenericRuleModel.Resolvers;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CygNetRuleModel.Resolvers.History;
using Microsoft.SqlServer.Management.Dmf;
using Techneaux.CygNetWrapper;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxUtility;


namespace CygNetRuleModel.Resolvers
{
    public static class ResolveCygNetHistoryRows
    {


        public static async Task<List<CygNetHistoryRow>> ResolveAllRows(
            Dictionary<string, CygNetRuleBase> rules,
            ICachedCygNetPoint srcPoint,
            List<CygNetHistoryEntry> srcHist,
            CancellationToken ct)
        {
            // Clone config objs
            //clone// rules = rules.ToDictionary(item => item.Key, item => item.Value.Clone() as CygNetRule);

            if (!srcHist.IsAny())
                return new List<CygNetHistoryRow>();

            var newRowList = new List<CygNetHistoryRow>();

            foreach (var hist in srcHist)
            {
                ct.ThrowIfCancellationRequested();

                var newRow = await ResolveSingleRow(rules,
                                                    srcPoint,
                                                    hist,
                                                    ct);
                newRowList.Add(newRow);
            }
            return newRowList;
        }

        public static async Task<List<CygNetHistoryRow>> ResolveAllRows(
            Dictionary<string, CygNetRuleBase> rules,
            ICachedCygNetPoint srcPoint,
            List<HistoryNormalization.NormalizedHistoryEntry> srcHist,
            CancellationToken ct)
        {
            // Clone config objs
            //clone// rules = rules.ToDictionary(item => item.Key, item => item.Value.Clone() as CygNetRule);

            if (!srcHist.IsAny())
                return new List<CygNetHistoryRow>();

            var newRowList = new List<CygNetHistoryRow>();

            foreach (var hist in srcHist)
            {
                ct.ThrowIfCancellationRequested();

                var newRow = await ResolveSingleRow(rules,
                    srcPoint,
                    hist,
                    ct);
                newRowList.Add(newRow);
            }
            return newRowList;
        }

        public static async Task<CygNetHistoryRow> ResolveSingleRow(
            Dictionary<string, CygNetRuleBase> rules,
            ICachedCygNetPoint srcPoint,
            HistoryNormalization.NormalizedHistoryEntry srcHist,
            CancellationToken ct)
        {
            var resRow = await ResolveSingleRow(
                rules,
                srcPoint,
                srcHist.NormalizedHistoryValue,
                ct);

            resRow.SrcRawHistoryEntry = null;
            resRow.SrcNormalizedHistoryEntry = srcHist;

            return resRow;
        }

        public static async Task<CygNetHistoryRow> ResolveSingleRow(
            Dictionary<string, CygNetRuleBase> rules,
            ICachedCygNetPoint srcPoint,
            CygNetHistoryEntry srcHist,
            CancellationToken ct)
        {
            var cygRow = new CygNetHistoryRow()
            {
                SrcRawHistoryEntry = srcHist
            };

            foreach (var rule in rules)
            {
                ct.ThrowIfCancellationRequested();

                CygNetConvertableValue val;
             
                var thisRule = rule.Value;
            
                //CygNetResolver DataResolver = new CygNetResolver(srcPoint, ThisRule);

                if (thisRule.DataElement == CygNetRuleBase.DataElements.PointHistory)
                {
                    val = CygNetResolver.GetPointHistoryValue(srcPoint, thisRule.PointHistoryOptions, srcHist);

                    if (thisRule.FormatConfig.EnableFormat)
                    {
                        if (val.HasError)
                        {
                            if (thisRule.DefaultIfError != null && thisRule.DefaultIfError != "")
                            {
                                val = FormattingResolver.Resolve(new CygNetConvertableValue(thisRule.DefaultIfError), thisRule.FormatConfig);
                            }
                            else
                            {
                                val = FormattingResolver.Resolve(val, thisRule.FormatConfig);
                            }
                        }
                        else
                        {
                            val = FormattingResolver.Resolve(val, thisRule.FormatConfig);
                        }

                    }
                }
                else
                {
                    val = await CygNetResolver.ResolveRule(thisRule, srcPoint, ct);
                    if (thisRule.FormatConfig.EnableFormat)
                    {
                        if (val.HasError)
                        {
                            if (thisRule.DefaultIfError != null && thisRule.DefaultIfError != "")
                            {
                                val = FormattingResolver.Resolve(new CygNetConvertableValue(thisRule.DefaultIfError), thisRule.FormatConfig);
                            }
                            else
                            {
                                val = FormattingResolver.Resolve(val, thisRule.FormatConfig);
                            }
                        }
                        else
                        {
                            val = FormattingResolver.Resolve(val, thisRule.FormatConfig);
                        }
                    }
                }

                cygRow.CellValues.Add(rule.Key, val);              
            }

            return cygRow;

        }
   
    }
}
