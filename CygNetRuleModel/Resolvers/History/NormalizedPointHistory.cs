using CygNet.Data.Core;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using TechneauxUtility;

namespace CygNetRuleModel.Resolvers.History
{
    public static class NormalizedPointHistory
    {
        // Static methods
        
        public static async Task<List<CygNetHistoryEntry>> GetPointHistory(
           ICachedCygNetPoint srcPoint,
           DateTime earliestDate,
           DateTime latestDate,
           bool includeUnreliable,
           IProgress<int> progress,
           CancellationToken ct)
        {
            // Read history and return
            try
            {
                var cygHistory = (await srcPoint.GetHistory(earliestDate, latestDate, progress, ct));
                if (cygHistory == null)
                    return new List<CygNetHistoryEntry>();
                if (!includeUnreliable)
                {
                    cygHistory = cygHistory.Where(entry => !entry.BaseStatus.HasFlag(BaseStatusFlags.Unreliable)).ToList();
                }
                cygHistory = cygHistory.Where(entry => entry.IsNumeric).ToList();
                return cygHistory;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "General exception trying to get point history from normalization methods");
                throw;
            }
        }
    

        public static async Task<List<CygNetHistoryEntry>> GetNormalizedHistoryValuesAsync(
            List<CygNetHistoryEntry> rawHist,
            DateTime earliestDate,
            DateTime latestDate,
            PointHistoryNormalizationOptions normalizationOpts
            )
        {
            List<CygNetHistoryEntry> normHist;
            if (rawHist.IsAny() && normalizationOpts.EnableNormalization != false)
            {
                var normalizer = new HistoryNormalization(normalizationOpts, earliestDate, latestDate);
                normHist = (await normalizer.Normalize(rawHist)).Where(hist => hist.IsValid && !hist.IsValueUncertain).Select(hist => hist.NormalizedHistoryValue).ToList();
            }
            else
            {
                normHist = null;
            }

            return normHist;
        }
    }
}

// get on demand here



