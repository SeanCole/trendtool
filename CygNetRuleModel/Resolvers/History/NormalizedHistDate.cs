﻿using System;

namespace CygNetRuleModel.Resolvers.History
{
    public partial class HistoryNormalization
    {
        public class NormalizedHistDate
        {
            public DateTime EarliestTimestampAllowed { get; set; }
            public DateTime TargetTimestamp { get; set; }
            public DateTime LatestTimestampAllowed { get; set; }
        }
    }
}

