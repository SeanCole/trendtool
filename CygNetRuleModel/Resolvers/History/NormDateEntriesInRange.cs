﻿using System.Collections.Generic;
using Techneaux.CygNetWrapper.Services.VHS;

namespace CygNetRuleModel.Resolvers.History
{
    public partial class HistoryNormalization
    {
        public class NormDateEntriesInRange
        {
            public NormDateEntriesInRange(NormalizedHistDate srcTs)
            {
                NormTimestamp = srcTs;
            }

            public NormalizedHistDate NormTimestamp { get; }
            public List<CygNetHistoryEntry> SrcItems { get; } = new List<CygNetHistoryEntry>();

            public bool TryAdd(CygNetHistoryEntry pnt)
            {
                if (pnt.AdjustedTimestamp > NormTimestamp.EarliestTimestampAllowed 
                    && pnt.AdjustedTimestamp <= NormTimestamp.LatestTimestampAllowed)
                {
                    SrcItems.Add(pnt);
                    return true;
                }

                return false;
            }
        }
    }
}

