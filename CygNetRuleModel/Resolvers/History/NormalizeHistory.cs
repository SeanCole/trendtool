using CygNet.Data.Historian;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using TechneauxUtility;

namespace CygNetRuleModel.Resolvers.History
{
    public partial class HistoryNormalization
    {
        private readonly PointHistoryNormalizationOptions _normRules;
        private DateTime _periodStart;
        private readonly DateTime _periodEnd;
        private List<CygNetHistoryEntry> _pointHistoryData;

        public HistoryNormalization(PointHistoryNormalizationOptions normalizationOpts, DateTime beginning, DateTime end)
        {
            _periodStart = beginning;
            _periodEnd = end;
            _normRules = normalizationOpts;

            _intervalBetweenNormEntriesInHours = Convert.ToDouble(_normRules.NormalizeWindowIntervalLength) / Convert.ToDouble(_normRules.NumWindowHistEntries);
        }

        public async Task<List<NormalizedHistoryEntry>> Normalize(List<CygNetHistoryEntry> rawValues)
        {
            return await Normalize(rawValues, false);
        }

        public async Task<List<NormalizedHistoryEntry>> Normalize(List<CygNetHistoryEntry> rawValues, bool hasHistLaterThanInterval)
        {
            if (1440 % _normRules.NormalizeWindowIntervalLength != 0)
                return new List<NormalizedHistoryEntry>();

            if(!rawValues.IsAny())
                return new List<NormalizedHistoryEntry>();
            
            _pointHistoryData = rawValues;
 
            var srcData = rawValues.OrderBy(val => val.AdjustedTimestamp).ToList();

            var normTimestamps = GetNormalizedTimestampsInWindow();
            var latestLocalTimestamp = srcData.Last().AdjustedTimestamp;

            var normTsWithSourceHist = normTimestamps.Select(ts => new NormDateEntriesInRange(ts)).ToList();

            await Task.Run(() => GetSourceRawValuesInRangeForNormTimestamps(srcData, normTsWithSourceHist));

            var normalizedHistoryData = await Task.Run(() => normTsWithSourceHist
                .Select(nts => new NormalizedHistoryEntry(
                    nts.NormTimestamp.TargetTimestamp,
                    nts.SrcItems,
                    (latestLocalTimestamp >= nts.NormTimestamp.LatestTimestampAllowed || hasHistLaterThanInterval),
                    _normRules.SelectedSampleType,
                    _intervalBetweenNormEntriesInHours))
                .ToList());

            //var NormalizedHistoryData = NormTimestamps
            //    .Select(nts => new NormalizedHistoryEntry(
            //        nts.TargetTimestamp,
            //        SrcData
            //            .SkipWhile(hs => hs.LocalTimestamp < nts.EarliestTimestampAllowed)
            //            .TakeWhile(hs => hs.LocalTimestamp <= nts.LatestTimestampAllowed),
            //        LatestLocalTimestamp >= nts.LatestTimestampAllowed,
            //        NormRules.SelectedSampleType,
            //        IntervalBetweenNormEntriesInHours))
            //    .ToList();

            return normalizedHistoryData;
        }

        public static void GetSourceRawValuesInRangeForNormTimestamps(List<CygNetHistoryEntry> srcData, List<NormDateEntriesInRange> normTsWithSourceHist)
        {
            var startIndex = 0;
            foreach (var pnt in srcData)
            {
                var foundFirst = false;

                if (startIndex >= normTsWithSourceHist.Count - 1)
                    break;

                if (pnt.AdjustedTimestamp <= normTsWithSourceHist[startIndex].NormTimestamp.EarliestTimestampAllowed)
                    continue;
                                    
                for (var i = startIndex; i < normTsWithSourceHist.Count; i++)
                {
                    var thisNt = normTsWithSourceHist[i];

                    if (thisNt.TryAdd(pnt))
                    {
                        foundFirst = true;
                    }
                    else
                    {
                        
                        if (foundFirst)
                        {
                            break;
                        }
                        else
                        {
                            startIndex += 1;
                        }
                    }
                }
            }
        }

        private readonly double _intervalBetweenNormEntriesInHours;
        public IEnumerable<NormalizedHistDate> GetNormalizedTimestampsInWindow()
        {
            var targetNormTs = _periodStart.Date.AddDays(-1);

            targetNormTs = targetNormTs.AddMinutes(_normRules.NormalizeWindowStartTime);

            var normalizedTimestamps = new List<NormalizedHistDate>();

            while (targetNormTs <= _periodEnd)
            {
                if (targetNormTs >= _periodStart)
                    normalizedTimestamps.Add(new NormalizedHistDate()
                    {
                        EarliestTimestampAllowed = targetNormTs.AddHours(-2 * _intervalBetweenNormEntriesInHours),
                        TargetTimestamp = targetNormTs,
                        LatestTimestampAllowed = targetNormTs.AddHours(2 * _intervalBetweenNormEntriesInHours)
                    });

                targetNormTs = targetNormTs.AddHours(_intervalBetweenNormEntriesInHours);
            }

            return normalizedTimestamps;
        }

        public static DateTime GetNextFutureNormalizedTimestamp(PointHistoryNormalizationOptions normOpts, DateTime refTimestamp)
        {
            var intervalBetweenNormEntriesInHours = Convert.ToDouble(normOpts.NormalizeWindowIntervalLength) / Convert.ToDouble(normOpts.NumWindowHistEntries);

            var desiredTimestamp = refTimestamp.Date.AddDays(-1);
            desiredTimestamp = desiredTimestamp.AddMinutes(normOpts.NormalizeWindowStartTime);

            while (desiredTimestamp <= refTimestamp)
            {
                desiredTimestamp = desiredTimestamp.AddHours(intervalBetweenNormEntriesInHours);
            }

            return desiredTimestamp;
        }

        //returns true if raw hist entries data is string type
        //private bool CheckTypesForString()
        //{
        //    foreach (var entry in _pointHistoryData)
        //    {
        //        if (entry.SourceHistoricalEntry.ValueType == HistoricalEntryValueType.UTF8)
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}
    }
}

