﻿using CygNet.Data.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;

namespace CygNetRuleModel.Resolvers.History
{
    public partial class HistoryNormalization
    {
        public class NormalizedHistoryEntry
        {
            public NormalizedHistoryEntry(
                DateTime normalizedTimestamp,
                IEnumerable<CygNetHistoryEntry> srcRawEntriesInWindow,
                bool laterValueExists,
                PointHistoryNormalizationOptions.SamplingType sampleType,
                double windowIntervalHours)
            {
                NormalizedTimestamp = normalizedTimestamp;
                SourceRawHistoryEntries = srcRawEntriesInWindow.ToList();
                SampleType = sampleType;
                WindowIntervalHours = windowIntervalHours;

                SingleWindowStartTime = NormalizedTimestamp.AddHours(-windowIntervalHours);
                SingleWindowEndTime = NormalizedTimestamp.AddHours(windowIntervalHours);
                DoubleWindowStartTime = NormalizedTimestamp.AddHours(-2 * windowIntervalHours);
                DoubleWindowEndTime = NormalizedTimestamp.AddHours(2 * windowIntervalHours);

                switch (sampleType)
                {
                    case PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before_After:
                        if (TryGetNearestBeforeOrAfter(
                            normalizedTimestamp,
                            SourceRawHistoryEntries,
                            out var normHistResult))
                        {
                            IsValid = true;
                            NormalizedHistoryValue = normHistResult.NewEntry;

                            if (!laterValueExists && normHistResult.SrcEntry.AdjustedTimestamp < NormalizedTimestamp)
                            {
                                CriticalTimestamp = NormalizedTimestamp.Add(NormalizedTimestamp - normHistResult.SrcEntry.AdjustedTimestamp);
                            }
                        }
                        break;
                    case PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before:
                        if (TryGetNearestBefore(
                            normalizedTimestamp,
                            SourceRawHistoryEntries,
                            out var normHistBefore))
                        {
                            IsValid = true;
                            NormalizedHistoryValue = normHistBefore;

                            if (!laterValueExists && normHistBefore.AdjustedTimestamp <= NormalizedTimestamp)
                            {
                                CriticalTimestamp = NormalizedTimestamp;
                            }
                        }
                        break;
                    case PointHistoryNormalizationOptions.SamplingType.Take_Nearest_After:
                        if (TryGetNearestAfter(
                           normalizedTimestamp,
                           SourceRawHistoryEntries,
                           out var normHistAfter))
                        {
                            IsValid = true;
                            NormalizedHistoryValue = normHistAfter;
                        }

                        break;
                    case PointHistoryNormalizationOptions.SamplingType.Simple_Average:
                        if (TryGetSimpleAverage(
                            normalizedTimestamp,
                            SingleWindowEntries,
                            out var normHistSimpleAveSingle))
                        {
                            // Single window interval

                            IsValid = true;
                            NormalizedHistoryValue = normHistSimpleAveSingle;

                            if (!laterValueExists &&
                                !SourceRawHistoryEntries.Any(hist => hist.AdjustedTimestamp >= SingleWindowEndTime))
                            {
                                CriticalTimestamp = NormalizedTimestamp.AddHours(WindowIntervalHours);
                            }
                        }
                        else
                        {
                            // Double window interval

                            if (TryGetSimpleAverage(
                            normalizedTimestamp,
                            SourceRawHistoryEntries,
                            out var normHistSimpleAveDouble))
                            {
                                IsValid = true;
                                NormalizedHistoryValue = normHistSimpleAveDouble;

                                if (!laterValueExists)
                                {
                                    CriticalTimestamp = NormalizedTimestamp.AddHours(2 * WindowIntervalHours);
                                }
                            }
                        }

                        break;
                    case PointHistoryNormalizationOptions.SamplingType.Weighted_Average:
                        if (TryGetWeightedAverage(
                                normalizedTimestamp,
                                SingleWindowEndTime,
                                SingleWindowEntries,
                                out var normHistWeightedAveSingle))
                        {
                            // Single window interval

                            IsValid = true;
                            NormalizedHistoryValue = normHistWeightedAveSingle;

                            if (!laterValueExists &&
                                !SourceRawHistoryEntries.Any(hist => hist.AdjustedTimestamp >= SingleWindowEndTime))
                            {
                                CriticalTimestamp = NormalizedTimestamp.AddHours(WindowIntervalHours);
                            }
                        }
                        else
                        {
                            // Double window interval

                            if (TryGetWeightedAverage(
                                normalizedTimestamp,
                                DoubleWindowEndTime,
                                SourceRawHistoryEntries,
                            out var normHistWeightedAveDouble))
                            {
                                IsValid = true;
                                NormalizedHistoryValue = normHistWeightedAveDouble;

                                if (!laterValueExists)
                                {
                                    CriticalTimestamp = NormalizedTimestamp.AddHours(2 * WindowIntervalHours);
                                }
                            }
                        }

                        break;
                    case PointHistoryNormalizationOptions.SamplingType.Linear_Interpolation:
                        var TryGetResults = TryGetInterpolated(
                           normalizedTimestamp,
                           SingleWindowEntries,
                           out var normHistInterpSingle);
                        bool success = TryGetResults.Item1;
                        if(success)
                        {
                            // Single window interval

                            IsValid = true;
                            NormalizedHistoryValue = normHistInterpSingle;
                        }
                        else
                        {
                            // Double window interval
                            var TryGetResultsDouble = TryGetInterpolated(
                            normalizedTimestamp,
                            SourceRawHistoryEntries,
                            out var normHistInterpDouble);
                            success = TryGetResultsDouble.Item1;
                            if (success)
                            {
                                IsValid = true;
                                NormalizedHistoryValue = normHistInterpDouble;
                                if(!laterValueExists && TryGetResultsDouble.Item2 != DateTime.MinValue)
                                {
                                    CriticalTimestamp = TryGetResultsDouble.Item2;
                                }
                            }
                        }
                        break;
                    default:
                        throw new NotImplementedException($"Sampling type [{sampleType.ToString()}] not implemented");
                };

                // If later val exists, then there is never a critical timestamp
                if (laterValueExists)
                    CriticalTimestamp = null;
            }

            //private bool TryGetSingleOrDoubleWindowSimpleAverage()
            //{

            //}

            public DateTime NormalizedTimestamp { get; }

            public bool IsValueUncertain => CriticalTimestamp != null;

            public DateTime? CriticalTimestamp { get; } = null;

            public List<CygNetHistoryEntry> SourceRawHistoryEntries { get; }

            public DateTime SingleWindowStartTime { get; }
            public DateTime SingleWindowEndTime { get; }

            public DateTime DoubleWindowStartTime { get; }
            public DateTime DoubleWindowEndTime { get; }

            public IEnumerable<CygNetHistoryEntry> SingleWindowEntries => SourceRawHistoryEntries
                .SkipWhile(hist => hist.AdjustedTimestamp < SingleWindowStartTime)
                .TakeWhile(hist => hist.AdjustedTimestamp < SingleWindowEndTime);

            public bool IsValid { get; } = false;

            public double WindowIntervalHours { get; }

            public PointHistoryNormalizationOptions.SamplingType SampleType { get; }

            public CygNetHistoryEntry NormalizedHistoryValue { get; } = null;

            // Static methods

            public static bool TryGetNearestBefore(
                DateTime targetTime,
                IEnumerable<CygNetHistoryEntry> srcVals,
                out CygNetHistoryEntry result)
            {
                var last = srcVals
                    .TakeWhile(hist => hist.AdjustedTimestamp <= targetTime)
                    .LastOrDefault();
                
                if (last != null)
                {
                    result = new CygNetHistoryEntry()
                    {
                        RawValue = last.RawValue,
                        BaseStatus = last.BaseStatus,
                        UserStatus = last.UserStatus,
                        RawTimestamp = targetTime,
                        ServerUtcOffset = last.ServerUtcOffset
                    };
                }
                else
                {
                    result = null;
                }

                return result != null;
            }

            public static bool TryGetNearestAfter(
               DateTime targetTime,
               IEnumerable<CygNetHistoryEntry> srcVals,
               out CygNetHistoryEntry result)
            {
                var first = srcVals.FirstOrDefault(hist => hist.AdjustedTimestamp >= targetTime);

                if (first != null)
                {
                    result = new CygNetHistoryEntry()
                    {
                        RawValue = first.RawValue,
                        BaseStatus = first.BaseStatus,
                        UserStatus = first.UserStatus,
                        RawTimestamp = targetTime,
                        ServerUtcOffset = first.ServerUtcOffset
                    };
                }
                else
                {
                    result = null;
                }

                return result != null;
            }

            public static bool TryGetNearestBeforeOrAfter(
                DateTime targetTime,
                IEnumerable<CygNetHistoryEntry> srcVals,
                out (CygNetHistoryEntry NewEntry, CygNetHistoryEntry SrcEntry) normHistResult)
            {
                var temp = srcVals
                    .OrderBy(hist => Math.Abs((targetTime - hist.AdjustedTimestamp).TotalHours));
                var srcHistEntry = temp.FirstOrDefault();
                
                CygNetHistoryEntry normHistEntry = null;                

                if (srcHistEntry != null)
                {
                    normHistEntry = new CygNetHistoryEntry()
                    {
                        RawValue = srcHistEntry.RawValue,
                        BaseStatus = srcHistEntry.BaseStatus,
                        UserStatus = srcHistEntry.UserStatus,
                        RawTimestamp = targetTime,
                        ServerUtcOffset = srcHistEntry.ServerUtcOffset
                    };
                }

                normHistResult = (normHistEntry, srcHistEntry);

                return normHistEntry != null;
            }

            public static bool TryGetSimpleAverage(
                DateTime targetTime,
                IEnumerable<CygNetHistoryEntry> srcVals,
                out CygNetHistoryEntry result)
            {
                // Check for entry on the timestamp
                var numericVals = srcVals.Where(hist => hist.IsNumeric).ToList();

                var matchingVal = numericVals.FirstOrDefault(hist => hist.AdjustedTimestamp == targetTime);
                if (matchingVal != null)
                {
                    result = matchingVal;
                    return true;
                }

                // Make sure there are values before and after
                if (numericVals.Any(hist => hist.AdjustedTimestamp < targetTime) &&
                    numericVals.Any(hist => hist.AdjustedTimestamp > targetTime))
                {
                    var ave = numericVals.Average(hist => hist.NumericValue);
                    var newBaseStatus = numericVals.Select(hist => hist.BaseStatus).Aggregate((x, y) => x | y);
                    var newUserStatus = numericVals.Select(hist => hist.UserStatus).Aggregate((x, y) => x | y);

                    result = new CygNetHistoryEntry()
                    {
                        RawValue = ave,
                        BaseStatus = newBaseStatus,
                        UserStatus = newUserStatus,
                        RawTimestamp = new DateTime(targetTime.Ticks, DateTimeKind.Local),
                        ServerUtcOffset = numericVals.First().ServerUtcOffset
                    };

                    return true;
                }
                else
                {
                    result = null;
                    return false;
                }
            }

            public static bool TryGetWeightedAverage(
               DateTime targetTime,
               DateTime endTime,
               IEnumerable<CygNetHistoryEntry> srcVals,
               out CygNetHistoryEntry result)
            {
                // Check for entry on the timestamp
                var numericVals = srcVals.Where(hist => hist.IsNumeric).ToList();

                var matchingVal = numericVals.FirstOrDefault(hist => hist.AdjustedTimestamp == targetTime);
                if (matchingVal != null)
                {
                    result = matchingVal;
                    return true;
                }

                // Make sure there are values before and after
                if (numericVals.Any(hist => hist.AdjustedTimestamp < targetTime) &&
                    numericVals.Any(hist => hist.AdjustedTimestamp > targetTime))
                {
                    double runningWeightedSum = 0;

                    var valList = numericVals.ToList();
                    for (var i = 0; i < valList.Count; i++)
                    {
                        if (i == valList.Count - 1)
                        {
                            runningWeightedSum += valList[i].NumericValue * (endTime - valList[i].AdjustedTimestamp).TotalHours;
                        }
                        else
                        {
                            runningWeightedSum += valList[i].NumericValue * (valList[i + 1].AdjustedTimestamp - valList[i].AdjustedTimestamp).TotalHours;
                        }
                    }

                    var ave = runningWeightedSum / (endTime - valList.First().AdjustedTimestamp).TotalHours;

                    var newBaseStatus = numericVals.Select(hist => hist.BaseStatus).Aggregate((x, y) => x | y);
                    var newUserStatus = numericVals.Select(hist => hist.UserStatus).Aggregate((x, y) => x | y);

                    result = new CygNetHistoryEntry()
                    {
                        RawValue = ave,
                        BaseStatus = newBaseStatus,
                        UserStatus = newUserStatus,
                        RawTimestamp = new DateTime(targetTime.Ticks, DateTimeKind.Local),
                        ServerUtcOffset = numericVals.First().ServerUtcOffset
                    };

                    return true;
                }
                else
                {
                    result = null;
                    return false;
                }
            }

            public static (bool, DateTime) TryGetInterpolated(
              DateTime targetTime,
              IEnumerable<CygNetHistoryEntry> srcVals,
              out CygNetHistoryEntry result)
            {
                // Check for entry on the timestamp
                var numericVals = srcVals.Where(hist => hist.IsNumeric).ToList();

                var matchingVal = numericVals.FirstOrDefault(hist => hist.AdjustedTimestamp == targetTime);
                if (matchingVal != null)
                {
                    result = matchingVal;
                    return (true, DateTime.MinValue);
                }

                // Make sure there are values before and after
                var firstBefore = numericVals.FirstOrDefault(hist => hist.AdjustedTimestamp < targetTime);
                var firstAfter = numericVals.FirstOrDefault(hist => hist.AdjustedTimestamp > targetTime);

                if (firstBefore != null &&
                    firstAfter != null)
                {
                    var valBefore = firstBefore.NumericValue;
                    var valAfter = firstAfter.NumericValue;

                    var slope = (valAfter - valBefore) /
                        (firstAfter.AdjustedTimestamp - firstBefore.AdjustedTimestamp).TotalHours;

                    var interpVal = slope * (targetTime - firstBefore.AdjustedTimestamp).TotalHours + valBefore;

                    var newBaseStatus = firstBefore.BaseStatus | firstAfter.BaseStatus;
                    var newUserStatus = firstBefore.UserStatus | firstAfter.UserStatus;

                    result = new CygNetHistoryEntry()
                    {
                        RawValue = interpVal,
                        BaseStatus = newBaseStatus,
                        UserStatus = newUserStatus,
                        RawTimestamp = targetTime,
                        ServerUtcOffset = numericVals.First().ServerUtcOffset
                    };

                    return (true, firstAfter.AdjustedTimestamp);
                }
                else
                {
                    DateTime CritTimeStamp = new DateTime();
                    if (firstAfter == null)
                    {
                        CritTimeStamp = targetTime;
                    }
                    else
                    {
                        CritTimeStamp = firstAfter.AdjustedTimestamp;
                    }
                    result = null;
                    return (false, CritTimeStamp); 
                }
            }
        }
    }
}

