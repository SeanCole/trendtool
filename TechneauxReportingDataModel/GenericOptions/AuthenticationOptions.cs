using System.Xml.Serialization;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.GenericOptions
{
    public class AuthenticationOptions : NotifyCopyDataModel
    {
        public AuthenticationOptions()
        {
            Username = "";
            Password = "";
            Source = "";
        }

        [XmlAttribute]
        public string Source
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string Username
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string Password
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

    }
}
