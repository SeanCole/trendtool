using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.CygNet.Rules
{
    public interface ICygNetPointReference: IFacilitySelection
    {
        string UDC { get; set; }
    }
}