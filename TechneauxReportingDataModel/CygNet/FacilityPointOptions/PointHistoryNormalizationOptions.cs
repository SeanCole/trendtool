using System;
using System.ComponentModel;
using System.Xml.Serialization;
using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.CygNet.FacilityPointOptions
{
    public class PointHistoryNormalizationOptions : NotifyCopyDataModel, IValidatedRule
    {
        public PointHistoryNormalizationOptions()
        {           
            // init all vars here
            EnableNormalization = false;
            NormalizeWindowStartTime = 0;
            NormalizeWindowIntervalLength = 1;
            //AveragingWindow = 0;
            NumWindowHistEntries = 1;
            //EnableTakeCurrentValueIfNoHist = false;
            SelectedSampleType = SamplingType.Take_Nearest_Before_After;
        }

        [XmlAttribute()]
        public bool EnableNormalization
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute()]
        public Double NormalizeWindowStartTime
        {
            get => GetPropertyValue<Double>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute()]
        public Decimal NormalizeWindowIntervalLength
        {
            get => GetPropertyValue<Decimal>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute()]
        public int NumWindowHistEntries
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute()]
        public SamplingType SelectedSampleType
        {
            get => GetPropertyValue<SamplingType>();
            set => SetPropertyValue(value);
        }

        //[XmlAttribute()]
        //public Decimal AveragingWindow
        //{
        //    get { return GetPropertyValue<Decimal>(); }
        //    set { SetPropertyValue(value); }
        //}

       // [XmlAttribute()]
        //public bool EnableTakeCurrentValueIfNoHist
        //{
        //    get { return GetPropertyValue<bool>(); }
        //    set { SetPropertyValue(value); }
        //}

        public enum SamplingType
        {
            [Description("Take Nearest (Before or After)")]
            Take_Nearest_Before_After,

            [Description("Take Nearest (Before)")]
            Take_Nearest_Before,

            [Description("Take Nearest (After)")]
            Take_Nearest_After,

            [Description("Simple Average")]
            Simple_Average,

            [Description("Weighted Average")]
            Weighted_Average,

            [Description("Linear Interpolation")]
            Linear_Interpolation,
        }

        public bool IsRuleValid => CheckIfValid.isValid;
        public string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid => (true, "No Error");
    }
}
