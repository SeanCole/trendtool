using GenericRuleModel.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;
using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.CygNet.FacilityPointOptions
{
    public class PointHistoryGeneralOptions: NotifyCopyDataModel, IValidatedRule
    {
        public PointHistoryGeneralOptions()
        {
            HistoryType = HistoryTypesNew.First_AfterRollupPeriodBegins;
            PointValueType = PointValueTypesNew.Value;
            IncludeUnreliable = false;
            BlankValueHandlingType = BlankValueHandlingTypes.IncludeBlanks;
            DefaultValueIfBlank = "";
            OnlyNumeric = true;

            SmartSheetHistoryTypes = EnumHelper.GetAllValuesAndDescriptions(HistoryType.GetType()).ToList();

            SmartSheetBlankHandlingTypes = EnumHelper.GetAllValuesAndDescriptions(BlankValueHandlingType.GetType()).ToList();

            SmartSheetPointValueTypes = EnumHelper.GetAllValuesAndDescriptions(PointValueType.GetType()).ToList();

        }
        [XmlIgnore]
        public List<ValueDescription> SmartSheetPointValueTypes
        {
            get => GetPropertyValue<List<ValueDescription>>();
            set => SetPropertyValue(value);
        }
        [XmlIgnore]
        public List<ValueDescription> SmartSheetHistoryTypes
        {
            get => GetPropertyValue<List<ValueDescription>>();
            set => SetPropertyValue(value);
        }
        [XmlIgnore]
        public List<ValueDescription> SmartSheetBlankHandlingTypes
        {
            get => GetPropertyValue<List<ValueDescription>>();
            set => SetPropertyValue(value);
        }

    
 
        public enum HistoryTypesNew
        {
            [Description("[First] After Contract Begins")]
            First_AfterRollupPeriodBegins,

            [Description("[First] Before Contract Ends")]
            First_BeforeRollupPeriodEnds,

            [Description("[First] Before Contract Ends (extend hist)")]
            First_BeforeRollupPeriodEndsExtend,

            [Description("[First] After Contract Ends")]
            First_AfterRollupPeriodEnds,

            [Description("[Value] Current")]
            Value_Current,

            [Description("[Value] at This Contract Hour")]
            Value_AtThisRollupPeriodStart,

            [Description("[Value] at Last Contract Hour")]
            Value_AtLastRollupPeriodStart,

            [Description("[Value] at Next Contract Hour")]
            Value_AtNextRollupPeriodStart,

            [Description("[Contract Period] Weighted Average")]
            RollupPeriod_CalcWeightedAvg,

            [Description("[Contract Period] Mean")]
            RollupPeriod_CalcMean,

            [Description("[Contract Period] Min")]
            RollupPeriod_CalcMin,

            [Description("[Contract Period] Max")]
            RollupPeriod_CalcMax,

            [Description("[Contract Period] Delta")]
            RollupPeriod_CalcDelta,

            [Description("All History Entries")]
            AllHistoryEntries
        }

        public enum BlankValueHandlingTypes
        {
            [Description("Include")]
            IncludeBlanks,

            [Description("Exclude")]
            ExcludeBlanks,

            [Description("Replace with default")]
            ReplaceWithDefault
        }

        [XmlAttribute]
        public string DefaultValueIfBlank
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public HistoryTypesNew HistoryType
        {
            get => GetPropertyValue<HistoryTypesNew>();
            set => SetPropertyValue(value);
        }

        public enum PointValueTypesNew
        {
            [Description("Raw Value")]
            Value,

            [Description("Alternate Value")]
            AltValue,

            [Description("Timestamp")]
            Timestamp,

            [Description("[Flag] IsInitialized")]
            IsInitialized,

            [Description("[Flag] IsUnreliable")]
            IsUnreliable,

            [Description("[Flag] IsUpdated")]
            IsUpdated,

            [Description("[Flag] ConfigurableBit1")]
            ConfigurableBit1,

            [Description("[Flag] ConfigurableBit2")]
            ConfigurableBit2,

            [Description("[Flag] ConfigurableBit3")]
            ConfigurableBit3,

            [Description("[Flag] ConfigurableBit4")]
            ConfigurableBit4,

            [Description("[Flag] ConfigurableBit5")]
            ConfigurableBit5,

            [Description("[Flag] ConfigurableBit6")]
            ConfigurableBit6,

            [Description("[Flag] ConfigurableBit7")]
            ConfigurableBit7,

            [Description("[Flag] String Data")]
            StringData,

            [Description("[Flag] ConfigurableBit8")]
            ConfigurableBit8,

            [Description("[Flag] Digital Data")]
            DigitalData,

            [Description("[Flag] Output Data")]
            OutputData,

            [Description("[Flag] ConfigurableBit9")]
            ConfigurableBit9,

            [Description("[Flag] ConfigurableBit10")]
            ConfigurableBit10,

            [Description("[Flag] UserBit1")]
            UserBit1,

            [Description("[Flag] UserBit2")]
            UserBit2,

            [Description("[Flag] UserBit3")]
            UserBit3,

            [Description("[Flag] UserBit4")]
            UserBit4,

            [Description("[Flag] UserBit5")]
            UserBit5,

            [Description("[Flag] UserBit6")]
            UserBit6,

            [Description("[Flag] UserBit7")]
            UserBit7,

            [Description("[Flag] UserBit8")]
            UserBit8,

            [Description("[Flag] UserBit9")]
            UserBit9,

            [Description("[Flag] UserBit10")]
            UserBit10,

            [Description("[Flag] UserBit11")]
            UserBit11,

            [Description("[Flag] UserBit12")]
            UserBit12,

            [Description("[Flag] UserBit13")]
            UserBit13,

            [Description("[Flag] UserBit14")]
            UserBit14,

            [Description("[Flag] UserBit15")]
            UserBit15,

            [Description("[Flag] UserBit16")]
            UserBit16,

            [Description("[Flag] PointScheme1")]
            PointScheme1,

            [Description("[Flag] PointScheme2")]
            PointScheme2,

            [Description("[Flag] PointScheme3")]
            PointScheme3,

            [Description("[Flag] PointScheme4")]
            PointScheme4,

            [Description("[Flag] AlarmPriorityCategory1")]            
            AlarmPriorityCategory1,

            [Description("[Flag] AlarmyPriorityCategory2")]
            AlarmPriorityCategory2,

            [Description("[Flag] AlarmPriorityCategory3")]
            AlarmPriorityCategory3,

            [Description("[Flag] ExternalValue")]
            ExternalValue,

            [Description("[Flag] VHS Value Edited")]
            VHSValueEdited,

            [Description("[Flag] Alarm Suppressed")]
            AlarmSuppressed,

            [Description("[Flag] ConfigurableBit11")]
            ConfigurableBit11,

            [Description("[Flag] ConfigurableBit12")]
            ConfigurableBit12,

            [Description("[Flag] ConfigurableBit13")]
            ConfigurableBit13,

            [Description("[Flag] VHS Value Deleted")]
            VHSValueDeleted,

            [Description("[Flag] ConfigurableBit14")]
            ConfigurableBit14,

            [Description("[Flag] ConfigurableBit15")]
            ConfigurableBit15
        }

        [XmlAttribute]
        public PointValueTypesNew PointValueType
        {
            get => GetPropertyValue<PointValueTypesNew>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public bool IncludeUnreliable
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public bool OnlyNumeric
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }
              
        public BlankValueHandlingTypes BlankValueHandlingType
        {
            get => GetPropertyValue<BlankValueHandlingTypes>();
            set => SetPropertyValue(value);
        }

        public bool IsRuleValid => CheckIfValid.isValid;
        public string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid => (true, "No Error");
    }
}
