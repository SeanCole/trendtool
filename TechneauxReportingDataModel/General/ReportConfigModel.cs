using System;
using System.ComponentModel;
using System.Xml.Serialization;
using TechneauxReportingDataModel.CygNet;
using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxReportingDataModel.SmartSheet;

using TechneauxReportingDataModel.SqlHistory;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.General
{
    [Serializable]
    [XmlRoot("ConfigItems"), XmlType("ConfigItems")]
    public class ReportConfigModel : NotifyCopyDataModel
    {
        const string ExcludedDomain = "wpx";
    // Class used to store all the configuration data
        public ReportConfigModel()
        {
          
            
            CygNetGeneral = new CygNetGeneralOptions();
            TrendGeneral = new TrendGeneralOptions();
            SourcePointRules = new BindingList<PointHistorySelectionRule>();

        }       

        public BindingList<PointHistorySelectionRule> SourcePointRules
        {
            get => GetPropertyValue<BindingList<PointHistorySelectionRule>>();
            set => SetPropertyValue(value);
        }



        public CygNetGeneralOptions CygNetGeneral
        {
            get => GetPropertyValue<CygNetGeneralOptions>();
            set => SetPropertyValue(value);
        }

        public TrendGeneralOptions TrendGeneral
        {
            get => GetPropertyValue<TrendGeneralOptions>();
            set => SetPropertyValue(value);
        }


    }
}