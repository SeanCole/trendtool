using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Media;
using System.Xml.Serialization;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using TechneauxUtility;
using XmlDataModelUtility;


namespace TechneauxReportingDataModel.General
{
    public class PointTrendOptions : NotifyCopyDataModel, IValidatedRule
    {

        public PointTrendOptions()
        {
            AxisId = AxisIdNames.LeftLeft;
            MinVal = 0;
            TrendNum = "1";
            MaxVal = 0;
            LineColor = Color.FromRgb(0, 0, 0);
            Label = "Temp";
        }

        [XmlAttribute]
        public AxisIdNames AxisId
        {
            get => GetPropertyValue<AxisIdNames>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public long MinVal
        {
            get => GetPropertyValue<long>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public long MaxVal
        {
            get => GetPropertyValue<long>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string TrendNum
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string LineColorArgb
        {
            get => $"{LineColor.A},{LineColor.R},{LineColor.B},{LineColor.G}";
            set
            {
                var argbVals = value.Split(',').Select(v => Convert.ToByte(v)).ToList();
                LineColor = Color.FromArgb(argbVals[0], argbVals[1], argbVals[2], argbVals[3]);
            }
        }

        [XmlIgnore]
        public Color LineColor
        {
            get => GetPropertyValue<Color>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string Label
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public enum AxisIdNames
        {
            [Description("Left, Left")]
            LeftLeft,

            [Description("Left, Right")]
            LeftRight,

            [Description("Right, Left")]
            RightLeft,
            
            [Description("Right, Right")]
            RightRight
        }

        public bool IsRuleValid => CheckIfValid.isValid;

        public string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid => throw new NotImplementedException();
    }
}
