using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using TechneauxUtility;
using XmlDataModelUtility;
using TechneauxReportingDataModel.General;


namespace TechneauxReportingDataModel.CygNet.Rules
{
    public class PointHistorySelectionRule : NotifyCopyDataModel, IValidatedRule
    {
        public PointHistorySelectionRule()
        {
            PointSelection = new FacilityPointSelection();
          
            HistoryNormalizationOptions = new PointHistoryNormalizationOptions();
            CygNetElementRule = new CygNetRuleBase();
            TrendOptions = new PointTrendOptions();
            GeneralHistoryOptions = new PointHistoryGeneralOptions();
        }

        public FacilityPointSelection PointSelection
        {
            get => GetPropertyValue<FacilityPointSelection>();
            set => SetPropertyValue(value);
        }

        public CygNetRuleBase CygNetElementRule
        {
            get => GetPropertyValue<CygNetRuleBase>();
            set => SetPropertyValue(value);
        }

        public PointTrendOptions TrendOptions
        {
            get => GetPropertyValue<PointTrendOptions>();
            set => SetPropertyValue(value);
        }

        public PointHistoryGeneralOptions GeneralHistoryOptions
        {
            get => GetPropertyValue<PointHistoryGeneralOptions>();
            set => SetPropertyValue(value);
        }

        public PointHistoryNormalizationOptions HistoryNormalizationOptions
        {
            get => GetPropertyValue<PointHistoryNormalizationOptions>();
            set => SetPropertyValue(value);
        }

        public bool IsRuleValid => CheckIfValid.isValid;
        public string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid
        {
            get
            {
                if (!PointSelection.IsRuleValid)
                    return (PointSelection.IsRuleValid, $"{PointSelection.ValidationErrorMessage}");

                if (!GeneralHistoryOptions.IsRuleValid)
                    return (GeneralHistoryOptions.IsRuleValid, $"{GeneralHistoryOptions.ValidationErrorMessage}");

                if (!HistoryNormalizationOptions.IsRuleValid)
                    return (HistoryNormalizationOptions.IsRuleValid, $"{HistoryNormalizationOptions.ValidationErrorMessage}");

                return (true, "No Error");
            }
        }

    }
}
