using System;
using System.Xml.Serialization;
using System.Windows.Media;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using TechneauxUtility;
using XmlDataModelUtility;

using System.Collections.Generic;
using System.Linq;

namespace TechneauxReportingDataModel.General
{
    public class TrendGeneralOptions : NotifyCopyDataModel, IValidatedRule
    {       
        public TrendGeneralOptions()
        {
            BackgroundColor = Color.FromRgb(255,255,255);
            DefaultNumDays = 7;
            UseNavBar = false;
  
        }

        [XmlAttribute]
        public string BackgroundColorArgb
        {
            get => $"{BackgroundColor.A},{BackgroundColor.R},{BackgroundColor.B},{BackgroundColor.G}";
            set
            {
                var argbVals = value.Split(',').Select(v => Convert.ToByte(v)).ToList();
                BackgroundColor = Color.FromArgb(argbVals[0], argbVals[1], argbVals[2], argbVals[3]);
            }
        }

        [XmlIgnore]
        public Color BackgroundColor
        {
            get => GetPropertyValue<Color>();
            set => SetPropertyValue(value);
        }               

        [XmlAttribute]
        public double DefaultNumDays
        {
            get => GetPropertyValue<double>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public bool UseNavBar
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        //public FacilityFilteringOptions FacilityFilteringRules
        //{
        //    get => GetPropertyValue<FacilityFilteringOptions>();
        //    set => SetPropertyValue(value);
        //}

        public bool IsRuleValid => CheckIfValid.isValid;

        public string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid => throw new NotImplementedException();
    }
}
