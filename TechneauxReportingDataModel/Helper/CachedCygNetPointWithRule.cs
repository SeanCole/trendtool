﻿using Techneaux.CygNetWrapper.Points;
using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxReportingDataModel.SqlHistory.Enumerable;

namespace TechneauxReportingDataModel.Helper
{
    public class CachedCygNetPointWithRule : CachedCygNetPoint
    {
        public PointHistorySelectionRule SrcRuleTrend { get; }

        public SqlPointSelection SrcRule { get; }

        public CachedCygNetPointWithRule(
            ICachedCygNetPoint srcPnt,
            PointHistorySelectionRule srcRule) 
            : base(srcPnt.PntService,
                  srcPnt.CvsService,
                  srcPnt.Facility,
                  srcPnt.Tag
                )
        {
            SrcRuleTrend = srcRule;
        }
        public CachedCygNetPointWithRule(
    ICachedCygNetPoint srcPnt,
    SqlPointSelection srcRule)
    : base(srcPnt.PntService,
          srcPnt.CvsService,
          srcPnt.Facility,
          srcPnt.Tag
        )
        {
            SrcRule = srcRule;
        }
    }
}
