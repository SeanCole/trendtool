using System.ComponentModel;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using TechneauxReportingDataModel.SqlHistory.SubOptions;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.SqlHistory
{
    public class SqlHistorySyncConfigModel : NotifyCopyDataModel
    {
        public SqlHistorySyncConfigModel()
        {
            SqlGeneralOpts = new SqlGeneralOptions();
            HistorySyncOpts = new HistorySyncOptions();

            SourcePointRules = new BindingList<SqlPointSelection>();
            TableMappingRules = new BindingList<SqlTableMapping>();                        
        }
        
        public SqlGeneralOptions SqlGeneralOpts
        {
            get => GetPropertyValue<SqlGeneralOptions>();
            set => SetPropertyValue(value);
        }

        public HistorySyncOptions HistorySyncOpts
        {
            get => GetPropertyValue<HistorySyncOptions>();
            set => SetPropertyValue(value);
        }

        public BindingList<SqlPointSelection> SourcePointRules
        {
            get => GetPropertyValue<BindingList<SqlPointSelection>>();
            set => SetPropertyValue(value);
        }

        public BindingList<SqlTableMapping> TableMappingRules
        {
            get => GetPropertyValue<BindingList<SqlTableMapping>>();
            set => SetPropertyValue(value);
        }

    }
}
