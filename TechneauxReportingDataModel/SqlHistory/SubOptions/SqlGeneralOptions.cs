using System.ComponentModel;
using System.Xml.Serialization;
using XmlDataModelUtility;


namespace TechneauxReportingDataModel.SqlHistory.SubOptions
{
    public class SqlGeneralOptions : NotifyCopyDataModel
    {

        public SqlGeneralOptions()
        {
            Username = "";
            Password = "";
            ConnectionErrorMessage = "";
            TableName = "";
            ServerName = "";
            DatabaseName = "";
            SelectedConnectionType = SqlConnectionType.SqlServer;
            AuthenticationType = AuthenticationTypes.Integrated;
        }
        

        public string GetSqlServerConnectionParamString()
        {
            var key = $"{ServerName}|{Username}|{Password}|{AuthenticationType}";
            return key;
        }

        public bool IsValid
        {
            get
            {
                if (string.IsNullOrEmpty(ServerName)) return false;
                
                return true;
            }
        }

        public enum AuthenticationTypes
        {
            [Description("Integrated Security (NT)")]
            Integrated,

            [Description("Specific User/Password")]
            UserPassword
        }


        [XmlAttribute]
        public AuthenticationTypes AuthenticationType
        {
            get => GetPropertyValue<AuthenticationTypes>();
            set => SetPropertyValue(value);
        }

        public enum SqlConnectionType
        {
            [Description("SQL Server")]
            SqlServer
        }

        [XmlAttribute]
        public string TableName
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string Username
        {
            get {   if(AuthenticationType == AuthenticationTypes.Integrated)
                    {
                        SetPropertyValue(System.Environment.UserDomainName + "\\" + System.Environment.UserName);
                        Password = "";
                    }    
                    return GetPropertyValue<string>(); }
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string Password
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string DatabaseName
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string ServerName
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlIgnore]
        public string ConnectionErrorMessage
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public SqlConnectionType SelectedConnectionType
        {
            get => GetPropertyValue<SqlConnectionType>();
            set => SetPropertyValue(value);
        }
    }

}
