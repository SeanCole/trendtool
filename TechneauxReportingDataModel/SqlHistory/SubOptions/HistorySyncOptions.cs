using System;
using System.Xml.Serialization;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.SqlHistory.SubOptions
{
    public class HistorySyncOptions : NotifyCopyDataModel
    {

        public HistorySyncOptions()
        {            
            FullAuditInterval = 0;
            AuditRunStartTime = DateTime.Now;
            
            AuditMaxPointsPerHour = 0;
            EnableFullAudit = false;
        }

        [XmlAttribute]
        public DateTime AuditRunStartTime
        {
            get => GetPropertyValue<DateTime>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public bool EnableFullAudit
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }       

        [XmlAttribute]
        public Double FullAuditInterval
        {
            get => GetPropertyValue<Double>();
            set => SetPropertyValue(value);
        }
      
        public TimeSpan AuditMaxRunTime
        {
            get => GetPropertyValue<TimeSpan>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public int AuditMaxPointsPerHour
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }
    }

}
