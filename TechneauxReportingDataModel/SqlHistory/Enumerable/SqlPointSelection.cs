using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.SqlHistory.Enumerable
{

    //Subclass containing column definition information
    public class SqlPointSelection : CygNetPointHistorySelectionRule, IValidatedRule
    {
        public SqlPointSelection()
        {
            PollingOptions = new PointHistoryPollingRetentionOptions();
        }

        public PointHistoryPollingRetentionOptions PollingOptions
        {
            get => GetPropertyValue<PointHistoryPollingRetentionOptions>();
            set => SetPropertyValue(value);
        }

        public new bool IsRuleValid => CheckIfValid.isValid;
        public new string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid
        {
            get
            {
                if (!base.IsRuleValid)
                    return (base.IsRuleValid, $"{base.ValidationErrorMessage}");

                if (!PollingOptions.IsRuleValid)
                    return (false, $"{PollingOptions.ValidationErrorMessage}");

                return (true, "No Error");
            }
        }

    }
}