﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.SqlServer.Management.Smo;
using Serilog;
using Techneaux.CygNetWrapper;
using Techneaux.CygNetWrapper.Points;
using TechneauxHistorySynchronization.Helper;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory.DatabaseOperations;
using TechneauxReportingDataModel.SqlHistory;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using TechneauxReportingDataModel.SqlHistory.SubOptions;
using TechneauxUtility;

namespace TechneauxHistorySynchronization.SqlServerHistory
{
    /// <summary>
    /// Defines the <see cref="SqlRowResolver" />
    /// </summary>
    public static class SqlRowResolver
    {
        #region Methods

        /// <summary>
        /// The DeleteBadSqlRows
        /// </summary>
        /// <param name="tableSchema">The <see cref="SimpleCombinedTableSchema"/></param>
        /// <param name="sqlRows">The <see cref="List{IValueSet}"/></param>
        /// <param name="ct">The <see cref="CancellationToken"/></param>
        /// <returns>The <see cref="Task"/></returns>
        public static async Task DeleteBadSqlRows(
            SimpleCombinedTableSchema tableSchema,
            List<IValueSet> sqlRows,
            CancellationToken ct)
        {
            Log.Debug("Starting sql delete process");
            try
            {
                var uniquePrimaryKeys = new Dictionary<string, string>();

                var timeCol = tableSchema.TimeColumn.SqlTableFieldName;

                foreach (var key in tableSchema.PriKeyColNames.Where(col => col != timeCol))
                {
                    var distinctValues = sqlRows.Select(item => item.CellValues[key].StringValue).Distinct().ToList();
                    if (distinctValues.Count() == 1)
                    {
                        uniquePrimaryKeys[key] = distinctValues.First().Trim();
                    }
                }

                var uniqueKeyCondition =
                    string.Join(" AND ", uniquePrimaryKeys.Select(item => $"{item.Key} = '{item.Value}'"));
                var query = $"DELETE FROM {tableSchema.SrcSqlGeneralOptions.TableName} WHERE {uniqueKeyCondition} ";
         
                const int numRowsDeleteAtATime = 400;

                var subLists = sqlRows.SplitList(numRowsDeleteAtATime);

                foreach (var subList in subLists)
                {
                    var timestampList = subList.Select(item => item.CellValues[timeCol].StringValue);

                    var thisQuery = query + $" AND {timeCol} IN ('{string.Join("','", timestampList)}')";

                    await SqlServerConnectionUtility.SqlConnectionLocker.WaitAsync();
                    try
                    {
                        var connStr =
                            await SqlServerConnectionUtility.SqlStringConnectionBuilder(
                                tableSchema.SrcSqlGeneralOptions, true);                       

                        using (var sqlConn = new SqlConnection(connStr))
                        using (var selectCmd = new SqlCommand(thisQuery))
                        {
                            sqlConn.Open();
                            selectCmd.Connection = sqlConn;
                            selectCmd.ExecuteNonQuery();
                        }
                    }
                    catch (SmoException ex)
                    {
                        //System.Windows.Forms.MessageBox.Show($"Failed to sync with table with exception: {ex.Message} {Environment.NewLine}Operation Failed");
                        //if (ex.InnerException != null)
                        //    System.Windows.Forms.MessageBox.Show($"Inner exception = {ex.InnerException.Message}");
                        Log.Error(ex, "Delete query failed");

                        throw;
                    }
                    finally
                    {
                        SqlServerConnectionUtility.SqlConnectionLocker?.Release();
                    }
                }

                //Log.Debug($"Speed is approx {sqlRows.Count / timeTakenForDelete.TotalSeconds} rows per second");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Record deletion general failure");

                throw;
            }
        }

        /// <summary>
        /// The DeleteBadSqlRows
        /// </summary>
        /// <param name="tableSchema">The <see cref="SimpleCombinedTableSchema"/></param>
        /// <param name="srcReportDataModel">The <see cref="SqlHistorySyncConfigModel"/></param>
        /// <param name="srcPnt">The <see cref="ICachedCygNetPoint"/></param>
        /// <param name="ct">The <see cref="CancellationToken"/></param>
        /// <returns>The <see cref="Task"/></returns>
        public static async Task DeleteAllSqlRowsForPoint(
            SimpleCombinedTableSchema tableSchema,
            ICachedCygNetPoint srcPnt,
            CancellationToken ct)
        {
            Log.Debug("Starting sql delete process");
            try
            {
                var fixedPriKeyVals = await tableSchema.GetUniquePrimaryKeysAndValues(srcPnt, ct);
                var fixedPkyCondition = string.Join(" AND ",
                    fixedPriKeyVals.Select(item => $"{item.Key} = '{item.Value.StringValue}'"));

                var query = $"DELETE FROM {tableSchema.SrcSqlGeneralOptions.TableName} WHERE {fixedPkyCondition}";

                try
                {

                    await SqlServerConnectionUtility.SqlConnectionLocker.WaitAsync();
                    var connStr =
                        await SqlServerConnectionUtility.SqlStringConnectionBuilder(tableSchema.SrcSqlGeneralOptions,
                            true);

                    using (var sqlConn = new SqlConnection(connStr))
                    using (var selectCmd = new SqlCommand(query))
                    {
                        sqlConn.Open();
                        selectCmd.Connection = sqlConn;
                        selectCmd.ExecuteNonQuery();
                    }
                }
                catch (SmoException ex)
                {
                    //System.Windows.Forms.MessageBox.Show($"Failed to sync with table with exception: {ex.Message} {Environment.NewLine}Operation Failed");
                    //if (ex.InnerException != null)
                    //    System.Windows.Forms.MessageBox.Show($"Inner exception = {ex.InnerException.Message}");
                    Log.Error(ex, "Delete query failed");
                    throw;
                }
                finally
                {
                    SqlServerConnectionUtility.SqlConnectionLocker?.Release();
                }              
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Record deletion general failure");
                throw;
            }
        }

        /// <summary>
        /// The DeleteBadSqlRows
        /// </summary>
        /// <param name="tableSchema">The <see cref="SimpleCombinedTableSchema"/></param>
        /// <param name="srcReportDataModel">The <see cref="SqlHistorySyncConfigModel"/></param>
        /// <param name="srcPnt">The <see cref="ICachedCygNetPoint"/></param>
        /// <param name="ct">The <see cref="CancellationToken"/></param>
        /// <returns>The <see cref="Task"/></returns>
        public static async Task<int> DeleteExpiredSqlForPoint(
            SimpleCombinedTableSchema tableSchema,
            ICachedCygNetPoint srcPnt,
            double RetentionDays,
            CancellationToken ct)
        {
            Log.Debug("Starting sql delete process");
            try
            {

                var fixedPriKeyVals = await tableSchema.GetUniquePrimaryKeysAndValues(srcPnt, ct);
                var fixedPkyCondition = string.Join(" AND ",
                    fixedPriKeyVals.Select(item => $"{item.Key} = '{item.Value.StringValue}'"));
                fixedPkyCondition = fixedPkyCondition + $" AND {tableSchema.TimeColumn.SqlTableFieldName} < '{DateTime.Now.AddDays(RetentionDays * -1).ToString()}'";
                var query = $"DELETE FROM {tableSchema.SrcSqlGeneralOptions.TableName} WHERE {fixedPkyCondition}";
                
 
                try
                {
                    await SqlServerConnectionUtility.SqlConnectionLocker.WaitAsync();
                    int rowsAffected;
                    var connStr =
                        await SqlServerConnectionUtility.SqlStringConnectionBuilder(tableSchema.SrcSqlGeneralOptions,
                            true);
                    //connLock = connectionLock;
                    //new SemaphoreSlim(1, 1);
                    using (var sqlConn = new SqlConnection(connStr))
                    using (var selectCmd = new SqlCommand(query))
                    {
                        sqlConn.Open();
                        selectCmd.Connection = sqlConn;
                        rowsAffected = selectCmd.ExecuteNonQuery();
                    }
                    return rowsAffected;
                }
                catch (SmoException ex)
                {
                    //System.Windows.Forms.MessageBox.Show($"Failed to sync with table with exception: {ex.Message} {Environment.NewLine}Operation Failed");
                    //if (ex.InnerException != null)
                    //    System.Windows.Forms.MessageBox.Show($"Inner exception = {ex.InnerException.Message}");
                    Log.Error(ex, "Delete query failed");
                    throw;
                }
                finally
                {
                    SqlServerConnectionUtility.SqlConnectionLocker?.Release();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Record deletion general failure");
                throw;
            }
        }

        /// <summary>
        /// The GetFirstFiveFromDb
        /// </summary>
        /// <param name="tableSchema">The <see cref="SimpleCombinedTableSchema"/></param>
        /// <param name="basePnt">The <see cref="ICachedCygNetPoint"/></param>
        /// <param name="numRecords">The <see cref="long"/></param>
        /// <param name="ct">The <see cref="CancellationToken"/></param>
        public static async Task<List<SqlHistoryRow>> GetFirstXFromDb(
            SimpleCombinedTableSchema tableSchema,
            ICachedCygNetPoint basePnt,
            long numRecords,
            CancellationToken ct)
        {
            var timeColumn = tableSchema.TimeColumn.SqlTableFieldName;

            var fixedPriKeyVals = await tableSchema.GetUniquePrimaryKeysAndValues(basePnt, ct);

            var fixedPkyCondition = string.Join(" AND ",
                fixedPriKeyVals.Select(item => $"{item.Key} = '{item.Value.StringValue}'"));

            var query =
                $"declare @top int = {numRecords}; SELECT TOP (@top) {string.Join(", ", tableSchema.MatchedColsNonFixedPrimaryKeys.Select(item => item.Name))} " +
                $"FROM {tableSchema.SrcSqlGeneralOptions.TableName} " +
                $"WHERE {fixedPkyCondition} " +
                $"ORDER BY {timeColumn} ASC";

            return await QuerySqlData(
                tableSchema,
                fixedPriKeyVals,
                query,
                ct);
        }

        /// <summary>
        /// The GetLastFiveFromDb
        /// </summary>
        /// <param name="tableSchema">The <see cref="SimpleCombinedTableSchema"/></param>
        /// <param name="sqlOpts">The <see cref="SqlGeneralOptions"/></param>
        /// <param name="basePnt">The <see cref="ICachedCygNetPoint"/></param>
        /// <param name="numRecords">The <see cref="long"/></param>
        /// <param name="ct">The <see cref="CancellationToken"/></param>
        public static async Task<List<SqlHistoryRow>> GetLastXFromDb(
            SimpleCombinedTableSchema tableSchema,
            ICachedCygNetPoint basePnt,
            long numRecords,
            CancellationToken ct)
        {
            var sqlOpts = tableSchema.SrcSqlGeneralOptions;
            var timeColumn = tableSchema.TimeColumn.SqlTableFieldName;

            var fixedPriKeyVals = await tableSchema.GetUniquePrimaryKeysAndValues(basePnt, ct);

            var fixedPkyCondition = string.Join(" AND ",
                fixedPriKeyVals.Select(item => $"{item.Key} = '{item.Value.StringValue}'"));

            var query =
                $"declare @top int = {numRecords}; SELECT TOP (@top) {string.Join(", ", tableSchema.MatchedColsNonFixedPrimaryKeys.Select(item => item.Name))} " +
                $"FROM {sqlOpts.TableName} " +
                $"WHERE {fixedPkyCondition} " +
                $"ORDER BY {timeColumn} DESC";

            return await QuerySqlData(
                tableSchema,
                fixedPriKeyVals,
                query,
                ct);
        }

        /// <summary>
        /// The GetMatchedColumnList
        /// </summary>
        /// <param name="currentDbColumns">The <see cref="IEnumerable{Column}"/></param>
        /// <param name="srcElms">The <see cref="IEnumerable{SqlTableMapping}"/></param>
        public static List<string> GetMatchedColumnList(
            IEnumerable<Column> currentDbColumns,
            IEnumerable<SqlTableMapping> srcElms)
        {
            var dbColumns = currentDbColumns.ToList();
            var sqlTableMappings = srcElms.ToList();

            if (!dbColumns.IsAny() || !sqlTableMappings.IsAny())
                return new List<string>();

            var dbColNames = dbColumns.Select(col => col.Name);
            var elmNames = sqlTableMappings.Select(elm => elm.SqlTableFieldName);

            var dbNameSet = new HashSet<string>(dbColNames);

            var matchedColNames = new List<string>();

            foreach (var elmName in elmNames)
            {
                if (dbNameSet.Contains(elmName))
                    matchedColNames.Add(elmName);
            }

            //MatchedColNames.UnionWith(dbColNames);
            //MatchedColNames.IntersectWith(elmNames);

            return matchedColNames.ToList();
        }

        //resolves rows from sql db into list of mappedsqlrow objects
        /// <summary>
        /// The GetProcessedRowsFromDb
        /// </summary>
        /// <param name="tableSchema">The <see cref="SimpleCombinedTableSchema"/></param>
        /// <param name="beginning">The <see cref="DateTime"/></param>
        /// <param name="end">The <see cref="DateTime"/></param>
        /// <param name="basePnt">The <see cref="ICachedCygNetPoint"/></param>
        /// <param name="ct">The <see cref="CancellationToken"/></param>
        public static async Task<List<SqlHistoryRow>> GetProcessedRowsFromDb(
            SimpleCombinedTableSchema tableSchema,
            DateTime beginning,
            DateTime end,
            ICachedCygNetPoint basePnt,
            CancellationToken ct)
        {
            try
            {
                var timeColumn = tableSchema.TimeColumn.SqlTableFieldName;

                var timestampConstraint = " " + timeColumn + " between '" + beginning + "' AND '" + end + "'";


                var fixedPriKeyVals = await tableSchema.GetUniquePrimaryKeysAndValues(basePnt, ct);

                var fixedPkyCondition = string.Join(" AND ",
                    fixedPriKeyVals.Select(item => $"{item.Key} = '{item.Value.StringValue}'"));
                string query;
                if (fixedPkyCondition == "")
                {
                    query = $"SELECT {string.Join(", ", tableSchema.MatchedColsNonFixedPrimaryKeys.Select(item => item.Name))} " +
                    $"FROM {tableSchema.SrcSqlGeneralOptions.TableName} " +
                    $"WHERE {timestampConstraint}";
                }
                else
                {
                    query =
                    $"SELECT {string.Join(", ", tableSchema.MatchedColsNonFixedPrimaryKeys.Select(item => item.Name))} " +
                    $"FROM {tableSchema.SrcSqlGeneralOptions.TableName} " +
                    $"WHERE {fixedPkyCondition} AND {timestampConstraint}";
                }
                ct.ThrowIfCancellationRequested();
                return await QuerySqlData(
                    tableSchema,
                    fixedPriKeyVals,
                    query,
                    ct);
            }
            catch (Exception ex)
            {
                Log.Warning(ex, "Error getting processed rows from DB");
                throw;
            }

        }

        /// <summary>
        /// The GetProcessedRowsFromDb
        /// </summary>
        /// <param name="tableSchema">The <see cref="SimpleCombinedTableSchema"/></param>
        /// <param name="sqlOpts">The <see cref="SqlGeneralOptions"/></param>
        /// <param name="sqlColumnList">The <see cref="List{SimpleSqlColumnInfo}"/></param>
        /// <param name="basePnt">The <see cref="ICachedCygNetPoint"/></param>
        /// <param name="numRecords">The <see cref="long"/></param>
        /// <param name="ct">The <see cref="CancellationToken"/></param>
        public static async Task<List<SqlHistoryRow>> GetProcessedRowsFromDb(
            SimpleCombinedTableSchema tableSchema,
            SqlGeneralOptions sqlOpts,
            List<SimpleSqlColumnInfo> sqlColumnList,
            ICachedCygNetPoint basePnt,
            long numRecords,
            CancellationToken ct)
        {
            try
            {
                var timeColumn = tableSchema.TimeColumn.SqlTableFieldName;

                var fixedPriKeyVals = await tableSchema.GetUniquePrimaryKeysAndValues(basePnt, ct);

                var fixedPkyCondition = string.Join(" AND ",
                    fixedPriKeyVals.Select(item => $"{item.Key} = '{item.Value.StringValue}'"));

                var query =
                    $"SELECT TOP {numRecords} {string.Join(", ", tableSchema.MatchedColsNonFixedPrimaryKeys.Select(item => item.Name))} " +
                    $"FROM {sqlOpts.TableName} " +
                    $"WHERE {fixedPkyCondition} " +
                    $"ORDER BY {timeColumn} DESC";

                return await QuerySqlData(
                    tableSchema,
                    fixedPriKeyVals,
                    query,
                    ct);
            }
            catch (Exception ex)
            {
                Log.Warning(ex, "Error getting processed rows from DB");
                throw;
            }

        }

        /// <summary>
        /// The InsertMissingSqlRows
        /// </summary>
        /// <param name="tableSchema">The <see cref="SimpleCombinedTableSchema"/></param>
        /// <param name="cygRows">The <see cref="List{IValueSet}"/></param>
        /// <param name="ct">The <see cref="CancellationToken"/></param>
        /// <returns>The <see cref="Task"/></returns>
        public static async Task InsertMissingSqlRows(
            SimpleCombinedTableSchema tableSchema,
            List<IValueSet> cygRows,
            CancellationToken ct)
        {
            try
            {
                var wsAll = new Stopwatch();
                wsAll.Start();
                await SqlServerConnectionUtility.SqlConnectionLocker.WaitAsync();

                using (var newTable = MakeTableForInsert(tableSchema, cygRows))
                {
                    var connStr =
                        await SqlServerConnectionUtility.SqlStringConnectionBuilder(tableSchema.SrcSqlGeneralOptions, true);
                    

                    using (var sqlConn = new SqlConnection(connStr))
                    {
                        await sqlConn.OpenAsync(ct);

                        using (var bulkCopy = new SqlBulkCopy(sqlConn))
                        {
                            bulkCopy.DestinationTableName = tableSchema.SrcSqlGeneralOptions.TableName;

                            await bulkCopy.WriteToServerAsync(newTable, ct);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Failed to run insert query");
            }
            finally
            {
                SqlServerConnectionUtility.SqlConnectionLocker?.Release();
            }

            //Log.Debug($"Speed is approx {cygRows.Count / timeTakenForInsert.TotalSeconds} rows per second");
        }

        /// <summary>
        /// The MakeTableForInsert
        /// </summary>
        /// <param name="tableSchema">The <see cref="SimpleCombinedTableSchema"/></param>
        /// <param name="cygRows">The <see cref="List{IValueSet}"/></param>
        /// <returns>The <see cref="DataTable"/></returns>
        public static DataTable MakeTableForInsert(
            SimpleCombinedTableSchema tableSchema,
            List<IValueSet> cygRows)
        {
            var tableForInsert = new DataTable("InsertTable");

            var pKeyColumnsNames = tableSchema.PriKeyColNames.ToList();

            var pKeyCols = new List<DataColumn>();

            foreach (var col in tableSchema.SqlColumns)
            {
                var newCol = new DataColumn
                {
                    ColumnName = col.Name,
                    DataType = SqlCygDataTypeResolver.DeriveClrType(col),
                    AllowDBNull = col.Nullable
                };

                tableForInsert.Columns.Add(newCol);

                if (pKeyColumnsNames.Contains(col.Name))
                {
                    pKeyCols.Add(newCol);
                }
            }

            tableForInsert.PrimaryKey = pKeyCols.ToArray();
            foreach (var row in cygRows)
            {
                var newRow = tableForInsert.NewRow();
                foreach (var name in row.CellValues.Keys)
                {
                    //DateTime nextDateTime;
                    //if (row.CellValues[name].TryGetDateTime(out nextDateTime))
                    //{
                    //    newRow[name] = nextDateTime;
                    //}
                    //else
                    //{
                    //if (row.CellValues[name].RawValue is IConvertibleValue conv)
                    //    newRow[name] = conv.RawValue;
                    //else
                        newRow[name] = row.CellValues[name].RawValue;

                    //}
                }
                try
                {
                    tableForInsert.Rows.Add(newRow);
                }
                catch (Exception ex)
                {
                    Log.Warning(ex, $"Can't add row with cell values {string.Join(",", newRow.ItemArray)}");
                }
            }

            try
            {
                tableForInsert.AcceptChanges();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Failed to accecpt changes on insert table");
                return tableForInsert;
            }

            Log.Debug(
                $"Table changes accepted, [{tableForInsert.Rows.Count}] sql rows out of [{cygRows.Count}] CygNet rows");

            return tableForInsert;
        }

        private static async Task<List<SqlHistoryRow>> QuerySqlData(
            SimpleCombinedTableSchema tableSchema,
            Dictionary<string, IConvertibleValue> fixedPriKeyVals,
            string query,
            CancellationToken ct)
        {
            var dbRowList = new List<SqlHistoryRow>();

            
            try
            {
                await SqlServerConnectionUtility.SqlConnectionLocker.WaitAsync();
                var connStr =
                    await SqlServerConnectionUtility.SqlStringConnectionBuilder(tableSchema.SrcSqlGeneralOptions, true);
                
                using (var sqlConn = new SqlConnection(connStr))
                {
                    await sqlConn.OpenAsync(ct);

                    using (var selectCmd = new SqlCommand(query, sqlConn))
                    using (var rdr = await selectCmd.ExecuteReaderAsync(CommandBehavior.CloseConnection, ct))
                    {
                        while (rdr.Read())
                        {
                            ct.ThrowIfCancellationRequested();
                            var sqlRow = new SqlHistoryRow();
                            foreach (var col in tableSchema.MatchedColsNonFixedPrimaryKeys)
                            {
                                IConvertibleValue val = new CygNetConvertableValue(rdr[col.Name]);
                                sqlRow.CellValues.Add(col.Name, val);
                            }

                            foreach (var col in fixedPriKeyVals)
                            {
                                sqlRow.CellValues.Add(col.Key, new CygNetConvertableValue(col.Value));
                            }

                            dbRowList.Add(sqlRow);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Log.Debug(ex, "Sql exception while reading DB rows");
                throw;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error reading DB rows");
                throw;
            }
            finally
            {
                SqlServerConnectionUtility.SqlConnectionLocker?.Release();
            }

            return dbRowList;
        }

        public static string GetCroppedString(SimpleSqlColumnInfo srcCol, string destStr)
        {
            if (srcCol == null && srcCol.DataType == null)
                return "";
            if (!destStr.IsAny())
                return "";
                
            if(srcCol.DataType.IsStringType && destStr.Length > srcCol.DataType.MaximumLength)
            {
                if (srcCol.DataType.MaximumLength < 4)
                    return "...".Substring(0,srcCol.DataType.MaximumLength);

                int index = srcCol.DataType.MaximumLength - 3;
                int truncateLength = destStr.Length - srcCol.DataType.MaximumLength + 3;
                destStr = destStr.Remove(index, truncateLength);
                destStr = destStr + "...";
            }

            return destStr;
        }

        #endregion
    }
}