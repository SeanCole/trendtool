﻿using System.Collections.Generic;
using TechneauxUtility;

namespace TechneauxHistorySynchronization.SqlServerHistory
{
    public class SqlRow : IValueSet
    {
        public Dictionary<string, IConvertibleValue> CellValues { get; set; } = new Dictionary<string, IConvertibleValue>();
    }
}
