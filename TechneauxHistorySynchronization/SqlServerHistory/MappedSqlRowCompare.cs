﻿using System.Collections.Generic;
using System.Xml.Serialization;
using CygNetRuleModel.Resolvers;
using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxHistorySynchronization.SqlServerHistory
{
    public class MappedSqlRowCompare : NotifyModelBase, IValueSet
    {
        public MappedSqlRowCompare()
        {
            CellValues = new Dictionary<string, IConvertibleValue>();
        }

        public MappedSqlRowCompare(CygNetHistoryRow src)
        {
            CellValues = src.CellValues;
        }

        public MappedSqlRowCompare(SqlHistoryRow src)
        {
            CellValues = src.CellValues;
        }

        public MappedSqlRowCompare(CygNetHistoryRow src, CygSqlCompare compType)
        {
            CellValues = src.CellValues;
            ComparisonValue = compType;
        }

        public MappedSqlRowCompare(SqlHistoryRow src, CygSqlCompare compType)
        {
            CellValues = src.CellValues;
            ComparisonValue = compType;
        }

        public enum CygSqlCompare
        {
            Equal,
            CygOnly,
            SqlOnly
        }

        public CygSqlCompare ComparisonValue
        {
            get => GetPropertyValue<CygSqlCompare>();
            set => SetPropertyValue(value);
        }

        [XmlIgnore]
        public Dictionary<string, IConvertibleValue> CellValues { get; set; }

        //public bool Equals(MappedSqlRow SqlRow)
        //{
        //    if(!PrimaryKeyFieldId.Equals(SqlRow.PrimaryKeyFieldId))
        //    {
        //        return false;
        //    }
        //    foreach (var CygPair in CellValues)
        //    {
        //        foreach (var SqlPair in SqlRow.CellValues)
        //        {
        //            v
        //        }
        //    }
        //}
    }
}
