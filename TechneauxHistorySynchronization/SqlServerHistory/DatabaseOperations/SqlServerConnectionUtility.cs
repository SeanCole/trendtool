﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using Microsoft.SqlServer.Management.Smo;
using Serilog;
using Techneaux.CygNetWrapper.Points.PointCaching;
using TechneauxHistorySynchronization.Helper;
using TechneauxHistorySynchronization.Models;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using TechneauxReportingDataModel.SqlHistory.SubOptions;
using static TechneauxReportingDataModel.SqlHistory.SubOptions.SqlGeneralOptions;

namespace TechneauxHistorySynchronization.SqlServerHistory.DatabaseOperations
{
    public class SqlServerConnectionUtility
    {
        public static async Task<SimpleCombinedTableSchema> GetTableSchema(SqlGeneralOptions opts, List<SqlTableMapping> sqlElmsList, bool forceRefresh = false)
        {
            var sqlSchema = await GetTableSchema(opts, forceRefresh);

            return sqlSchema?.WithCygNetColumns(sqlElmsList);
        }

        public static async Task<SimpleSqlTableSchema> GetTableSchema(SqlGeneralOptions opts, bool forceRefresh = false)
        {
            try
            {
                var currentTable = await GetTable(opts);

                if (forceRefresh)
                    currentTable.Refresh();
                if (currentTable != null)
                    return new SimpleSqlTableSchema(opts, currentTable.Columns.Cast<Column>());
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "General exception getting table schema");
                return null;
            }
        }

        private static readonly ExpiringCache<SqlGeneralOptions, Server> CachedConnections;

        static SqlServerConnectionUtility()
        {
            CachedConnections = new ExpiringCache<SqlGeneralOptions, Server>(GetConnectedServerNew, TimeSpan.FromMinutes(10));
        }

        private static readonly SemaphoreSlim LockServerRequests = new SemaphoreSlim(1, 1);

        public static async Task<Server> GetConnectedServer(SqlGeneralOptions srcOpts, CancellationToken ct)
        {
            return await CachedConnections.GetCachedItem(srcOpts, ct);
        }

        private static async Task<Server> GetConnectedServerNew(SqlGeneralOptions srcOpts, CancellationToken ct)
        {
            try
            {
                var key = srcOpts.GetSqlServerConnectionParamString();

                //if (CachedConnections.TryGetValue(key, out Server serv))

                if (!srcOpts.IsValid)
                    return null;
                
                var server = await GetConnectedSqlServer(srcOpts);

                ct.ThrowIfCancellationRequested();

                if (server == null)
                {
                    throw new ConnectionFailureException("Failed to connect to SQL server");
                }

                string[] columnPersistentProperties = { "InPrimaryKey", "Nullable", "DataType", "Name" };

                string[] tableProperties = { "Name" };

                string[] dbProperties = { "Name", "IsAccessible", "IsSystemObject", "IsDatabaseSnapshot" };

                server.SetDefaultInitFields(typeof(Column), columnPersistentProperties);
                server.SetDefaultInitFields(typeof(Table), tableProperties);
                server.SetDefaultInitFields(typeof(Database), dbProperties);

                ct.ThrowIfCancellationRequested();

                var availableDbs = server.Databases.GetAvailableDatabases().Values.Where(db => db.IsAccessible);


                foreach (var db in availableDbs)
                {
                    ct.ThrowIfCancellationRequested();

                    await Task.Run(() => db.Refresh());

                    ct.ThrowIfCancellationRequested();
                    db.Tables.Refresh();

                    //foreach (Table table in db.Tables)
                    //{
                    //    table.Columns.Refresh();
                    //}
                }

                return server;
            }
            catch (OperationCanceledException) { return null; }
            catch (Exception ex)
            {
                Log.Warning(ex, "General exception getting connected SQL server");
                
            }

            return null;
        }

        public static async Task<Database> GetDatabase(SqlGeneralOptions srcOpts)
        {
            var thisServer = await GetConnectedServer(srcOpts, CancellationToken.None);

            if (thisServer == null)
            {
                throw new ConnectionFailureException("Failed to connect to SQL server");
            }

            var urnPath = $"Server[@Name='{srcOpts.ServerName}']/Database[@Name='{srcOpts.DatabaseName}']";
            var urnDb = new Urn(urnPath);

            var db = thisServer.GetSmoObject(urnDb) as Database;

            return db;
        }

        public static async Task<Table> GetTable(SqlGeneralOptions srcOpts)
        {
            if (string.IsNullOrWhiteSpace(srcOpts.DatabaseName))
                return null;

            if (string.IsNullOrWhiteSpace(srcOpts.TableName))
                return null;

            var thisServer = await GetConnectedServer(srcOpts, CancellationToken.None);

            if (thisServer == null)
            {
                throw new ConnectionFailureException("Failed to connect to SQL server");
            }

            var urnPath = $"Server[@Name='{srcOpts.ServerName}']/Database[@Name='{srcOpts.DatabaseName}']/Table[@Name='{srcOpts.TableName}' and @Schema='dbo']";
            var urnTable = new Urn(urnPath);

            var thisTable = thisServer.GetSmoObject(urnTable) as Table;

            return thisTable;
        }

        //private readonly SqlGeneralOptions _myOpts;
        //public SqlServerConnectionUtility(SqlGeneralOptions opts)
        //{
        //    _myOpts = opts;
        //}


        public async static Task<Server> GetConnectedSqlServer(SqlGeneralOptions opts)
        {
            try
            {
                await SqlConnectionLocker.WaitAsync();

                var connStr = await SqlStringConnectionBuilder(opts);


                var sqlCon = new SqlConnection(connStr);

                sqlCon.Open();

                var conn = new ServerConnection(sqlCon);
                conn.Connect();

                var server = new Server(conn);

                return server;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "General exception getting sql server connection");
                return null;
            }
            finally
            {
                SqlConnectionLocker?.Release();
            }
        }

        public static SemaphoreSlim SqlConnectionLocker { get; private set; } = new SemaphoreSlim(1, 1);
        public static async Task<string> SqlStringConnectionBuilder(
            SqlGeneralOptions opts,
            bool includeDb = false)
        {


            SqlConnectionStringBuilder connStrBuild;
            try
            {

                if (opts.AuthenticationType == AuthenticationTypes.Integrated)
                {
                    connStrBuild = new SqlConnectionStringBuilder
                    {
                        //InitialCatalog = "dbase",
                        UserID = $@"{Environment.UserDomainName}\{Environment.UserName}",
                        Password = "",
                        DataSource = opts.ServerName,
                        IntegratedSecurity = true,
                        MaxPoolSize = 10

                    };
                }
                else
                {
                    connStrBuild = new SqlConnectionStringBuilder
                    {
                        //InitialCatalog = "dbase",
                        UserID = opts.Username,
                        Password = opts.Password,
                        DataSource = opts.ServerName,
                        IntegratedSecurity = false,
                        MaxPoolSize = 10
                    };

                }

                connStrBuild.ConnectTimeout = 60;

                if (includeDb)
                {
                    connStrBuild.InitialCatalog = opts.DatabaseName;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Exception getting SQL connection string");
                connStrBuild = new SqlConnectionStringBuilder();
            }

            return connStrBuild.ToString();
        }

        //returns list of tuples. item 1 is a database object, item 2 is a string message indicating there was an error 
        //accessing a table.

        private static readonly SemaphoreSlim LockGetDatabases = new SemaphoreSlim(1, 1);
        private static readonly Dictionary<Server, DateTime> CacheLastDbRefresh = new Dictionary<Server, DateTime>();
        public static async Task<BindingList<Tuple<Database, string>>> GetDatabases(Server currentSqlServer, bool forceRefresh = false)
        {
            try
            {
                await LockGetDatabases.WaitAsync();
        
                Console.WriteLine("GetDb");

                var dbResults = new BindingList<Tuple<Database, string>>();

                if (currentSqlServer != null && CacheLastDbRefresh.TryGetValue(currentSqlServer, out var lastRefresh) &&
                    (DateTime.Now - lastRefresh).TotalMinutes > 5)
                {
                    forceRefresh = true;
                }



                if (currentSqlServer != null)
                {
                    CacheLastDbRefresh[currentSqlServer] = DateTime.Now;
                    var availableDbCollection = await Task.Run(() => currentSqlServer.Databases.GetAvailableDatabases());

                    foreach (var db in availableDbCollection.Values)
                    {
                        var newDb = new Database();
                        var errorMessage = "";
                        try
                        {
                            errorMessage = "";

                            newDb.Name = db.Name;
                            newDb = db;
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex, $"General exception reading db");
                            errorMessage = "Permission Denied";
                        }

                        dbResults.Add(new Tuple<Database, string>(newDb, errorMessage));
                    }
                }

                return dbResults;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                //if (ex.InnerException != null)
                //{
                //    MessageBox.Show(ex.InnerException.Message);
                //}

                Log.Error(ex, "General exception getting databases");

                throw ex;
            }
            finally
            {
                LockGetDatabases.Release();
            }
        }

        //public BindingList<Column> GetAvailableFieldIdList(SqlGeneralOptions dataModel, List<Database> dbList)
        //{
        //    var fieldIdList = new BindingList<Column>();
        //    if (dbList != null)
        //    {
        //        foreach (var sqlDb in dbList)
        //        {
        //            if (sqlDb != null && sqlDb.Name == dataModel.DatabaseName)
        //            {
        //                foreach (Table sqlTable in sqlDb.Tables)
        //                {
        //                    if (sqlTable.Name == dataModel.TableName)
        //                    {
        //                        var tempBinding = new BindingList<Column>();
        //                        var tempCollection = sqlTable.Columns;
        //                        for (var i = 0; i < tempCollection.Count; i++)
        //                        {
        //                            tempBinding.Add(tempCollection[i]);
        //                        }
        //                        return tempBinding;
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    return fieldIdList;
        //}
    }
}
