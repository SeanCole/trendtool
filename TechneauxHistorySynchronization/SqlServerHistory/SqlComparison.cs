﻿using System;
using System.Collections.Generic;
using System.Linq;
using CygNetRuleModel.Resolvers;
using TechneauxUtility;

namespace TechneauxHistorySynchronization.SqlServerHistory
{
    public static class SqlComparison
    {
        public static List<MappedSqlRowCompare> Compare(
            string timeColumnKey, 
            List<CygNetHistoryRow> cyg, 
            List<SqlHistoryRow> sql)
        {           
            MappedSqlRowCompare comparisonRow;
            var comparedRows = new List<MappedSqlRowCompare>();
       
            var sqlDict = sql
                .Where(row => DateTime.TryParse(row.CellValues[timeColumnKey].StringValue, out DateTime res))
                .ToDictionary(row => Convert.ToDateTime(row.CellValues[timeColumnKey]), row => row);

            var cygDict = cyg
                .Where(row => DateTime.TryParse(row.CellValues[timeColumnKey].StringValue, out DateTime res))
                .ToDictionary(row => Convert.ToDateTime(row.CellValues[timeColumnKey]), row => row);

            var sqlKeys = new HashSet<DateTime>(sqlDict.Keys);
            var cygKeys = new HashSet<DateTime>(cygDict.Keys);

            var sqlOnlyKeys = sqlKeys.Except(cygKeys).ToList();
            var cygOnlyKeys = cygKeys.Except(sqlKeys).ToList();
            var commonKeys = sqlKeys.Intersect(cygKeys).ToList();
            
            // Start with timestamp compare
            comparedRows.AddRange(sqlOnlyKeys.Select(key => 
                new MappedSqlRowCompare(sqlDict[key], 
                    MappedSqlRowCompare.CygSqlCompare.SqlOnly)));

            comparedRows.AddRange(cygOnlyKeys.Select(key =>
                new MappedSqlRowCompare(cygDict[key], 
                    MappedSqlRowCompare.CygSqlCompare.CygOnly)));
            
            foreach (var key in commonKeys)
            {
                var sqlRow = sqlDict[key];
                var cygRow = cygDict[key];
                
                if (CompareSingleRow(sqlRow, cygRow))
                {
                    comparisonRow = new MappedSqlRowCompare(cygRow, 
                        MappedSqlRowCompare.CygSqlCompare.Equal);

                    comparedRows.Add(comparisonRow);
                }
                else
                {
                    comparedRows.Add(new MappedSqlRowCompare(sqlRow, MappedSqlRowCompare.CygSqlCompare.SqlOnly));
                    comparedRows.Add(new MappedSqlRowCompare(cygRow, MappedSqlRowCompare.CygSqlCompare.CygOnly));
                }
            }

            comparedRows = comparedRows.OrderBy(obj => Convert.ToDateTime(obj.CellValues[timeColumnKey])).ToList();

            return comparedRows;
            


            //foreach (var cygRow in cyg)
            //{
            //    foreach (var sqlRow in sql)
            //    {
            //        if (CompareSingleRow(sqlRow, cygRow))
            //        {

            //            comparisonRow = new MappedSqlRowCompare(cygRow)
            //            {
            //                ComparisonValue = MappedSqlRowCompare.CygSqlCompare.Equal
            //            };
            //            //sql.Remove(sql[j]);
            //            sqlOnlyRows.Add(sqlRow);
            //            cygOnlyRows.Add(cygRow);
            //            equalRows.Add(comparisonRow);

            //            break;
            //        }
            //        if (cygRow.CellValues["TimeStamp"].StringValue == "1/3/2018 4:10:17 PM" || cygRow.CellValues["TimeStamp"].StringValue == "1/3/2018 4:10:18 PM") 
            //            Console.WriteLine("");
            //    }
            //}
            ////remove cyg only's when double
            //foreach (var garb in sqlOnlyRows)
            //{
            //    sql.Remove(garb);
            //}
            //foreach (var garb in cygOnlyRows)
            //{
            //    cyg.Remove(garb);
            //}

            //// add remaining cyg only and sql only
            //foreach (var cygRow in cyg)
            //{
            //    comparisonRow = new MappedSqlRowCompare(cygRow)
            //    {
            //        ComparisonValue = MappedSqlRowCompare.CygSqlCompare.CygOnly
            //    };
            //    equalRows.Add(comparisonRow);
            //}
            //foreach (var sqlRow in sql)
            //{
            //    comparisonRow = new MappedSqlRowCompare(sqlRow)
            //    {
            //        ComparisonValue = MappedSqlRowCompare.CygSqlCompare.SqlOnly
            //    };
            //    equalRows.Add(comparisonRow);
            //}
            ////string time = obj.CellValuies[TimeColumn].StringValue;
            ////foreache()

            ////ComparisonList = ComparisonList.OrderBy(obj => Convert.ToDateTime(obj.CellValues[TimeColumn].StringValue)).ToList();
            //return equalRows;
        }
    

        private static bool CompareSingleRow(SqlHistoryRow sqlRow, CygNetHistoryRow cygRow)
        {
            return sqlRow.CellValues.IsEqualTo(cygRow.CellValues);

            foreach (var sqlKey in sqlRow.CellValues.Keys)
            {
                if(cygRow.CellValues.TryGetValue(sqlKey, out var val))
                {
                    if(sqlRow.CellValues[sqlKey].StringValue.Trim() != val.StringValue.Trim())
                    {
                    

                            
                        return false;
                    }
                }
            }
  
            return true;
        }
    }
}
