﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.SqlServer.Management.Smo;
using Techneaux.CygNetWrapper;
using TechneauxReportingDataModel.SqlHistory.SubOptions;
using TechneauxUtility;

namespace TechneauxHistorySynchronization.Models
{
    public class SimpleSqlTableSchema
    {
        public bool IsEmptySqlSchema { get; private set; }

        public SqlGeneralOptions SrcSqlGeneralOptions { get; }

        // ---- SQL Columns
        private List<Column> _sqlColumns;
        public List<Column> SqlColumns
        {
            get => _sqlColumns;
            set
            {
                _sqlColumns = value ?? throw new ArgumentNullException(nameof(value), "Sql Columns must not be null");

                IsEmptySqlSchema = !value.IsAny();

                TableColumns = new Dictionary<string, SimpleSqlColumnInfo>();
                
                // build list of simplecolinfo
                foreach (var col in SqlColumns)
                {
                    bool dateTimeValid = col.DataType.SqlDataType == SqlDataType.DateTime;

                    var newCol = new SimpleSqlColumnInfo
                    {
                        Name = col.Name,
                        IsPrimaryKey = col.InPrimaryKey,
                        IsNullable = col.Nullable,
                        IsDateTime = dateTimeValid,
                        DataType = col.DataType
                    };

                    TableColumns.Add(col.Name, newCol);
                }
            }
        }

        public Dictionary<string, SimpleSqlColumnInfo> TableColumns { get; private set; }

        public IEnumerable<string> SqlColumnNames() => TableColumns
            .Select(col => col.Key);

        public IEnumerable<SimpleSqlColumnInfo> PriKeyCols => TableColumns
            .Values
            .Where(col => col.IsPrimaryKey);
        
        public IEnumerable<string> PriKeyColNames => PriKeyCols
            .Select(col => col.Name);

        public IEnumerable<SimpleSqlColumnInfo> NonPriKeyCols => TableColumns
            .Values
            .Where(col => !col.IsPrimaryKey);

        public IEnumerable<SimpleSqlColumnInfo> NullableColumns => TableColumns
            .Values
            .Where(col => col.IsNullable);

        public IEnumerable<SimpleSqlColumnInfo> NonNullableColumns => TableColumns 
            .Values
            .Where(col => !col.IsNullable);        
        
        // ---- Constructor
        public SimpleSqlTableSchema(SqlGeneralOptions srcSqlOpts, IEnumerable<Column> sqlColumns)
        {
            SrcSqlGeneralOptions = srcSqlOpts;
            SqlColumns = sqlColumns.ToList();   
        }

        public static SimpleSqlTableSchema Empty()
        {
            return new SimpleSqlTableSchema();
        }

        private SimpleSqlTableSchema()
        {
            SqlColumns = new List<Column>();
        }
    }
}





