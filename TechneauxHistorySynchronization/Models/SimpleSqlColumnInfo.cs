﻿using Microsoft.SqlServer.Management.Smo;

namespace TechneauxHistorySynchronization.Models
{
    public class SimpleSqlColumnInfo
    {
        public string Name { get; set; }
        public bool IsPrimaryKey { get; set; }
        public bool IsNullable { get; set; }

        public bool IsDateTime { get; set; }

        public DataType DataType { get; set; } 

        public override bool Equals(object obj)
        {
            if(obj != null && obj is SimpleSqlColumnInfo)
            {
                var compObj = obj as SimpleSqlColumnInfo;

                return Name == compObj.Name;
            }
            return false;
        }

        public override string ToString()
        {
            return Name;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}




