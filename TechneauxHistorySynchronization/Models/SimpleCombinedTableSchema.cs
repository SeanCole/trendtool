﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CygNetRuleModel.Resolvers;
using Microsoft.SqlServer.Management.Smo;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using TechneauxReportingDataModel.SqlHistory.SubOptions;
using TechneauxUtility;

namespace TechneauxHistorySynchronization.Models
{
    public class SimpleCombinedTableSchema : SimpleSqlTableSchema
    {
        public IEnumerable<SimpleSqlColumnInfo> FixedValuePrimaryKeyColumns => TableColumns
           .Values
           .Where(col => FixedValueCygNetColumns.ContainsKey(col.Name) && PriKeyCols.Contains(col));

        // Checks
        public bool ArePrimaryKeysMatchedInCygNet { get; private set; }

        public bool HasTimeColumn => TimeColumn != null;


        public IEnumerable<SimpleSqlColumnInfo> MatchedCols => TableColumns
            .Where(col => CygNetColumnNames.Contains(col.Key))
            .Select(col => col.Value);

        public IEnumerable<SimpleSqlColumnInfo> MatchedColsNonFixedPrimaryKeys => MatchedCols
            .Where(col => (!FixedValuePrimaryKeyColumns.Contains(col))/* && PriKeyCols.Contains(col)*/);

        public IEnumerable<Tuple<Type, string>> ColumnClrTypeMapping => SqlTableValueMapping.GetValueTypes(SqlColumns, CygNetMappingRules);

        // ---- CygNet Columns
        private List<SqlTableMapping> _cygNetMappingRules = new List<SqlTableMapping>();
        public List<SqlTableMapping> CygNetMappingRules
        {
            get => _cygNetMappingRules;
            set
            {
                _cygNetMappingRules = value ?? throw new ArgumentNullException(nameof(CygNetMappingRules), "CygNet columns list must not be null");
                
                FixedValueCygNetColumns = value
                    .Where(elm => !string.IsNullOrEmpty(elm.SqlTableFieldName))
                    .Where(elm => elm.CygNetElementRule.DataElement == CygNetRuleBase.DataElements.PointAttribute ||
                       elm.CygNetElementRule.DataElement == CygNetRuleBase.DataElements.FacilityAttribute)
                       .ToDictionary(elm => elm.SqlTableFieldName, elm => elm);
            }
        }

        public IEnumerable<string> CygNetColumnNames => CygNetMappingRules
            .Select(col => col.SqlTableFieldName);

        public Dictionary<string, SqlTableMapping> FixedValueCygNetColumns { get; private set; }

        public async Task<Dictionary<string, IConvertibleValue>> GetUniquePrimaryKeysAndValues(ICachedCygNetPoint srcPoint, CancellationToken ct)
        {
            var rulesToPass = FixedValueCygNetColumns
                .Where(item => PriKeyColNames.Contains(item.Key))
                .ToDictionary(item => item.Key, item => item.Value.CygNetElementRule);

            var vals = await ResolveCygNetHistoryRows.ResolveSingleRow(rulesToPass, srcPoint, new CygNetHistoryEntry(), ct);

            return vals.CellValues;
        }

        public async Task<Dictionary<string, IConvertibleValue>> GetFixedColumnsAndValues(ICachedCygNetPoint srcPoint, CancellationToken ct)
        {
            var rulesToPass = FixedValueCygNetColumns.ToDictionary(item => item.Key, item => item.Value.CygNetElementRule);

            var vals = await ResolveCygNetHistoryRows.ResolveSingleRow(rulesToPass, srcPoint, new CygNetHistoryEntry(), ct);

            return vals.CellValues;
        }

        public bool TimeColumnIsValid
        {
            get
            {
                if (!HasTimeColumn)
                    return false;

                if(TableColumns[TimeColumn.SqlTableFieldName].IsDateTime 
                    && TimeColumn.CygNetElementRule.DataElement == CygNetRuleBase.DataElements.PointHistory
                    && TimeColumn.CygNetElementRule.PointHistoryOptions.PointValueType == TechneauxReportingDataModel.CygNet.FacilityPointOptions.PointHistoryGeneralOptions.PointValueTypesNew.Timestamp)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public SqlTableMapping TimeColumn => CygNetMappingRules.FirstOrDefault(elm => elm.TimeColumn);

        // ---- Constructor
        public SimpleCombinedTableSchema(
            SqlGeneralOptions srcSqlGeneralOptions, 
            IEnumerable<Column> sqlColumns, 
            IEnumerable<SqlTableMapping> reportElms) : base(srcSqlGeneralOptions, sqlColumns)
        {
            CygNetMappingRules = reportElms.ToList();
        }

        public SimpleCombinedTableSchema(
            SqlGeneralOptions srcSqlGeneralOptions, 
            IEnumerable<Column> sqlColumns) : base(srcSqlGeneralOptions, sqlColumns)
        {
            CygNetMappingRules = new List<SqlTableMapping>();
        }

        public new static SimpleCombinedTableSchema Empty()
        {
            return new SimpleCombinedTableSchema();
        }

        private SimpleCombinedTableSchema() : base(new SqlGeneralOptions(), new List<Column>())
        {
            CygNetMappingRules = new List<SqlTableMapping>();
        }
    }
}




