﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CygNetRuleModel.Resolvers;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxReportingDataModel.Helper;
using TechneauxReportingDataModel.SqlHistory.Enumerable;

namespace TechneauxHistorySynchronization.Models
{
    public class CygNetPointResolver
    {
        public static async Task<(BindingList<CachedCygNetPointWithRule> Points, List<FacilityUdcResult> UdcResults)> GetPointsAsync(
           List<SqlPointSelection> fullPntRules,
           List<CachedCygNetFacility> cachedFacs,
           List<ChildFacilityGroup> childGroups,
           CancellationToken ct)
        {
            try
            {


                var cts = new CancellationTokenSource();

                var pointList = new BindingList<CachedCygNetPointWithRule>();

                var facUdcResults = new Dictionary<ICachedCygNetFacility, FacilityUdcResult>();

                if (cachedFacs != null)
                {
                    foreach (var fac in cachedFacs)
                    {
                        foreach (var rule in fullPntRules)
                        {
                            if (!rule.PointSelection.IsRuleValid)
                            {
                                continue;
                                throw new ArgumentException("Invalid point selection rule");
                            }

                            var (facility, error) = CygNetResolver.ResolveChildFacility(rule.PointSelection, fac, childGroups);
                            if (error == CygNetResolver.ResolverErrorConditions.None)
                            {
                                if (!facUdcResults.TryGetValue(facility, out var thisFacResult))
                                {
                                    thisFacResult = new FacilityUdcResult(facility, new List<string>());
                                    facUdcResults[facility] = thisFacResult;
                                }

                                var point = (await facility.GetPoint(rule.PointSelection.UDC, cts.Token));
                                if (point != null)
                                {
                                    pointList.Add(new CachedCygNetPointWithRule(point, rule));
                                    thisFacResult.AddUdc(rule.PointSelection.UDC);
                                }
                            }
                            else
                            {
                                //throw new InvalidOperationException(error.GetDescription());
                            }
                        }
                    }
                }

                return (pointList, facUdcResults.Values.ToList());
            }
            catch (Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show(
                //  $@"Failed to get point list with: {ex.Message} {Environment.NewLine}Operation Failed");

                //if (ex.InnerException != null)
                //    System.Windows.Forms.MessageBox.Show($@"Inner exception = {ex.InnerException.Message}");

                var pointList = new BindingList<CachedCygNetPointWithRule>();
                var facUdcResults = new List<FacilityUdcResult>();

                return (pointList, facUdcResults);
            }
        }

        public static async Task<(BindingList<CachedCygNetPointWithRule> Points, List<FacilityUdcResult> UdcResults)> GetPointsAsync(
   List<PointHistorySelectionRule> fullPntRules,
   List<CachedCygNetFacility> cachedFacs,
   List<ChildFacilityGroup> childGroups,
   CancellationToken ct)
        {
            try
            {


                var cts = new CancellationTokenSource();

                var pointList = new BindingList<CachedCygNetPointWithRule>();

                var facUdcResults = new Dictionary<ICachedCygNetFacility, FacilityUdcResult>();

                if (cachedFacs != null)
                {
                    foreach (var fac in cachedFacs)
                    {
                        foreach (var rule in fullPntRules)
                        {
                            if (!rule.PointSelection.IsRuleValid)
                            {
                                continue;
                                throw new ArgumentException("Invalid point selection rule");
                            }

                            var (facility, error) = CygNetResolver.ResolveChildFacility(rule.PointSelection, fac, childGroups);
                            if (error == CygNetResolver.ResolverErrorConditions.None)
                            {
                                if (!facUdcResults.TryGetValue(facility, out var thisFacResult))
                                {
                                    thisFacResult = new FacilityUdcResult(facility, new List<string>());
                                    facUdcResults[facility] = thisFacResult;
                                }

                                var point = (await facility.GetPoint(rule.PointSelection.UDC, cts.Token));
                                if (point != null)
                                {
                                    pointList.Add(new CachedCygNetPointWithRule(point, rule));
                                    thisFacResult.AddUdc(rule.PointSelection.UDC);
                                }
                            }
                            else
                            {
                                //throw new InvalidOperationException(error.GetDescription());
                            }
                        }
                    }
                }

                return (pointList, facUdcResults.Values.ToList());
            }
            catch (Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show(
                //  $@"Failed to get point list with: {ex.Message} {Environment.NewLine}Operation Failed");

                //if (ex.InnerException != null)
                //    System.Windows.Forms.MessageBox.Show($@"Inner exception = {ex.InnerException.Message}");

                var pointList = new BindingList<CachedCygNetPointWithRule>();
                var facUdcResults = new List<FacilityUdcResult>();

                return (pointList, facUdcResults);
            }
        }

    }
}
