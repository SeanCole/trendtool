﻿using System.Collections.Generic;
using TechneauxUtility;

namespace TechneauxHistorySynchronization
{
    public class SqlHistoryRow : IValueSet
    {

        public List<string> FixedValueColumnNames { get; set; }
                
        public Dictionary<string, IConvertibleValue> CellValues { get; set; } = new Dictionary<string, IConvertibleValue>();
    }
}
