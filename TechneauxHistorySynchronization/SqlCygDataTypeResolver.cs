﻿using System;
using System.Collections.Generic;
using Microsoft.SqlServer.Management.Smo;
using Techneaux.CygNetWrapper;

namespace TechneauxHistorySynchronization
{
    public static class SqlCygDataTypeResolver
    {
        //resolves a tuple of .nettypes and column names into a string for a sql deletion with right datatypes.
        public static string ResolveCygDataTypeDelete(CygNetConvertableValue columnValue, Tuple<Type, string> clrDataTypeMapping)
        {
            string values;
            double tempDouble;
            int tempInt;
            string tempString;

            //resolve data types

            if (clrDataTypeMapping.Item1 == typeof(Int64))
            {
                if (columnValue.TryGetDouble(out tempDouble))
                {
                    values = clrDataTypeMapping.Item2 + " = " + tempDouble;                               
                }
                else
                {
                    return null;
                }
            }
            else if (clrDataTypeMapping.Item1 == typeof(Double))
            {
                if (columnValue.TryGetDouble(out tempDouble))
                {
                    values = clrDataTypeMapping.Item2 + " = " + tempDouble;            
                    
                }
                else
                {
                    return null;
                }
            }
            else if (clrDataTypeMapping.Item1 == typeof(Decimal))
            {
                if (columnValue.TryGetDouble(out tempDouble))
                {
                    values = clrDataTypeMapping.Item2 + " = " + tempDouble;
                }
                else
                {
                    return null;
                }
            }
            else if (clrDataTypeMapping.Item1 == typeof(Int16))
            {
                if (columnValue.TryGetInt(out tempInt))
                {
                    values = clrDataTypeMapping.Item2 + " = " + tempInt;
                }
                else
                {
                    return null;
                }
            }
            else if (clrDataTypeMapping.Item1 == typeof(Int32))
            {
                if (columnValue.TryGetInt(out tempInt))
                {
                    values = clrDataTypeMapping.Item2 + " = " + tempInt;
                }
                else
                {
                    return null;
                }
            }
            else if (clrDataTypeMapping.Item1 == typeof(String))
            {
                tempString = "\'" + columnValue.StringValue + "\'";
                values = clrDataTypeMapping.Item2 + " = " + tempString;

            }
            else if (clrDataTypeMapping.Item1 == typeof(Byte))
            {
                if (columnValue.TryGetInt(out tempInt))
                {
                    var tempByte = Convert.ToByte(tempInt);
                    values = clrDataTypeMapping.Item2 + " = " + tempByte;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                tempString = "\'" + columnValue.StringValue + "\'";
                values = clrDataTypeMapping.Item2 + " = " + tempString;
            }

            return values;
        }


        //resolves tuple of .net types and column names into a string for sql insertion with right data types.
        public static string ResolveCygDataTypeInsert(List<CygNetConvertableValue> columnsValues, List<Tuple<Type, string>> clrDataTypeList)
        {
            var values = "";

            //resolve data types
            for (var i = 0; i < clrDataTypeList.Count; i++)
            {
                double tempDouble;
                if (clrDataTypeList[i].Item1 == typeof(Int64))
                {
                    if (columnsValues[i].TryGetDouble(out tempDouble))
                    {
                        if (i != 0)
                        {
                            values = values + ", ";
                            values = values + tempDouble;
                        }
                        else
                        {
                            values = values + tempDouble;

                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else if (clrDataTypeList[i].Item1 == typeof(Double))
                {
                    if (columnsValues[i].TryGetDouble(out tempDouble))
                    {
                        if (i != 0)
                        {
                            values = values + ", ";
                            values = values + tempDouble;
                        }
                        else
                        {
                            values = values + tempDouble;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else if (clrDataTypeList[i].Item1 == typeof(Decimal))
                {
                    if (columnsValues[i].TryGetDouble(out tempDouble))
                    {
                        if (i != 0)
                        {
                            values = values + ", ";
                            values = values + tempDouble;
                        }
                        else
                        {
                            values = values + tempDouble;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    int tempInt;
                    if (clrDataTypeList[i].Item1 == typeof(Int16))
                    {
                        if (columnsValues[i].TryGetInt(out tempInt))
                        {
                            if (i != 0)
                            {
                                values = values + ", ";
                                values = values + tempInt;
                            }
                            else
                            {
                                values = values + tempInt;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else if (clrDataTypeList[i].Item1 == typeof(Int32))
                    {
                        if (columnsValues[i].TryGetInt(out tempInt))
                        {
                            if (i != 0)
                            {
                                values = values + ", ";
                                values = values + tempInt;
                            }
                            else
                            {
                                values = values + tempInt;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        string tempString;
                        if (clrDataTypeList[i].Item1 == typeof(String))
                        {
                            tempString = "\'" + columnsValues[i].StringValue + "\'";
                            if (i != 0)
                            {
                                values = values + ", ";
                                values = values + tempString;
                            }
                            else
                            {
                                values = values + tempString;
                            }
                        }
                        else if (clrDataTypeList[i].Item1 == typeof(Byte))
                        {
                            if (columnsValues[i].TryGetInt(out tempInt))
                            {
                                byte tempByte;
                                if (i != 0)
                                {
                                    values = values + ", ";
                                    tempByte = Convert.ToByte(tempInt);
                                    values = values + tempByte;
                                }
                                else
                                {
                                    tempByte = Convert.ToByte(tempInt);
                                    values = values + tempByte;
                                }
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else if (clrDataTypeList[i].Item1 == typeof(DateTime))
                        {
                            if (columnsValues[i].TryGetDateTime(out var tempDateTime))
                            {
                                tempString = "\'" + tempDateTime + "\'";
                                if (i != 0)
                                {
                                    values = values + ", ";
                                    values = values + tempString;
                                    var temp = columnsValues[i].StringValue;
                                }
                                else
                                {
                                    values = values + tempString;
                                }

                            }
                            else
                            {
                                return null;
                            }
                        }
                    }
                }
            }
            return values;
        }

        public static Type DeriveClrType(Column col)
        {
            var typ = typeof(string);
            var colType = col.DataType.SqlDataType;

            switch (colType)
            {
                case SqlDataType.BigInt:
                    typ = typeof(Int64);
                    break;
                case SqlDataType.Bit:
                    typ = typeof(Boolean);
                    break;
                case SqlDataType.Char:
                case SqlDataType.VarChar:
                case SqlDataType.VarCharMax:

                    typ = typeof(String);
                    break;
                case SqlDataType.DateTime:
                case SqlDataType.DateTime2:
                case SqlDataType.SmallDateTime:
                    typ = typeof(DateTime);
                    break;
                case SqlDataType.Decimal:
                case SqlDataType.Money:
                case SqlDataType.SmallMoney:
                    typ = typeof(Decimal);
                    break;
                case SqlDataType.Int:
                    typ = typeof(Int32);
                    break;
                case SqlDataType.NChar:
                case SqlDataType.NText:
                case SqlDataType.NVarChar:
                case SqlDataType.NVarCharMax:
                case SqlDataType.Text:
                    typ = typeof(String);
                    break;
                case SqlDataType.Real:
                case SqlDataType.Numeric:
                case SqlDataType.Float:
                    typ = typeof(Double);
                    break;
                case SqlDataType.Timestamp:
                case SqlDataType.Binary:
                    typ = typeof(Byte);
                    break;
                case SqlDataType.TinyInt:
                case SqlDataType.SmallInt:
                    typ = typeof(Int16);
                    break;
                case SqlDataType.UniqueIdentifier:
                    typ = typeof(Guid);
                    break;
                case SqlDataType.UserDefinedDataType:
                case SqlDataType.UserDefinedType:
                case SqlDataType.Variant:
                case SqlDataType.Image:
                    typ = typeof(Object);
                    break;
                default:
                    typ = typeof(String);
                    break;
            }
            return typ;
        }
    }
}
