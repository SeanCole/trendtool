﻿using System;
using Techneaux.CygNetWrapper;
using System.Text.RegularExpressions;
using ExpressionRuleModel.Rules;

namespace ExpressionRuleModel.Resolvers
{
    public class StringOpsResolver
    {
        public static CygNetConvertableValue Resolve(CygNetConvertableValue evaluatedResults, StringOperations stringOps)
        {
            try
            {

                //dont resolve if already in error
                if (evaluatedResults.HasError)
                {
                    return evaluatedResults;
                }

                var opString = evaluatedResults.StringValue;
                switch (stringOps.OpsType)
                {
                    //*******************************************
                    //   None
                    //*******************************************
                    case StringOperations.OperationType.None:
                        return evaluatedResults;

                    //*******************************************
                    //   Left
                    //*******************************************
                    case StringOperations.OperationType.Left:
                        if (stringOps.FirstIndexType.Equals(StringOperations.IndexRelation.Fixed))
                        {
                            var length = stringOps.NumCharacters;
                            //To make this ops safe
                            if (length < opString.Length)
                                opString = opString.Substring(0, length);
                            return new CygNetConvertableValue(opString);
                        }
                        else if (stringOps.FirstIndexType.Equals(StringOperations.IndexRelation.LengthDiff))
                        {
                            var length = opString.Length - stringOps.NumCharacters;
                            //To make this ops safe
                            if (length < 0)
                                opString = "";
                            else
                                opString = opString.Substring(0, length);
                            return new CygNetConvertableValue(opString);
                        }
                        else
                        {
                            return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "Uknown FirstIndexType: " + stringOps.FirstIndexType);
                        }

                    //*******************************************
                    //   Mid
                    //*******************************************
                    case StringOperations.OperationType.Mid:
                        if (stringOps.FirstIndexType.Equals(StringOperations.IndexRelation.Fixed))
                        {
                            var numChars = stringOps.NumCharacters;
                            //Make string ops safe
                            if (stringOps.FirstIndex > opString.Length)
                                opString = "";
                            else
                            {
                                if (stringOps.FirstIndex + stringOps.NumCharacters > opString.Length)
                                    numChars = opString.Length - stringOps.FirstIndex;
                                opString = opString.Substring(stringOps.FirstIndex, numChars);
                            }
                            return new CygNetConvertableValue(opString);
                        }
                        else if (stringOps.FirstIndexType.Equals(StringOperations.IndexRelation.LengthDiff))
                        {
                            var index = opString.Length - stringOps.FirstIndex;
                            var numChars = stringOps.NumCharacters;
                            //make stringops safe
                            if (index < 0)
                            {
                                numChars = numChars + index;
                                index = 0;
                            }
                            if (numChars < 0)
                                opString = "";
                            else
                            {
                                if (index + numChars > opString.Length)
                                    numChars = opString.Length - index;
                                opString = opString.Substring(index, numChars);
                            }
                            return new CygNetConvertableValue(opString);
                        }
                        else
                        {
                            return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "Uknown FirstIndexType: " + stringOps.FirstIndexType);
                        }

                    //*******************************************
                    //   Replace
                    //*******************************************
                    case StringOperations.OperationType.Replace:
                        opString = opString.Replace(stringOps.ReplaceMatch, stringOps.ReplaceWith);
                        return new CygNetConvertableValue(opString);

                    //*******************************************
                    //   Right
                    //*******************************************
                    case StringOperations.OperationType.Right:
                        if (stringOps.FirstIndexType.Equals(StringOperations.IndexRelation.Fixed))
                        {
                            var firstIndex = opString.Length - stringOps.NumCharacters;
                            var length = stringOps.NumCharacters;
                            //make stringops safe
                            if (firstIndex > 0)
                                opString = opString.Substring(firstIndex, length);
                            return new CygNetConvertableValue(opString);
                        }
                        else if (stringOps.FirstIndexType.Equals(StringOperations.IndexRelation.LengthDiff))
                        {
                            var firstIndex = opString.Length - (opString.Length - stringOps.NumCharacters); //which = stringOps.NumCharacters
                            //make stringops safe
                            if (firstIndex > opString.Length)
                                opString = "";
                            else
                                opString = opString.Substring(firstIndex, opString.Length - stringOps.NumCharacters);
                            return new CygNetConvertableValue(opString);
                        }
                        else
                        {
                            return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "Uknown FirstIndexType: " + stringOps.FirstIndexType);
                        }

                    //*******************************************
                    //   Split
                    //*******************************************
                    case StringOperations.OperationType.Split:
                        var splitIndex = stringOps.FirstIndex;
                        var splitChar = stringOps.SplitChar;

                        if (splitChar == null || splitChar.Equals(""))
                        {
                            splitChar = "";
                        }
                        var opStringSplits = opString.Split(splitChar.ToCharArray());
                        if (stringOps.FirstIndexType.Equals(StringOperations.IndexRelation.Fixed))
                        {
                            if (opStringSplits.Length > 0)
                            {
                                if (opStringSplits.Length > splitIndex)
                                {
                                    //Special case, if index is less than 0, treat it like index is 0
                                    if (splitIndex < 0)
                                    {
                                        splitIndex = 0;
                                    }
                                    return new CygNetConvertableValue(opStringSplits[splitIndex]);
                                }
                                else
                                {
                                    return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "Split index: "+ splitIndex + " out of range: " + opStringSplits.Length);
                                }
                            }
                            else
                            {
                                return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "No result form split of: " + opString + " by: " + splitChar);
                            }
                        }
                        else if (stringOps.FirstIndexType.Equals(StringOperations.IndexRelation.LengthDiff))
                        {
                            //Get index based on length
                            splitIndex = opStringSplits.Length - splitIndex;
                            
                            //Special case, if index is less than 0, treat it like index is 0
                            if (splitIndex < 0)
                            {
                                splitIndex = 0;
                            }
                            return new CygNetConvertableValue(opStringSplits[splitIndex]);
                        }
                        else
                        {
                            return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "Uknown FirstIndexType: " + stringOps.FirstIndexType);
                        }

                    case StringOperations.OperationType.Regex:
                        Regex regex;

                        try
                        {
                            regex = new Regex(stringOps.ReplaceMatch);
                        }
                        catch (ArgumentException ex)
                        {
                            return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "Regular expression could not compile: " + ex.Message);
                        }

                        var regexIndex = stringOps.FirstIndex;

                        var match = regex.Match(opString);

                        string matchStr = null;

                        while (match.Success)
                        {
                            if (match.Index == regexIndex)
                            {
                                matchStr = match.Value;
                                break;
                            }
                            match = match.NextMatch();
                        }

                        if (matchStr != null)
                        {
                            return new CygNetConvertableValue(match);
                        }
                        else
                        {
                            if (stringOps.DefaultIfNoMatch != null &&  !stringOps.DefaultIfNoMatch.Equals(""))
                            {
                                return new CygNetConvertableValue(stringOps.DefaultIfNoMatch);
                            }
                            else
                            {
                                return new CygNetConvertableValue("Could not find match with regex");
                            }
                        }


                    default:
                        return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "Uknown OpsType: " + stringOps.OpsType);
                }
            }
            catch (Exception ex)
            {
                return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.UnknownErr, "Uknown exception thrown[BF]: " + ex.Message);
            }
        }
    }
}
