﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using CygNetRuleModel.Resolvers;
using Techneaux.CygNetWrapper;
using ExpressionRuleModel.Rules;
using GenericRuleModel.Resolvers;
using GenericRuleModel.Rules;
using TechneauxReportingDataModel.CygNet.Rules;

namespace ExpressionRuleModel.Resolvers
{
    public class CygNetExpressionElementResolver
    {
        internal static async Task<CygNetConvertableValue> Resolve(
            CygNetExpressionElement expElem,
            Dictionary<string, CygNetExpressionElement> expElemMap,
            ICollection<EnumerationTable> GlobalEnums,
            ICygNetPointReference tagRule, 
            CancellationToken token,
            bool testMode,
            List<string> antiLoopVisitorList = null)
        {
            try
            {
                //*******************************************
                //   ExpType
                //*******************************************
                switch (expElem.ExpType)
                {
                    //*******************************************
                    //   CygNetRule
                    //*******************************************
                    case CygNetExpressionElement.CygNetExpressionType.CygNetRule:
                        switch (expElem.CygRule.RuleType)
                        {
                            //*******************************************
                            //   CygNetElement
                            //*******************************************
                            case CygNetExpressionRule.CygNetExpRuleType.CygNetElement:
                                if (expElem.CygRule.CygRule is CygNetRollupRuleBase cygElem)
                                {

                                    //Resolve it
                                    var resVal = new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "Unresolved");

                                    if (!testMode)
                                        throw new NotImplementedException();
                                        //resVal = await ResolveCygNet(cygElem, token);
                                    else
                                        resVal = new CygNetConvertableValue(expElem.TestValue);

                                    //Check for error states and use defaults if necessary
                                    if (resVal.HasError)
                                    {
                                        if (cygElem.DefaultIfError != null && cygElem.DefaultIfError != "")
                                        {
                                            resVal = FormattingResolver.Resolve(new CygNetConvertableValue(cygElem.DefaultIfError), cygElem.FormatConfig);
                                        }
                                    }
                                    else
                                    {
                                        // apply formatting
                                        resVal = FormattingResolver.Resolve(resVal, cygElem.FormatConfig);
                                    }

                                    //Apply StringOps
                                    resVal = StringOpsResolver.Resolve(resVal, expElem.StringOps);

                                    return resVal;
                                }

                                //*******************************************
                                //   CygShortNetElement
                                //*******************************************
                                else if (expElem.CygRule.CygRule is CygNetRuleBase cygShortElem)
                                {

                                    //Resolve it
                                    CygNetConvertableValue resShortVal;
                                    if (!testMode)
                                    {
                                        //Make sure we have the point link rule to complete
                                        if (tagRule == null)
                                        {
                                            return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "Point tag rule was not defined.");
                                        }

                                        throw new NotImplementedException();
                                        //resShortVal = await ResolveCygNet(tagRule, cygShortElem, token);
                                    }
                                    else
                                        resShortVal = new CygNetConvertableValue(expElem.TestValue);

                                    //run formatter
                                    resShortVal = FormattingResolver.Resolve(resShortVal, cygShortElem.FormatConfig);

                                    //Apply StringOps
                                    resShortVal = StringOpsResolver.Resolve(resShortVal, expElem.StringOps);

                                    return resShortVal;
                                }
                                else
                                {
                                    return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "Uknown Rule type, was expecting CygNetElement or CygNetRuleBase but got: " + expElem.CygRule.RuleType);
                                }

                            //*******************************************
                            //   Random Number
                            //*******************************************
                            case CygNetExpressionRule.CygNetExpRuleType.RandomNumber:
                                var randomNumElem = (RandomNumber)expElem.CygRule.CygRule;
                                var minMaxValue = new IntMinMax(randomNumElem.MinValue, randomNumElem.MaxValue);
                                var bucketValue = RandomNumberGenerator.Instance.Next(minMaxValue);

                                //apply string ops
                                var resRandVal = new CygNetConvertableValue(bucketValue);
                                resRandVal = StringOpsResolver.Resolve(resRandVal, expElem.StringOps);
                                return resRandVal;

                            //*******************************************
                            //   Default
                            //*******************************************
                            default:
                                return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "Uknown Rule Type: " + expElem.ExpType);
                        }


                    //*******************************************
                    //   Reference
                    //*******************************************
                    case CygNetExpressionElement.CygNetExpressionType.Reference:
                        //this is to prevent loops
                        if (antiLoopVisitorList == null)
                        {
                            antiLoopVisitorList = new List<string>();
                        }
                        if (antiLoopVisitorList.Contains(expElem.ExpName))
                        {
                            return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "Loop detected with reference: " + expElem.ExpName);
                        }
                        else
                        {
                            antiLoopVisitorList.Add(expElem.ExpName);
                        }

                        //if the reference exists, go resolve it
                        if (expElemMap.ContainsKey(expElem.Reference))
                        {
                            //go resolve reference expression
                            var resVal = await Resolve(expElemMap[expElem.Reference], expElemMap, GlobalEnums, tagRule, token, testMode, antiLoopVisitorList);

                            //run formatter
                            resVal = FormattingResolver.Resolve(resVal, expElem.FormatConfig);

                            //Apply StringOps
                            resVal = StringOpsResolver.Resolve(resVal, expElem.StringOps);

                            return resVal;
                        }
                        //else error out
                        else
                        {
                            return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "Expression reference {" + expElem.Reference + "} not found while resolving: " + expElem.ExpName);
                        }

                    //*******************************************
                    //   Default
                    //*******************************************
                    default:
                        return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.ExpressionErr, "Uknown Expression Ref Type: " + expElem.CygRule.CygRule.GetType());
                }
            }
            catch (Exception ex)
            {
                
                return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.UnknownErr, "Uknown exception thrown[C8]: " + ex.Message);
            }
        }
    }
}
