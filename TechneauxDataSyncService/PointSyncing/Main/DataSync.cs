﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using Serilog;
using Serilog.Core;

namespace TechneauxDataSyncService.Main
{
    public partial class DataSync : ServiceBase
    {
        public DataSync()
        {           

            var LoggingLevelAttr = ConfigurationManager.AppSettings.Get("LogLevelSync");
            Serilog.Events.LogEventLevel newLevel = (Serilog.Events.LogEventLevel)Enum.Parse(typeof(Serilog.Events.LogEventLevel), LoggingLevelAttr);
            var logLevelSwitch = new LoggingLevelSwitch(newLevel);

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(logLevelSwitch)
                .WriteTo
                .RollingFile($@"{AppDomain.CurrentDomain.BaseDirectory}\Logs\General Sync Service Log.txt", shared: true)
                .CreateLogger();
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "General Service init failed");
            }
        }

        NewFileCheckService _fileCheckService;

        protected override async void OnStart(string[] args)
        {
            try
            {
                var thisExePath = Assembly.GetExecutingAssembly().Location;
                var thisConfigPath = $@"{Path.GetDirectoryName(thisExePath)}\ServiceConfigs\";

                _fileCheckService = new NewFileCheckService(thisConfigPath);
                await _fileCheckService.RunNewFileCheck();
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"New File Service init failed");
                _fileCheckService?.CancelAll();
            }
            finally
            {
                
            }
        }

        protected override void OnStop()
        {
            _fileCheckService?.CancelAll();
        }
    }
}
