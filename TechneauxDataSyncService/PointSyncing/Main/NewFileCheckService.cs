﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Serilog;
using TechneauxDataSyncService.ConfigFileSyncing;
using TechneauxReportingDataModel.General;
using XmlDataModelUtility;

namespace TechneauxDataSyncService.Main
{
    public class NewFileCheckService : NotifyModelBase
    {
        [XmlIgnore()]
        public string ThisConfigFilesPath
        {
            get => GetPropertyValue<string>();
            private set => SetPropertyValue(value);
        }
        [XmlIgnore()]
        public SyncFileUtility SelectedConfig
        {
            get => GetPropertyValue<SyncFileUtility>();
            set => SetPropertyValue(value);
        }
        public BindingList<SyncFileUtility> OpenFiles
        {
            get => GetPropertyValue<BindingList<SyncFileUtility>>();
            private set => SetPropertyValue(value);
        }

        public NewFileCheckService() { }

        public bool RunWithTestHarness = false;


        public NewFileCheckService(string pathToCheck, bool usingTestHarness = false)
        {
            RunWithTestHarness = usingTestHarness;
            //if (Directory.Exists(pathToCheck))
            //{
            ThisConfigFilesPath = pathToCheck;
            //}           
       
            OpenFiles = new BindingList<SyncFileUtility>();

            if (RunWithTestHarness)
            {
                Observable.Interval(TimeSpan.FromMinutes(.5))
             .ObserveOnDispatcher()
             .Subscribe(async evt =>
             {
                 await RunNewFileCheck();
             });
            }
            else
            {
                Observable.Interval(TimeSpan.FromMinutes(.5))
                    .Subscribe(async evt =>{
                        await RunNewFileCheck();
                    });
            }
    //        Observable.Interval(TimeSpan.FromMinutes(.5))
    //.Subscribe(async evt => {
    //    await RunNewFileCheck();
    //});


        }

        public CancellationTokenSource Cts = new CancellationTokenSource();

        public void CancelAll()
        {
            Log.Debug("Cancel all.");
            Cts.Cancel();
        }

        private readonly SemaphoreSlim _fileCheckLock = new SemaphoreSlim(1, 1);
        public async Task RunNewFileCheck()
        {
            try
            {
                await _fileCheckLock.WaitAsync();

                if (string.IsNullOrWhiteSpace(ThisConfigFilesPath) || !Directory.Exists(ThisConfigFilesPath))
                {
                    Log.Debug($"Could not find path: {ThisConfigFilesPath}");
                    return;
                }

                List<string> fileList = Directory.GetFiles(ThisConfigFilesPath)
                    .ToList()
                    .Where(strPath => Path.GetFileName(strPath).ToLower().EndsWith(".xml"))
                    .ToList();

                Log.Debug($"Files found:");
                fileList.ForEach(file => Log.Debug($" -- {file}"));

                var existingFilePaths = OpenFiles.Select(fs => fs.FilePath).ToList();

                Log.Debug($"Existing Files:");
                existingFilePaths.ForEach(file => Log.Debug($" -- {file}"));

                var newFiles = fileList.Where(file => !existingFilePaths.Contains(file)).ToList();
                var existingFilesToCheck = existingFilePaths.Where(file => fileList.Contains(file)).ToList();
                var filesToRemove = existingFilePaths.Where(file => !fileList.Contains(file)).ToList();

                var newOrUpdatedFileSyncObjs = new List<SyncFileUtility>();

                foreach (var filePath in newFiles)
                {
                    try
                    {
                        var newModel = XmlFileSerialization<ReportConfigModel>.LoadModelFromFile(filePath);

                        var newFileUtil = new SyncFileUtility(filePath, newModel, File.GetLastWriteTime(filePath), Cts.Token, RunWithTestHarness);

                        OpenFiles.Add(newFileUtil);
                        newFileUtil.UpdatePointList();
                    }
                    catch (Exception ex)
                    {
                        Log.Debug($"Failed to open file path: {filePath}");
                        if (ex.InnerException != null)
                            Log.Debug($"Inner exception = {ex.InnerException.Message}");
                    }
                }

                foreach (var filePath in existingFilesToCheck)
                {
                    var curFileUtility = OpenFiles.First(fs => fs.FilePath == filePath);
                    var thisFileLastMod = File.GetLastWriteTime(filePath);

                    if (thisFileLastMod > curFileUtility.LastModifiedTimestamp)
                    {
                        try
                        {
                            var newModel = XmlFileSerialization<ReportConfigModel>.LoadModelFromFile(filePath);
                            curFileUtility.SrcDataModel = newModel;
                            curFileUtility.LastModifiedTimestamp = File.GetLastWriteTime(filePath);

                            curFileUtility.UpdatePointList();
                        }
                        catch (Exception ex)
                        {
                            Log.Debug($"Failed to load config {filePath}");
                            if (ex.InnerException != null)
                                Log.Debug($"Inner exception = {ex.InnerException.Message}");

                            curFileUtility?.CancelAll();
                            OpenFiles.Remove(curFileUtility);
                        }
                    }
                }

                foreach (var filePath in filesToRemove)
                {
                    var curFileUtility = OpenFiles.First(fs => fs.FilePath == filePath);
                    Log.Debug($"Removing file {filePath}");
                    curFileUtility?.CancelAll();
                    OpenFiles.Remove(curFileUtility);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "General exception during new file check");
            }
            finally
            {
                _fileCheckLock.Release();
            }


        }


    }


}
