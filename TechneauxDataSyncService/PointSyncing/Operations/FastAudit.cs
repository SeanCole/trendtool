﻿using System;
using System.Collections.Generic;
using CygNetRuleModel.Resolvers;
using TechneauxDataSyncService.PointSyncing.Operations.Sub;
using TechneauxHistorySynchronization;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.Helper;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public class FastAudit : TimePeriodAudit
    {
        private const uint FastAuditHours = 6;

        public FastAudit(
            SimpleCombinedTableSchema newSchema,
            CachedCygNetPointWithRule newSrcPoint) 
            : base(newSchema, newSrcPoint, false)
        {
            var ran = new Random();

            var randomFraction = (double)ran.Next(1, 85) / 100;
            
            if (RetentionDays < 1)
                throw new InvalidOperationException("Retention days must be non-zero");

            var fractionalHoursToAdd = RetentionDays * 24 * randomFraction;
            var newEarliest = DateTime.Now.AddDays(-RetentionDays).AddHours(fractionalHoursToAdd);

            if (newEarliest.AddHours(FastAuditHours) > DateTime.Now)
                newEarliest = DateTime.Now.AddHours(-FastAuditHours);

            EarliestDate = newEarliest;

            LatestDate = EarliestDate.AddHours(FastAuditHours);

            Results = new SimpleSqlSyncResults(nameof(FastAudit));
        }
    }
}
