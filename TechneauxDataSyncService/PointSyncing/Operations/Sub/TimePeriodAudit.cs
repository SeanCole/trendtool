﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ReactiveUI;
using Serilog;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing.Operations.Base;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.Helper;
using TechneauxUtility;

namespace TechneauxDataSyncService.PointSyncing.Operations.Sub
{
    public class TimePeriodAudit : TimedOperation, ISqlSyncOperation
    {
        // Constructor
        public TimePeriodAudit(
            SimpleCombinedTableSchema newSchema,
            CachedCygNetPointWithRule newSrcPoint,
            bool newTryUpdateSql)
        {
            if (newSchema == null || newSchema.IsEmptySqlSchema)
                throw new ArgumentNullException(nameof(newSchema), "Schema must not be null or empty");

            if (newSrcPoint == null)
                throw new ArgumentNullException(nameof(newSrcPoint), "Src Point must not be empty");

            if (newSrcPoint.SrcRule == null || !newSrcPoint.SrcRule.IsRuleValid)
                throw new ArgumentException("Src point rule must be non-null and valid");

            TableSchema = newSchema;
            SrcPoint = newSrcPoint;
            TryUpdateSql = newTryUpdateSql;
        }


        public SimpleSqlSyncResults Results { get; set; } 
        public ISqlSyncDetailedResults DetailedHistoryResults { get; } = new SqlSyncDetailedResults();

        // Private vars
        private CygNetSqlDataCompareModel _compareModel;

        // Option vars
        public ILogger LocalLog;

        // Operation vars
        internal double RetentionDays => SrcPoint.SrcRule.PollingOptions.RetentionDays;
        

        private CachedCygNetPointWithRule SrcPoint { get; }
        private SimpleCombinedTableSchema TableSchema { get; }
        private bool TryUpdateSql { get; }

        // Internal variables
        internal DateTime EarliestDate { get; set; }
        internal DateTime LatestDate { get; set; }
        

        public async Task DoOperationAsync(
            IProgress<(int, string)> progress,
            CancellationToken ct)
        {
            Results.OperationInterval = (EarliestDate, LatestDate);

            // Start timing operation
            StartTiming();

            var pointSyncAuditTrailBuilder = new StringBuilder();

            pointSyncAuditTrailBuilder.AppendLine($"Starting sync");

            try
            {
                _compareModel = new CygNetSqlDataCompareModel(SrcPoint);

                // Get CygNet history
                StartSubtask(nameof(_compareModel.GetCygNetHistory));
                //if (EarliestDate == DateTime.MinValue)
                //{
                //    Console.WriteLine("minvalue full");
                //}
                //if (EarliestDate < LatestDate.AddYears(-10))
                //{
                //    Console.WriteLine("toosmall earliest full");
                //}
                var cygHist = await Task.Run(() => _compareModel.GetCygNetHistory(EarliestDate, LatestDate, SrcPoint.SrcRule, null, ct), ct);

                pointSyncAuditTrailBuilder.AppendLine(
                    GetSubTaskDuration(nameof(_compareModel.GetCygNetHistory)).ToString());

                // Check if any history
                if (!cygHist.IsAnyHistory)
                {
                    // Delete all SQL data in analysis period
                    await SqlRowResolver.DeleteAllSqlRowsForPoint(TableSchema, SrcPoint, ct);
                    Results.OpResult = OpStatuses.Fail;
                    Results.StatusMessage = "Failed, no history in retention period";
                    return;
                }

                ct.ThrowIfCancellationRequested();

                pointSyncAuditTrailBuilder.AppendLine(
                    $"History found: {cygHist.RawHistoryEntries.Count} raw entries from {cygHist.RawHistoryEntries.Min(hist => hist.AdjustedTimestamp)} to {cygHist.RawHistoryEntries.Max(hist => hist.AdjustedTimestamp)}");

                Results.NumRawCygNetEntriesFound = cygHist.RawHistoryEntries.Count;
                Results.MostRecentRawHistTimestamp = cygHist.RawHistoryEntries.Max(entry => entry.AdjustedTimestamp);
                
                if (cygHist.NormalizedHistoryEntries.IsAny())
                {
                    var validNormEntries = cygHist.NormalizedHistoryEntries
                        .Where(entry => entry.IsValid && !entry.IsValueUncertain)
                        .ToList();
                   
                    Results.NumNormEntriesFound = cygHist.NormalizedHistoryEntries.Count;
                    Results.NumValidNormEntriesFound = cygHist.NormalizedHistoryEntries.Count(entry => entry.IsValid && !entry.IsValueUncertain);
                    Results.NumCertainNormEntriesFound =
                        validNormEntries.Count;

                    if (validNormEntries.Any())
                    {
                        Results.MostRecentNormTimestamp =
                            validNormEntries.Max(entry => entry.NormalizedHistoryValue.AdjustedTimestamp);

                        pointSyncAuditTrailBuilder.AppendLine(
                            $"Normalized History found: {validNormEntries.Count} entries from {validNormEntries.Min(hist => hist.NormalizedTimestamp)} to {validNormEntries.Max(hist => hist.NormalizedTimestamp)}");
                    }
                    else
                    {
                        Results.MostRecentNormTimestamp = Results.MostRecentRawHistTimestamp;
                    }
                }
                else
                {
                    Results.MostRecentNormTimestamp = Results.MostRecentRawHistTimestamp;
                }

                // populate lists of history, etc
                DetailedHistoryResults.FixedCygNetValues = await TableSchema.GetFixedColumnsAndValues(SrcPoint,ct);
                DetailedHistoryResults.CygNetSrcRawHistory = cygHist.RawHistoryEntries;
                DetailedHistoryResults.CygNetSrcNormalizedHistory = cygHist.NormalizedHistoryEntries;

                // Get SQL data and compare
                StartSubtask(nameof(_compareModel.GetNewSqlComparisonResults));
                var compResults = await Task.Run(() => _compareModel.GetNewSqlComparisonResults(TableSchema, cygHist, ct), ct);
                if (!compResults.IsSuccessful)
                {
                    Results.OpResult = OpStatuses.Exception;
                    Results.FailedException = compResults.CompException;
                }

                Results.NumCygNetRows = compResults.CygNetRows.Count;
                Results.NumTotalSqlRows = compResults.SqlRows.Count;

                DetailedHistoryResults.SqlSrcRows = compResults.SqlRows;
                DetailedHistoryResults.ComparedRows = compResults.ComparedRows;

                pointSyncAuditTrailBuilder.AppendLine(
                    GetSubTaskDuration(nameof(_compareModel.GetNewSqlComparisonResults)).ToString());

                ct.ThrowIfCancellationRequested();

                // Update SQL

                Results.NumMatchingSqlRows = compResults.ComparedRows.Count(row =>
                    row.ComparisonValue == MappedSqlRowCompare.CygSqlCompare.Equal);

                if (TryUpdateSql)
                {
                    await Task.Run(() => _compareModel.UpdateSqlTable(TableSchema, compResults.ComparedRows, ct), ct);

                    Results.NumSqlRowsInserted = compResults.ComparedRows.Count(row =>
                        row.ComparisonValue == MappedSqlRowCompare.CygSqlCompare.CygOnly);

                    Results.NumSqlRowsDeleted = compResults.ComparedRows.Count(row =>
                        row.ComparisonValue == MappedSqlRowCompare.CygSqlCompare.SqlOnly);

                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage = "Finished compare, new data pushed to SQL";
                }
                else
                {
                    Results.OpResult = compResults.IsRowMismatch ? OpStatuses.Fail : OpStatuses.Pass;
                    Results.StatusMessage = "Finished compare, no new data pushed";
                }
            }
            catch (Exception ex)
            {
                Results.FailedException = ex;
                Results.OpResult = OpStatuses.Exception;
                Results.StatusMessage = "General operation failure";
            }
            finally
            {
                Results.LoggingMessages = pointSyncAuditTrailBuilder.ToString();
                Results.TimeTakenPerformingOperation = GetTotalTimeTaken();
            }
        }
    }
}
