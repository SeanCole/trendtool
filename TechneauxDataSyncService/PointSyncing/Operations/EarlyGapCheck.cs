﻿using System.Collections.Generic;
using System;
using System.Threading;
using System.Threading.Tasks;
using CygNetRuleModel.Resolvers;
using Serilog;
using TechneauxHistorySynchronization;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using static TechneauxDataSyncService.PointSyncing.PointSync;
using Techneaux.CygNetWrapper.Points;
using TechneauxUtility;
using System.Linq;
using TechneauxReportingDataModel.SqlHistory;
using CygNetRuleModel.Resolvers.History;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxDataSyncService.PointSyncing.Operations.Base;
using TechneauxReportingDataModel.Helper;
using TechneauxDataSyncService.Helper;
using System.Text;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public class EarlyGapCheck : TimedOperation, ISqlSyncOperation
    {
        public EarlyGapCheck(
            SimpleCombinedTableSchema simpleSchema,
            CachedCygNetPointWithRule srcPnt)
        {
            if (simpleSchema == null || simpleSchema.IsEmptySqlSchema)
                throw new ArgumentNullException(nameof(simpleSchema), "Schema must not be null or empty");

            if (srcPnt == null)
                throw new ArgumentNullException(nameof(srcPnt), "Src Point must not be empty");

            if (srcPnt.SrcRule == null || !srcPnt.SrcRule.IsRuleValid)
                throw new ArgumentException("Src point rule must be non-null and valid");

            Schema = simpleSchema;
            SrcPoint = srcPnt;
        }

        #region LocalVars

        private SimpleCombinedTableSchema Schema { get; }
        private CachedCygNetPointWithRule SrcPoint { get; }
        
        private DateTime EarliestDate { get; set; }
        private DateTime LatestDate { get; set; }

        private string TimeColId => Schema.TimeColumn.SqlTableFieldName;
        private double RetentionDays => SrcPoint.SrcRule.PollingOptions.RetentionDays;
        private bool IsHistoryNormalized => SrcPoint.SrcRule.HistoryNormalizationOptions.EnableNormalization;
        private double NormIntervalHours => (double) SrcPoint.SrcRule.HistoryNormalizationOptions.NormalizeWindowIntervalLength / 
            SrcPoint.SrcRule.HistoryNormalizationOptions.NumWindowHistEntries;

        #endregion

        #region Results

        public SimpleSqlSyncResults Results { get; } = new SimpleSqlSyncResults(nameof(EarlyGapCheck));
        public ISqlSyncDetailedResults DetailedHistoryResults { get; } = new SqlSyncDetailedResults();
        
        #endregion
        
        private StringBuilder _pointSyncAuditTrailBuilder = new StringBuilder();
        

        public async Task DoOperationAsync(IProgress<(int, string)> progress, CancellationToken ct)
        {
            try
            {
                // Start timing operation
                StartTiming();

                // -- Get sql rows
                StartSubtask(nameof(SqlRowResolver.GetFirstXFromDb));

                List<SqlHistoryRow> earliestSqlRows = await SqlRowResolver.GetFirstXFromDb(
                    Schema,
                    SrcPoint,
                    1,
                    ct);

                ct.ThrowIfCancellationRequested();
                _pointSyncAuditTrailBuilder.AppendLine(GetSubTaskDuration(nameof(SqlRowResolver.GetFirstXFromDb)).ToString());
                
                if(!earliestSqlRows.IsAny() ||
                   earliestSqlRows.Any(row => !row.CellValues.ContainsKey(TimeColId)) ||
                   earliestSqlRows.Any(row => !row.CellValues[TimeColId].TryGetDateTime(out var dt)))
                {
                   
                    Results.OpResult = OpStatuses.Fail;
                    Results.StatusMessage = "Some sql rows didn't have values for time column, or had invalid values";
                    return;
                }
                Results.NumMatchingSqlRows = earliestSqlRows.Count;
                // Get dates
                EarliestDate = DateTime.Now.AddDays(-RetentionDays);
                LatestDate = earliestSqlRows.Max(row => (DateTime)row.CellValues[TimeColId].RawValue);

                if (IsHistoryNormalized)
                    LatestDate = LatestDate.AddHours(-2 * NormIntervalHours);           

                if (LatestDate < EarliestDate)
                {
                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage =
                        "The earliest sql entries are very near the start of the retention period, no gap was found.";
                    return;
                }

                Results.OperationInterval = (EarliestDate, LatestDate);
                
                ct.ThrowIfCancellationRequested();

                if (await SrcPoint.HistoryAvailableInInterval(EarliestDate, LatestDate))
                {
                    Results.OpResult = OpStatuses.Fail;
                    Results.StatusMessage =
                        "There is history in the retention period predating the earliest SQL entries";
                    return;
                }


                Results.OpResult = OpStatuses.Pass;
                Results.StatusMessage = "No gap was found";

            }
            catch (Exception ex)
            {
                Results.FailedException = ex;
                Results.OpResult = OpStatuses.Exception;
                Results.StatusMessage = "General operation failure - First Check";
            }
            finally
            {
                Results.LoggingMessages = _pointSyncAuditTrailBuilder.ToString();
                Results.TimeTakenPerformingOperation = GetTotalTimeTaken();
            }
        }


    }
}
