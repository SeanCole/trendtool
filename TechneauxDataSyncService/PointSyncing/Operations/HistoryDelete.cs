﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing.Operations.Base;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.Helper;
using TechneauxReportingDataModel.SqlHistory;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public class HistoryDelete : TimedOperation, ISqlSyncOperation
    {
        public HistoryDelete(
            SimpleCombinedTableSchema newSchema, 
            CachedCygNetPointWithRule srcPnt)
        {
            Schema = newSchema;

            if (Schema == null || Schema.IsEmptySqlSchema)
                throw new ArgumentNullException(nameof(newSchema), "Schema must not be null or empty");
            if (srcPnt == null)
                throw new ArgumentNullException(nameof(srcPnt), "Src Point must not be empty");

            if (srcPnt.SrcRule == null || !srcPnt.SrcRule.IsRuleValid)
                throw new ArgumentException("Src point rule must be non-null and valid");
            SrcPoint = srcPnt;
            
        }

        #region LocalVars

        private SimpleCombinedTableSchema Schema { get; }
        private CachedCygNetPointWithRule SrcPoint { get; }

        private double RetentionDays => SrcPoint.SrcRule.PollingOptions.RetentionDays;

        #endregion





        private StringBuilder pointSyncAuditTrailBuilder;
        public SimpleSqlSyncResults Results { get; } = new SimpleSqlSyncResults(nameof(HistoryDelete));
        public ISqlSyncDetailedResults DetailedHistoryResults { get; } = new SqlSyncDetailedResults();
        

        public async Task DoOperationAsync(IProgress<(int, string)> progress, CancellationToken ct)
        {
            try
            {
                // Start timing operation
                StartTiming();
                
                pointSyncAuditTrailBuilder = new StringBuilder();
                StartSubtask(nameof(SqlRowResolver.DeleteExpiredSqlForPoint));
                Results.NumSqlRowsDeleted = await SqlRowResolver.DeleteExpiredSqlForPoint(Schema, SrcPoint, SrcPoint.SrcRule.PollingOptions.RetentionDays,ct);
            
                ct.ThrowIfCancellationRequested();
                pointSyncAuditTrailBuilder.AppendLine(GetSubTaskDuration(nameof(SqlRowResolver.DeleteExpiredSqlForPoint)).ToString());
                pointSyncAuditTrailBuilder.AppendLine($"Sql expired rows delete finished with {Results.NumSqlRowsDeleted} rows deletd.");
      
                Results.StatusMessage = "Finished expired history delete.";
            }
            catch (Exception ex)
            {
                Results.FailedException = ex;
                Results.OpResult = OpStatuses.Exception;
                Results.StatusMessage = "General operation failure - History Delete";
            }
            finally
            {
                Results.LoggingMessages = pointSyncAuditTrailBuilder.ToString();
                Results.TimeTakenPerformingOperation = GetTotalTimeTaken();
            }
        }
    }
}
