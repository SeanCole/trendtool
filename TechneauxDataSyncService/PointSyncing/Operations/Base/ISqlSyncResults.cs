﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechneauxDataSyncService.PointSyncing.Operations
{

    public interface ISqlSyncResults
    {
        string OpName { get; set; }

        OpStatuses OpResult { get; set; }
        
        Exception FailedException { get; set; }

        string StatusMessage { get; set; }

        string LoggingMessages { get; set; }

        TimeSpan TimeTakenPerformingOperation { get; set; }

        (DateTime EarliestDate, DateTime LatestDate) OperationInterval { get; set; }

        int? NumRawCygNetEntriesFound { get; set; }

        int? NumNormEntriesFound { get; set; }

        int? NumValidNormEntriesFound { get; set; }

        int? NumCertainNormEntriesFound { get; set; }

        int? NumMatchingSqlRows { get; set; }

        int? NumCygNetRows { get; set; }

        int? NumTotalSqlRows { get; set; }

        int? NumSqlRowsInserted { get; set; }

        int? NumSqlRowsDeleted { get; set; }

        bool NewHistoryFound { get; set; }

        DateTime MostRecentRawHistTimestamp { get; set; }

        DateTime MostRecentNormTimestamp { get; set; }
    }

    public enum OpStatuses
    {
        Pass,
        Fail,
        Exception
    }

    public class SimpleSqlSyncResults : ISqlSyncResults
    {
        public SimpleSqlSyncResults(string opName)
        {

            OpName = opName;
        }

        public string OpName { get; set; }

        public OpStatuses OpResult { get; set; }

        public Exception FailedException { get; set; }

        public string StatusMessage { get; set; } 

        public string LoggingMessages { get; set; } 

        public TimeSpan TimeTakenPerformingOperation { get; set; }

        public string TimeTakenPerformingOperationSecs =>
            $"{TimeTakenPerformingOperation.TotalSeconds:0.00} secs";
        
        public DateTime EarliestDate => OperationInterval.EarliestDate;
        
        public DateTime LatestDate => OperationInterval.LatestDate;

        public (DateTime EarliestDate, DateTime LatestDate) OperationInterval { get; set; } 

        public int? NumRawCygNetEntriesFound { get; set; } 

        public int? NumNormEntriesFound { get; set; }

        public int? NumValidNormEntriesFound { get; set; }

        public int? NumCertainNormEntriesFound { get; set; }

        public int? NumCygNetRows { get; set; }

        public int? NumTotalSqlRows { get; set; }

        public int? NumMatchingSqlRows { get; set; } 

        public int? NumSqlRowsInserted { get; set; }

        public int? NumSqlRowsDeleted { get; set; } 

        public bool NewHistoryFound { get; set; }

        public DateTime MostRecentRawHistTimestamp { get; set; } 

        public DateTime MostRecentNormTimestamp { get; set; }

        public override string ToString()
        {
            return $"{nameof(OpResult)}=[{OpResult}], " +
                   $"{nameof(StatusMessage)}=[{StatusMessage}], " +
                   $"{nameof(LoggingMessages)}=[{LoggingMessages}], " +
                   $"{nameof(TimeTakenPerformingOperation)}=[{TimeTakenPerformingOperation}], " +
                   $"{nameof(OperationInterval.EarliestDate)}=[{OperationInterval.EarliestDate}], " +
                   $"{nameof(OperationInterval.LatestDate)}=[{OperationInterval.LatestDate}]"+
                   $"{nameof(NewHistoryFound)}=[{NewHistoryFound}]" +
                   $"{nameof(NumValidNormEntriesFound)}=[{NumValidNormEntriesFound}]" +
                   $"{nameof(NumCertainNormEntriesFound)}=[{NumCertainNormEntriesFound}]" +
                   $"{nameof(NumRawCygNetEntriesFound)}=[{NumRawCygNetEntriesFound}]" +
                   $"{nameof(NumValidNormEntriesFound)}=[{NumValidNormEntriesFound}]" +
                   $"{nameof(NumMatchingSqlRows)}=[{NumMatchingSqlRows}]" +
                   $"{nameof(NumSqlRowsInserted)}=[{NumSqlRowsInserted}]" +
                   $"{nameof(NumSqlRowsDeleted)}=[{NumSqlRowsDeleted}]" +
                   $"{nameof(NumTotalSqlRows)}=[{NumTotalSqlRows}]" +
                   $"{nameof(NumNormEntriesFound)}=[{NumNormEntriesFound}]" +
                   $"{nameof(NumCygNetRows)}=[{NumCygNetRows}]" +
                   $"{nameof(TimeTakenPerformingOperationSecs)}=[{TimeTakenPerformingOperationSecs}]" +
                   $"{nameof(MostRecentRawHistTimestamp)}=[{MostRecentRawHistTimestamp}]" +
                   $"{nameof(MostRecentNormTimestamp)}=[{MostRecentNormTimestamp}]";
        }
    }
}
