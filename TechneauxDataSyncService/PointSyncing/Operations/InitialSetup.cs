﻿using CygNetRuleModel.Resolvers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper.Points;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing.Operations.Base;
using TechneauxHistorySynchronization;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.Helper;
using TechneauxReportingDataModel.SqlHistory;
using TechneauxUtility;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public class InitialSetup : TimedOperation, ISqlSyncOperation
    {
        public InitialSetup(
            SimpleCombinedTableSchema simpleSchema,
            CachedCygNetPointWithRule srcPnt,
            int numberRows)
        {
            if (simpleSchema == null || simpleSchema.IsEmptySqlSchema)
                throw new ArgumentNullException(nameof(simpleSchema), "Schema must not be null or empty");

            if (srcPnt == null)
                throw new ArgumentNullException(nameof(srcPnt), "Src Point must not be empty");

            if (srcPnt.SrcRule == null || !srcPnt.SrcRule.IsRuleValid)
                throw new ArgumentException("Src point rule must be non-null and valid");
            Schema = simpleSchema;

            SrcPoint = srcPnt;
            NumRows = numberRows;
        }

        #region LocalVars

        private SimpleCombinedTableSchema Schema { get; }
        private CachedCygNetPointWithRule SrcPoint { get; }

        private DateTime EarliestDate { get; set; }
        private DateTime LatestDate { get; set; }

        private string TimeColId => Schema.TimeColumn.SqlTableFieldName;
        private int NumRows { get; }

        #endregion

        private StringBuilder _pointSyncAuditTrailBuilder = new StringBuilder();

        public SimpleSqlSyncResults Results { get; private set; } = new SimpleSqlSyncResults(nameof(InitialSetup));
        public ISqlSyncDetailedResults DetailedHistoryResults { get; } = new SqlSyncDetailedResults();

        public async Task DoOperationAsync(IProgress<(int, string)> progress, CancellationToken ct)
        {
            try
            {
                var compareModel = new CygNetSqlDataCompareModel(SrcPoint);

                // Start timing operation
                StartTiming();

                // -- Get sql rows
                StartSubtask(nameof(SqlRowResolver.GetLastXFromDb));

                List<SqlHistoryRow> latestSqlRows = await SqlRowResolver.GetLastXFromDb(
                    Schema,
                    SrcPoint,
                    NumRows,
                    ct);

                ct.ThrowIfCancellationRequested();
                _pointSyncAuditTrailBuilder.AppendLine(GetSubTaskDuration(nameof(SqlRowResolver.GetLastXFromDb)).ToString());
                var sqlTS = latestSqlRows.Select(x => x.CellValues[Schema.TimeColumn.SqlTableFieldName].StringValue);

                if (!latestSqlRows.IsAny() ||
                    latestSqlRows.Any(row => !row.CellValues.ContainsKey(TimeColId)) ||
                    latestSqlRows.Any(row => !row.CellValues[TimeColId].TryGetDateTime(out var dt)))
                {
                    _pointSyncAuditTrailBuilder.AppendLine("latestSqlRows: ");
                    foreach(var row in latestSqlRows)
                    {
                        _pointSyncAuditTrailBuilder.AppendLine($"{string.Join(" , ", row.CellValues.Values.ToList().Select(x => x.StringValue))}");
                    }

                    _pointSyncAuditTrailBuilder.AppendLine($"sql TS: {string.Join(" , ", sqlTS)}");
                    Results.LoggingMessages = _pointSyncAuditTrailBuilder.ToString();
                    Results.OpResult = OpStatuses.Fail;
                    Results.StatusMessage = "Some sql rows didn't have values for time column, or had invalid values";
                    return;
                }

                Results.NumTotalSqlRows = latestSqlRows.Count;

                // Get dates
                EarliestDate = latestSqlRows.Min(row => (DateTime)row.CellValues[TimeColId].RawValue);
                LatestDate = latestSqlRows.Max(row => (DateTime)row.CellValues[TimeColId].RawValue);

                _pointSyncAuditTrailBuilder.AppendLine($"Latest: {LatestDate}");
                _pointSyncAuditTrailBuilder.AppendLine($"Earliest: {EarliestDate}");

                var early = new DateTime(EarliestDate.Ticks, DateTimeKind.Local);
                var late = new DateTime(LatestDate.Ticks, DateTimeKind.Local);

                _pointSyncAuditTrailBuilder.AppendLine($"newEarliest: {early}");
                _pointSyncAuditTrailBuilder.AppendLine($"newLatest: {late}");

                var cygHist = await compareModel.GetCygNetHistory(early, late,
                    SrcPoint.SrcRule.GeneralHistoryOptions,
                    SrcPoint.SrcRule.HistoryNormalizationOptions,
                    null,
                    ct);

                if (!cygHist.IsAnyHistory)
                {
                    Results.OpResult = OpStatuses.Fail;
                    Results.StatusMessage = "No history found, data must not match";
                    return;
                }

                Results.NumRawCygNetEntriesFound = cygHist.RawHistoryEntries.Count;
                Results.MostRecentRawHistTimestamp = cygHist.RawHistoryEntries.Max(entry => entry.AdjustedTimestamp);

                if (cygHist.NormalizedHistoryEntries.IsAny())
                {

                    var validNormEntries = cygHist.NormalizedHistoryEntries.Where(entry => entry.IsValid && !entry.IsValueUncertain).ToList();
                    _pointSyncAuditTrailBuilder.AppendLine($"validNormEntries Count: {validNormEntries.Count}");
                    Results.NumNormEntriesFound = cygHist.NormalizedHistoryEntries.Count;
                    Results.NumValidNormEntriesFound = cygHist.NormalizedHistoryEntries.Count(entry => entry.IsValid && !entry.IsValueUncertain);
                    Results.NumCertainNormEntriesFound =
                        cygHist.NormalizedHistoryEntries.Count(entry => entry.IsValid && !entry.IsValueUncertain);

                    if (validNormEntries.Any())
                        Results.MostRecentNormTimestamp = validNormEntries.Max(entry => entry.NormalizedHistoryValue.AdjustedTimestamp);
                    else
                        Results.MostRecentNormTimestamp = Results.MostRecentRawHistTimestamp;
                }
                else
                {
                    Results.MostRecentNormTimestamp = Results.MostRecentRawHistTimestamp;
                }

                DetailedHistoryResults.CygNetSrcRawHistory = cygHist.RawHistoryEntries;
                DetailedHistoryResults.CygNetSrcNormalizedHistory = cygHist.NormalizedHistoryEntries;

                var cygHistRows = await compareModel.GetCygNetRows(cygHist, Schema.CygNetMappingRules, ct);
                var cygTS = cygHistRows.Select(x => x.CellValues[Schema.TimeColumn.SqlTableFieldName].StringValue);
                var rawTS = cygHist.RawHistoryEntries.Select(x => x.AdjustedTimestamp);

                _pointSyncAuditTrailBuilder.AppendLine($"cygRawTS: {string.Join(" , ", rawTS)}");

                if (!cygHistRows.IsAny())
                {
                    _pointSyncAuditTrailBuilder.AppendLine($"sql TS: {string.Join(" , ", sqlTS)}");
                    _pointSyncAuditTrailBuilder.AppendLine($"CygHistLatest: {cygHist.LatestTime}");
                    _pointSyncAuditTrailBuilder.AppendLine($"CygHistEarliest: {cygHist.EarliestTime}");
                    _pointSyncAuditTrailBuilder.AppendLine($"isnormalizesd: {cygHist.IsNormalized}");
                    _pointSyncAuditTrailBuilder.AppendLine($"cyghistrawcount: {cygHist.RawHistoryEntries.Count}");
                    _pointSyncAuditTrailBuilder.AppendLine($"cyg TS: {string.Join(" , ", cygTS)}");
                    Results.LoggingMessages = _pointSyncAuditTrailBuilder.ToString();
                    Results.OpResult = OpStatuses.Fail;
                    Results.StatusMessage = "No cygnet rows resolved from history";
                    return;
                }

                Results.NumCygNetRows = cygHistRows.Count;

                StartSubtask(nameof(compareModel.GetComparedRows));
                //comparing cyg resolved and sql to determine if audit is valid
                
                var latestComparedRows = compareModel.GetComparedRows(Schema.TimeColumn.SqlTableFieldName, cygHistRows, latestSqlRows);

                _pointSyncAuditTrailBuilder.AppendLine(GetSubTaskDuration(nameof(compareModel.GetComparedRows)).ToString());
                
                _pointSyncAuditTrailBuilder.AppendLine($"cyg TS: {string.Join(" , ", cygTS)}");
                _pointSyncAuditTrailBuilder.AppendLine($"sql TS: {string.Join(" , ", sqlTS)}");

                // All sql rows should match, so no "sql only" rows should be found
               
                bool compareIsEqual = !(latestComparedRows.Any(elm => elm.ComparisonValue != MappedSqlRowCompare.CygSqlCompare.Equal));

              
                DetailedHistoryResults.ComparedRows = latestComparedRows;
                DetailedHistoryResults.SqlSrcRows = latestSqlRows;
                DetailedHistoryResults.FixedCygNetValues = await Schema.GetFixedColumnsAndValues(SrcPoint, ct);

                Results.NumMatchingSqlRows =
                    latestComparedRows.Count(row => row.ComparisonValue == MappedSqlRowCompare.CygSqlCompare.Equal);

                if (compareIsEqual)
                {
                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage = "Data matches";
                }
                else
                {
                    _pointSyncAuditTrailBuilder.AppendLine("cygHistRows: ");
                    foreach(var row in cygHistRows)
                    {
                        _pointSyncAuditTrailBuilder.AppendLine($"{string.Join(" , ", row.CellValues.Values.ToList().Select(x => x.StringValue))}");
                    }
                    _pointSyncAuditTrailBuilder.AppendLine("latestSqlRows: ");
                    foreach(var row in latestSqlRows)
                    {
                        _pointSyncAuditTrailBuilder.AppendLine($"{string.Join(" , ", row.CellValues.Values.ToList().Select(x => x.StringValue))}");
                    }
                    Results.OpResult = OpStatuses.Fail;
                    Results.StatusMessage = "Comparison failed";
                }
            }
            catch (Exception ex)
            {
                Results.FailedException = ex;
                Results.OpResult = OpStatuses.Exception;
                Results.StatusMessage = "General operation failure - First Check";
            }
            finally
            {
                Results.LoggingMessages = _pointSyncAuditTrailBuilder.ToString();
                Results.TimeTakenPerformingOperation = GetTotalTimeTaken();
            }
        }


    }
}
