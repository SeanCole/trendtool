﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CygNetRuleModel.Resolvers;
using CygNetRuleModel.Resolvers.History;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing.Operations.Base;
using TechneauxHistorySynchronization;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.Helper;
using TechneauxUtility;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public class MaintenanceSync : ISqlSyncOperation
    {
        private CachedCygNetPointWithRule SrcPoint { get; }
        private DateTime LastCygNetTimestamp { get; }
        private DateTime LastNormTimestamp { get; }
        private SimpleCombinedTableSchema TableSchema { get; }
        private IDictionary<string, IConvertibleValue> OldCachedFixedValues { get; }

        private DateTime EarliestDate { get; set; }
        private DateTime LatestDate { get; }

        public SimpleSqlSyncResults Results { get; private set; } = new SimpleSqlSyncResults(nameof(MaintenanceSync));
        public ISqlSyncDetailedResults DetailedHistoryResults { get; } = new SqlSyncDetailedResults();

        public MaintenanceSync(
            SimpleCombinedTableSchema newSchema,
            CachedCygNetPointWithRule newSrcPoint,
            IDictionary<string, IConvertibleValue> oldCachedFixedValues,
            DateTime lastCygNetRawHist,
            DateTime lastValidNormTimestamp)
        {
            TableSchema = newSchema;
            SrcPoint = newSrcPoint;
            OldCachedFixedValues = oldCachedFixedValues;
            LastCygNetTimestamp = lastCygNetRawHist;
            LastNormTimestamp = lastValidNormTimestamp;

            if (IsHistoryNormalized)
            {
                EarliestDate = LastNormTimestamp.AddHours(
                        (double)SrcPoint.SrcRule.HistoryNormalizationOptions.NormalizeWindowIntervalLength /
                        SrcPoint.SrcRule.HistoryNormalizationOptions.NumWindowHistEntries)
                    .AddSeconds(-5);
            }
            else
            {
                EarliestDate = LastCygNetTimestamp.AddSeconds(1);
            }

            LatestDate = DateTime.Now;

            Results.OperationInterval = (EarliestDate, LatestDate);
        }

        private bool IsHistoryNormalized => SrcPoint.SrcRule.HistoryNormalizationOptions.EnableNormalization;

        public async Task DoOperationAsync(IProgress<(int, string)> progress, CancellationToken ct)
        {
            try
            {
                var newCachedValues = await TableSchema.GetFixedColumnsAndValues(SrcPoint, ct);

                if (!newCachedValues.IsEqualTo(OldCachedFixedValues))
                {
                    Results.OpResult = OpStatuses.Fail;
                    Results.LoggingMessages = $"New fixed values are : {string.Join(",", newCachedValues.Values.Select(x => x.StringValue))} ; Old fixed values are: {string.Join(",", newCachedValues.Values.Select(x => x.StringValue))}";
                    Results.StatusMessage = "Fixed values do not match";
                    return;
                }

                ct.ThrowIfCancellationRequested();
                //checks for new hist first before sync
                if (!await SrcPoint.HistoryAvailableInInterval(EarliestDate, LatestDate))
                {
                    Results.OpResult = OpStatuses.Pass;
                    Results.MostRecentNormTimestamp = LastNormTimestamp;
                    Results.MostRecentRawHistTimestamp = LastCygNetTimestamp;
                    Results.StatusMessage = "No new history found";
                    return;
                }

                var compareModel = new CygNetSqlDataCompareModel(SrcPoint);
                ct.ThrowIfCancellationRequested();
                //if(EarliestDate == DateTime.MinValue)
                //{
                //    Console.WriteLine("minvalue maintenance");
                //}
                //if(EarliestDate < LatestDate.AddYears(-10))
                //{
                //    Console.WriteLine("toosmall earliest");
                //}
                var cygHist = await Task.Run(() => compareModel.GetCygNetHistory(EarliestDate, LatestDate, SrcPoint.SrcRule, null, ct), ct);

                if (!cygHist.IsAnyHistory)
                {
                    Results.MostRecentNormTimestamp = LastNormTimestamp;
                    Results.MostRecentRawHistTimestamp = LastCygNetTimestamp;
                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage = "Didn't find any history on second attempt";
                    return;
                }

                Results.NumRawCygNetEntriesFound = cygHist.RawHistoryEntries.Count;
                Results.MostRecentRawHistTimestamp = cygHist.RawHistoryEntries.Max(entry => entry.AdjustedTimestamp);
                DetailedHistoryResults.CygNetSrcRawHistory = cygHist.RawHistoryEntries;

                if (cygHist.NormalizedHistoryEntries.IsAny())
                {
                    var validNormEntries = cygHist
                        .NormalizedHistoryEntries
                        .Where(entry => entry.IsValid && !entry.IsValueUncertain)
                        .ToList();

                    if (validNormEntries.Any())
                    {
                        Results.MostRecentNormTimestamp =
                                            validNormEntries.Max(entry => entry.NormalizedHistoryValue.AdjustedTimestamp);
                    }
                    else
                    {
                        Results.MostRecentNormTimestamp = LastNormTimestamp;
                    }

                    Results.NumNormEntriesFound = cygHist.NormalizedHistoryEntries.Count;
                    Results.NumValidNormEntriesFound = cygHist.NormalizedHistoryEntries.Count(entry => entry.IsValid & !entry.IsValueUncertain);
                    Results.NumCertainNormEntriesFound =
                        cygHist.NormalizedHistoryEntries.Count(entry => entry.IsValid && !entry.IsValueUncertain);
                    
                    DetailedHistoryResults.CygNetSrcNormalizedHistory = cygHist.NormalizedHistoryEntries;
                }
                else
                {
                    Results.MostRecentNormTimestamp = Results.MostRecentRawHistTimestamp;
                }

                ct.ThrowIfCancellationRequested();
                List<CygNetHistoryRow> cygRows = await compareModel.GetCygNetRows(cygHist, TableSchema.CygNetMappingRules, ct);

                if (cygRows.IsAny())
                {
                    Results.NumCygNetRows = cygRows.Count;

                    await Task.Run(() => compareModel.UpdateSqlTableFast(TableSchema, cygRows.Cast<IValueSet>().ToList(), ct), ct);

                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage = "New rows inserted";
                }
            }
            catch (Exception ex)
            {
                Results.FailedException = ex;
                Results.OpResult = OpStatuses.Exception;
                Results.StatusMessage = "General operation failure - MaintenanceSync";
            }
            finally
            {
                //Results.LoggingMessages = _pointSyncAuditTrailBuilder.ToString();
                //Results.TimeTakenPerformingOperation = GetTotalTimeTaken();
            }

        }



    }
}
