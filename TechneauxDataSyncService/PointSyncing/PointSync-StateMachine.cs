﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ReactiveUI;
using TechneauxDataSyncService.PointSyncing.Operations;
using TechneauxDataSyncService.PointSyncing.Operations.Sub;
using TechneauxHistorySynchronization.Models;

namespace TechneauxDataSyncService.PointSyncing
{
    public partial class PointSync
    {
        private async Task<bool> ExecuteSingleOperation(
            SimpleCombinedTableSchema tableSchema, 
            CancellationToken ct)
        {
            ISqlSyncOperation thisOp = null;

            try
            {
                // -- Check state
                switch (SyncState)
                {
                    case SyncStates.HistoryCheck:
                        // Run a hist check op
                        thisOp = new HistoryCheck(SrcPoint);
                        var histCheckOp = (HistoryCheck) thisOp;
                        
                        await histCheckOp.DoOperationAsync(null, ct);

                        switch (histCheckOp.Results.OpResult)
                        {
                            case OpStatuses.Pass:
                                if (_firstRun)
                                    SyncState = SyncStates.EarlyGapCheck;
                                else
                                    SyncState = SyncStates.FullResync;
                                return true;
                            case OpStatuses.Fail:
                                NextOperationScheduledTime = DateTime.Now.AddHours(.25);
                                SyncState = SyncStates.HistoryCheck;

                                return false;
                            case OpStatuses.Exception:
                                NextOperationScheduledTime = DateTime.Now.AddHours(.25);
                                return false;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }

                    case SyncStates.EarlyGapCheck:
                        _log.Debug($"FirstRun");

                        // Do first check
                        thisOp = new EarlyGapCheck(tableSchema, SrcPoint);
                        var earlyGapCheckOp = (EarlyGapCheck) thisOp;

                        await earlyGapCheckOp.DoOperationAsync(null, ct);

                        switch (earlyGapCheckOp.Results.OpResult)
                        {
                            case OpStatuses.Pass:
                                SyncState = SyncStates.InitialSetup;
                                return true;
                            case OpStatuses.Fail:
                                SyncState = SyncStates.FullResync;
                                return true;
                            case OpStatuses.Exception:
                                NextOperationScheduledTime = DateTime.Now.AddHours(.25);
                                return false;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }

                    case SyncStates.InitialSetup:
                        _firstRun = false;

                        _log.Debug($"FirstRun");

                        // Do first check
                        thisOp = new InitialSetup(tableSchema, SrcPoint, FirstCheckRowCount);
                        var firstCheckOp = (InitialSetup)thisOp;

                        await firstCheckOp.DoOperationAsync(null, ct);

                        switch (firstCheckOp.Results.OpResult)
                        {
                            case OpStatuses.Pass:
                                LastCygNetTimestamp = firstCheckOp.Results.MostRecentRawHistTimestamp;
                                LastNormalizedTimestamp = firstCheckOp.Results.MostRecentNormTimestamp;
                                SetNewMaintenanceSyncTime(firstCheckOp.DetailedHistoryResults.CygNetSrcRawHistory);
                                CachedColVals = firstCheckOp.DetailedHistoryResults.FixedCygNetValues;

                                SyncState = SyncStates.MaintenanceSync;

                                return false;
                            case OpStatuses.Fail:
                                SyncState = SyncStates.FullResync;
                                return true;
                            case OpStatuses.Exception:
                                NextOperationScheduledTime = DateTime.Now.AddHours(.25);
                                return false;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }

                    case SyncStates.MaintenanceSync:
                        thisOp = new MaintenanceSync(tableSchema, SrcPoint, CachedColVals, LastCygNetTimestamp, LastNormalizedTimestamp);
                        var maintSyncOp = (MaintenanceSync) thisOp;

                        await maintSyncOp.DoOperationAsync(null, ct);

                        switch (maintSyncOp.Results.OpResult)
                        {
                            case OpStatuses.Pass:
                                LastCygNetTimestamp = maintSyncOp.Results.MostRecentRawHistTimestamp;
                                LastNormalizedTimestamp = maintSyncOp.Results.MostRecentNormTimestamp;
                                SetNewMaintenanceSyncTime(maintSyncOp.DetailedHistoryResults.CygNetSrcRawHistory);

                                return true;
                            case OpStatuses.Fail:
                                SyncState = SyncStates.FullResync;
                                return true;
                            case OpStatuses.Exception:
                                NextOperationScheduledTime = DateTime.Now.AddHours(.25);
                                return false;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }

                    case SyncStates.FullResync:
                        thisOp = new FullResync(tableSchema, SrcPoint);
                        var fullResyncOp = (FullResync) thisOp;

                        await fullResyncOp.DoOperationAsync(null, ct);

                        switch (fullResyncOp.Results.OpResult)
                        {
                            case OpStatuses.Pass:
                                LastCygNetTimestamp = fullResyncOp.Results.MostRecentRawHistTimestamp;
                                LastNormalizedTimestamp = fullResyncOp.Results.MostRecentNormTimestamp;
                                SetNewMaintenanceSyncTime(fullResyncOp.DetailedHistoryResults.CygNetSrcRawHistory);
                                CachedColVals = fullResyncOp.DetailedHistoryResults.FixedCygNetValues;

                                SyncState = SyncStates.MaintenanceSync;
                                return false;
                            case OpStatuses.Fail:
                                NextOperationScheduledTime = DateTime.Now.AddHours(.25);
                                SyncState = SyncStates.HistoryCheck;
                                return false;
                            case OpStatuses.Exception:
                                NextOperationScheduledTime = DateTime.Now.AddHours(.25);
                                return false;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }

                    case SyncStates.FastAudit:
                        thisOp = new FastAudit(tableSchema, SrcPoint);
                        var fastAuditOp = (FastAudit)thisOp;

                        await fastAuditOp.DoOperationAsync(null, ct);

                        switch (fastAuditOp.Results.OpResult)
                        {
                            case OpStatuses.Pass:
                                SyncState = SyncStates.MaintenanceSync;
                                return false;
                            case OpStatuses.Fail:
                                SyncState = SyncStates.FullResync;
                                return true;
                            case OpStatuses.Exception:
                                NextOperationScheduledTime = DateTime.Now.AddHours(.25);
                                return false;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    case SyncStates.HistoryDelete:
                        thisOp = new HistoryDelete(tableSchema, SrcPoint);
                        var historyDeleteOp = (HistoryDelete)thisOp;
                        await historyDeleteOp.DoOperationAsync(null, ct);

                        SyncState = SyncStates.MaintenanceSync;
                        return false;
                                                
                    default:
                        throw new InvalidOperationException("State unknown");
                }
            }
            catch (OperationCanceledException)
            {
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                _log.Error(e, $"General exception during point sync for [{SrcPoint.Tag.GetTagPointIdFull()}] ");
                return false;
            }
            finally
            {
                if (thisOp == null)
                    throw new InvalidOperationException("Op var must not be null");

                if (ServiceOps.LogAllOpResults)
                {
                    OpsRecords.Add(thisOp.Results);
                    OpsRecordsHarness = OpsRecords.ToList();
                }

                if (thisOp.Results.OpResult == OpStatuses.Exception)
                {
                    _log.Error(thisOp.Results.FailedException,
                        $"Operation type [{thisOp.GetType()}] failed for point [{SrcPoint.Tag.GetTagPointIdFull()}] with results [{thisOp.Results}]");
                }
                else
                {
                    _log.Debug(
                        $"Operation type [{thisOp.GetType()}] ran for point [{SrcPoint.Tag.GetTagPointIdFull()}] with results [{thisOp.Results}]");
                }
                
                if (SyncState == SyncStates.MaintenanceSync && DateTime.Now > _nextFastAudit)
                {
                    SyncState = SyncStates.FastAudit;
                    _nextFastAudit = _nextFastAudit.AddDays(1);
                }
                else if(SyncState == SyncStates.MaintenanceSync && DateTime.Now > _nextHistoryDelete)
                {
                    SyncState = SyncStates.HistoryDelete;
                    _nextHistoryDelete = _nextHistoryDelete.AddDays(1);
                }
            }
        }
    }
}
