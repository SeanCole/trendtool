﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ReactiveUI;
using Serilog;
using TechneauxDataSyncService.ConfigFileSyncing;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing.Operations;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using TechneauxReportingDataModel.Helper;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxDataSyncService.PointSyncing
{
    public partial class PointSync : NotifyModelBase, ICancellable, IPointSync
    {
        #region Constants-Opts

        private const int MaxOpsPerSync = 5;
        private const int FirstCheckRowCount = 5;

        #endregion


        private DateTime _nextHistoryDelete;

        #region LocalProperties

        private SyncFileUtility ParentSyncClass { get; }

        private SqlPointSelection ThisPointOpts => ParentSyncClass
            .SrcDataModel
            .SqlConfigModel
            .SourcePointRules
            .First(elm => elm.PointSelection.UDC == SrcPoint.Tag.UDC);

        private PointHistoryNormalizationOptions NormalizationOpts => ThisPointOpts.HistoryNormalizationOptions;

        private double RetentionDays => ThisPointOpts.PollingOptions.RetentionDays;

        private bool IsNormalized => NormalizationOpts.EnableNormalization;

        private double NormalizationInterval => Convert.ToDouble(NormalizationOpts.NormalizeWindowIntervalLength / NormalizationOpts.NumWindowHistEntries);

        public CachedCygNetPointWithRule SrcPoint
        {
            get => GetPropertyValue<CachedCygNetPointWithRule>();
            set => SetPropertyValue(value);
        }

        public Dictionary<string, IConvertibleValue> CachedColVals
        {
            get; set;
        }

        private readonly CancellationTokenSource _cts;

        #endregion

        #region InterfaceVars

        public TimeSpan PredictionInterval
        {
            get => GetPropertyValue<TimeSpan>();
            private set => SetPropertyValue(value);
        }

        public bool LastSyncInException { get; }


        public string CurrentStatus
        {
            get => GetPropertyValue<string>();
            private set => SetPropertyValue(value);
        }

        public string LastOperationType { get; }

        public DateTime LastSyncRunTime
        {
            get => GetPropertyValue<DateTime>();
            private set => SetPropertyValue(value);
        }

        public string LastSyncDuration
        {
            get => GetPropertyValue<string>();
            private set => SetPropertyValue(value);
        }

        public DateTime NextOperationScheduledTime
        {
            get => GetPropertyValue<DateTime>();
            private set => SetPropertyValue(value);
        }

        public double MinsUntilNextSync
        {
            get => GetPropertyValue<double>();
            private set => SetPropertyValue(value);
        }

        public double AveMinsDelayScheduleVsActual
        {
            get => GetPropertyValue<double>();
            private set => SetPropertyValue(value);
        }

        public int NumSyncsRun
        {
            get => GetPropertyValue<int>();
            private set => SetPropertyValue(value);
        }

        public bool TrackAllOps { get; set; }

        public List<ISqlSyncResults> OpsRecords { get; } = new List<ISqlSyncResults>();

        public List<ISqlSyncResults> OpsRecordsHarness
        {
            get => GetPropertyValue<List<ISqlSyncResults>>();
            private set => SetPropertyValue(value);
        }


        public DateTime LatestSqlDateTimeForFirstSync
        {
            get => GetPropertyValue<DateTime>();
            private set => SetPropertyValue(value);
        }

        public DateTime LastCygNetTimestamp
        {
            get => GetPropertyValue<DateTime>();
            private set => SetPropertyValue(value);
        }

        public DateTime LastNormalizedTimestamp
        {
            get => GetPropertyValue<DateTime>();
            private set => SetPropertyValue(value);
        }

        public string LastExceptionMessage
        {
            get => GetPropertyValue<string>();
            private set => SetPropertyValue(value);
        }

        #endregion

        #region Logging
                       
        private readonly ILogger _log;

        #endregion       

        public PointSync(
            SyncFileUtility prntSync,
            CachedCygNetPointWithRule newSrcPoint,
            ILogger parentLog,
            CancellationToken parentCt)
        {
            ParentSyncClass = prntSync;
            SrcPoint = newSrcPoint;
            CurrentStatus = "Constructed - Waiting";
       
            _log = parentLog;

            var randMin = new Random();
            
            _nextFastAudit = DateTime.Now.AddHours(2).AddMinutes(randMin.Next(0, 60 * 24));
            _nextHistoryDelete = DateTime.Now.AddHours(1).AddMinutes(randMin.Next(0, 60 * 24));
            if (_nextFastAudit == _nextHistoryDelete)
            {
                _nextHistoryDelete.AddHours(1);
            }

            CachedColVals = new Dictionary<string, IConvertibleValue>();
          
            _cts = CancellationTokenSource.CreateLinkedTokenSource(parentCt);

            SyncState = SyncStates.HistoryCheck;
        }


        public enum SyncStates
        {
            HistoryCheck,
            EarlyGapCheck,
            InitialSetup,
            MaintenanceSync,
            FullResync,
            FastAudit,
            HistoryDelete
        }

        public SyncStates SyncState
        {
            get => GetPropertyValue<SyncStates>();
            private set => SetPropertyValue(value);
        }

        public void CancelAll()
        {
            _cts.Cancel();
        }

        public async Task SyncNewData()
        {
            await SyncData(_cts.Token);
        }

        int _taskCount = 0;
        public bool IsBusy => _taskCount > 0;

        BindingList<ISqlSyncResults> IPointSync.OpsRecords { get; }

        private bool _firstRun = true;

        public async Task SyncData(CancellationToken ct)
        {
            CurrentStatus = "Syncing";
            LastSyncRunTime = DateTime.Now;
          
            var sw = new Stopwatch();
            sw.Start();

            try
            {
                // -- Lock task
                Interlocked.Increment(ref _taskCount);


                if (NextOperationScheduledTime != default)
                {
                    double totalDelayMins = AveMinsDelayScheduleVsActual * NumSyncsRun + (DateTime.Now - NextOperationScheduledTime).TotalMinutes;

                    AveMinsDelayScheduleVsActual = totalDelayMins == 0 ? 0 : totalDelayMins / (NumSyncsRun + 1);
                }


                // -- Schema
                var thisSchema = await ParentSyncClass.GetTableSchema();
                if (thisSchema == null)
                {
                    _log.Warning($"{SrcPoint.Tag.GetTagPointIdFull()}: Failed to get SqL table schema, stopping sync");
                    return;
                }

                int opCount = 0;
                bool contin;

                do
                {
                    contin = await ExecuteSingleOperation(thisSchema, ct);

                    opCount++;
                } while (opCount <= MaxOpsPerSync && contin);

            }
            catch (OperationCanceledException)
            {
                _log.Warning($"{SrcPoint.Tag.GetTagPointIdFull()}: Sync cancelled");            
            }
            catch (Exception ex)
            {
                _log.Error(ex, $"{SrcPoint.Tag.GetTagPointIdFull()}: Point sync failed");

            }
            finally
            {
                Interlocked.Decrement(ref _taskCount);
                CurrentStatus = "Waiting for next sync";
                NumSyncsRun++;

                sw.Stop();

                if (sw.Elapsed < TimeSpan.FromMilliseconds(100))
                {
                    LastSyncDuration = sw.ElapsedMilliseconds.ToString("0") + " msec";
                }
                else
                {
                    LastSyncDuration = sw.Elapsed.TotalSeconds.ToString("0.00") + " secs";
                }
            }
        }


        private DateTime _nextFastAudit;


        public override bool Equals(object obj)
        {
            if (obj != null && obj is PointSync)
            {
                return SrcPoint == (obj as PointSync).SrcPoint;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return SrcPoint.GetHashCode();
        }


    }
}
