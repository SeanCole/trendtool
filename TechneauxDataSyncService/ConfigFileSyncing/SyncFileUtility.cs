﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using CygNet.Data.Core;
using CygNetRuleModel.Resolvers;
using Serilog;
using System.Diagnostics;
using Serilog.Core;
using Techneaux.CygNetWrapper.Points;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory.DatabaseOperations;
using TechneauxReportingDataModel.General;
using TechneauxReportingDataModel.Helper;
using TechneauxUtility;
using XmlDataModelUtility;
using System.Data.SqlClient;
using System.Data.Odbc;

namespace TechneauxDataSyncService.ConfigFileSyncing
{
    public class SyncFileUtility : NotifyModelBase, ICancellable
    {
        const int MinsBetweenPointCheck = 10;
        const int MinsBetweenPointSync = 5;

        public ReportConfigModel SrcDataModel
        {
            get => GetPropertyValue<ReportConfigModel>();
            set => SetPropertyValue(value);
        }

        [XmlIgnore()]
        public string FilePath
        {
            get => GetPropertyValue<string>();
            private set
            {
                FileName = Path.GetFileName(value);
                SetPropertyValue(value);
            }
        }

        [XmlIgnore()]
        public string FileName
        {
            get => GetPropertyValue<string>();
            private set => SetPropertyValue(value);
        }

        [XmlIgnore()]
        public string Status
        {
            get => GetPropertyValue<string>();
            private set => SetPropertyValue(value);
        }

        public DateTime LastModifiedTimestamp
        {
            get => GetPropertyValue<DateTime>();
            set => SetPropertyValue(value);
        }

        private int MaxPoints { get; set; } = -1;

        public SyncFileUtility()
        {
            var sAttr = ConfigurationManager.AppSettings.Get("MaxPoints");
        
            if (int.TryParse(sAttr, out int num))
            {
                MaxPoints = num;
            }
        }

        private CancellationTokenSource Cts { get; set; }

        private readonly ILogger _configFileLog;


        private IDisposable CheckForNewPointsTimer;
        private IDisposable CheckForNewPointDataTimer;

        public SyncFileUtility(
            string newFileName, 
            ReportConfigModel newConfigModel, 
            DateTime lastModDate, 
            CancellationToken parentCt, 
            bool usingTestHarness = false)
        {
            FilePath = newFileName;
            //MaxPoints = 30;
          
            var LoggingLevelAttr = ConfigurationManager.AppSettings.Get("LogLevelConfig");
            Serilog.Events.LogEventLevel newLevel = (Serilog.Events.LogEventLevel)Enum.Parse(typeof(Serilog.Events.LogEventLevel), LoggingLevelAttr);
            var logLevelSwitch = new LoggingLevelSwitch(newLevel);
            _configFileLog = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(logLevelSwitch)
                .WriteTo
                .File($@"{AppDomain.CurrentDomain.BaseDirectory}\Logs\Config Log [{Path.GetFileNameWithoutExtension(FilePath)}] {DateTime.Now.ToString("MM-dd-yyyy hh-mm-ss")}.txt", shared: true)
                .CreateLogger();

            _configFileLog.Information("================================================");
            _configFileLog.Information($"Initializing new sync file with path=[{FilePath}]");

            SrcDataModel = newConfigModel;
            LastModifiedTimestamp = lastModDate;

            _configFileLog.Information($"Binding timer events: Point check every [{MinsBetweenPointCheck}] mins, Point sync every [{MinsBetweenPointSync}] mins");

            MyPoints = new BindingList<PointSync>();
            if (usingTestHarness)
            {
                CheckForNewPointsTimer = Observable.Interval(TimeSpan.FromMinutes(MinsBetweenPointCheck))
              .ObserveOnDispatcher()
              .Subscribe(evt =>
              {
                  UpdatePointList();
              });

                CheckForNewPointDataTimer = Observable.Interval(TimeSpan.FromMinutes(MinsBetweenPointSync))
                                .ObserveOnDispatcher()
                              .Subscribe(evt =>
                              {
                                  CheckForNewPointData();
                              });
            }
            else
            {
                CheckForNewPointsTimer = Observable.Interval(TimeSpan.FromMinutes(MinsBetweenPointCheck))
              .Subscribe(evt =>
              {
                  UpdatePointList();
              });

                CheckForNewPointDataTimer = Observable.Interval(TimeSpan.FromMinutes(MinsBetweenPointSync))
                              .Subscribe(evt =>
                              {
                                  CheckForNewPointData();
                              });
            }

            Cts = CancellationTokenSource.CreateLinkedTokenSource(parentCt);
        }

        public void CancelAll()
        {
            _configFileLog.Debug("Cancel all.");

            CheckForNewPointsTimer.Dispose();
            CheckForNewPointDataTimer.Dispose();

            Cts.Cancel();
        }

        private readonly SemaphoreSlim _lockPointCheck = new SemaphoreSlim(1, 1);

        private readonly SemaphoreSlim _schemaLock = new SemaphoreSlim(1, 1);
        private SimpleCombinedTableSchema _mySchema = null;
        private DateTime _lastSchemaRefreshTime;

        public async Task<SimpleCombinedTableSchema> GetTableSchema()
        {
            try
            {
                await _schemaLock.WaitAsync();

                _configFileLog.Debug($"Trying to read table schmema");

                if (_mySchema == null || (DateTime.Now - _lastSchemaRefreshTime).TotalMinutes > 15)
                {
                    _configFileLog.Debug("Schema needed to be refreshed, reading from table");

                    _mySchema = await Task.Run(() => SqlServerConnectionUtility.GetTableSchema(
                        SrcDataModel.SqlConfigModel.SqlGeneralOpts, 
                        SrcDataModel.SqlConfigModel.TableMappingRules.ToList()));

                    _lastSchemaRefreshTime = DateTime.Now;
                }

                return _mySchema;
            }
            catch (Exception ex)
            {
                _configFileLog.Error(ex, "Failed to get schema with exception:");
                return null;
            }
            finally
            {
                _schemaLock.Release();
            }
        }

        public int NumberOfPointUpdates { get; private set; } = 0;

        public async Task UpdatePointList()
        {
            var swPoint = new Stopwatch();

            try
            {
                await _lockPointCheck.WaitAsync();

                swPoint.Start();
                _configFileLog.Debug($"Cancellation at start of updatepointlist is {Cts.IsCancellationRequested}");

                NumberOfPointUpdates += 1;

                _configFileLog.Information("Starting point update check #[{NumberOfPointUpdates}]", NumberOfPointUpdates);

                Status = "Getting table schema";

                var thisSchema = await GetTableSchema();
                Status = "Validating Config";

                if (thisSchema != null && CygNetSqlDataCompareModel.ValidateRulesDataOnly(thisSchema).IsValid)
                {
                    _configFileLog.Debug($@"Starting UpdatePointList");
                    Status = "Updating PointsList";
                    
                    var resolver = new CachedFacilityResolver(SrcDataModel.CygNetGeneral);
                    _configFileLog.Debug($"CachedFacilityResolver made with {SrcDataModel.CygNetGeneral.FacSiteService} and {SrcDataModel.CygNetGeneral.FixedDomainId}");

                    // Get base facilities
                    var facResults = await Task.Run(() => resolver.FindFacilities(Cts.Token));

                    var pointsFound = new List<CachedCygNetPointWithRule>();

                    if (facResults.SearchSucceeded && facResults.FacsFound.IsAny())
                    {
                        var (points, udcResults) = await Task.Run(() => 
                            CygNetPointResolver.GetPointsAsync(
                                SrcDataModel.SqlConfigModel.SourcePointRules.ToList(),
                                facResults.FacsFound, 
                                SrcDataModel.CygNetGeneral.FacilityFilteringRules.ChildGroups.ToList(), 
                                Cts.Token));

                        if (points.IsAny())
                        {
                            pointsFound = MaxPoints >= 0 ? points.Take(MaxPoints).ToList() : points.ToList();
                        }

                        _configFileLog.Debug($"Point Search Succeeded = {points.IsAny()} and CancellationRequested is {Cts.IsCancellationRequested}");
                    }

                    Cts.Token.ThrowIfCancellationRequested();
                    _configFileLog.Debug("Cancellation");

                    var newPointSyncObjs = new List<PointSync>();

                    if (pointsFound.IsAny())
                    {
                        _configFileLog.Debug("Point search successful with [{FoundCount}] points found", pointsFound.Count);

                        HashSet<ICachedCygNetPoint> existingPoints = new HashSet<ICachedCygNetPoint>(MyPoints.Select(ps => ps.SrcPoint));

                        var newPoints = pointsFound.Where(pt => !existingPoints.Contains(pt)).ToList();

                        foreach (var newPoint in newPoints)
                        {
                            var newPointSync = new PointSync(this, newPoint, _configFileLog, Cts.Token);
                            newPointSyncObjs.Add(newPointSync);
                            MyPoints.Add(newPointSync);
                            //await Task.Run(() => NewPointSync.DoInitialCheck());
                        }

                        var pointsNoLongerAvailable = MyPoints.Where(ps => !pointsFound.Contains(ps.SrcPoint)).ToList();

                        if (pointsNoLongerAvailable.Any())
                        {
                            _configFileLog.Warning("Removing [{UnavailablePointCount}] points which are no longer available", pointsNoLongerAvailable.Count);

                            foreach (var pnt in pointsNoLongerAvailable)
                            {
                                pnt?.CancelAll();
                                MyPoints.Remove(pnt);
                            }
                        }

                        if (newPointSyncObjs.Any())
                        {
                            Interlocked.Increment(ref TaskNum);

                            _configFileLog.Warning("Starting sync tasks for [{PointCount}] points that are due for a sync, Task #{TaskNum}. Current Memory: {mem}",
                                newPointSyncObjs.Count, TaskNum, Process.GetCurrentProcess().WorkingSet64);

                            await Task.Run(() => newPointSyncObjs.ForEachAsync(10, async pntTask =>
                            {
                                Cts.Token.ThrowIfCancellationRequested();
                                await Task.Run(pntTask.SyncNewData);
                            }));

                            _configFileLog.Warning("Finishing sync tasks for [{PointCount}] points that are due for a sync, Task #{TaskNum}. Current Memory: {mem}",
                                newPointSyncObjs.Count, TaskNum, Process.GetCurrentProcess().WorkingSet64);
                        }
                    }
                    else
                    {
                        _configFileLog.Warning($"No points were found, clearing all existing points and stopping all syncs");

                        MyPoints.ToList().ForEach(pnt => pnt?.CancelAll());

                        MyPoints.Clear();
                    }
                    Status = "Waiting";
                }
                else
                {
                    _configFileLog.Warning("Failed Validation in UpdatePointsList, removing all pointsync objects");
                    Status = "Failed Validation";

                    foreach (var pnt in MyPoints)
                    {
                        pnt?.CancelAll();
                    }

                    MyPoints.Clear();
                }
            }
            catch (Exception ex)
            {
                _configFileLog.Error(ex, $"Exception during point check");
            }
            finally
            {
                SqlConnection.ClearAllPools();
                //OdbcConnection.ReleaseObjectPool();
                GC.Collect();
                GC.WaitForPendingFinalizers();

                _configFileLog.Debug("Finished Point update check #[{NumberOfPointUpdates}] with duration [{dur}]", _numNewDataCheck, swPoint.Elapsed);
                                
                _lockPointCheck.Release();
            }
        }

        private bool _validation = true;

        int _numNewDataCheck = 0;

        private int TaskNum = 0;
        public async Task CheckForNewPointData()
        {
            var swNewData = new Stopwatch();

            try
            {
                await _lockPointCheck.WaitAsync();
                swNewData.Start();

                _numNewDataCheck += 1;
                _configFileLog.Information("Starting point new data check #[{_numNewDataCheck}]", _numNewDataCheck);
                                

                _validation = CygNetSqlDataCompareModel.ValidateRulesDataOnly((await GetTableSchema())).IsValid;
                if (_validation)
                {
                    _configFileLog.Debug($"Validation succeeded, looking for points due for checking");
                    Status = "Checking for New point Data";

                    Cts.Token.ThrowIfCancellationRequested();
                    var pointsDueForCheck = MyPoints
                        .Where(pnt => !pnt.IsBusy)
                        .Where(item => item.NextOperationScheduledTime <= DateTime.Now).ToList();

                    if (pointsDueForCheck.Any())
                    {
                        Interlocked.Increment(ref TaskNum);
                        _configFileLog.Warning("Starting sync tasks for [{PointCount}] points that are due for a sync, Task #{TaskNum}. Current Memory: {mem}", 
                            pointsDueForCheck.Count, TaskNum, Process.GetCurrentProcess().WorkingSet64);

                        await MultiTaskAwaitHelper.ForEachAsync(pointsDueForCheck, 10, async pntTask =>
                                            {                                                
                                                Cts.Token.ThrowIfCancellationRequested();
                                                await Task.Run(() => pntTask.SyncNewData());                                     
                                            });

                        _configFileLog.Warning("Finishing sync tasks for [{PointCount}] points that are due for a sync, Task #{TaskNum}. Current Memory: {mem}",
                            pointsDueForCheck.Count, TaskNum, Process.GetCurrentProcess().WorkingSet64);
                    }
                }
                else
                {
                    _configFileLog.Debug("Failed Validation in CheckForNewPointData");
                    Status = "Failed config validation";
                }
            }
            catch (Exception ex)
            {
                _configFileLog.Error(ex, "Failed new data check #[{numCheck}]", _numNewDataCheck);
            }
            finally
            {
                SqlConnection.ClearAllPools();

                if (_validation)
                    Status = "Waiting";

                _configFileLog.Debug("Finished point new data check #[{_numNewDataCheck}] with duration [{dur}]", _numNewDataCheck, swNewData.Elapsed);

                _lockPointCheck.Release();
            }
        }

        [XmlIgnore()]
        public BindingList<PointSync> MyPoints
        {
            get => GetPropertyValue<BindingList<PointSync>>();
            set => SetPropertyValue(value);
        }

        [XmlIgnore()]
        public PointSync SelectedPoint
        {
            get => GetPropertyValue<PointSync>();
            set => SetPropertyValue(value);
        }
    }
}
