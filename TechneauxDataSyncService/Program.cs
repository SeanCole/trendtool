﻿using System.ServiceProcess;
using TechneauxDataSyncService.Main;

namespace TechneauxDataSyncService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] servicesToRun;
            ServiceOps.LogAllOpResults = false;
            servicesToRun = new ServiceBase[]
            {
                
                new DataSync()
            };
            ServiceBase.Run(servicesToRun);
        }
    }
}
