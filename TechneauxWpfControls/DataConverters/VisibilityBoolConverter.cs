using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Markup;
using System.Windows;


namespace TechneauxWpfControls.DataConverters
{
    public class VisibilityBoolConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {


            switch (value)
            {
                case null:
                    return System.Windows.Visibility.Visible;
                case bool _ when (bool)value == false:
                    return System.Windows.Visibility.Collapsed;
                case bool _ when (bool)value == true:
                    return System.Windows.Visibility.Visible;
                default:
                    return System.Windows.Visibility.Visible;
            }
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

    }
}
