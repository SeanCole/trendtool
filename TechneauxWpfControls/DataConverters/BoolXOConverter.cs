using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Markup;
using TechneauxReportingDataModel.Helper;

namespace TechneauxWpfControls.DataConverters
{
    public class BoolXOConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var checkUdc = parameter as string;
            var result = value as FacilityUdcResult;
            if(result.HasUdc(checkUdc))
            {
                return "X";           
            }
            else
            {
                return "";
            }
            

        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

    }
}
