
using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Markup;

namespace TechneauxWpfControls.DataConverters
{
    public class DoubleLongConverter : MarkupExtension, IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return System.Convert.ToDouble(0);
            }
            if (value as long? == 0)
            {
                return System.Convert.ToDouble(0);
            }            
            
            return System.Convert.ToDouble(value);
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
 
            if (value == null)
            {
                return System.Convert.ToInt64(0);
            }
            if (value as Double? == 0)
            {
                return System.Convert.ToInt64(0);
            }

            return System.Convert.ToInt64(value);
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

    }
}

