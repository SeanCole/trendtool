﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CygNet.API.Core;
using CygNet.API.Points;
using CygNet.Data.Core;
using DataExporter;
using static DataExporterDataModel.ReportElm;
using static CygNetRuleModel.Models.Rules.PointHistoryGeneralOptions;
using static DataExporter.PointHistory;
using TechneauxUtility;
using Serilog.Core;
using Serilog;

namespace DataExporter
{
    public static partial class PointHistory
    {
        public class PointValue
        {
            public string Value { get; }
            public DateTime Timestamp { get; }
            public short Status { get; }

            public PointValue(string value, DateTime timestamp, short status)
            {
                Value = value;
                Timestamp = timestamp;
                Status = status;
            }

            public double NumericValue
            {
                get
                {
                    return DataConvert.String_toDouble(Value);
                }
            }


            public bool GetStatus(PointValueTypes valType)
            {
                CXSCRIPTLib.GlobalFunctions CygGlob = new CXSCRIPTLib.GlobalFunctions();

                switch (valType)
                {
                    case PointValueTypes.IsInitialized:
                        return CygGlob.IsInitialized(Status);
                    case PointValueTypes.IsUnreliable:
                        return CygGlob.IsUnreliable(Status);
                    case PointValueTypes.IsUpdated:
                        return CygGlob.IsUpdated(Status);
                    case PointValueTypes.IsDigitalAlarm:
                        return CygGlob.IsDigitalAlarm(Status);
                    case PointValueTypes.IsDigitalChattering:
                        return CygGlob.IsDigitalChattering(Status);
                    case PointValueTypes.IsDigitalWarning:
                        return CygGlob.IsDigitalWarning(Status);
                    case PointValueTypes.IsHighAlarm:
                        return CygGlob.IsHighAlarm(Status);
                    case PointValueTypes.IsHighOutOfRange:
                        return CygGlob.IsHighOutOfRange(Status);
                    case PointValueTypes.IsHighWarning:
                        return CygGlob.IsHighWarning(Status);
                    case PointValueTypes.IsLowAlarm:
                        return CygGlob.IsLowAlarm(Status);
                    case PointValueTypes.IsLowOutOfRange:
                        return CygGlob.IsLowOutOfRange(Status);
                    case PointValueTypes.IsLowWarning:
                        return CygGlob.IsLowWarning(Status);
                    case PointValueTypes.IsHighDeviation:
                        return CygGlob.IsHighDeviation(Status);
                    case PointValueTypes.IsStringAlarm1:
                        return CygGlob.IsStringAlarm1(Status);
                    case PointValueTypes.IsStringAlarm2:
                        return CygGlob.IsStringAlarm2(Status);
                    case PointValueTypes.IsStringAlarm3:
                        return CygGlob.IsStringAlarm3(Status);
                    case PointValueTypes.IsStringAlarm4:
                        return CygGlob.IsStringAlarm4(Status);
                    case PointValueTypes.IsStringAlarm5:
                        return CygGlob.IsStringAlarm5(Status);
                    case PointValueTypes.IsStringAlarm6:
                        return CygGlob.IsStringAlarm6(Status);
                    default:
                        return false;
                }
            }
        }

        public enum HistDirection
        {
            Forward = 0,
            Reverse = 1,
        }

        public static ConcurrentDictionary<string, RealtimeClient> RealtimeClients = new ConcurrentDictionary<string, RealtimeClient>();

        public static PointValue GetUdcValue(
            string uisService,
            string facTag,
            string udc,
            string vhsService,
            bool allowUnreliable,
            HistoryTypes rollupType,
            DateTime earliestDate,
            DateTime latestDate,
            TimePeriod reportPeriod)
        {
            if (udc == "" || udc == null)
                return null;

            PointValue PntVal = null;
            string PointKey = PointCache.GetCacheKey(uisService, facTag, udc, vhsService, allowUnreliable, rollupType, earliestDate, latestDate);

            if (PointCache.TryGetPointFromCache(PointKey, out PntVal))
            {
                return PntVal;
            }

            string PointTagX = $"{facTag.Replace("::", ":")}_{udc}";

            if (rollupType == HistoryTypes.Value_Current)
                return GetCurrentValue(uisService, PointTagX);
            else
                PntVal = GetHistory(vhsService, PointTagX, allowUnreliable, rollupType, earliestDate, latestDate, reportPeriod);

            PointCache.AddPointToCache(PointKey, PntVal);

            return PntVal;
        }

        private static ushort? CurrentDomain = null;

        public static PointValue GetCurrentValue(string siteServ, string pointTag)
        {
            PointValue PntValue = null;

            try
            {
                if (!CurrentDomain.HasValue)
                {
                    ServiceInformation ServInfo = new ServiceInformation();

                    CurrentDomain = ServInfo.GetAmbientDomain();

                    //MessageBox.Show($"Current Domain: {CurrentDomain}");
                }

                if (!RealtimeClients.ContainsKey(siteServ))
                {
                    RealtimeClients.TryAdd(siteServ, new RealtimeClient(new DomainSiteService($"[{CurrentDomain}]{siteServ}")));
                }

                RealtimeClient ThisCvsClient = RealtimeClients[siteServ];

                PointTag ThisTag = new PointTag(pointTag);

                if (ThisCvsClient.PointExists(ThisTag))
                {
                    PointValueRecord ThisValue = ThisCvsClient.GetPointValueRecord(ThisTag);

                    string CurStrValue = ((string)ThisValue.Value).Trim();
                    if (CurStrValue == "") { CurStrValue = null; };

                    if (CurStrValue != null)
                    {
                        PntValue = new PointValue(CurStrValue, ThisValue.Timestamp, (short)ThisValue.Status);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error getting current value");
                PntValue = null;
            }
            finally { }

            return PntValue;
        }


        public static PointValue GetHistory(
            string vhsService,
            string pointTag,
            bool allowUnreliable,
            HistoryTypes rollupType,
            DateTime earliestTime,
            DateTime latestTime,
            TimePeriod reportPeriod)
        {
            PointValue ThisValue = null;

            switch (rollupType)
            {
                case HistoryTypes.First_AfterRollupPeriodBegins:
                    ThisValue = GetHistVal(vhsService, pointTag, allowUnreliable, earliestTime, latestTime, HistDirection.Forward);
                    break;

                case HistoryTypes.First_BeforeRollupPeriodEnds:
                    ThisValue = GetHistVal(vhsService, pointTag, allowUnreliable, earliestTime, latestTime, HistDirection.Reverse);
                    break;

                case HistoryTypes.First_AfterRollupPeriodEnds:
                    ThisValue = GetHistVal(vhsService, pointTag, allowUnreliable, earliestTime.AddDays(1), latestTime.AddDays(1), HistDirection.Forward);
                    break;

                case HistoryTypes.Value_AtRollupPeriodHour:
                    ThisValue = GetHistVal(vhsService, pointTag, allowUnreliable, earliestTime, earliestTime.AddSeconds(1), HistDirection.Forward);
                    break;

                case HistoryTypes.Value_AtLastRollupPeriodHour:
                    ThisValue = GetHistVal(vhsService, pointTag, allowUnreliable, earliestTime.AddDays(-1), earliestTime.AddDays(-1).AddSeconds(1), HistDirection.Forward);
                    break;

                case HistoryTypes.Value_AtNextRollupPeriodHour:
                    ThisValue = GetHistVal(vhsService, pointTag, allowUnreliable, earliestTime.AddDays(1), earliestTime.AddDays(1).AddSeconds(1), HistDirection.Forward);
                    break;

                case HistoryTypes.RollupPeriod_CalcWeightedAvg:
                case HistoryTypes.RollupPeriod_CalcMean:
                case HistoryTypes.RollupPeriod_CalcMin:
                case HistoryTypes.RollupPeriod_CalcMax:
                case HistoryTypes.RollupPeriod_CalcDelta:
                    //CygnetVhsRollups VhsRollup = (CygnetVhsRollups)Enum.Parse(typeof(CygnetVhsRollups), rollupType.ToString());
                    ThisValue = GetHistRollupValue(vhsService, pointTag, allowUnreliable, rollupType, earliestTime, latestTime, reportPeriod);
                    break;
                case HistoryTypes.First_BeforeRollupPeriodEndsExtend:
                    ThisValue = GetHistVal(vhsService, pointTag, allowUnreliable, DateTime.MinValue, latestTime, HistDirection.Reverse);
                    break;

                default:
                    ThisValue = null;
                    break;
            }

            return ThisValue;
        }

        public static List<PointValue> GetHistoryValues(
            string vhsService,
            string pointTag,
            DateTime earliestTime,
            DateTime latestTime,
            bool allowUnreliable)
        {
            CxVhsLib.ValueIterator vhsIter = new CxVhsLib.ValueIterator();
            CxVhsLib.ValueEntryEx HistEntry = new CxVhsLib.ValueEntryEx();
            CXSCRIPTLib.GlobalFunctions globalObj = new CXSCRIPTLib.GlobalFunctions();

            try
            {
                if (vhsIter.Initialize(vhsService, pointTag, earliestTime, latestTime, 0) == 0)
                {
                    //LogEvent("Missing Point", pointTag + " is not found or does not have history.");
                    return null;
                }
                vhsIter.MoveFirst();
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                Log.Error(ex, "Error getting history values");
                return null;
            }

            List<PointValue> HistValues = new List<PointValue>();

            while (vhsIter.GetForwardEx(HistEntry) == 1)
            {
                string curValue = HistEntry.Value.Trim();
                if (HistEntry.TimeStamp > earliestTime && HistEntry.TimeStamp < latestTime && DataConvert.IsNumeric(curValue))
                {
                    if (allowUnreliable || !globalObj.IsUnreliable(HistEntry.Status))
                    {
                        HistValues.Add(new PointValue(curValue, HistEntry.TimeStamp, HistEntry.Status));
                    }
                }
            }
            return HistValues;
        }

        public static PointValue GetHistRollupValue(
            string vhsService,
            string pointTag,
            bool allowUnreliable,
            HistoryTypes rollupType,
            DateTime earliestTime,
            DateTime latestTime,
            TimePeriod reportPeriod)
        {
            double Hour20PercentOffset = (latestTime - earliestTime).TotalHours * .2;

            DateTime AdjEarliestDate = earliestTime.AddHours(-Hour20PercentOffset);
            DateTime AdjLatestDate = latestTime.AddHours(Hour20PercentOffset);

            HistoryCache.AddHistoryToCache(
                vhsService,
                pointTag,
                reportPeriod.Start,
                reportPeriod.End,
                allowUnreliable);

            List<PointValue> HistValues = null;
            if (!HistoryCache.TryGetHistoryFromCache(vhsService, pointTag, AdjEarliestDate, AdjLatestDate, allowUnreliable, out HistValues))
            {
                HistValues = GetHistoryValues(vhsService, pointTag, AdjEarliestDate, AdjLatestDate, allowUnreliable);
            }

            var ValuesInRange = HistValues.Where(item => item.Timestamp >= earliestTime && item.Timestamp <= latestTime);

            if (!HistValues.Any() || !ValuesInRange.Any())
            {
                return null;
            }

            switch (rollupType)
            {
                case HistoryTypes.RollupPeriod_CalcWeightedAvg:
                case HistoryTypes.RollupPeriod_CalcMean:
                    List<PointValue> ValueSubset = new List<PointValue>();

                    if (ValuesInRange.First().Timestamp > earliestTime && HistValues.Any(item => item.Timestamp < earliestTime))
                    {
                        PointValue PreceedingValue = HistValues.Last(item => item.Timestamp < earliestTime);

                        if ((ValuesInRange.First().Timestamp - earliestTime).TotalMinutes > .5 * (earliestTime - PreceedingValue.Timestamp).TotalMinutes)
                        {
                            ValueSubset.Add(new PointValue(PreceedingValue.Value, earliestTime, PreceedingValue.Status));
                        }
                    }

                    ValueSubset.AddRange(ValuesInRange);

                    if (rollupType == HistoryTypes.RollupPeriod_CalcMean)
                    {
                        double Avg = ValueSubset.Average(item => item.NumericValue);

                        return new PointValue(Avg.ToString(), latestTime, 0);
                    }
                    else
                    {
                        double RunningSum = 0;

                        for (int i = 0; i < ValueSubset.Count - 1; i++)
                        {
                            RunningSum += ValueSubset[i].NumericValue * (ValueSubset[i + 1].Timestamp - ValueSubset[i].Timestamp).TotalMinutes;
                        }

                        RunningSum += ValueSubset.Last().NumericValue * (latestTime - ValueSubset.Last().Timestamp).TotalMinutes;

                        double Avg = RunningSum / (latestTime - ValueSubset.First().Timestamp).TotalMinutes;

                        return new PointValue(Avg.ToString(), latestTime, 0);
                    }

                case HistoryTypes.RollupPeriod_CalcMin:
                    double MinValue = ValuesInRange.Min(item => item.NumericValue);
                    return HistValues.First(item => item.NumericValue == MinValue);

                case HistoryTypes.RollupPeriod_CalcMax:
                    double MaxValue = ValuesInRange.Max(item => item.NumericValue);
                    return HistValues.First(item => item.NumericValue == MaxValue);

                case HistoryTypes.RollupPeriod_CalcDelta:
                    MaxValue = ValuesInRange.Max(item => item.NumericValue);
                    MinValue = ValuesInRange.Min(item => item.NumericValue);

                    PointValue MaxPoint = HistValues.First(item => item.NumericValue == MaxValue);

                    return new PointValue((MaxValue - MinValue).ToString(), latestTime, MaxPoint.Status);

                default:
                    // Invalid rollup type, throw exception
                    throw new Exception("Can't get a rollup value from a non-rollup history type");
            }
        }

        public static PointValue GetHistVal(
          string vhsService,
          string pointTag,
          bool allowUnreliable,
          DateTime earliestTime,
          DateTime latestTime,
          HistDirection iterDirection)
        {
            // Get value after the latest date (in order to pull in a YDY point and have it aligned with the day it actually occurred)

            CxVhsLib.ValueEntryEx HistEntry = new CxVhsLib.ValueEntryEx();
            CxVhsLib.ValueIterator vhsIter = new CxVhsLib.ValueIterator();
            CXSCRIPTLib.GlobalFunctions globalObj = new CXSCRIPTLib.GlobalFunctions();

            PointValue ThisValue = null;

            if (vhsIter.Initialize(vhsService, pointTag, earliestTime, latestTime, 0) == 0)
            {
                Log.Error("Missing Point", pointTag + " is not found or does not have history.");
                return null;
            }

            try
            {
                if (iterDirection == HistDirection.Forward)
                {

                    vhsIter.MoveFirst();
                }
                else
                {
                    vhsIter.MoveLast();
                }
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                Log.Error(ex, $"Error in {nameof(GetHistVal)}");
                return null;
            }

            while ((iterDirection == HistDirection.Forward) ?
                vhsIter.GetForwardEx(HistEntry) == 1 :
                vhsIter.GetBackwardEx(HistEntry) == 1)
            {
                if (HistEntry.TimeStamp >= earliestTime && HistEntry.TimeStamp <= latestTime)
                {
                    string curValue = HistEntry.Value.Trim();
                    if (curValue == "")
                        curValue = null;

                    if (allowUnreliable)
                    {
                        ThisValue = new PointValue(curValue, HistEntry.TimeStamp, HistEntry.Status);
                        break;
                    }
                    else
                    {
                        if (globalObj.IsUnreliable(HistEntry.Status))
                        {
                            ThisValue = null;
                        }
                        else
                        {
                            ThisValue = new PointValue(curValue, HistEntry.TimeStamp, HistEntry.Status);
                            break;
                        }
                    }
                }
            }

            return ThisValue;
        }
    }
}