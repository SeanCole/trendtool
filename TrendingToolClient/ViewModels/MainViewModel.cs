using System.Collections.Generic;
using System.Threading.Tasks;
using TrendingToolClient.SqlHistorySync.ViewModels;
using TrendingToolClient.Utility;
using TrendingToolClient.ViewModels.CygNet.Reactive;
using Techneaux.CygNetWrapper.Points;
using TechneauxReportingDataModel.General;
using XmlDataModelUtility;

namespace TrendingToolClient.ViewModels
{
    public class MainViewModel : NotifyDynamicBase<ReportConfigModel>
    {

        public List<ICachedCygNetPoint> PointList
        {
            get => GetPropertyValue<List<ICachedCygNetPoint>>();
            set => SetPropertyValue(value);
        }

        public CygNetFacilityPointSearchViewModel ReactiveCygNetViewModel
        {
            get => GetPropertyValue<CygNetFacilityPointSearchViewModel>();
            set => SetPropertyValue(value);
        }

        public TrendGeneralViewModel ReactiveTrendGeneralViewModel
        {
            get => GetPropertyValue<TrendGeneralViewModel>();
            set => SetPropertyValue(value);
        }

        public MainViewModel(ReportConfigModel srcModel) : base(srcModel)
        {
            ReactiveCygNetViewModel = new CygNetFacilityPointSearchViewModel(srcModel.CygNetGeneral);
            ReactiveTrendGeneralViewModel = new TrendGeneralViewModel(srcModel.TrendGeneral);
            PointSelectionVm = new PointTrendSelectionViewModel(ReactiveCygNetViewModel, MyDataModel.SourcePointRules);
            LivePreviewVm = new LiveTrendPreviewViewModel(PointSelectionVm, ReactiveTrendGeneralViewModel ,srcModel);
                          
        }
        public LiveTrendPreviewViewModel LivePreviewVm
        {
            get => GetPropertyValue<LiveTrendPreviewViewModel>();
            set => SetPropertyValue(value);
        }

        public PointTrendSelectionViewModel PointSelectionVm
        {
            get => GetPropertyValue<PointTrendSelectionViewModel>();
            set => SetPropertyValue(value);
        }


    }
}