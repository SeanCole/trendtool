using System.Collections.Generic;
using System.Linq;
using TechneauxHistorySynchronization.SqlServerHistory;
using XmlDataModelUtility;

namespace TrendingToolClient.ViewModels
{
    public class SqlRowViewModel : NotifyModelBase
    {
        public MappedSqlRowCompare MyDataModel
        { get; }
        public SqlRowViewModel(MappedSqlRowCompare srcModel)
        {
            MyDataModel = srcModel;
         
            SqlValuesList = srcModel.CellValues.ToDictionary(item => item.Key, item => item.Value.StringValue);
                   
            Comparison = srcModel.ComparisonValue;
        }

        public MappedSqlRowCompare.CygSqlCompare Comparison
        {
            get => GetPropertyValue<MappedSqlRowCompare.CygSqlCompare>();
            set => SetPropertyValue(value);
        }
        
        public Dictionary<string, string> SqlValuesList
        {
            get => GetPropertyValue<Dictionary<string, string>>();
            set => SetPropertyValue(value);
        }
    }
}
