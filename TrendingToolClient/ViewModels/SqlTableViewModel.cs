using System.Collections.Generic;
using Microsoft.SqlServer.Management.Smo;
using XmlDataModelUtility;

namespace TrendingToolClient.ViewModels
{
    public class SqlTableViewModel : NotifyModelBase
    {     
        public SqlTableViewModel(Table srcModel)
        {
            Name = srcModel.Name;
            SqlColumnSubViewModels = new List<SqlColumnViewModel>();

            return;
            foreach (Column col in srcModel.Columns)
            {
                SqlColumnSubViewModels.Add(new SqlColumnViewModel(col));
            }
        }

        public string Name
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
        public List<SqlColumnViewModel> SqlColumnSubViewModels
        {
            get => GetPropertyValue<List<SqlColumnViewModel>>();
            set => SetPropertyValue(value);
        }
    }
}
