using TechneauxReportingDataModel.Helper;

namespace TrendingToolClient.ViewModels
{
    public class SinglePointCompareSummaryViewModel// : ReactiveUI.ReactiveObject*/
    {
        public CachedCygNetPointWithRule MyDataModel
        {
            get;set;
        }

        public SinglePointCompareSummaryViewModel(CachedCygNetPointWithRule srcModel)
        {
            Udc = srcModel.Tag.UDC;
            MyDataModel = srcModel;
            
            FacilityTagUdc = srcModel.Tag.GetTagFacilityUDC();
            ShortId = srcModel.ShortId;
            


        }

        //ObservableAsPropertyHelper<>

        public string Udc { get; }

        public string FacilityTagUdc { get; }

        public string ShortId { get; }

        public int TotalRaw { get; private set; }

        public int TotalNorm { get; private set; }

        public double HistDataInSql { get; private set; }
    }
}
