using System.Windows;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using XmlDataModelUtility;

namespace TrendingToolClient.ViewModels
{
    public class PointHistoryViewModel : NotifyDynamicBase<PointHistoryGeneralOptions>
    {
        public PointHistoryViewModel(PointHistoryGeneralOptions srcModel) : base(srcModel)
        {
            IsExcludeBlanksVisible = Visibility.Visible;
            IsDefVisible = Visibility.Visible;
        }

        [AffectedByOtherPropertyChange(nameof(PointHistoryGeneralOptions.BlankValueHandlingType))]
        public Visibility IsDefVisible
        {
            get
            {
                var tempRule = (PointHistoryGeneralOptions)MyDataModel;
                if (tempRule.BlankValueHandlingType == PointHistoryGeneralOptions.BlankValueHandlingTypes.ReplaceWithDefault)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        public Visibility IsExcludeBlanksVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }


        public Visibility IsOnlyNumericVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
    }
}