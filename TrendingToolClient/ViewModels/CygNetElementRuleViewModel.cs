using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows;
using CygNetRuleModel.Models;
using TrendingToolClient.Forms;
using ReactiveUI;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Facilities.Attributes;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.FAC;
using Techneaux.CygNetWrapper.Summary;
using TechneauxReportingDataModel.CygNet;
using TechneauxReportingDataModel.General;
using XmlDataModelUtility;
using TechneauxReportingDataModel.CygNet.Rules;

namespace TrendingToolClient.ViewModels
{
    public class CygNetElementRuleViewModel : NotifyDynamicBase<CygNetRuleBase>
    {


        public CygNetElementRuleViewModel(CygNetRuleBase srcModel) : base(srcModel)
        {
            IsFacilityAttributeSelectionVisible = Visibility.Visible;
            IsHistoryTypeVisible = Visibility.Visible;
            IsNoteHistoryTypeVisible = Visibility.Collapsed;
            IsNoteTypeVisible = Visibility.Collapsed;
            IsAttributeTypeVisible = Visibility.Visible;
            IsPointValueTypeVisible = Visibility.Visible;
            //IsChildGroupVisible = Visibility.Visible;
            IsNoteValueTypeVisible = Visibility.Collapsed;
            IsNoteHistoryTypeVisible = Visibility.Collapsed;
            IsNoteTypeVisible = Visibility.Collapsed;
            IsUnreliableVisible = Visibility.Visible;
            IsNoteValueTypeVisible = Visibility.Collapsed;
           
            IsFacilityNoteSelectionVisible = Visibility.Collapsed;
            IsPointNoteSelectionVisible = Visibility.Collapsed;
           
            IsPointAttributeSelectionVisible = Visibility.Visible;
            IsFacilityNoteSelectionVisible = Visibility.Collapsed;
            IsIdVisible = Visibility.Visible;
            IsPointNoteSelectionVisible = Visibility.Collapsed;

        }
        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsAttributeTypeVisible
        {
            get
            {
                if (MyDataModel.DataElement == CygNetRuleBase.DataElements.FacilityAttribute ||
                    MyDataModel.DataElement == CygNetRuleBase.DataElements.PointAttribute)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsHistoryTypeVisible
        {
            get
            {
                if (MyDataModel.DataElement == CygNetRuleBase.DataElements.PointHistory || MyDataModel.DataElement == CygNetRuleBase.DataElements.PointCurrentValue)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsIdVisible
        {
            get
            {
                if (MyDataModel.DataElement == CygNetRuleBase.DataElements.PointAttribute ||
                    MyDataModel.DataElement == CygNetRuleBase.DataElements.FacilityAttribute)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsPointValueTypeVisible
        {
            get
            {
                if (MyDataModel.DataElement == CygNetRuleBase.DataElements.PointHistory || MyDataModel.DataElement == CygNetRuleBase.DataElements.PointCurrentValue)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsFacilityAttributeSelectionVisible
        {
            get
            {
                if (
                    MyDataModel.DataElement == CygNetRuleBase.DataElements.FacilityAttribute)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsPointAttributeSelectionVisible
        {
            get
            {
                if (
                    MyDataModel.DataElement == CygNetRuleBase.DataElements.PointAttribute)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsUnreliableVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsNoteTypeVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsFacilityNoteSelectionVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsPointNoteSelectionVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsNoteValueTypeVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsNoteHistoryTypeVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

    }
}