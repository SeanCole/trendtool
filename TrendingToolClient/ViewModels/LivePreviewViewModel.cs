﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Navigation;
using TrendingToolClient.Forms;
using TrendingToolClient.SqlHistorySync.ViewModels;
using ReactiveUI;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.SqlHistory;

namespace TrendingToolClient.ViewModels
{
    public class LivePreviewViewModel : ReactiveObject
    {
        private SqlHistorySyncConfigModel _srcConfigModel;
        private PointSelectionViewModel _pointSelectionVm;
        private SqlGeneralViewModelReactive _sqlSelectionVm;

        private bool _firstPointSelectionFlag = false;
        public bool FirstPointSelectionFlag
        {
            get => _firstPointSelectionFlag;
            set => this.RaiseAndSetIfChanged(ref _firstPointSelectionFlag, value);
        }

        public LivePreviewViewModel(
            PointSelectionViewModel pointSelectionVm,
            SqlGeneralViewModelReactive sqlSelectionVm,
            SqlHistorySyncConfigModel srcConfigModel)
        {
            _srcConfigModel = srcConfigModel;
            _pointSelectionVm = pointSelectionVm;
            _sqlSelectionVm = sqlSelectionVm;

            _summaryList = _pointSelectionVm.WhenAnyValue(x => x.PointList)
                .Select(x => x.Select(pnt => new SinglePointCompareSummaryViewModel(pnt)).ToList())
                .ToProperty(this, x => x.SummaryList, new List<SinglePointCompareSummaryViewModel>());

            LivePointCompareVm = new LivePreviewPointSubViewModel(
                this,
                sqlSelectionVm,
                pointSelectionVm.PointSelectionConfigRuleList,
                _srcConfigModel.TableMappingRules);

            //this.WhenAnyValue(me => me.SelectedPoint, me => me.FirstPointSelectionFlag)
            //    .Subscribe(pnt => LivePointCompareVM.SrcPoint = pnt.Item1?.MyDataModel);

            //.Select(x => new LivePreviewPointSubViewModel(x, sqlSelectionVM, SrcConfigModel.TableMappingRules))
            //ToProperty(this, x => x.LivePointCompareVM);


            //var SelectedPntChanged = this.WhenAnyValue(me => me.SelectedPoint.MyDataModel, me => me.SelectedPoint.MyDataModel.SrcPntRule).Publish().RefCount();
            ////_CurrentCompareModel = SelectedPntChanged.Select(pnt => new CygNetSqlDataCompareModel(srcConfigModel, pnt.Item1))
            ////    .ToProperty(this, x => x.CurrentCompareModel, new CygNetSqlDataCompareModel(SrcConfigModel));


            //SelectedPntChanged.Subscribe(x => Sync.Execute().Subscribe());
            _isSummaryPreviewBusy = pointSelectionVm.PointListUpdateCmd.IsExecuting.ToProperty(this, x => x.IsSummaryPreviewBusy, false);

            //CurrentCompareModel = new CygNetSqlDataCompareModel(SrcConfigModel, SelectedPoint.MyDataModel);
            //_IsFacilitiesBusy = UpdateFacilitySearchResults.IsExecuting.ToProperty(this, x => x.IsFacilitiesBusy, false);
            //MyDataModel = srcConfigModel;
            //_UdcList = Observable.FromEventPattern<ListChangedEventHandler, ListChangedEventArgs>
            //    (h => srcPointRules.ListChanged += h, h => srcPointRules.ListChanged -= h)
            //    .Where(args => args.EventArgs.ListChangedType == ListChangedType.ItemAdded ||
            //            args.EventArgs.ListChangedType == ListChangedType.ItemDeleted ||
            //            args.EventArgs.ListChangedType == ListChangedType.ItemChanged)
            //    .Select(args => new HashSet<string>((args.Sender as BindingList<SqlPointSelection>)
            //                    .Select(p => p.PointSelection.UDC)))
            //    .DistinctUntilChanged()
            //    .Sample(TimeSpan.FromSeconds(1))
            //    .Select(set => set.ToList())
            //    .ToProperty(this, x => x.UdcList, new List<string>());

            //var PointSrcDataChanged = this.WhenAnyValue(me => me.UdcList, me => me._SrcCygNetModel.CachedFacilities)
            //    .Select(args => (srcFacs: args.Item1, udcList: args.Item2))
            //    .Publish().RefCount();

            //var PointListUpdateCmd = ReactiveCommand
            //    .CreateFromObservable<(IEnumerable<ICachedCygNetFacility> srcFacs, IEnumerable<string> udcList), List<ICachedCygNetPoint>>
            //        (args => Observable.StartAsync(ct => GetPointList(args.srcFacs, args.udcList, ct))
            //    .TakeUntil(PointSrcDataChanged));

            //PointSrcDataChanged.Subscribe(_ => PointListUpdateCmd.Execute().Subscribe());

        }

        public ReactiveCommand SyncAll => ReactiveCommand.Create(() =>
        {
            AllPointPopupWindowSync.ShowPopup(_pointSelectionVm.PointList.ToList(), _srcConfigModel);
            LivePointCompareVm.ButtonClickedFlag = !LivePointCompareVm.ButtonClickedFlag;
        });

        private LivePreviewPointSubViewModel _livePointCompareVm;
        public LivePreviewPointSubViewModel LivePointCompareVm
        {
            get => _livePointCompareVm;
            set => this.RaiseAndSetIfChanged(ref _livePointCompareVm, value);
        }



        private readonly ObservableAsPropertyHelper<List<SinglePointCompareSummaryViewModel>> _summaryList;
        public List<SinglePointCompareSummaryViewModel> SummaryList => _summaryList.Value;

        private SinglePointCompareSummaryViewModel _selectedPoint;
        public SinglePointCompareSummaryViewModel SelectedPoint
        {
            get => _selectedPoint;
            set
            {
                var wasValSwitchedFromNull = _selectedPoint == null && value != null;

                this.RaiseAndSetIfChanged(ref _selectedPoint, value);

                if (wasValSwitchedFromNull)
                    FirstPointSelectionFlag = !FirstPointSelectionFlag;
            }

        }

        //private PointPreviewCompareViewModel _FacilityPointPreviewCompareViewModel;
        //public PointPreviewCompareViewModel FacilityPointPreviewCompareViewModel
        //{
        //    get { return _FacilityPointPreviewCompareViewModel; }
        //    set { this.RaiseAndSetIfChanged(ref _FacilityPointPreviewCompareViewModel, value); }
        //}

        //        //public ReactiveCommand<(ICachedCygNetPoint, DateTime, DateTime, bool), List<CygNetHistoryEntry>> UpdatePointPreview { get; protected set; }

        ObservableAsPropertyHelper<List<CygNetHistoryEntry>> _searchResults;
        public List<CygNetHistoryEntry> SearchResults => _searchResults.Value;

        //ObservableAsPropertyHelper<bool> _IsRawHistBusy;
        //public bool IsRawHistBusy => _IsRawHistBusy.Value;


        //ObservableAsPropertyHelper<bool> _IsNormHistBusy;
        //public bool IsNormHistBusy => _IsNormHistBusy.Value;


        ObservableAsPropertyHelper<bool> _isSummaryPreviewBusy;
        public bool IsSummaryPreviewBusy => _isSummaryPreviewBusy.Value;


    }

}


