using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TrendingToolClient.BusinessLogic.Helper;
using TechneauxHistorySynchronization.Helper;
using TechneauxHistorySynchronization.SqlServerHistory.DatabaseOperations;
using TechneauxReportingDataModel.Helper;
using TechneauxReportingDataModel.SqlHistory;
using XmlDataModelUtility;

namespace TrendingToolClient.ViewModels
{
    public class SyncAllPointsViewModel : NotifyModelBase
    {
        public List<CachedCygNetPointWithRule> SourcePoints
        {
            get => GetPropertyValue<List<CachedCygNetPointWithRule>>();
            set => SetPropertyValue(value);
        }
        public SqlHistorySyncConfigModel SourceConfigModel
        {
            get => GetPropertyValue<SqlHistorySyncConfigModel>();
            set => SetPropertyValue(value);
        }

        public SyncAllPointsViewModel(List<CachedCygNetPointWithRule> srcPoints, SqlHistorySyncConfigModel srcConfigModel)
        {
            SourceConfigModel = srcConfigModel;
            SourcePoints = srcPoints;

            MaxPointsCount = srcPoints.Count();
            CurrentPointCount = 0;
        }

        private readonly Stopwatch _sw = new Stopwatch();

        public async Task StartCompareOps(CancellationToken ct)
        {
            var schema = await SqlServerConnectionUtility.GetTableSchema(SourceConfigModel.SqlGeneralOpts);

            if (schema == null)
            {
                ProgressMessage = "Failed to read SQL Table Schema";
                return;
            }

            var newPointList = SourcePoints.Select(pnt => new SinglePointCompareOpsStatusSubViewModel(
                    schema.WithCygNetColumns(SourceConfigModel.TableMappingRules),
                    pnt,
                    SourceConfigModel))
                .ToList();

            PointList = newPointList;

            _sw.Start();

            try
            {
                await newPointList.ForEachAsync(10, async pnt =>
                  {
                      ct.ThrowIfCancellationRequested();

                      await Task.Run(() => pnt.CompareAndUpdate(ct), ct);
                      Interlocked.Increment(ref _currentPointCount);

                      CurrentPointCount = _currentPointCount;
                  });
            }
            catch (OperationCanceledException) { }
            catch (Exception ex)
            {
                Log.Error(ex, "Failure during sinking of all points");
                ProgressMessage = $@"Sync failed with exception: [{ex.Message}]";
            }

            //foreach (var pnt in NewPointList)
            //{
            //    await Task.Run ( ()=> pnt.CompareAndUpdate(ct));
            //}

            //for (int i = 0; i < NewPointList.Count; i += 10)
            //{
            //    var thisGroup = NewPointList.GetRange(i, Math.Min(NewPointList.Count - 1 - i, 10));

            //   await Task.Run(()=> Parallel.ForEach(
            //        thisGroup,
            //        new ParallelOptions { MaxDegreeOfParallelism = 2 },
            //        async pnt =>
            //   {
            //       await pnt.CompareAndUpdate(ct);
            //   }));
            //}
        }

        // Datasource for the listview in the window
        public List<SinglePointCompareOpsStatusSubViewModel> PointList
        {
            get => GetPropertyValue<List<SinglePointCompareOpsStatusSubViewModel>>();
            set => SetPropertyValue(value);
        }

        private double _minsRemaining;
        private int _currentPointCount;
        public int CurrentPointCount
        {
            get => _currentPointCount;
            set
            {
                if (value > MaxPointsCount)
                    return;

                var minsMessage = "";
                if (value > 0)
                {
                    _minsRemaining = _sw.Elapsed.TotalMinutes / value * (MaxPointsCount - value);
                    minsMessage = $"[{Math.Round(_minsRemaining,1)} minutes remaining]";
                }
                
                ProgressMessage = $"Completed {value}/{MaxPointsCount} points {minsMessage}";
                SetPropertyValue(value);
            }
        }

        public string ProgressMessage
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public int MaxPointsCount
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }
    }
}
