﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using TrendingToolClient.Forms;
using TrendingToolClient.SqlHistorySync.ViewModels.Sub;
using Microsoft.SqlServer.Management.Common;
using ReactiveUI;
using Serilog;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory.DatabaseOperations;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using TechneauxReportingDataModel.SqlHistory.SubOptions;

namespace TrendingToolClient.ViewModels
{
    public class SqlGeneralViewModelReactive : ReactiveObject
    {
        public SqlGeneralOptions SrcDataModel { get; }
               

        public SqlGeneralViewModelReactive(SqlGeneralOptions srcModel, BindingList<SqlTableMapping> tableMappingRules)
        {
            SrcDataModel = srcModel;
            DataOnlyModel = new SqlOptsViewModel(srcModel);

            PropertyChanged += SqlGeneralViewModelReactive_PropertyChanged;

            SqlFieldList = new RuleGridViewModel<SqlTableMapping, SqlTableMappingGridViewModel>(tableMappingRules);


            var getSelectedTableSchema = ReactiveCommand
                .CreateFromTask<SqlGeneralOptions, SimpleSqlTableSchema>(opts => SqlServerConnectionUtility.GetTableSchema(opts));
            var schemaOptsChanged = SrcDataModel.WhenAnyValue(
                        opts => opts.SelectedConnectionType,
                        opts => opts.Username,
                        opts => opts.Password,
                        opts => opts.ServerName,
                        opts => opts.DatabaseName,
                        opts => opts.TableName,
                        opts => opts.AuthenticationType)
                  .Select(_ => SrcDataModel)
                  .Throttle(TimeSpan.FromSeconds(1))
                  .InvokeCommand(getSelectedTableSchema);
            //var SchemaObservable = Observable.FromEventPattern<PropertyChangedEventHandler, PropertyChangedEventArgs>
            //    (h => SrcDataModel.PropertyChanged += h, h => SrcDataModel.PropertyChanged -= h)
            //    .Sample(TimeSpan.FromSeconds(.5))
            //    .Select(args => args.Sender as SqlGeneralOptions)
            //    .Subscribe


            _sqlSchema = getSelectedTableSchema
                .Select(sch => sch ?? SimpleSqlTableSchema.Empty())
                .ToProperty(this, x => x.SqlSchema, SimpleSqlTableSchema.Empty());

            _isSqlTableSchemaBusy = getSelectedTableSchema
                .IsExecuting
                .ToProperty(this, x => x.IsSqlTableSchemaBusy, true);

            _tableColumns = this.WhenAnyValue(me => me.SqlSchema)
                .Select(sch => sch.SqlColumns.Select(col => new SqlColumnViewModel(col)).ToList())
                .ToProperty(this, x => x.TableColumns, new List<SqlColumnViewModel>());

            // Get new dbs and tables with cancellation
            var sqlOptsChanged = SrcDataModel.WhenAnyValue(
                    opts => opts.SelectedConnectionType,
                    opts => opts.Username,
                    opts => opts.Password,
                    opts => opts.ServerName,
                    opts => opts.AuthenticationType).Publish().RefCount();

            //Observable.FromEventPattern<PropertyChangedEventHandler, PropertyChangedEventArgs>
            //    (h => SrcDataModel.PropertyChanged += h, h => SrcDataModel.PropertyChanged -= h)
            //    .Where(arg => arg.EventArgs.PropertyName != "DatabaseName" && arg.EventArgs.PropertyName != "TableName")
            //    .Sample(TimeSpan.FromSeconds(.5))
            //    .Select(args => (args.Sender as SqlGeneralOptions))
            //    .Publish().RefCount();

            var test = Observable.FromEventPattern<PropertyChangedEventHandler, PropertyChangedEventArgs>
                    (h => SrcDataModel.PropertyChanged += h, h => SrcDataModel.PropertyChanged -= h)
                .Where(arg => arg.EventArgs.PropertyName != "DatabaseName" && arg.EventArgs.PropertyName != "TableName")
                .Sample(TimeSpan.FromSeconds(.5)).Subscribe(arg => Console.WriteLine(arg.EventArgs.PropertyName));

            var updateTablesCmd = ReactiveCommand
                .CreateFromObservable(() => Observable
                    .StartAsync(ct => UpdateSqlTables(ct))
                .TakeUntil(sqlOptsChanged));

            sqlOptsChanged.Subscribe(_ => updateTablesCmd.Execute().Subscribe());

            _dbList = updateTablesCmd
                .ToProperty(this, x => x.DbList, new List<SqlDatabaseViewModel>());

            SqlFieldIdWindow.StaticDataContext = this;
            SqlFieldIdWindow.SqlRuleMappingList = tableMappingRules;

            _isBusyUpdatingTables = updateTablesCmd.IsExecuting.ToProperty(this, x => x.IsBusyUpdatingTables, false);

            // Choose db window
            //OpenDatabaseTableWindow = ReactiveCommand.Create(() =>
            //{
            //    var windowInstance = SqlDatabaseTableChooser.Instance();

            //    bool success = windowInstance.TryGetKeyDescId(out ValueDescription result);
            //    if (success && result.Value != null)
            //    {
            //        SrcDataModel.DatabaseName = result.Description as string;
            //        SrcDataModel.TableName = result.Value as string;
            //    }
            //});

            //SetServer();
        }

        private RuleGridViewModel<SqlTableMapping, SqlTableMappingGridViewModel> _sqlFieldList;
        public RuleGridViewModel<SqlTableMapping, SqlTableMappingGridViewModel> SqlFieldList
        {
            get => _sqlFieldList;
            set => this.RaiseAndSetIfChanged(ref _sqlFieldList, value);
        }

        private readonly ObservableAsPropertyHelper<List<SqlColumnViewModel>> _tableColumns;
        public List<SqlColumnViewModel> TableColumns => _tableColumns.Value;


        private readonly ObservableAsPropertyHelper<bool> _isSqlTableSchemaBusy;
        public bool IsSqlTableSchemaBusy => _isSqlTableSchemaBusy.Value;

        private readonly ObservableAsPropertyHelper<SimpleSqlTableSchema> _sqlSchema;
        public SimpleSqlTableSchema SqlSchema => _sqlSchema.Value;

        private void SqlGeneralViewModelReactive_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // for testing
        }

        //public static async 
        private SqlOptsViewModel _dataOnlyModel;
        public SqlOptsViewModel DataOnlyModel
        {
            get => _dataOnlyModel;
            set => this.RaiseAndSetIfChanged(ref _dataOnlyModel, value);
        }

        //function only runs at debug.
        [Conditional("DEBUG")]
        private void SetServer()
        {
            // runs at debug only
            SrcDataModel.ServerName = "Dwcyg19";
            SrcDataModel.AuthenticationType = SqlGeneralOptions.AuthenticationTypes.UserPassword;
            SrcDataModel.Username = "CygNet_Hist_SQL_dev";
            SrcDataModel.Password = "WpxEnergy2017D!@";
        }

        public async Task<List<SqlDatabaseViewModel>> UpdateSqlTables(CancellationToken ct)
        {
            try
            {
                var sw = new Stopwatch();
                sw.Start();

                var currentServer = await Task.Run(() => SqlServerConnectionUtility.GetConnectedServer(SrcDataModel, ct), ct);
                if (currentServer == null)
                {
                  throw new ConnectionFailureException("Failed to connect to SQL server");
                }
                var rawDbList = (await Task.Run(() => SqlServerConnectionUtility.GetDatabases(currentServer), ct));
                
                var constrDbList = await Task.Run(() =>
                { return new List<SqlDatabaseViewModel>(rawDbList.Select(rawDb => new SqlDatabaseViewModel(rawDb.Item1, rawDb.Item2)).ToList()); }, ct);

                Console.WriteLine($@"Table refresh took {sw.Elapsed.TotalSeconds} secs");
                SrcDataModel.ConnectionErrorMessage = "";
                return constrDbList.ToList();
            }
            catch (Exception ex)
            {
                Log.Debug(ex, "Failure updating tables. Exception: " + ex.Message);
                SrcDataModel.ConnectionErrorMessage = "Failure updating tables. Exception : " + ex.Message;
                return new List<SqlDatabaseViewModel>();
            }
        }
        
        private readonly ObservableAsPropertyHelper<bool> _isBusyUpdatingTables;
        public bool IsBusyUpdatingTables => _isBusyUpdatingTables.Value;


        private readonly ObservableAsPropertyHelper<List<SqlDatabaseViewModel>> _dbList;
        public List<SqlDatabaseViewModel> DbList => _dbList.Value;


        //public ReactiveCommand OpenDatabaseTableWindow { get; }

    }
}
