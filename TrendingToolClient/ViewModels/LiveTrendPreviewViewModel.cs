﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Navigation;
using TrendingToolClient.Forms;
using TrendingToolClient.SqlHistorySync.ViewModels;
using ReactiveUI;

using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.SqlHistory;
using TechneauxReportingDataModel.General;

namespace TrendingToolClient.ViewModels
{
    public class LiveTrendPreviewViewModel : ReactiveObject
    {
        private ReportConfigModel _srcConfigModel;
        private PointTrendSelectionViewModel _pointSelectionVm;
        private TrendGeneralViewModel _reactiveTrendViewModel;
        private List<string> AllList;


        private bool _firstPointSelectionFlag = false;
        public bool FirstPointSelectionFlag
        {
            get => _firstPointSelectionFlag;
            set => this.RaiseAndSetIfChanged(ref _firstPointSelectionFlag, value);
        }
        
        public LiveTrendPreviewViewModel(
            PointTrendSelectionViewModel pointSelectionVm,   
            TrendGeneralViewModel rxTrendVm,
            ReportConfigModel srcConfigModel)
        {
            SelectedFac = "ALL";
            AllList = new List<string>();
            AllList.Add("ALL");
            _reactiveTrendViewModel = rxTrendVm;
            _srcConfigModel = srcConfigModel;
            _pointSelectionVm = pointSelectionVm;

            _summaryList = _pointSelectionVm.WhenAnyValue(x => x.PointList)
                .Select(x => x.Select(pnt => new PointViewModel(pnt, rxTrendVm)).ToList())
                .ToProperty(this, x => x.SummaryList, new List<PointViewModel>());

            _uniqueFacList = this.WhenAnyValue(x => x.SummaryList)
                .Select(x => x.Select(pnt => pnt.MyDataModel.Facility.TagString).Distinct().ToList())
                .Do(y => y.Insert(0,"ALL"))
                .ToProperty(this, x => x.UniqueFacList, new List<string>(AllList));

            //_uniqueFacList = this.WhenAnyValue(x => x.UniqueFacList).Do(x => UniqueFacList.Add("ALL"))
            //    .ToProperty(this, x => x.UniqueFacList, new List<string>(AllList));

            var SelectedFacChanged = this
                .WhenAnyValue(x => x.SelectedFac)
                .Throttle(TimeSpan.FromSeconds(1), RxApp.MainThreadScheduler)
                .Subscribe(_ => SetPointListFac());

            var SelectedFacListChanged = this.WhenAnyValue(x => x.SummaryList)
                .Throttle(TimeSpan.FromSeconds(1), RxApp.MainThreadScheduler)
                .Subscribe(_ => SetPointListFac());
            LiveTrendVm = new LiveTrendViewModel(
                this,
                SummaryList,
                _reactiveTrendViewModel);

            //this.WhenAnyValue(me => me.SelectedPoint, me => me.FirstPointSelectionFlag)
            //    .Subscribe(pnt => LivePointCompareVM.SrcPoint = pnt.Item1?.MyDataModel);

            //.Select(x => new LivePreviewPointSubViewModel(x, sqlSelectionVM, SrcConfigModel.TableMappingRules))
            //ToProperty(this, x => x.LivePointCompareVM);

            //var SelectedPntChanged = this.WhenAnyValue(me => me.SelectedPoint.MyDataModel, me => me.SelectedPoint.MyDataModel.SrcPntRule).Publish().RefCount();
            ////_CurrentCompareModel = SelectedPntChanged.Select(pnt => new CygNetSqlDataCompareModel(srcConfigModel, pnt.Item1))
            ////    .ToProperty(this, x => x.CurrentCompareModel, new CygNetSqlDataCompareModel(SrcConfigModel));

            //SelectedPntChanged.Subscribe(x => Sync.Execute().Subscribe());
            _isSummaryPreviewBusy = pointSelectionVm.PointListUpdateCmd.IsExecuting.ToProperty(this, x => x.IsSummaryPreviewBusy, false);

            //CurrentCompareModel = new CygNetSqlDataCompareModel(SrcConfigModel, SelectedPoint.MyDataModel);
            //_IsFacilitiesBusy = UpdateFacilitySearchResults.IsExecuting.ToProperty(this, x => x.IsFacilitiesBusy, false);
            //MyDataModel = srcConfigModel;
            //_UdcList = Observable.FromEventPattern<ListChangedEventHandler, ListChangedEventArgs>
            //    (h => srcPointRules.ListChanged += h, h => srcPointRules.ListChanged -= h)
            //    .Where(args => args.EventArgs.ListChangedType == ListChangedType.ItemAdded ||
            //            args.EventArgs.ListChangedType == ListChangedType.ItemDeleted ||
            //            args.EventArgs.ListChangedType == ListChangedType.ItemChanged)
            //    .Select(args => new HashSet<string>((args.Sender as BindingList<SqlPointSelection>)
            //                    .Select(p => p.PointSelection.UDC)))
            //    .DistinctUntilChanged()
            //    .Sample(TimeSpan.FromSeconds(1))
            //    .Select(set => set.ToList())
            //    .ToProperty(this, x => x.UdcList, new List<string>());

            //var PointSrcDataChanged = this.WhenAnyValue(me => me.UdcList, me => me._SrcCygNetModel.CachedFacilities)
            //    .Select(args => (srcFacs: args.Item1, udcList: args.Item2))
            //    .Publish().RefCount();

            //var PointListUpdateCmd = ReactiveCommand
            //    .CreateFromObservable<(IEnumerable<ICachedCygNetFacility> srcFacs, IEnumerable<string> udcList), List<ICachedCygNetPoint>>
            //        (args => Observable.StartAsync(ct => GetPointList(args.srcFacs, args.udcList, ct))
            //    .TakeUntil(PointSrcDataChanged));

            //PointSrcDataChanged.Subscribe(_ => PointListUpdateCmd.Execute().Subscribe());
        }

        //public ReactiveCommand SyncAll => ReactiveCommand.Create(() =>
        //{
        //    AllPointPopupWindowSync.ShowPopup(_pointSelectionVm.PointList.ToList(), _srcConfigModel);
        //    LivePointCompareVm.ButtonClickedFlag = !LivePointCompareVm.ButtonClickedFlag;
        //});

        private LiveTrendViewModel _liveTrendVm;
        public LiveTrendViewModel LiveTrendVm
        {
            get => _liveTrendVm;
            set => this.RaiseAndSetIfChanged(ref _liveTrendVm, value);
        }

        private readonly ObservableAsPropertyHelper<List<string>> _uniqueFacList;

        public List<string> UniqueFacList => _uniqueFacList.Value;

        private ObservableAsPropertyHelper<List<PointViewModel>> _summaryList;
        public List<PointViewModel> SummaryList => _summaryList.Value;
       

        private string _selectedFac;
        public string SelectedFac
        {
            get => _selectedFac;
            set => this.RaiseAndSetIfChanged(ref _selectedFac, value);
        }

        private void SetPointListFac()
        {
            //if (SelectedFac == "ALL")
            //{
            //    foreach (var pnt in SummaryList)
            //    {


            //        pnt.IsChecked = false;
            //        pnt.IsVisibleForFac = true;
            //    }
            //}
            //else
            //{
                foreach (var pnt in SummaryList)
                {
                    //if (pnt.MyDataModel.Facility.TagString == SelectedFac)
                    //{
                        pnt.IsChecked = true;
                        pnt.IsVisibleForFac = true;
                    //}
                    //else
                    //{
                    //    pnt.IsChecked = false;
                    //    pnt.IsVisibleForFac = false;
                    //}
                }

            

        }
        private PointViewModel _selectedPoint;
        public PointViewModel SelectedPoint
        {
            get => _selectedPoint;
            set
            {
                var wasValSwitchedFromNull = _selectedPoint == null && value != null;

                this.RaiseAndSetIfChanged(ref _selectedPoint, value);

                if (wasValSwitchedFromNull)
                    FirstPointSelectionFlag = !FirstPointSelectionFlag;
            }

        }

        //private PointPreviewCompareViewModel _FacilityPointPreviewCompareViewModel;
        //public PointPreviewCompareViewModel FacilityPointPreviewCompareViewModel
        //{
        //    get { return _FacilityPointPreviewCompareViewModel; }
        //    set { this.RaiseAndSetIfChanged(ref _FacilityPointPreviewCompareViewModel, value); }
        //}

        //        //public ReactiveCommand<(ICachedCygNetPoint, DateTime, DateTime, bool), List<CygNetHistoryEntry>> UpdatePointPreview { get; protected set; }

        //ObservableAsPropertyHelper<List<CygNetHistoryEntry>> _searchResults;
        //public List<CygNetHistoryEntry> SearchResults => _searchResults.Value;

        //ObservableAsPropertyHelper<bool> _IsRawHistBusy;
        //public bool IsRawHistBusy => _IsRawHistBusy.Value;


        //ObservableAsPropertyHelper<bool> _IsNormHistBusy;
        //public bool IsNormHistBusy => _IsNormHistBusy.Value;


        ObservableAsPropertyHelper<bool> _isSummaryPreviewBusy;
        public bool IsSummaryPreviewBusy => _isSummaryPreviewBusy.Value;


    }

}


