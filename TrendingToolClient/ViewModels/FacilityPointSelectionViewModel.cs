using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows;
using CygNetRuleModel.Models;
using TrendingToolClient.Forms;
using ReactiveUI;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Facilities.Attributes;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.FAC;
using Techneaux.CygNetWrapper.Summary;
using TechneauxReportingDataModel.CygNet;
using System.Windows.Input;
using TechneauxReportingDataModel.General;
using XmlDataModelUtility;
using TechneauxReportingDataModel.CygNet.Rules;

namespace TrendingToolClient.ViewModels
{
    public class FacilityPointSelectionViewModel : NotifyDynamicBase<FacilityPointSelection>
    {


        public FacilityPointSelectionViewModel(FacilityPointSelection srcModel) : base(srcModel)
        {
            IsChildGroupVisible = Visibility.Visible;
            IsChildOrdVisible = Visibility.Visible;
        }
        public ICommand OpenChildGroupWindow => new DelegateCommand(OpenSelectChildGroupWindow);

        public void OpenSelectChildGroupWindow(object inputObject)
        {
            var windowInstance = CygNetChildFacilityWindow.Instance();

            var success = windowInstance.TryGetKeyDescId(out var result);
            if (success)
            {
                if (result.Value != null)
                {
                    MyDataModel.ChildGroupKey.Id = result.Value as string;

                }
                if (result.Description != null)
                {
                    MyDataModel.ChildGroupKey.Desc = result.Description as string;
                }
            }
        }
        [AffectedByOtherPropertyChange(nameof(FacilityPointSelection.FacilityChoice))]
        public Visibility IsChildOrdVisible
        {
            get
            {
                if (MyDataModel.FacilityChoice.Equals(FacilitySelection.FacilityChoices.ChildFacility))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(FacilityPointSelection.FacilityChoice))]
        public Visibility IsChildGroupVisible
        {
            get
            {
                if (MyDataModel.FacilityChoice.Equals(FacilitySelection.FacilityChoices.ChildFacility))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }


    }
}