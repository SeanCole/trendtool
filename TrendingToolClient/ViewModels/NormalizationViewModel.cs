using System.Windows;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using XmlDataModelUtility;

namespace TrendingToolClient.ViewModels
{
    public class NormalizationViewModel : NotifyDynamicBase<PointHistoryNormalizationOptions>
    {
        public NormalizationViewModel(PointHistoryNormalizationOptions srcModel) : base(srcModel)
        {
            IsEnableNormalizationVisible = Visibility.Visible;
            IsNormalizationAveragingWindowVisible = Visibility.Visible;
            IsWindowIntervalVisible = Visibility.Visible;
            IsWindowStartTimeVisible = Visibility.Visible;
            IsSamplingTypeVisible = Visibility.Visible;
            IsNumWinHistEntriesVisible = Visibility.Visible;
            IsEnableCurrentValueVisible = Visibility.Visible;
        }
    
        [AffectedByOtherPropertyChange(nameof(PointHistoryNormalizationOptions.EnableNormalization))]
        public Visibility IsEnableCurrentValueVisible
        {
            get
            {                
                if (MyDataModel.EnableNormalization == false)
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                else
                {
                    SetPropertyValue(Visibility.Visible);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(PointHistoryNormalizationOptions.EnableNormalization))]
        public Visibility IsWindowTimeVisible
        {
            get
            {
                if (MyDataModel.EnableNormalization == false)
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                else
                {
                    SetPropertyValue(Visibility.Visible);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(PointHistoryNormalizationOptions.EnableNormalization))]
        public Visibility IsNumWinHistEntriesVisible
        {
            get
            {
                if (MyDataModel.EnableNormalization == false)
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                else
                {
                    SetPropertyValue(Visibility.Visible);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(PointHistoryNormalizationOptions.EnableNormalization))]
        public Visibility IsWindowStartTimeVisible
        {
            get
            {

                if (MyDataModel.EnableNormalization == false)
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                else
                {
                    SetPropertyValue(Visibility.Visible);
                }

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(PointHistoryNormalizationOptions.EnableNormalization))]
        public Visibility IsSamplingTypeVisible
        {
            get
            {

                if (MyDataModel.EnableNormalization == false)
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                else
                {
                    SetPropertyValue(Visibility.Visible);
                }

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(PointHistoryNormalizationOptions.EnableNormalization))]
        public Visibility IsWindowIntervalVisible
        {
            get
            {

                if (MyDataModel.EnableNormalization == false)
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                else
                {
                    SetPropertyValue(Visibility.Visible);
                }

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
       
        public Visibility IsEnableNormalizationVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(PointHistoryNormalizationOptions.EnableNormalization))]
        [AffectedByOtherPropertyChange(nameof(PointHistoryNormalizationOptions.SelectedSampleType))]
        public Visibility IsNormalizationAveragingWindowVisible
        {
            get
            {

                if (MyDataModel.EnableNormalization == true &&
                    (MyDataModel.SelectedSampleType == PointHistoryNormalizationOptions.SamplingType.Weighted_Average ||
                    MyDataModel.SelectedSampleType == PointHistoryNormalizationOptions.SamplingType.Simple_Average))
                {
                    SetPropertyValue(Visibility.Visible);
                    
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

    }
}