using System;
using System.ComponentModel;
using Microsoft.SqlServer.Management.Smo;
using Serilog;
using XmlDataModelUtility;

namespace TrendingToolClient.ViewModels
{
    public class SqlDatabaseViewModel : NotifyModelBase
    {
        public SqlDatabaseViewModel(Database db, string errorMessage) 
        {
            Name = db.Name;
            SqlTableSubViewModel = new BindingList<SqlTableViewModel>();

            if (! db.IsAccessible)
                return;
            
            try
            {
                foreach (Table tbl in db.Tables)
                {
                    SqlTableSubViewModel.Add(new SqlTableViewModel(tbl));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error reading database");
            }

            ErrorMessage = errorMessage;
        }

        public SqlDatabaseViewModel()
        {
            ErrorMessage = "";
            
        }

        public string Name
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public BindingList<SqlTableViewModel> SqlTableSubViewModel
        {
            get => GetPropertyValue<BindingList<SqlTableViewModel>>();
            set => SetPropertyValue(value);
        }

        public string ErrorMessage
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

    }
}
