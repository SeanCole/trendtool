using System;
using System.Windows;
using Microsoft.SqlServer.Management.Smo;
using Serilog;
using XmlDataModelUtility;

namespace TrendingToolClient.ViewModels
{
    public class SqlColumnViewModel : NotifyModelBase
    {
        public SqlColumnViewModel(Column srcModel)
        {
            Name = srcModel.Name;
            Id = srcModel.ID.ToString();
            try
            {
                DataTypeName = srcModel.DataType.Name;
                DataTypeSize = srcModel.DataType.MaximumLength.ToString();

                if (srcModel.InPrimaryKey)
                {
                    IsPrimaryKeyVisible = Visibility.Visible;
                }
                else
                {
                    IsPrimaryKeyVisible = Visibility.Collapsed;
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex, "Error creating view modes for SQL columns");
                IsPrimaryKeyVisible = Visibility.Collapsed;
            }
        }

        public Visibility IsPrimaryKeyVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        public string Name
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public string Id
        {
            get => GetPropertyValue<string>();
            set { SetPropertyValue(value); }
        }

        public string DataTypeName
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public string DataTypeSize
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
    }
}
