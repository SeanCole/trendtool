using System;
using System.Windows.Media;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows;
using CygNetRuleModel.Models;
using TrendingToolClient.Forms;
using ReactiveUI;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Facilities.Attributes;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.FAC;
using Techneaux.CygNetWrapper.Summary;
using TechneauxReportingDataModel.CygNet;
using TechneauxReportingDataModel.General;

namespace TrendingToolClient.ViewModels
{
    public class TrendGeneralViewModel : ReactiveObject
    {
        public TrendGeneralOptions TrendOptsModel { get; }

        public TrendGeneralViewModel(TrendGeneralOptions trendOpts)
        {
            TrendOptsModel = trendOpts;
            _useNavBar = trendOpts.UseNavBar;
            _backgroundColor = trendOpts.BackgroundColor;
            _defaultNumDays = trendOpts.DefaultNumDays;
           
        }

        private int _signalRuleChange = 0;

        private int SignalRuleChange
        {
            get => _signalRuleChange;
            set => this.RaiseAndSetIfChanged(ref _signalRuleChange, value);
        }

        private Color _backgroundColor;
        public Color BackgroundColor
        {
            get => TrendOptsModel.BackgroundColor;
            set
            {
                this.RaiseAndSetIfChanged(ref _backgroundColor, value);
                TrendOptsModel.BackgroundColor = value;
                this.RaisePropertyChanged();
            }
        }

        private bool _useNavBar;
        public bool UseNavBar
        {
            get => TrendOptsModel.UseNavBar;
            set
            {
                this.RaiseAndSetIfChanged(ref _useNavBar, value);
                TrendOptsModel.UseNavBar = value;
                this.RaisePropertyChanged();
            }
        }
        private double _defaultNumDays;
        public double DefaultNumDays
        {
            get => TrendOptsModel.DefaultNumDays;
            set
            {
                this.RaiseAndSetIfChanged(ref _defaultNumDays, value);
                TrendOptsModel.DefaultNumDays = value;
                this.RaisePropertyChanged();
            }
        }

    }
}