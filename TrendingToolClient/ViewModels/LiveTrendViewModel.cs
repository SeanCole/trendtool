﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using TrendingToolClient.Forms;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading;
using System.Windows.Media;
using System.Threading.Tasks;
using System.Windows.Input;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using TechneauxHistorySynchronization.Helper;
using Serilog;
using static TechneauxHistorySynchronization.Models.CygNetSqlDataCompareModel;
using TechneauxHistorySynchronization.Models;
using TechneauxReportingDataModel.CygNet.Rules;
using Techneaux.CygNetWrapper.Services.VHS;
using CygNetRuleModel.Resolvers.History;
using TechneauxUtility;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using TechneauxReportingDataModel.General;
using TechneauxReportingDataModel.Helper;

namespace TrendingToolClient.ViewModels
{
    public class LiveTrendViewModel : ReactiveObject
    {
        private bool _firstPointSelectionFlag = false;
        private bool FirstPointSelectionFlag
        {
            get => _firstPointSelectionFlag;
            set => this.RaiseAndSetIfChanged(ref _firstPointSelectionFlag, value);
        }



        private TrendGeneralViewModel _trendVm;
        private LiveTrendPreviewViewModel _parentVm;

        public LiveTrendViewModel(
            LiveTrendPreviewViewModel parentVm,
            List<PointViewModel> pntSelectionRules,
            TrendGeneralViewModel trendOpts
           )
        {
            // UniqueTrends = new List<string>(pntSelectionRules.Select(x => x.TrendOptions.TrendNum).Distinct());
            //_srcPnt = srcPoint ?? throw new ArgumentNullException($"Source point [{nameof(srcPoint)}] must not be null");           
            //MappingRules = sqlMappingRules;
            // Point history

            _trendVm = trendOpts;
            _parentVm = parentVm;

            //var updateTrendCount = Observable.FromEventPattern<ListChangedEventHandler, ListChangedEventArgs>(
            //        h => pntSelectionRules.ListChanged += h,
            //        h => pntSelectionRules.ListChanged -= h)
            //    .Where(evt => evt.EventArgs.ListChangedType == ListChangedType.ItemChanged)
            //    .Select(evt => pntSelectionRules[evt.EventArgs.NewIndex])
            //    .Where(updatedRule => updatedRule.LastPropertyUpdated == nameof(PointHistorySelectionRule.TrendOptions))
            //    .Do(y => UniqueTrends = new List<string>(pntSelectionRules.Select(x => x.TrendOptions.TrendNum).Distinct()));



            //var pointHistRulesChanged = this
            //    .WhenAnyValue(me => me.SrcPntRule, me => me.FirstPointSelectionFlag)
            //    .Throttle(TimeSpan.FromSeconds(.8))
            //    .Publish()
            //    .RefCount();


            //var SelectedFacChanged = this
            //    .WhenAnyValue(x => x._parentVm.SelectedFac)
            //    .Throttle(TimeSpan.FromSeconds(1), RxApp.MainThreadScheduler)
            //    .Subscribe(_ => SetPointListFac());
                //.Publish()
                //.RefCount();

            // set summary list to correct visibility and selection
            //var setPointListCmd = ReactiveCommand
            //    .CreateFromObservable(() => Observable
            //        .Start(SetPointListFac)
            //        .TakeUntil(SelectedFacChanged));
            // Update CygNet history
            //var getNewCygNetHistoryCmd = ReactiveCommand
            //    .CreateFromObservable(()=> Observable
            //        .StartAsync(GetTrendHistoryAsync//remakepointlist)
            //        .TakeUntil(pointHistRulesChanged));

            //pointHistRulesChanged.Subscribe(_ => getNewCygNetHistoryCmd.Execute().Subscribe());
            //SelectedFacChanged.Subscribe(_ => setPointListCmd.Execute().Subscribe());


            _historyTrendVm1 = _parentVm.WhenAnyValue(x => x.SummaryList)
                .Select(x => new HistoryTrendToolViewModel(x.Where(y => y.MyDataModel.SrcRuleTrend.TrendOptions.TrendNum == "1").ToList()))
                .ToProperty(this, x => x.HistoryTrendVm1);

            _historyTrendVm2 = _parentVm.WhenAnyValue(x => x.SummaryList)
                .Select(x => new HistoryTrendToolViewModel(x.Where(y => y.MyDataModel.SrcRuleTrend.TrendOptions.TrendNum == "2").ToList()))
                .ToProperty(this, x => x.HistoryTrendVm2);

            _historyTrendVm3 = _parentVm.WhenAnyValue(x => x.SummaryList)
                .Select(x => new HistoryTrendToolViewModel(x.Where(y => y.MyDataModel.SrcRuleTrend.TrendOptions.TrendNum == "3").ToList()))
                .ToProperty(this, x => x.HistoryTrendVm3);

        }
        //public PropertyProgress<int> HistoryProgress { get; } = new PropertyProgress<int>(0);

        //private CachedCygNetPointWithRule _srcPnt;
        //public CachedCygNetPointWithRule SrcPoint
        //{
        //    get => _srcPnt;
        //    set => this.RaiseAndSetIfChanged(ref _srcPnt, value);
        //}

        //private ObservableAsPropertyHelper<PointHistorySelectionRule> _srcPntRule;
        //public PointHistorySelectionRule SrcPntRule => _srcPntRule.Value;

        //private ObservableAsPropertyHelper<BindingList<SqlTableMapping>> _mappingRules;
        //public BindingList<SqlTableMapping> MappingRules => _mappingRules.Value;

        private bool _buttonClickedFlag;
        public bool ButtonClickedFlag
        {
            get => _buttonClickedFlag;
            set => this.RaiseAndSetIfChanged(ref _buttonClickedFlag, value);
        }

        public List<string> UniqueTrends
        {
            get;
            set;
        }

        //private readonly ObservableAsPropertyHelper<CygNetHistoryResults> _cygHistory;
        //public CygNetHistoryResults CygHistory => _cygHistory.Value;

        //private readonly ObservableAsPropertyHelper<SqlCompareResults> _sqlCompare;
        //public SqlCompareResults SqlCompare => _sqlCompare.Value;



        //private readonly ObservableAsPropertyHelper<CygNetHistoryResults> _cygHistory;
        //public CygNetHistoryResults CygHistory => _cygHistory.Value;

        //private readonly ObservableAsPropertyHelper<SqlCompareResults> _sqlCompare;
        //public SqlCompareResults SqlCompare => _sqlCompare.Value;

        private readonly ObservableAsPropertyHelper<HistoryTrendToolViewModel> _historyTrendVm1;
        public HistoryTrendToolViewModel HistoryTrendVm1 => _historyTrendVm1.Value;

        private readonly ObservableAsPropertyHelper<HistoryTrendToolViewModel> _historyTrendVm2;
        public HistoryTrendToolViewModel HistoryTrendVm2 => _historyTrendVm2.Value;

        private readonly ObservableAsPropertyHelper<HistoryTrendToolViewModel> _historyTrendVm3;
        public HistoryTrendToolViewModel HistoryTrendVm3 => _historyTrendVm3.Value;

        //private readonly ObservableAsPropertyHelper<SimpleCombinedTableSchema> _currentTableSchema;
        //public SimpleCombinedTableSchema CurrentTableSchema => _currentTableSchema.Value;

        //private readonly ObservableAsPropertyHelper<IReactiveDerivedList<SqlRowViewModel>> _sqlCompareList;
        //public IReactiveDerivedList<SqlRowViewModel> SqlCompareList => _sqlCompareList.Value;

        private string _validationError;
        public string ValidationError
        {
            get => _validationError;
            set => this.RaiseAndSetIfChanged(ref _validationError, value);
        }

        private bool _isGetHistBusy;
        public bool IsGetHistBusy
        {
            get => _isGetHistBusy;
            set => this.RaiseAndSetIfChanged(ref _isGetHistBusy, value);
        }
        private bool _isCompareViewBusy;
        public bool IsCompareViewBusy
        {
            get => _isCompareViewBusy;
            set => this.RaiseAndSetIfChanged(ref _isCompareViewBusy, value);
        }

        public bool InverseIsCompareViewBusy
        {
            get => !_isCompareViewBusy;
        }

        //public ICommand SyncButton => new DelegateCommand(SyncTaskAsync);
        //public async void SyncTaskAsync(object inputObject)
        //{
        //    //fix later. make command for binding to button . currentcomparemodel.updatesqltable on button click
        //    //Task.Run(() => SqlSyncAsync());
        //    var (isValid, failureReason) = ValidateRulesDataOnly(CurrentTableSchema, CygHistory, _parentVm?.SelectedPoint?.MyDataModel);

        //    //var SqlCompareModel = new CygNetSqlDataCompareModel(SrcPoint);

        //    if (isValid)
        //    {
        //        SinglePointSyncPopup.ShowPopup(
        //            SqlCompareModel,
        //            CurrentTableSchema,
        //            SqlCompare.ComparedRows);
        //        ButtonClickedFlag = !ButtonClickedFlag;
        //    }

        //    ValidationError = failureReason;
        //}
        
        // Methods

        public class CygNetHistoryResults
        {
            public List<CygNetHistoryEntry> RawHistoryEntries { get; set; }
            public List<HistoryNormalization.NormalizedHistoryEntry> NormalizedHistoryEntries { get; set; }
            public bool IsNormalized => NormalizedHistoryEntries != null;
            public bool IsAnyHistory => RawHistoryEntries.IsAny();

            public DateTime EarliestTime { get; set; }
            public DateTime LatestTime { get; set; }

            public bool HasHistLaterThanRange { get; set; }
        }

    }
}