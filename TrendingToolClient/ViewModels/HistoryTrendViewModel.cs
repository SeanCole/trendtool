﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Media;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Animation;
using System.Xml.Serialization;
using LiveCharts.Geared;
using LiveCharts;
using LiveCharts.Defaults;
using TrendingToolClient.ViewModels;
using static TechneauxHistorySynchronization.Models.CygNetSqlDataCompareModel;
using LiveCharts.Wpf;
using Newtonsoft.Json;
using XmlDataModelUtility;

namespace TrendingToolClient.ViewModels
{
    public class HistoryTrendViewModel : ReactiveObject
    {
        public HistoryTrendViewModel(CygNetHistoryResults cygHist)
        {
            Series = null;
            ScrollSeries = null;

            From = DateTime.Now.AddDays(-1).Ticks;
            To = DateTime.Now.Ticks;
            var newSeriesCollection = new SeriesCollection();
            var newScrollSeriesCollection = new SeriesCollection();

            MinRange = TimeSpan.FromHours(1).Ticks;
            MaxRange = MinRange;

            if (!cygHist.IsAnyHistory)
            {
                Series = new SeriesCollection();
                return;
            }


           
            var rawPoints = cygHist
                .RawHistoryEntries
                .Where(val => val.IsNumeric)
                .Select(x => new DateTimePoint(x.AdjustedTimestamp,
                    x.NumericValue))
                .ToList();

            Console.WriteLine($"Raw point count = {rawPoints.Count}");

            //var path = $@"{Environment.CurrentDirectory}\PointHist.txt";
            //using (var sw = new StreamWriter(path))
            //{
            //    var str = JsonConvert.SerializeObject(cygHist);

            //    sw.Write(str);

            //    rawPoints.ForEach(pnt => sw.WriteLine($"{pnt.DateTime},{pnt.Value}"));
            //}


            //foreach (var rs in rawPoints)
            //{
            //    if (rs.DateTime > DateTime.Now.AddHours(1.5) || rs.DateTime < DateTime.Now.AddDays(-180))
            //        continue;
            //    if (double.IsInfinity(rs.Value) || double.IsNaN(rs.Value))
            //        continue;
            //}

            Console.WriteLine($"Max: {rawPoints.Max(e => e.Value)}");
            Console.WriteLine($"Min: {rawPoints.Min(e=> e.Value)}");
            Console.WriteLine($"Max Time: {rawPoints.Max(e => e.DateTime)}");
            Console.WriteLine($"Min Time: {rawPoints.Min(e => e.DateTime)}");


            var rawDataSeries = new GLineSeries
            {
                Values = rawPoints.AsGearedValues().WithQuality(Quality.Highest),
                Stroke = Brushes.Blue,
                Fill = Brushes.Transparent,
                Title = "Raw History"
            };
            newSeriesCollection.Add(rawDataSeries);


            var rawScrollDataSeries = new GLineSeries
            {
                Values = rawPoints.AsGearedValues().WithQuality(Quality.Highest),
                Fill = Brushes.Transparent,
                Stroke = Brushes.Blue,
                StrokeThickness = .5,
                PointGeometry = DefaultGeometries.None,
                Title = "Raw History"
            };
            newScrollSeriesCollection.Add(rawScrollDataSeries);


            if (cygHist.IsNormalized)
            {
                var normPoints = cygHist
                    .NormalizedHistoryEntries
                    .Where(val => val.IsValid && !val.IsValueUncertain)
                    .Select(val => val.NormalizedHistoryValue)
                    .Where(val => val.IsNumeric)

                    .Select(x => new DateTimePoint(x.AdjustedTimestamp,
                        x.NumericValue))
                    .ToList();

                var newNormSeries = new GLineSeries
                {
                    Values = normPoints.AsGearedValues().WithQuality(Quality.Highest),
                    Fill = Brushes.Transparent,
                    Stroke = Brushes.Red,
                    PointGeometry = DefaultGeometries.Triangle,
                    Title = "Normalized History"
                };

                var newScrollNormSeries = new GLineSeries
                {
                    Values = normPoints.AsGearedValues().WithQuality(Quality.Highest),
                    Fill = Brushes.Transparent,
                    PointGeometry = DefaultGeometries.None,
                    Stroke = Brushes.Red,
                    StrokeThickness = .5,
                    Title = "Normalized History"
                };

                newSeriesCollection.Add(newNormSeries);
                newScrollSeriesCollection.Add(newScrollNormSeries);
            }

            To = cygHist.RawHistoryEntries.Last().AdjustedTimestamp.Ticks;
            From = cygHist.RawHistoryEntries.Last().AdjustedTimestamp.AddDays(-1).Ticks;

            MinRange = TimeSpan.FromHours(1).Ticks;
            MaxRange = (cygHist.LatestTime - cygHist.EarliestTime).Ticks;
            
            Series = newSeriesCollection;
            ScrollSeries = newScrollSeriesCollection;

            XFormatter = val => new DateTime((long)val).ToString("M/dd h:mm t");
            YFormatter = val => val.ToString("0.00");

            
        }

        private SeriesCollection _series;
        public SeriesCollection Series
        {
            get => _series;
            set => this.RaiseAndSetIfChanged(ref _series, value);
        }

        private SeriesCollection _scrollSeries;
        public SeriesCollection ScrollSeries
        {
            get => _scrollSeries;
            set => this.RaiseAndSetIfChanged(ref _scrollSeries, value);
        }


        public ZoomingOptions ZoomingMode { get; } = ZoomingOptions.X;

        private double _to;
        public double To
        {
            get => _to;
            set => this.RaiseAndSetIfChanged(ref _to, value);
        }

        private double _from;
        public double From
        {
            get => _from;
            set => this.RaiseAndSetIfChanged(ref _from, value);
        }

        private double _minRange;
        public double MinRange
        {
            get => _minRange;
            set => this.RaiseAndSetIfChanged(ref _minRange, value);
        }

        private double _maxRange;
        public double MaxRange
        {
            get => _maxRange;
            set => this.RaiseAndSetIfChanged(ref _maxRange, value);
        }


        private Func<double, string> _xFormatter;
        public Func<double, string> XFormatter
        {
            get => _xFormatter;
            set => this.RaiseAndSetIfChanged(ref _xFormatter, value);
        }

        public Func<double, string> YFormatter { get; set; }

        public double Unit { get; } = TimeSpan.FromMinutes(5).Ticks;
    }

}
