using CygNetRuleModel.Resolvers;
using LiveCharts.Wpf;
using ReactiveUI;
using System.Threading;
using System.Windows.Media;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper;
using Techneaux.CygNetWrapper.Facilities;
using System.ComponentModel;

using Techneaux.CygNetWrapper.Points;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using TechneauxReportingDataModel.General;
using TechneauxReportingDataModel.Helper;
using System.Runtime.CompilerServices;
using System;
using TrendingToolClient.ViewModels;
using static TrendingToolClient.ViewModels.LiveTrendViewModel;
using Serilog;
using TechneauxReportingDataModel.CygNet.Rules;
using CygNetRuleModel.Resolvers.History;
using System.Linq;
using System.Reactive.Linq;
using LiveCharts.Geared;
using LiveCharts;
using LiveCharts.Defaults;
using Techneaux.CygNetWrapper.Services.VHS;
using System.Collections.Generic;
using System.Windows;

namespace TrendingToolClient.ViewModels
{
    public class PointViewModel : ReactiveObject
    {
        public CachedCygNetPointWithRule MyDataModel
        {
            get; set;
        }

        private TrendGeneralViewModel rxTrendVm;

        public Series rawDataSeries
        {
            get; set;
        }

        public Series rawScrollDataSeries
        {
            get; set;
        }

        public PointViewModel(CachedCygNetPointWithRule srcModel, TrendGeneralViewModel pntTrendOpts)
        {
            FacDesc = "";
            Udc = srcModel.Tag.UDC;
            MyDataModel = srcModel;
            CygHistory = new CygNetHistoryResults();
            rxTrendVm = pntTrendOpts;
            HasHistory = false;
            IsChecked = false;
            IsVisibleForFac = true;

            FacilityTagUdc = srcModel.Tag.GetTagFacilityUDC();
            ShortId = srcModel.ShortId;
            rawDataSeries = new GLineSeries
            {
                IsEnabled = false,
                Stroke = new SolidColorBrush(this.MyDataModel.SrcRuleTrend.TrendOptions.LineColor),
                Fill = Brushes.Transparent,
                Title = this.MyDataModel.SrcRuleTrend.TrendOptions.Label,
                ScalesYAt = (int)MyDataModel.SrcRuleTrend.TrendOptions.AxisId
            };
            //rawDataSeries .Visibility = System.Windows.Visibility.Hidden;

            rawScrollDataSeries = new GLineSeries
            {
                IsEnabled = false,
                Fill = Brushes.Transparent,
                Stroke = new SolidColorBrush(this.MyDataModel.SrcRuleTrend.TrendOptions.LineColor),
                StrokeThickness = .5,
                PointGeometry = DefaultGeometries.None,
                Title =  this.MyDataModel.SrcRuleTrend.TrendOptions.Label
            };
            GetDescription();




            //rawScrollDataSeries.Visibility = System.Windows.Visibility.Hidden;

            //var numDaysChanged = rxTrendVm
            //    .WhenAnyValue(x => x.DefaultNumDays)
            //    .Do(y => Console.WriteLine("trend days changed"))
            //    .Do(y =>
            //    {   if(IsVisibleForFac == true && IsChecked==true && HasHistory == true)
            //        {
            //            GetTrendHistoryAsync(new CancellationToken());

            //        }
            //         });
            //     if (!CygHistory.IsAnyHistory)
            //                {
            //                    rawDataSeries = new Series();
            //                    return;
            //                }

            //var rawPoints = cygHist
            //    .RawHistoryEntries
            //    .Where(val => val.IsNumeric)
            //    .Select(x => new DateTimePoint(x.AdjustedTimestamp,
            //        x.NumericValue))
            //    .ToList();

            var setSeries = this
                .WhenAnyValue(x => x.IsChecked, x => x.IsVisibleForFac)
                .Where(y => y.Item1 == true && y.Item2 == true)
                //.Do(x => Console.WriteLine("Reached history event"))
                .Subscribe(async z =>
                {
                    //if (z.Item1 && z.Item2)
                    await GetTrendHistoryAsync(CancellationToken.None);

                    //rawDataSeries.IsEnabled = z.Item1 && z.Item2;
                    //rawScrollDataSeries.IsEnabled = z.Item1 && z.Item2;
                });

            var setMainTrendEnabled = this
              .WhenAnyValue(x => x.IsChecked, x => x.IsVisibleForFac)
              .Select(vals => vals.Item1 && vals.Item2)
              //.Do(enab => Console.WriteLine($"Got selected value of  [{enab}]"))
              .Subscribe(enab =>
              {
                  rawDataSeries.IsEnabled = enab;
              });

            var setScrollTrendEnabled = this
             .WhenAnyValue(x => x.IsChecked, x => x.IsVisibleForFac)
             .Select(vals => vals.Item1 && vals.Item2)
              //.Do(enab => Console.WriteLine($"Got selected value of  [{enab}]"))
              .Subscribe(enab =>
              {
                  rawScrollDataSeries.IsEnabled = enab;
              });

            _rawPoints = this
                .WhenAnyValue(x => x.CygHistory)
                .Where(z => z.RawHistoryEntries != null)
                .Select(y => y.RawHistoryEntries.Where(val => val.IsNumeric).Select(z => new DateTimePoint(z.AdjustedTimestamp, z.NumericValue)).ToList())
                .ToProperty(this, u => u.rawPoints, new List<DateTimePoint>());

            this.WhenAnyValue(x => x.rawPoints)
                .Subscribe(x =>
                {
                    rawDataSeries.Values = rawPoints.AsGearedValues().WithQuality(Quality.Highest);
                    //rawDataSeries.IsEnabled = true;
                    rawScrollDataSeries.Values = rawPoints.AsGearedValues().WithQuality(Quality.Highest);
                    //rawScrollDataSeries.IsEnabled = true;
                });


            HistoryProgress = new Progress<int>(percent =>
            {
                HistoryProgressPercent = percent;
            });

        }

        public IProgress<int> HistoryProgress { get; set; }

        private string FacDesc { get; set; }

        private int _historyProgressPercent;
        public int HistoryProgressPercent
        {
            get => _historyProgressPercent;
            set => this.RaiseAndSetIfChanged(ref _historyProgressPercent, value);
        }

        private async void GetDescription()
        {
            FacPntDesc = "";
            var facDesc = await Task.Run(() => CygNetResolver.GetFacAttributeValue(MyDataModel.Facility as CachedCygNetFacility, "facility_desc",
                FacilityPointAttributeOptions.AttributeMetaType.Value));
            FacilityPointAttributeOptions tempAttr = new FacilityPointAttributeOptions();
            tempAttr.AttributeKey = new TechneauxUtility.IdDescKey();
            tempAttr.AttributeKey.Id = "Description";
            CancellationTokenSource ctSource = new CancellationTokenSource();
            var pntDesc = await Task.Run(() => CygNetResolver.GetPointAttributeValue(tempAttr, MyDataModel, ctSource.Token));
            if (facDesc.Error == CygNetResolver.ResolverErrorConditions.None && !facDesc.Value.HasError)
            {
                FacPntDesc = facDesc.Value.StringValue;
                FacDesc = facDesc.Value.StringValue;
            }
            if (!pntDesc.HasError)
            {
                FacPntDesc = FacPntDesc + "." + pntDesc.StringValue;
            }
            rawDataSeries.Title = FacDesc + "::" + this.MyDataModel.SrcRuleTrend.TrendOptions.Label;
            rawScrollDataSeries.Title = FacDesc + "::" + this.MyDataModel.SrcRuleTrend.TrendOptions.Label;
        }

        public string Udc { get; }

        private readonly ObservableAsPropertyHelper<List<DateTimePoint>> _rawPoints;
        public List<DateTimePoint> rawPoints => _rawPoints.Value;

        private string _facPntDesc;
        public string FacPntDesc
        {
            get => _facPntDesc;
            set => this.RaiseAndSetIfChanged(ref _facPntDesc, value);
        }

        private SemaphoreSlim _blockGetHist = new SemaphoreSlim(1, 1);
        private SemaphoreSlim _blockHasHist = new SemaphoreSlim(1, 1);

        //private async gethist{}
        private bool _hasHistory;
        //private SemaphoreSlim
        private bool HasHistory
        {
            get
            {
                return _hasHistory;
            }
            set
            {
                _blockHasHist.Wait();
                _hasHistory = value;
                _blockHasHist.Release();

            }
        }

        private bool _isVisibleForFac;



        public bool IsVisibleForFac
        {
            get => _isVisibleForFac;
            set
            {
                //if(value == true && IsChecked == true && HasHistory == false)
                //{
                //    CancellationTokenSource cts = new CancellationTokenSource();
                //    HasHistory = true;
                //    GetTrendHistoryAsync(cts.Token);

                //}
                this.RaiseAndSetIfChanged(ref _isVisibleForFac, value);
            }

        }
        private bool _isChecked;

        public bool IsChecked
        {
            get => _isChecked;
            set
            {
                //if (value == true && IsVisibleForFac == true && HasHistory == false)
                //{
                //    CancellationTokenSource cts = new CancellationTokenSource();
                //    HasHistory = true;
                //    GetTrendHistoryAsync(cts.Token);
                //}
                this.RaiseAndSetIfChanged(ref _isChecked, value);

            }
        }
        private CygNetHistoryResults _cygHistory;
        public CygNetHistoryResults CygHistory
        {
            get => _cygHistory;
            set
            {
                this.RaiseAndSetIfChanged(ref _cygHistory, value);
            }
        }
        public string FacilityTagUdc
        {
            get;
        }

        public string ShortId { get; }

        public async Task GetTrendHistoryAsync(
     CancellationToken ct)
        {
            try
            {
                Console.WriteLine("Entered on hist lock");
                await _blockGetHist.WaitAsync();
                HistoryProgress.Report(0);
                var srcPoint = MyDataModel;
                var pntRule = MyDataModel.SrcRuleTrend;

                //IsGetHistBusy = true;

                //var SqlCompareModel = new CygNetSqlDataCompareModel(srcPoint);

                if (srcPoint == null)
                    CygHistory = new CygNetHistoryResults();

                var cygHist = await GetCygNetHistory(
                    rxTrendVm.DefaultNumDays,
                    pntRule.GeneralHistoryOptions,
                    pntRule.HistoryNormalizationOptions,
                    HistoryProgress,
                    ct);

                CygHistory = cygHist;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error getting CygNet history");

                //System.Windows.Forms.MessageBox.Show(
                //    $@"Failed to get history: {ex.Message} {Environment.NewLine}Operation Failed");

                //if (ex.InnerException != null)
                //    System.Windows.Forms.MessageBox.Show($@"Inner exception = {ex.InnerException.Message}");

                CygHistory = new CygNetHistoryResults();
            }
            finally
            {
                //IsGetHistBusy = false;
                _blockGetHist.Release();
                Console.WriteLine("Release hist lock");
            }
        }

        private async Task<CygNetHistoryResults> GetCygNetHistory(
          DateTime earliestDate,
          DateTime latestDate,
          PointHistorySelectionRule rule,
          IProgress<int> progress,
          CancellationToken ct)
        {
            return await GetCygNetHistory(
                earliestDate,
                latestDate,
                rule.GeneralHistoryOptions,
                rule.HistoryNormalizationOptions,
                progress,
                ct);
        }

        private async Task<CygNetHistoryResults> GetCygNetHistory(
            double retentionDays,
            PointHistoryGeneralOptions genHistOpts,
            PointHistoryNormalizationOptions normOpts,
            IProgress<int> progress,
            CancellationToken ct)
        {
            return await GetCygNetHistory(
                DateTime.Now.AddDays(-retentionDays),
                DateTime.Now,
                genHistOpts,
                normOpts,
                progress,
                ct);
        }

        private async Task<CygNetHistoryResults> GetCygNetHistory(
            DateTime earliestDate,
            DateTime latestDate,
            PointHistoryGeneralOptions genHistOpts,
            PointHistoryNormalizationOptions normOpts,
            IProgress<int> progress,
            CancellationToken ct)
        {
            if (latestDate > DateTime.Now)
                latestDate = DateTime.Now;

            if (earliestDate > latestDate)
                earliestDate = latestDate;

            var histResults = new CygNetHistoryResults
            {
                EarliestTime = earliestDate,
                LatestTime = latestDate
            };

            if (!await MyDataModel.HasAnyHistory())
                return histResults;

            if (normOpts.EnableNormalization)
            {
                var normalizationWindowHours = Convert.ToDouble(normOpts.NormalizeWindowIntervalLength /
                                                                   normOpts.NumWindowHistEntries);

                var normEarliestDateForRawHistory = earliestDate.AddHours(-2 * normalizationWindowHours);
                var normLatestDateForRawHistory = latestDate.AddHours(2 * normalizationWindowHours);

                histResults.RawHistoryEntries = await NormalizedPointHistory.GetPointHistory(
                    MyDataModel,
                    normEarliestDateForRawHistory,
                    normLatestDateForRawHistory,
                    genHistOpts.IncludeUnreliable,
                    progress,
                    ct);

                histResults.HasHistLaterThanRange = await MyDataModel.HistoryAvailableInInterval(normLatestDateForRawHistory.AddMilliseconds(1), DateTime.Now);


                var normalizer = new HistoryNormalization(normOpts, earliestDate, latestDate);
                histResults.NormalizedHistoryEntries = await normalizer.Normalize(histResults.RawHistoryEntries, histResults.HasHistLaterThanRange);
                histResults.NormalizedHistoryEntries = histResults.NormalizedHistoryEntries.Where(x => x.NormalizedTimestamp <= latestDate && x.NormalizedTimestamp >= earliestDate).ToList();

            }
            else
            {
                histResults.RawHistoryEntries = await NormalizedPointHistory.GetPointHistory(
                    MyDataModel,
                    earliestDate,
                    latestDate,
                    genHistOpts.IncludeUnreliable,
                    progress,
                    ct);

                histResults.HasHistLaterThanRange = await MyDataModel.HistoryAvailableInInterval(latestDate.AddMilliseconds(1), DateTime.Now);

                histResults.NormalizedHistoryEntries = null;
            }

            return histResults;
        }
    }
}
