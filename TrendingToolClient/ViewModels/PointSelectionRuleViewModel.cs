using System.Windows;
using System.Windows.Input;
using TrendingToolClient.Forms;
using TrendingToolClient.ViewModels;
using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using TechneauxWpfControls.DataConverters;
using XmlDataModelUtility;
using TrendingToolClient.ViewModels.CygNet.Sub;

namespace TrendingToolClient.ViewModels
{
    public class PointSelectionRuleViewModel : NotifyDynamicBase<PointHistorySelectionRule>
    {
        PointHistorySelectionRule _fullSrcDataModel;
        public PointSelectionRuleViewModel(PointHistorySelectionRule srcModel) : base(srcModel)
        {
            _fullSrcDataModel = srcModel;

            Normalization = new NormalizationViewModel(srcModel.HistoryNormalizationOptions);

            FacilityPointSelectionView = new FacilityPointSelectionViewModel(srcModel.PointSelection);
            CygNetElmRuleView = new CygNetElementRuleViewModel(srcModel.CygNetElementRule);
            
            TrendOptions = new PointTrendOptionsViewModel(srcModel.TrendOptions);
            
            NoteHistory = new NoteHistoryViewModel(srcModel.CygNetElementRule.NoteHistoryOptions);
            PointHistory = new PointHistoryViewModel(srcModel.CygNetElementRule.PointHistoryOptions);
            AttributeView = new AttributeViewModel(srcModel.CygNetElementRule.AttributeOptions);
            FormatConfig = new FormatViewModel(srcModel.CygNetElementRule.FormatConfig);

            IsUdcVisible = Visibility.Visible;
            IsDataVisible = Visibility.Visible;
            IsFacVisible = Visibility.Visible;
            //IsChildOrdVisible = System.Windows.Visibility.Visible;
       
            IsUdcVisible = Visibility.Visible;
            IsDataVisible = Visibility.Visible;
            IsFacVisible = Visibility.Visible;                    
        }
 
        //[AffectedByOtherPropertyChange(nameof(CygNetRule.DataElement))]
        public Visibility IsUdcVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        public Visibility IsDataVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        public Visibility IsFacVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
       
        public CygNetElementRuleViewModel CygNetElmRuleView
        {
            get => GetPropertyValue<CygNetElementRuleViewModel>();
            set => SetPropertyValue(value);
        }
        public FacilityPointSelectionViewModel FacilityPointSelectionView
        {
            get => GetPropertyValue<FacilityPointSelectionViewModel>();
            set => SetPropertyValue(value);
        }       
     
        public PollingRetentionViewModel Polling
        {
            get => GetPropertyValue<PollingRetentionViewModel>();
            set => SetPropertyValue(value);
        }

        public NormalizationViewModel Normalization
        {
            get => GetPropertyValue<NormalizationViewModel>();
            set => SetPropertyValue(value);
        }

        public PointHistoryViewModel PointHistory
        {
            get => GetPropertyValue<PointHistoryViewModel>();
            set => SetPropertyValue(value);
        }              

        public NoteHistoryViewModel NoteHistory
        {
            get => GetPropertyValue<NoteHistoryViewModel>();
            set => SetPropertyValue(value);
        }

        public AttributeViewModel AttributeView
        {
            get => GetPropertyValue<AttributeViewModel>();
            set => SetPropertyValue(value);
        }

        public PointTrendOptionsViewModel TrendOptions
        {
            get => GetPropertyValue<PointTrendOptionsViewModel>();
            set => SetPropertyValue(value);
        }

        public FormatViewModel FormatConfig
        {
            get => GetPropertyValue<FormatViewModel>();
            set => SetPropertyValue(value);
        }

    }
}
