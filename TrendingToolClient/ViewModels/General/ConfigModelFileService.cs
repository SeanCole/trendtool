using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;
using ReactiveUI;
using Serilog;
using TechneauxReportingDataModel.General;
using XmlDataModelUtility;

namespace TrendingToolClient.ViewModels.General
{
    public class ConfigModelFileService : NotifyCopyDataModel
    {
        public event EventHandler ConfigModified;
        public event EventHandler NewConfigCreated;

        [XmlIgnore]
        public MetroWindow OwnerWindow { get; private set; } = null;

        private string BasePath { get; set; }

        private const string FileExt = "ttrend";

        public ConfigModelFileService(MetroWindow owner, string basePath)
        {
            OwnerWindow = owner;
            BasePath = basePath;
            FilePath = BasePath;

            var configFiles = Directory
                .GetFiles(BasePath)
                .Where(file => Path.GetExtension(file) == $".{FileExt}")
                .Select(Path.GetFileNameWithoutExtension)
                .ToList();

            TrendConfigNames = new BindingList<string>(configFiles);

            if (TrendConfigNames.Any())
            {
                OpenConfigFile(TrendConfigNames.First());
            }
            else
            {

            }
        }

        public ConfigModelFileService()
        {
        }

        public bool ConfigIsModified
        {
            get => GetPropertyValue<bool>();
            set
            {
                SetPropertyValue(value);

                if (value)
                {
                    SaveButtonVisibility = Visibility.Visible;
                }
                else
                {
                    SaveButtonVisibility = Visibility.Hidden;
                }
            }
        }

        public Visibility SaveButtonVisibility
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        public string FilePath
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public string FileName
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public BindingList<string> TrendConfigNames
        {
            get => GetPropertyValue<BindingList<string>>();
            set => SetPropertyValue(value);
        }

        public string FileNameWithoutExtension
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlIgnore]
        public ReportConfigModel ThisConfigModel { get; private set; }

        // Attempt to check for unsaved changes
        public bool TryClose()
        {
            var configSaveStatus = CheckConfigSaved();

            if (configSaveStatus == SaveStatuses.DialogCancelled ||
              configSaveStatus == SaveStatuses.SaveFailed)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //public void SetFilePath(string fullPath)
        //{
        //    FullFilePath = new FileInfo(fullPath);
        //}

        public void ResetModified()
        {
            ConfigIsModified = false;
        }

        public async Task<(bool Completed, string NewName)> CreateNewConfig()
        {
            return await InitNewConfig();
        }
        
        public bool OpenConfigFile(string fileName)
        {
            // CODE HERE => Implement recent folder
            if (!TryClose())
                return false;

            var thisConfigPath = $@"{BasePath}\{fileName}.{FileExt}";

            var loadedConfig = XmlFileSerialization<ReportConfigModel>.LoadModelFromFile(thisConfigPath);

            if (loadedConfig == null)
            {
                MessageBox.Show(OwnerWindow, "Failed to load config file. Please check the format or create a new one.");

                return false;
            }
            else
            {
                FilePath = Path.GetDirectoryName(thisConfigPath);
                FileName = Path.GetFileName(thisConfigPath);
                FileNameWithoutExtension = Path.GetFileNameWithoutExtension(thisConfigPath);

                ThisConfigModel = loadedConfig;

                Observable.FromEventPattern<PropertyChangedEventHandler, PropertyChangedEventArgs>
                        (h => ThisConfigModel.PropertyChanged += h, h => ThisConfigModel.PropertyChanged -= h)
                    .Throttle(TimeSpan.FromSeconds(1), RxApp.MainThreadScheduler)
                    .Subscribe(x => SaveConfig());
                
                //ThisConfigModel.PropertyChanged += SyncConfig_PropertyChanged;

                return true;
            }


            return false;
        }

        public async Task SaveAsNew()
        {
            await InitNewConfig(ThisConfigModel);
        }

        public bool SaveConfigFile(bool saveNew = false)
        {
            if (string.IsNullOrWhiteSpace(FilePath) || saveNew)
            {
                var saveFileDialog = new SaveFileDialog
                {
                    // Browse for path
                    DefaultExt = "xml",
                    Filter = "XML Files (*.xml)|*.xml"
                };

                if (saveFileDialog.ShowDialog(OwnerWindow) == true)
                {
                    FilePath = Path.GetDirectoryName(saveFileDialog.FileName);
                    FileName = Path.GetFileName(saveFileDialog.FileName);
                    FileNameWithoutExtension = Path.GetFileNameWithoutExtension(saveFileDialog.FileName);
                }
                else
                {
                    return false;
                }
            }

            try
            {
                //Check if the config file path exists
                if (!Directory.Exists(Path.GetDirectoryName(FilePath)))
                {
                    MessageBox.Show(OwnerWindow, "Config File path is invalid. Save failed.");
                    return false;
                }

                if (SaveConfig())
                {
                    ConfigIsModified = false;
                    return true;
                }
                else
                {
                    MessageBox.Show(OwnerWindow, "Failed to save file. Please verify that the file is not open in another program " +
                        "and that you have permission to write to the destination folder.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                //if (ThrowExceptions) { throw; }
                Log.Error(ex, "Failure saving config file");
                MessageBox.Show(OwnerWindow, $"Failed to save file with exception: {ex.Message}");
                return false;
            }
        }

        private async Task<(bool Completed, string NewName)> InitNewConfig(ReportConfigModel existingModel = null)
        {
            var msgConfig = new MetroDialogSettings()
            {
                AnimateShow = false,
                AnimateHide = false
            };

            var newName = await OwnerWindow.ShowInputAsync("Trend Name", "Please enter a new trend config name", msgConfig);
            
            if (string.IsNullOrWhiteSpace(newName))
            {
                await OwnerWindow.ShowMessageAsync("", "No name selected, a new trend was NOT created", MessageDialogStyle.Affirmative, msgConfig);
                return (false, null);
            }

            FileName = $"{newName}.{FileExt}";

            if (existingModel == null)
            {
                ThisConfigModel = new ReportConfigModel();

                Observable.FromEventPattern<PropertyChangedEventHandler, PropertyChangedEventArgs>
                        (h => ThisConfigModel.PropertyChanged += h, h => ThisConfigModel.PropertyChanged -= h)
                    .Throttle(TimeSpan.FromSeconds(1), RxApp.MainThreadScheduler)
                    .Subscribe(x => SaveConfig());
                //ThisConfigModel.PropertyChanged += SyncConfig_PropertyChanged;
            }
            else
            {
                ThisConfigModel = existingModel;

                Observable.FromEventPattern<PropertyChangedEventHandler, PropertyChangedEventArgs>
                        (h => ThisConfigModel.PropertyChanged += h, h => ThisConfigModel.PropertyChanged -= h)
                    .Throttle(TimeSpan.FromSeconds(1), RxApp.MainThreadScheduler)
                    .Subscribe(x => SaveConfig());

                //ThisConfigModel.PropertyChanged += SyncConfig_PropertyChanged;
            }

            SaveConfig();

            TrendConfigNames.Insert(0, newName);
            
            NewConfigCreated?.Invoke(this, new EventArgs());
            //_SyncCygService = new SyncCygnetService(ThisConfigModel.CygNetGeneral);

            return (true, newName);
        }

        //private void SyncConfig_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //{
        //    SaveConfig();

        //    //ConfigIsModified = true;
        //    //ConfigModified?.Invoke(this, new EventArgs());
        //}

        public bool SaveConfig()
        {
            try
            {
                using (var sw = new StreamWriter(Path.Combine(FilePath, FileName)))
                {
                    //XmlSerializer xmlserial = new XmlSerializer(typeof(T), new XmlRootAttribute("SyncConfig"));

                    var xmlSerial = new XmlSerializer(typeof(ReportConfigModel), DerivedTypes);
                    xmlSerial.Serialize(sw, ThisConfigModel);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, $@"Error saving config file to {FilePath}\{FileName}");
                return false;
            }
        }

        private SaveStatuses CheckConfigSaved()
        {
            if (ConfigIsModified)
            {
                var saveButton = MessageBox.Show(
                        OwnerWindow,
                        "The current config has not been saved, would you like to save it now?",
                        "Save File",
                        MessageBoxButton.YesNoCancel);

                switch (saveButton)
                {
                    case MessageBoxResult.Cancel:
                        return SaveStatuses.DialogCancelled;

                    case MessageBoxResult.Yes:
                        if (SaveConfigFile())
                        {
                            return SaveStatuses.SaveSucceeded;
                        }
                        else
                        {
                            return SaveStatuses.SaveFailed;
                        }
                    case MessageBoxResult.No:
                        return SaveStatuses.SaveRejected;

                    default:
                        return SaveStatuses.DialogCancelled;
                }
            }

            return SaveStatuses.SaveNotNeeded;
        }

        private enum SaveStatuses
        {
            SaveNotNeeded,
            SaveSucceeded,
            SaveFailed,
            SaveRejected,
            DialogCancelled
        }
    }
}
