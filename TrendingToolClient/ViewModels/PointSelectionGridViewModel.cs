using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using XmlDataModelUtility;

namespace TrendingToolClient.ViewModels
{
    public class PointSelectionGridViewModel : NotifyDynamicBase<PointHistorySelectionRule>
    {    
        public PointSelectionGridViewModel(PointHistorySelectionRule srcModel) : base(srcModel)
        {           
            ConfiguredRuleView = new PointSelectionRuleViewModel(MyDataModel);
        }

        public PointSelectionRuleViewModel ConfiguredRuleView
        {
            get => GetPropertyValue<PointSelectionRuleViewModel>();
            set => SetPropertyValue(value);
        }    
    }
}
