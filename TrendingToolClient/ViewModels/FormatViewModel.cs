using System.Windows;
using System.Windows.Input;
using TrendingToolClient.Forms;
using GenericRuleModel.Rules;
using TechneauxWpfControls.DataConverters;
using XmlDataModelUtility;

namespace TrendingToolClient.ViewModels
{
    public class FormatViewModel : NotifyDynamicBase<FormattingOptions>
    {
        public FormatViewModel(FormattingOptions srcModel) : base(srcModel)
        {
            IsFormatDecVisible = Visibility.Visible;
            IsFormatTypeVisible = Visibility.Visible;
            IsFormatVisible = Visibility.Visible;
            IsFormatCustomVisible = Visibility.Visible;
            IsFormatDefVisible = Visibility.Visible;
            IsFormatNumDefVisible = Visibility.Visible;
            IsFormatDateDefVisible = Visibility.Visible;
            IsFormatRoundingTypeVisible = Visibility.Visible;
            IsFormatBoolDefVisible = Visibility.Visible;
            IsFormatParsingVisible = Visibility.Visible;
            IsFormatScalingVisible = Visibility.Visible;
            IsFormatScalingFactorVisible = Visibility.Visible;
            IsEnumerationTableVisible = Visibility.Visible;
        }

        public Visibility IsFormatVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(FormattingOptions.EnableFormat))]
        public Visibility IsFormatTypeVisible
        {
            get
            {
               
                if (MyDataModel.EnableFormat == false)
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                else
                {
                    SetPropertyValue(Visibility.Visible);
                }

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(FormattingOptions.EnableFormat))]
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.DataType))]
        public Visibility IsFormatCustomVisible
        {
            get
            {
                if (MyDataModel.EnableFormat == true && MyDataModel.DataType.Equals(FormattingOptions.OutputDataType.Text))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }


                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(FormattingOptions.DataType))]
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.EnableFormat))]
        public Visibility IsFormatDefVisible
        {
            get
            {

                if (MyDataModel.EnableFormat == true && MyDataModel.DataType.Equals(FormattingOptions.OutputDataType.EnumerationTable))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(FormattingOptions.DataType))]
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.EnableFormat))]
        public Visibility IsFormatNumDefVisible
        {
            get
            {
           
                if (MyDataModel.EnableFormat == true && MyDataModel.DataType.Equals(FormattingOptions.OutputDataType.Numeric))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.DataType))]
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.EnableFormat))]
        public Visibility IsFormatParsingVisible
        {
            get
            {

                if (MyDataModel.EnableFormat == true && MyDataModel.DataType.Equals(FormattingOptions.OutputDataType.Numeric))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.DataType))]
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.EnableFormat))]

        public Visibility IsFormatScalingVisible
        {
            get
            {
         
                if (MyDataModel.EnableFormat == true &&
                    MyDataModel.DataType.Equals(FormattingOptions.OutputDataType.Numeric))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.DataType))]
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.EnableFormat))]
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.ScalingTypeChoice))]
        public Visibility IsFormatScalingFactorVisible
        {
            get
            {
                if (MyDataModel.EnableFormat == true &&       
                    MyDataModel.DataType.Equals(FormattingOptions.OutputDataType.Numeric) &&
                    MyDataModel.ScalingTypeChoice != FormattingOptions.ScalingType.NoScaling)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.DataType))]
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.EnableFormat))]
        public Visibility IsFormatDateDefVisible
        {
            get
            {

                if (MyDataModel.EnableFormat == true &&
                    MyDataModel.DataType.Equals(FormattingOptions.OutputDataType.DateTime))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.EnableFormat))]
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.DataType))]
        public Visibility IsFormatBoolDefVisible
        {
            get
            {

                if (MyDataModel.EnableFormat == true &&
                    MyDataModel.DataType.Equals(FormattingOptions.OutputDataType.Boolean))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.DataType))]
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.EnableFormat))]
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.NumberPartOption))]
        public Visibility IsFormatRoundingTypeVisible
        {
            get
            {

                if (MyDataModel.EnableFormat == true &&
                    MyDataModel.NumberPartOption != FormattingOptions.NumberPart.IntegerPart &&
                    MyDataModel.DataType.Equals(FormattingOptions.OutputDataType.Numeric))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.EnableFormat))]
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.DataType))]
        public Visibility IsEnumerationTableVisible
        {
            get
            {
            
                if (MyDataModel.EnableFormat == true &&
                    MyDataModel.DataType.Equals(FormattingOptions.OutputDataType.EnumerationTable))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        public ICommand OpenEnumTableWindow => new DelegateCommand(OpenSelectEnumTableWindow);

        public void OpenSelectEnumTableWindow(object inputObject)
        {
            var windowInstance = EnumTableChooser.Instance();
            var success = windowInstance.TryGetKeyDescId(out var result);
            if (!success) return;
            if (result.Value == null) return;
            MyDataModel.TableName = result.Value as string;
            MyDataModel.TableDescription = result.Description as string;
        }



        [AffectedByOtherPropertyChange(nameof(FormattingOptions.DataType))]
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.EnableFormat))]
        [AffectedByOtherPropertyChange(nameof(FormattingOptions.NumberPartOption))]
        public Visibility IsFormatDecVisible
        {
            get
            {
    
                if (MyDataModel.EnableFormat == true &&
                    MyDataModel.NumberPartOption != FormattingOptions.NumberPart.IntegerPart &&
                    MyDataModel.DataType.Equals(FormattingOptions.OutputDataType.Numeric))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

    }
}