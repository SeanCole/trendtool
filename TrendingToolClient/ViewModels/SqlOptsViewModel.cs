﻿using System.Windows;
using System.Windows.Input;
using TrendingToolClient.Forms;
using TechneauxReportingDataModel.SqlHistory.SubOptions;
using TechneauxWpfControls.DataConverters;
using XmlDataModelUtility;

namespace TrendingToolClient.ViewModels
{
    public class SqlOptsViewModel : NotifyDynamicBase<SqlGeneralOptions>
    {      
        public SqlOptsViewModel(SqlGeneralOptions srcModel) : base(srcModel)
        {
            IsCredentialsVisible = Visibility.Visible;
        }
       
        [AffectedByOtherPropertyChange(nameof(SqlGeneralOptions.AuthenticationType))]
        public Visibility IsCredentialsVisible
        {
            get
            {
                if (MyDataModel.AuthenticationType == SqlGeneralOptions.AuthenticationTypes.Integrated)
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                else
                {
                    SetPropertyValue(Visibility.Visible);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        public ICommand OpenDatabaseTableWindow => new DelegateCommand(OpenSelectedDatabaseTableWindow);

        public void OpenSelectedDatabaseTableWindow(object inputObject)
        {
            var windowInstance = SqlDatabaseTableChooser.Instance();

            var success = windowInstance.TryGetKeyDescId(out var result);
            if (success)
            {
                if (result.Value != null)
                {
                    MyDataModel.DatabaseName = result.Description as string;
                    MyDataModel.TableName = result.Value as string;
                }
            }
        }
    }
}
