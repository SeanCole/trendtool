﻿using System.Windows;
using System.Windows.Input;
using TrendingToolClient.Forms;
using TrendingToolClient.ViewModels.CygNet.Sub;
using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxWpfControls.DataConverters;
using XmlDataModelUtility;

namespace TrendingToolClient.ViewModels
{
    public class CygNetRuleView : NotifyDynamicBase<CygNetRuleBase>
    {
        public CygNetRuleView(CygNetRuleBase srcModel) : base(srcModel)
        {
            //normalization = new NormalizationViewModel(srcModel.NormalizationOptions);
            NoteHistory = new NoteHistoryViewModel(srcModel.NoteHistoryOptions);
            PointHistory = new PointHistoryViewModel(srcModel.PointHistoryOptions);
            AttributeView = new AttributeViewModel(srcModel.AttributeOptions);
            FormatConfig = new FormatViewModel(srcModel.FormatConfig);

            IsUdcVisible = Visibility.Visible;
            IsDataVisible = Visibility.Visible;
            IsFacVisible = Visibility.Visible;
            IsChildOrdVisible = Visibility.Visible;
            IsFacilityAttributeSelectionVisible = Visibility.Visible;
            IsHistoryTypeVisible = Visibility.Visible;
            IsNoteHistoryTypeVisible = Visibility.Collapsed;
            IsNoteTypeVisible = Visibility.Collapsed;
            IsAttributeTypeVisible = Visibility.Visible;
            IsPointValueTypeVisible = Visibility.Visible;
            IsChildGroupVisible = Visibility.Visible;
            IsNoteValueTypeVisible = Visibility.Collapsed;

            IsUnreliableVisible = Visibility.Visible;
            IsPointAttributeSelectionVisible = Visibility.Visible;
            IsFacilityNoteSelectionVisible = Visibility.Collapsed;
            IsIdVisible = Visibility.Visible;
            IsPointNoteSelectionVisible = Visibility.Collapsed;
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsFacilityNoteSelectionVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsPointNoteSelectionVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsNoteValueTypeVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsAttributeTypeVisible
        {
            get
            {
                var tempRule = (CygNetRollupRuleBase)MyDataModel;
                if (tempRule.DataElement == CygNetRuleBase.DataElements.FacilityAttribute ||
                    tempRule.DataElement == CygNetRuleBase.DataElements.PointAttribute)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsHistoryTypeVisible
        {
            get
            {
                var tempRule = (CygNetRollupRuleBase)MyDataModel;
                if (tempRule.DataElement == CygNetRuleBase.DataElements.PointHistory || tempRule.DataElement == CygNetRuleBase.DataElements.PointCurrentValue)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsNoteHistoryTypeVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        //[AffectedByOtherPropertyChange(nameof(CygNetRule.DataElement))]
        public Visibility IsUdcVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(FacilityPointSelection.FacilityChoice))]
        public Visibility IsChildOrdVisible
        {
            get
            {
                var tempRule = (CygNetRollupRuleBase)MyDataModel;
                if (tempRule.FacilityPointOptions.FacilityChoice.Equals(FacilitySelection.FacilityChoices.ChildFacility))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        public Visibility IsDataVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        public Visibility IsFacVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsIdVisible
        {
            get
            {
                var tempRule = (CygNetRollupRuleBase)MyDataModel;
                if (tempRule.DataElement == CygNetRuleBase.DataElements.PointAttribute ||
                    tempRule.DataElement == CygNetRuleBase.DataElements.FacilityAttribute)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(FacilityPointSelection.FacilityChoice))]
        public Visibility IsChildGroupVisible
        {
            get
            {
                var tempRule = (CygNetRollupRuleBase)MyDataModel;
                if (tempRule.FacilityPointOptions.FacilityChoice.Equals(FacilitySelection.FacilityChoices.ChildFacility))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }


        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsUnreliableVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsNoteTypeVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }


        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsPointValueTypeVisible
        {
            get
            {
                var tempRule = (CygNetRollupRuleBase)MyDataModel;
                if (tempRule.DataElement == CygNetRuleBase.DataElements.PointHistory || tempRule.DataElement == CygNetRuleBase.DataElements.PointCurrentValue)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsFacilityAttributeSelectionVisible
        {
            get
            {
                var tempRule = (CygNetRollupRuleBase)MyDataModel;
                if (
                    tempRule.DataElement == CygNetRuleBase.DataElements.FacilityAttribute)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    //attributeView.ClearControl();
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsPointAttributeSelectionVisible
        {
            get
            {
                var tempRule = (CygNetRollupRuleBase)MyDataModel;
                if (
                    tempRule.DataElement == CygNetRuleBase.DataElements.PointAttribute)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    //attributeView.ClearControl();
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        public ICommand OpenChildGroupWindow => new DelegateCommand(OpenSelectChildGroupWindow);

        public void OpenSelectChildGroupWindow(object inputObject)
        {
            var windowInstance = CygNetChildFacilityWindow.Instance();

            var success = windowInstance.TryGetKeyDescId(out var result);
            if (success)
            {
                if (result.Value != null)
                {
                    var tempRule = (CygNetRollupRuleBase)MyDataModel;

                    tempRule.FacilityPointOptions.ChildGroupKey.Id = result.Value as string;

                }
                if (result.Description != null)
                {
                    var tempRule = (CygNetRollupRuleBase)MyDataModel;
                    tempRule.FacilityPointOptions.ChildGroupKey.Desc = result.Description as string;
                }
            }
        }

        public PollingRetentionViewModel Polling
        {

            get => GetPropertyValue<PollingRetentionViewModel>();
            set => SetPropertyValue(value);
        }

        public NormalizationViewModel Normalization
        {
            get => GetPropertyValue<NormalizationViewModel>();
            set => SetPropertyValue(value);
        }

        public NoteHistoryViewModel NoteHistory
        {
            get => GetPropertyValue<NoteHistoryViewModel>();
            set => SetPropertyValue(value);
        }
        public PointHistoryViewModel PointHistory
        {
            get => GetPropertyValue<PointHistoryViewModel>();
            set => SetPropertyValue(value);
        }
        public AttributeViewModel AttributeView
        {
            get => GetPropertyValue<AttributeViewModel>();
            set => SetPropertyValue(value);
        }
        public FormatViewModel FormatConfig
        {
            get => GetPropertyValue<FormatViewModel>();
            set => SetPropertyValue(value);
        }
    }
}
