using System.Windows;
using TechneauxReportingDataModel.SqlHistory.SubOptions;
using XmlDataModelUtility;

namespace TrendingToolClient.ViewModels
{
    public class HistorySyncViewModel : NotifyDynamicBase<HistorySyncOptions>
    {
        public HistorySyncViewModel(HistorySyncOptions srcModel) : base(srcModel)
        {
        }

        [AffectedByOtherPropertyChange(nameof(HistorySyncOptions.EnableFullAudit))]
        public Visibility IsFullAuditEnabled
        {
            get
            {
                if (MyDataModel.EnableFullAudit == true)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
     
        [AffectedByOtherPropertyChange(nameof(HistorySyncOptions.EnableFullAudit))]
        public Visibility IsFullAuditOptionsVisible
        {
            get
            {
                if (MyDataModel.EnableFullAudit == true)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }


                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

    }


}
