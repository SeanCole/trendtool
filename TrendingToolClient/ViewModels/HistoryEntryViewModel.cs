using System;
using Techneaux.CygNetWrapper.Services.VHS;
using XmlDataModelUtility;

namespace TrendingToolClient.ViewModels
{
    public class HistoryEntryViewModel : NotifyModelBase
    {
        public CygNetHistoryEntry MyDataModel { get; }

        public HistoryEntryViewModel(CygNetHistoryEntry srcModel)
        {
            MyDataModel = srcModel;
            BaseStatus = srcModel.BaseStatus.ToString();
            ValueString = srcModel.Value.ToString();

            if (srcModel.Value.TryGetDouble(out var temp))
            {
                ValueDouble = temp;
            }

            Timestamp = srcModel.AdjustedTimestamp.ToLocalTime().ToString();
            TimestampDateTime = srcModel.AdjustedTimestamp.ToLocalTime();
        }

        public string Timestamp
        {
            get
            {
                SetPropertyValue(MyDataModel.AdjustedTimestamp.ToString());
                return GetPropertyValue<string>();
            }
            set => SetPropertyValue(value);
        }

        public DateTime TimestampDateTime
        {
            get => GetPropertyValue<DateTime>();
            set => SetPropertyValue(value);
        }

        public double ValueDouble
        {
            get => GetPropertyValue<double>();
            set => SetPropertyValue(value);

        }

        public string ValueString
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public string BaseStatus
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
    }
}
