﻿using LiveCharts;
using LiveCharts.Geared;
using LiveCharts.Wpf;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using static TechneauxReportingDataModel.General.PointTrendOptions;

namespace TrendingToolClient.ViewModels
{
    public class HistoryTrendToolViewModel : ReactiveObject
    {
        private List<PointViewModel> trendPointViewModels;
        public HistoryTrendToolViewModel(List<PointViewModel> trendPntViewModels)
        {
            Series = null;
            ScrollSeries = null;
            Axis1MinValue = 0;
            Axis1MaxValue = 0;
            Axis2MaxValue = 0;
            Axis2MinValue = 0;
            Axis0MinValue = 0;
            Axis3MinValue = 0;
            Axis0MaxValue = 0;
            Axis3MaxValue = 0;
            Axis0Title = "Axis 0";
            Axis1Title = "Axis 1";
            Axis2Title = "Axis 2";
            Axis3Title = "Axis 3";      

            var UniqueAxes = trendPntViewModels.Select(x => x.MyDataModel.SrcRuleTrend.TrendOptions.AxisId).Distinct().ToList();
            if(UniqueAxes.Contains(AxisIdNames.LeftLeft))
            {               
                Axis0MinValue = Convert.ToDouble(trendPntViewModels.Where(x => x.MyDataModel.SrcRuleTrend.TrendOptions.AxisId == AxisIdNames.LeftLeft).Select(y => y.MyDataModel.SrcRuleTrend.TrendOptions.MinVal).Min());
                Axis0MaxValue = Convert.ToDouble(trendPntViewModels.Where(x => x.MyDataModel.SrcRuleTrend.TrendOptions.AxisId == AxisIdNames.LeftLeft).Select(y => y.MyDataModel.SrcRuleTrend.TrendOptions.MaxVal).Max());
            }
            else
                Axis0Visibility = false;
         

            if (UniqueAxes.Contains(AxisIdNames.LeftRight))
            {
                Axis1MinValue = Convert.ToDouble(trendPntViewModels.Where(x => x.MyDataModel.SrcRuleTrend.TrendOptions.AxisId == AxisIdNames.LeftRight).Select(y => y.MyDataModel.SrcRuleTrend.TrendOptions.MinVal).Min());
                Axis1MaxValue = Convert.ToDouble(trendPntViewModels.Where(x => x.MyDataModel.SrcRuleTrend.TrendOptions.AxisId == AxisIdNames.LeftRight).Select(y => y.MyDataModel.SrcRuleTrend.TrendOptions.MaxVal).Max());               
            }
            else
                Axis1Visibility = false;
        

            if (UniqueAxes.Contains(AxisIdNames.RightLeft))
            {              
                Axis2MinValue = Convert.ToDouble(trendPntViewModels.Where(x => x.MyDataModel.SrcRuleTrend.TrendOptions.AxisId == AxisIdNames.RightLeft).Select(y => y.MyDataModel.SrcRuleTrend.TrendOptions.MinVal).Min());
                Axis2MaxValue = Convert.ToDouble(trendPntViewModels.Where(x => x.MyDataModel.SrcRuleTrend.TrendOptions.AxisId == AxisIdNames.RightLeft).Select(y => y.MyDataModel.SrcRuleTrend.TrendOptions.MaxVal).Max());
            }
            else
                Axis2Visibility = false;
            
            if (UniqueAxes.Contains(AxisIdNames.RightRight))
            {              
                Axis3MinValue = Convert.ToDouble(trendPntViewModels.Where(x => x.MyDataModel.SrcRuleTrend.TrendOptions.AxisId == AxisIdNames.RightRight).Select(y => y.MyDataModel.SrcRuleTrend.TrendOptions.MinVal).Min());
                Axis3MaxValue = Convert.ToDouble(trendPntViewModels.Where(x => x.MyDataModel.SrcRuleTrend.TrendOptions.AxisId == AxisIdNames.RightRight).Select(y => y.MyDataModel.SrcRuleTrend.TrendOptions.MaxVal).Max());
            }
            else
                Axis3Visibility = false;

            if (Axis0MinValue == 0 && Axis0MaxValue == 0)
            {
                Axis0MinValue = double.NaN;
                Axis0MaxValue = double.NaN;
            }

            if (Axis1MinValue == 0 && Axis1MaxValue == 0)
            {
                Axis1MinValue = double.NaN;
                Axis1MaxValue = double.NaN;
            }
            if(Axis2MinValue == 0 && Axis2MaxValue == 0)
            {
                Axis2MinValue = double.NaN;
                Axis2MaxValue = double.NaN;
            }
            if (Axis3MinValue == 0 && Axis3MaxValue == 0)
            {
                Axis3MinValue = double.NaN;
                Axis3MaxValue = double.NaN;
            }


            TrendVisibility = Visibility.Collapsed;
           
            trendPointViewModels = trendPntViewModels; //.Take(10).ToList();

            From = DateTime.Now.AddDays(-1).Ticks;
            To = DateTime.Now.Ticks;

            MinRange = TimeSpan.FromHours(1).Ticks;
            MaxRange = TimeSpan.FromDays(7).Ticks;

            var tmpSeries = new SeriesCollection();
            var tmpScrollSeries = new SeriesCollection();
            
            foreach (var pnt in trendPointViewModels)
            {
                pnt.rawDataSeries.IsEnabledChanged += RawDataSeries_IsEnabledChanged;
                pnt.rawScrollDataSeries.IsEnabledChanged += RawScrollDataSeries_IsEnabledChanged;
                //tmpSeries.Add(pnt.rawDataSeries);
                //pnt.rawDataSeries.Visibility = System.Windows.Visibility.Hidden;

                //tmpScrollSeries.Add(pnt.rawScrollDataSeries);
            }          

            From = DateTime.Now.AddDays(-1).Ticks;
            To = DateTime.Now.Ticks;

            MinRange = TimeSpan.FromHours(1).Ticks;
            MaxRange = TimeSpan.FromDays(7).Ticks;

            Series = tmpSeries;
            ScrollSeries = tmpScrollSeries;

            XFormatter = val => new DateTime((long)val).ToString("M/dd h:mm t");
            YFormatter = val => val.ToString("#.##");
        }

        private void RawScrollDataSeries_IsEnabledChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true && !ScrollSeries.Contains((Series)sender))
            {
                ScrollSeries.Add(sender as Series);

                TrendVisibility = Visibility.Visible;
            }

            if ((bool)e.NewValue == false && ScrollSeries.Contains((Series)sender))
            {
                ScrollSeries.Remove(sender as Series);

                if (!ScrollSeries.Any())
                    TrendVisibility = Visibility.Collapsed;
            }            
        }

        private void RawDataSeries_IsEnabledChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            Axis0Title = "";
            Axis1Title = "";
            Axis2Title = "";
            Axis3Title = "";
            if ((bool)e.NewValue == true && !Series.Contains((Series)sender))
            {
                var srs = sender as GLineSeries;
                if (srs != null)
                {
                    if (srs.ScalesYAt == (int)AxisIdNames.LeftLeft)
                    {
                        Axis0Visibility = true;
              

                    }
                    else if (srs.ScalesYAt == (int)AxisIdNames.LeftRight)
                    {
                        Axis1Visibility = true;
                    }
                    else if (srs.ScalesYAt == (int)AxisIdNames.RightLeft)
                    {
                        Axis2Visibility = true;
                    }
                    else if (srs.ScalesYAt == (int)AxisIdNames.RightRight)
                    {
                        Axis3Visibility = true;
                    }
                }

                Series.Add(sender as Series);
            }

            if ((bool)e.NewValue == false && Series.Contains((Series)sender))
            {
                Series.Remove(sender as Series);
                var srs = sender as GLineSeries;
                if (srs != null)
                {

                    foreach (var tmpSeries in Series)
                    {
                        if (tmpSeries.ScalesYAt == srs.ScalesYAt)
                        {
                            return;
                        }
                    }

                    if (srs.ScalesYAt == (int)AxisIdNames.LeftLeft)
                    {
                        Axis0Visibility = false;
                    }
                    else if (srs.ScalesYAt == (int)AxisIdNames.LeftRight)
                    {
                        Axis1Visibility = false;
                    }
                    else if (srs.ScalesYAt == (int)AxisIdNames.RightLeft)
                    {
                        Axis2Visibility = false;
                    }
                    else if (srs.ScalesYAt == (int)AxisIdNames.RightRight)
                    {
                        Axis3Visibility = false;
                    }

                }
            }
            foreach (var tmpSeries in Series)
            {
                var tmp = tmpSeries as GLineSeries;
                if (tmp.IsEnabled && tmp.ScalesYAt == (int)AxisIdNames.LeftLeft && !Axis0Title.Contains(StringLabelHelper(tmp.Title)))
                {
                   
                    Axis0Title = Axis0Title + "/" + StringLabelHelper(tmp.Title);
                }
                else if (tmp.IsEnabled && tmp.ScalesYAt == (int)AxisIdNames.LeftRight && !Axis1Title.Contains(StringLabelHelper(tmp.Title)))
                {
                    Axis1Title = Axis1Title + "/" + StringLabelHelper(tmp.Title);
                }
                else if (tmp.IsEnabled && tmp.ScalesYAt == (int)AxisIdNames.RightLeft && !Axis2Title.Contains(StringLabelHelper(tmp.Title)))
                {
                    Axis2Title = Axis2Title + "/" + StringLabelHelper(tmp.Title);
                }
                else if (tmp.IsEnabled && tmp.ScalesYAt == (int)AxisIdNames.RightRight && !Axis3Title.Contains(StringLabelHelper(tmp.Title)))
                {
                    Axis3Title = Axis3Title + "/" + StringLabelHelper(tmp.Title);
                }
            }
            if (Axis0Title == "")
            {
                Axis0Title = "Axis 0";
            }
            else
            {
                Axis0Title = Axis0Title.Remove(0, 1);
            }
            if (Axis1Title == "")
            {
                Axis1Title = "Axis 1";
            }
            else
            {
                Axis1Title = Axis1Title.Remove(0, 1);
            }
            if (Axis2Title == "")
            {
                Axis2Title = "Axis 2";
            }
            else
            {
                Axis2Title = Axis2Title.Remove(0, 1);
            }
            if (Axis3Title == "")
            {
                Axis3Title = "Axis 3";
            }
            else
            {
                Axis3Title = Axis3Title.Remove(0, 1);
            }

        }
        private string StringLabelHelper(string source)
        {
            if(source.Contains("::"))
            {
                var index = source.IndexOf("::") + 2;
                return source.Substring(index);
            }
            else
            {
                return "";
            }
        }

        private SeriesCollection _series;
        public SeriesCollection Series
        {
            get => _series;
            set => this.RaiseAndSetIfChanged(ref _series, value);
        }

        private Visibility _trendVisibility;
        public Visibility TrendVisibility
        {
            get => _trendVisibility;
            set => this.RaiseAndSetIfChanged(ref _trendVisibility, value);
        }

        private SeriesCollection _scrollSeries;
        public SeriesCollection ScrollSeries
        {
            get => _scrollSeries;
            set => this.RaiseAndSetIfChanged(ref _scrollSeries, value);
        }


        public ZoomingOptions ZoomingMode { get; } = ZoomingOptions.X;

        private double _to;
        public double To
        {
            get => _to;
            set => this.RaiseAndSetIfChanged(ref _to, value);
        }

        private double _axis1MinValue;

        public double Axis1MinValue
        {
            get => _axis1MinValue;
            set => this.RaiseAndSetIfChanged(ref _axis1MinValue, value);
        }

        private double _axis2MinValue;

        public double Axis2MinValue
        {
            get => _axis2MinValue;
            set => this.RaiseAndSetIfChanged(ref _axis2MinValue, value);
        }

        private string _axis2Title;

        public string Axis2Title
        {
            get => _axis2Title;
            set => this.RaiseAndSetIfChanged(ref _axis2Title, value);
        }

        private string _axis0Title;

        public string Axis0Title
        {
            get => _axis0Title;
            set => this.RaiseAndSetIfChanged(ref _axis0Title, value);
        }

        private string _axis1Title;

        public string Axis1Title
        {
            get => _axis1Title;
            set => this.RaiseAndSetIfChanged(ref _axis1Title, value);
        }

        private string _axis3Title;

        public string Axis3Title
        {
            get => _axis3Title;
            set => this.RaiseAndSetIfChanged(ref _axis3Title, value);
        }

        private double _axis1MaxValue;

        public double Axis1MaxValue
        {
            get => _axis1MaxValue;
            set => this.RaiseAndSetIfChanged(ref _axis1MaxValue, value);
        }

        private double _axis2MaxValue;

        public double Axis2MaxValue
        {
            get => _axis2MaxValue;
            set => this.RaiseAndSetIfChanged(ref _axis2MaxValue, value);
        }


        private double _axis0MinValue;

        public double Axis0MinValue
        {
            get => _axis0MinValue;
            set => this.RaiseAndSetIfChanged(ref _axis0MinValue, value);
        }

        private double _axis3MinValue;

        public double Axis3MinValue
        {
            get => _axis3MinValue;
            set => this.RaiseAndSetIfChanged(ref _axis3MinValue, value);
        }

        private double _axis0MaxValue;

        public double Axis0MaxValue
        {
            get => _axis0MaxValue;
            set => this.RaiseAndSetIfChanged(ref _axis0MaxValue, value);
        }       

        private double _axis3MaxValue;

        public double Axis3MaxValue
        {
            get => _axis3MaxValue;
            set => this.RaiseAndSetIfChanged(ref _axis3MaxValue, value);
        }

        private double _from;
        public double From
        {
            get => _from;
            set => this.RaiseAndSetIfChanged(ref _from, value);
        }

        private double _minRange;
        public double MinRange
        {
            get => _minRange;
            set => this.RaiseAndSetIfChanged(ref _minRange, value);
        }

        public bool _axis1Visibility;
        public bool Axis1Visibility
        {
            get => _axis1Visibility;
            set => this.RaiseAndSetIfChanged(ref _axis1Visibility, value);
        }

    
        public bool _axis2Visibility;
        public bool Axis2Visibility
        {
            get => _axis2Visibility;
            set => this.RaiseAndSetIfChanged(ref _axis2Visibility, value);
        }
        private bool _axis0Visibility;
        public bool Axis0Visibility
        {
            get => _axis0Visibility;
            set => this.RaiseAndSetIfChanged(ref _axis0Visibility, value);
        }

        private bool _axis3Visibility;
        public bool Axis3Visibility
        {
            get => _axis3Visibility;
            set => this.RaiseAndSetIfChanged(ref _axis3Visibility, value);
        }

        private double _maxRange;
        public double MaxRange
        {
            get => _maxRange;
            set => this.RaiseAndSetIfChanged(ref _maxRange, value);
        }


        private Func<double, string> _xFormatter;
        public Func<double, string> XFormatter
        {
            get => _xFormatter;
            set => this.RaiseAndSetIfChanged(ref _xFormatter, value);
        }

        public Func<double, string> YFormatter { get; set; }

        public double Unit { get; } = TimeSpan.FromMinutes(5).Ticks;
    }

}
