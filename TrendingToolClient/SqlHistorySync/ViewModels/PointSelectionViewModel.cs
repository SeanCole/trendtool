﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using TechneauxReportingDataModel.CygNet.Rules;
using System.Threading.Tasks;
using CygNetRuleModel.Resolvers;
using TrendingToolClient.SqlHistorySync.ViewModels.Sub;
using TrendingToolClient.Utility;
using TrendingToolClient.ViewModels;
using TrendingToolClient.ViewModels.CygNet.Reactive;
using ReactiveUI;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using TechneauxHistorySynchronization.Models;
using TechneauxReportingDataModel.Helper;
using TechneauxReportingDataModel.SqlHistory.Enumerable;

namespace TrendingToolClient.SqlHistorySync.ViewModels
{
    public class PointSelectionViewModel : ReactiveObject
    {
        private readonly CygNetFacilityPointSearchViewModel _srcCygNetModel;


        public BindingList<SqlPointSelection> PointSelectionConfigRuleList { get; }

        private bool _firstRun;
        public bool FirstRun
        {
            get => _firstRun;
            set => this.RaiseAndSetIfChanged(ref _firstRun, value);
        }



        public PointSelectionViewModel(
            CygNetFacilityPointSearchViewModel srcCygNetModel,
            BindingList<SqlPointSelection> srcPointRules)
        {
            var sqlRuleComparer = new CompareRuleOnPointSelection();

            UniquePointSelectionRules = new HashSet<SqlPointSelection>(srcPointRules
                                .Where(rule => rule.PointSelection.IsRuleValid)).ToList();

            PointSelectionConfigRuleList = srcPointRules;

            RuleList = new RuleGridViewModel<SqlPointSelection, SqlPointSelectionGridViewModel>(srcPointRules);
            _srcCygNetModel = srcCygNetModel;

            Observable.FromEventPattern<ListChangedEventHandler, ListChangedEventArgs>
                (h => srcPointRules.ListChanged += h, h => srcPointRules.ListChanged -= h)
                .Where(args => args.EventArgs.ListChangedType == ListChangedType.ItemAdded ||
                        args.EventArgs.ListChangedType == ListChangedType.ItemDeleted ||
                        args.EventArgs.ListChangedType == ListChangedType.ItemChanged)
                .Select(args => (args.Sender as BindingList<SqlPointSelection>)
                                .Where(rule => rule.PointSelection.IsRuleValid))
                .Select(rules => new HashSet<SqlPointSelection>(rules, sqlRuleComparer))
                .DistinctUntilChanged()
                .Sample(TimeSpan.FromSeconds(1))
                .ObserveOnDispatcher()
                .Select(set => set.ToList())
                .Subscribe(res => { UniquePointSelectionRules = res; });


            _udcList = this.WhenAnyValue(me => me.UniquePointSelectionRules)
                .Select(rules => new HashSet<string>(rules.Select(item => item.PointSelection.UDC)))
                .DistinctUntilChanged()
                .Select(set => set.ToList())
                .ToProperty(this, x => x.UdcList, new List<string>());

            var pointSrcDataChanged = this.WhenAnyValue(me => me.UniquePointSelectionRules, me => me._srcCygNetModel.CachedFacilities)
                .Sample(TimeSpan.FromSeconds(1))
                .Select(args => (srcFacs: args.Item1, udcList: args.Item2))
                .Throttle(TimeSpan.FromSeconds(1))
                .Publish().RefCount();

            PointListUpdateCmd = ReactiveCommand
                .CreateFromObservable<(IEnumerable<SqlPointSelection> pointSelectRules, IEnumerable<CachedCygNetFacility> srcFacs),
                                        (BindingList<CachedCygNetPointWithRule> Points, List<FacilityUdcResult> UdcResults)>
                    (args => Observable.StartAsync(ct => CygNetPointResolver.GetPointsAsync(UniquePointSelectionRules, _srcCygNetModel.CachedFacilities, _srcCygNetModel.CygNetOptsModel.FacilityFilteringRules.ChildGroups.ToList(), ct))
                .TakeUntil(pointSrcDataChanged));

            pointSrcDataChanged.Subscribe(_ => PointListUpdateCmd.Execute().Subscribe());

            _pointList = PointListUpdateCmd
                .Select(results => results.Points)
                .ToProperty(this, x => x.PointList, new BindingList<CachedCygNetPointWithRule>());

            _facilityUdcGrid = PointListUpdateCmd.Select(results => results.UdcResults.ToObservable().Delay(TimeSpan.FromMilliseconds(10)).CreateCollection())
                .ToProperty(this, x => x.FacilityUdcGrid);


            // Build list of point selection view models here
            // Init point selection datagrid here

        }


        public class CompareRuleOnPointSelection : IEqualityComparer<SqlPointSelection>
        {
            public bool Equals(SqlPointSelection x, SqlPointSelection y)
            {
                return x.PointSelection == y.PointSelection;
            }

            public int GetHashCode(SqlPointSelection obj)
            {
                return obj.PointSelection.GetHashCode();
            }
        }

        public ReactiveCommand<(IEnumerable<SqlPointSelection> pointSelectRules, IEnumerable<CachedCygNetFacility>
                srcFacs), (BindingList<CachedCygNetPointWithRule> Points, List<FacilityUdcResult> UdcResults)>
            PointListUpdateCmd
        { get; set; }

        // Expose list of point selection viewmodels to data grid

        private List<SqlPointSelection> _uniquePointSelectionRules;
        public List<SqlPointSelection> UniquePointSelectionRules
        {
            get => _uniquePointSelectionRules;
            set => this.RaiseAndSetIfChanged(ref _uniquePointSelectionRules, value);
        }


        private readonly ObservableAsPropertyHelper<List<string>> _udcList;
        public List<string> UdcList => _udcList.Value;


        private readonly ObservableAsPropertyHelper<IReactiveDerivedList<FacilityUdcResult>> _facilityUdcGrid;
        public IReactiveDerivedList<FacilityUdcResult> FacilityUdcGrid => _facilityUdcGrid.Value;


        private readonly ObservableAsPropertyHelper<BindingList<CachedCygNetPointWithRule>> _pointList;
        public BindingList<CachedCygNetPointWithRule> PointList => _pointList.Value;


        private RuleGridViewModel<SqlPointSelection, SqlPointSelectionGridViewModel> _ruleList;
        public RuleGridViewModel<SqlPointSelection, SqlPointSelectionGridViewModel> RuleList
        {
            get => _ruleList;
            set => this.RaiseAndSetIfChanged(ref _ruleList, value);
        }

        //public async Task<List<FacilityUdcResult>> UpdatePointGridResults(List<CachedCygNetPointWithRule> srcPoints)
        //{
        //    var ThisFacPointResult = new FacilityUdcResult(fac);
        //    facResultList.Add(ThisFacPointResult);

        //    var fac = new CachedCygNetFacility();

        //    fac.PointCache.

        //    FacilityUdcGrid = facResultList;
        //}
    }
}

