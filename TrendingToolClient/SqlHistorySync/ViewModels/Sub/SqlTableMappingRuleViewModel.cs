using System.Windows;
using TrendingToolClient.ViewModels;
using TrendingToolClient.ViewModels.CygNet.Sub;
using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using XmlDataModelUtility;

namespace TrendingToolClient.SqlHistorySync.ViewModels.Sub
{
    public class SqlTableMappingRuleViewModel : NotifyDynamicBase<CygNetRuleBase>
    {
        public SqlTableMappingRuleViewModel(SqlTableMapping srcModel) : base(srcModel.CygNetElementRule)
        {
            //normalization = new NormalizationViewModel(srcModel.NormalizationOptions);
            NoteHistory = new NoteHistoryViewModel(srcModel.CygNetElementRule.NoteHistoryOptions);
            PointHistory = new PointHistoryViewModel(srcModel.CygNetElementRule.PointHistoryOptions);
            AttributeView = new AttributeViewModel(srcModel.CygNetElementRule.AttributeOptions);
            FormatConfig = new FormatViewModel(srcModel.CygNetElementRule.FormatConfig);

            IsUdcVisible = Visibility.Visible;
            IsDataVisible = Visibility.Visible;
            IsFacVisible = Visibility.Visible;
            //IsChildOrdVisible = System.Windows.Visibility.Visible;
            IsFacilityAttributeSelectionVisible = Visibility.Visible;
            IsHistoryTypeVisible = Visibility.Visible;
            IsNoteHistoryTypeVisible = Visibility.Collapsed;
            IsNoteTypeVisible = Visibility.Collapsed;
            IsAttributeTypeVisible = Visibility.Visible;
            IsPointValueTypeVisible = Visibility.Visible;
            //IsChildGroupVisible = Visibility.Visible;
            IsNoteValueTypeVisible = Visibility.Collapsed;

            IsUnreliableVisible = Visibility.Visible;
            IsPointAttributeSelectionVisible = Visibility.Visible;
            IsFacilityNoteSelectionVisible = Visibility.Collapsed;
            IsIdVisible = Visibility.Visible;
            IsPointNoteSelectionVisible = Visibility.Collapsed;
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsFacilityNoteSelectionVisible
        {
            get
            {
                SetPropertyValue(Visibility.Collapsed);
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsPointNoteSelectionVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsNoteValueTypeVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsAttributeTypeVisible
        {
            get
            {
                if (MyDataModel.DataElement == CygNetRuleBase.DataElements.FacilityAttribute ||
                    MyDataModel.DataElement == CygNetRuleBase.DataElements.PointAttribute)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsHistoryTypeVisible
        {
            get
            {
                if (MyDataModel.DataElement == CygNetRuleBase.DataElements.PointHistory || MyDataModel.DataElement == CygNetRuleBase.DataElements.PointCurrentValue)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsNoteHistoryTypeVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        //[AffectedByOtherPropertyChange(nameof(CygNetRule.DataElement))]
        public Visibility IsUdcVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        public Visibility IsDataVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        public Visibility IsFacVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsIdVisible
        {
            get
            {
                if (MyDataModel.DataElement == CygNetRuleBase.DataElements.PointAttribute ||
                    MyDataModel.DataElement == CygNetRuleBase.DataElements.FacilityAttribute)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsUnreliableVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsNoteTypeVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        
        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsPointValueTypeVisible
        {
            get
            {
                if (MyDataModel.DataElement == CygNetRuleBase.DataElements.PointHistory || MyDataModel.DataElement == CygNetRuleBase.DataElements.PointCurrentValue)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsFacilityAttributeSelectionVisible
        {
            get
            {
                if (
                    MyDataModel.DataElement == CygNetRuleBase.DataElements.FacilityAttribute)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsPointAttributeSelectionVisible
        {
            get
            {
                if (
                    MyDataModel.DataElement == CygNetRuleBase.DataElements.PointAttribute)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        public PollingRetentionViewModel Polling
        {

            get => GetPropertyValue<PollingRetentionViewModel>();
            set => SetPropertyValue(value);
        }

        public NormalizationViewModel Normalization
        {
            get => GetPropertyValue<NormalizationViewModel>();
            set => SetPropertyValue(value);
        }

        public NoteHistoryViewModel NoteHistory
        {
            get => GetPropertyValue<NoteHistoryViewModel>();
            set => SetPropertyValue(value);
        }
        public PointHistoryViewModel PointHistory
        {
            get => GetPropertyValue<PointHistoryViewModel>();
            set => SetPropertyValue(value);
        }
        public AttributeViewModel AttributeView
        {
            get => GetPropertyValue<AttributeViewModel>();
            set => SetPropertyValue(value);
        }
        public FormatViewModel FormatConfig
        {
            get => GetPropertyValue<FormatViewModel>();
            set => SetPropertyValue(value);
        }
    }
}
