using System.Windows;
using System.Windows.Input;
using TrendingToolClient.Forms;
using TrendingToolClient.ViewModels;
using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using TechneauxWpfControls.DataConverters;
using XmlDataModelUtility;

namespace TrendingToolClient.SqlHistorySync.ViewModels.Sub
{
    public class SqlPointSelectionRuleViewModel : NotifyDynamicBase<FacilityPointSelection>
    {
        SqlPointSelection _fullSrcDataModel;
        public SqlPointSelectionRuleViewModel(SqlPointSelection srcModel) : base(srcModel.PointSelection)
        {
            _fullSrcDataModel = srcModel;

            Normalization = new NormalizationViewModel(srcModel.HistoryNormalizationOptions);
            PointHistory = new PointHistoryViewModel(srcModel.GeneralHistoryOptions);
            Polling = new PollingRetentionViewModel(srcModel.PollingOptions);

            IsUdcVisible = Visibility.Visible;
            IsDataVisible = Visibility.Visible;
            IsFacVisible = Visibility.Visible;
            IsChildOrdVisible = Visibility.Visible;
            IsNoteHistoryTypeVisible = Visibility.Collapsed;
            IsNoteTypeVisible = Visibility.Collapsed;
            IsChildGroupVisible = Visibility.Visible;
            IsNoteValueTypeVisible = Visibility.Collapsed;

            IsUnreliableVisible = Visibility.Visible;
            IsFacilityNoteSelectionVisible = Visibility.Collapsed;
            IsPointNoteSelectionVisible = Visibility.Collapsed;
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsFacilityNoteSelectionVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsPointNoteSelectionVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsNoteValueTypeVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsNoteHistoryTypeVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        //[AffectedByOtherPropertyChange(nameof(CygNetRule.DataElement))]
        public Visibility IsUdcVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(FacilityPointSelection.FacilityChoice))]
        public Visibility IsChildOrdVisible
        {
            get
            {                
                if (MyDataModel.FacilityChoice.Equals(FacilitySelection.FacilityChoices.ChildFacility))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        public Visibility IsDataVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        public Visibility IsFacVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
       

        [AffectedByOtherPropertyChange(nameof(FacilityPointSelection.FacilityChoice))]
        public Visibility IsChildGroupVisible
        {
            get
            {               
                if (MyDataModel.FacilityChoice.Equals(FacilitySelection.FacilityChoices.ChildFacility))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        
        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsUnreliableVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsNoteTypeVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }     

        public ICommand OpenChildGroupWindow => new DelegateCommand(OpenSelectChildGroupWindow);

        public void OpenSelectChildGroupWindow(object inputObject)
        {
            var windowInstance = CygNetChildFacilityWindow.Instance();

            var success = windowInstance.TryGetKeyDescId(out var result);
            if (success)
            {
                if (result.Value != null)
                {
                    MyDataModel.ChildGroupKey.Id = result.Value as string;

                }
                if (result.Description != null)
                {
                    MyDataModel.ChildGroupKey.Desc = result.Description as string;
                }
            }
        }

        public PollingRetentionViewModel Polling
        {
            get => GetPropertyValue<PollingRetentionViewModel>();
            set => SetPropertyValue(value);
        }

        public NormalizationViewModel Normalization
        {
            get => GetPropertyValue<NormalizationViewModel>();
            set => SetPropertyValue(value);
        }

        public PointHistoryViewModel PointHistory
        {
            get => GetPropertyValue<PointHistoryViewModel>();
            set => SetPropertyValue(value);
        }
       
    }
}
