using TechneauxReportingDataModel.SqlHistory.Enumerable;
using XmlDataModelUtility;

namespace TrendingToolClient.SqlHistorySync.ViewModels.Sub
{
    public class SqlPointSelectionGridViewModel : NotifyDynamicBase<SqlPointSelection>
    {    
        public SqlPointSelectionGridViewModel(SqlPointSelection srcModel) : base(srcModel)
        {           
            ConfiguredRuleView = new SqlPointSelectionRuleViewModel(MyDataModel);
        }

        public SqlPointSelectionRuleViewModel ConfiguredRuleView
        {
            get => GetPropertyValue<SqlPointSelectionRuleViewModel>();
            set => SetPropertyValue(value);
        }    
    }
}
