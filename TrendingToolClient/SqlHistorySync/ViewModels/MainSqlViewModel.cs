using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using TrendingToolClient.Forms;
using TrendingToolClient.ViewModels;
using TrendingToolClient.ViewModels.CygNet.Reactive;
using Techneaux.CygNetWrapper.Points;
using TechneauxReportingDataModel.SqlHistory;
using XmlDataModelUtility;

namespace TrendingToolClient.SqlHistorySync.ViewModels
{
    public class MainSqlViewModel : NotifyDynamicBase<SqlHistorySyncConfigModel>
    {  
        public CygNetFacilityPointSearchViewModel CygNetDataViewModel { get; }

        public List<ICachedCygNetPoint> PointList
        {
            get => GetPropertyValue<List<ICachedCygNetPoint>>();
            set => SetPropertyValue(value);
        }
        
        public MainSqlViewModel(SqlHistorySyncConfigModel srcConfigModel, CygNetFacilityPointSearchViewModel cygViewModel) : base(srcConfigModel)
        {            
            SqlDatabaseTableChooser.StaticDataContext = this;

            SqlGeneralSubViewModel = new SqlGeneralViewModelReactive(srcConfigModel.SqlGeneralOpts, srcConfigModel.TableMappingRules);
            GeneralHistoryViewModel = new HistorySyncViewModel(srcConfigModel.HistorySyncOpts);
            
        //    FacilityPointPreviewViewModel = new PointPreviewSubViewModel(SqlGeneralSubViewModel.DataOnlyModel, MyDataModel);
            var udcList = MyDataModel.SourcePointRules
                                    .Select(rule => rule.PointSelection.UDC);

            //FacilityPointPreviewViewModel.UdcPreviewList = udcList.ToList();
            //PointSelectionVm = new PointSelectionViewModel(cygViewModel, MyDataModel.SourcePointRules);
            LivePreviewVm = new LivePreviewViewModel(PointSelectionVm, SqlGeneralSubViewModel, srcConfigModel);

            HookUpPointPreviewTriggers();
            MyDataModel.TableMappingRules.ListChanged += SqlFieldElements_ListChanged;
            CygNetDataViewModel = cygViewModel;
        }

        public LivePreviewViewModel LivePreviewVm
        {
            get => GetPropertyValue<LivePreviewViewModel>();
            set => SetPropertyValue(value);
        }

        public PointSelectionViewModel PointSelectionVm
        {
            get => GetPropertyValue<PointSelectionViewModel>();
            set => SetPropertyValue(value);
        }
        
        private void SqlFieldElements_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.PropertyDescriptor != null)
            {
                if (e.PropertyDescriptor.Name == "TimeColumn")
                {
                    if (MyDataModel.TableMappingRules[e.NewIndex].LastPropertyUpdated == "TimeColumn")
                    {
                        if (MyDataModel.TableMappingRules[e.NewIndex].TimeColumn == true)
                        {
                            for (var i = 0; i < MyDataModel.TableMappingRules.Count; i++)
                            {
                                if (i != e.NewIndex)
                                {
                                    MyDataModel.TableMappingRules[i].TimeColumn = false;
                                }
                            }
                        }
                    }
                }
            }
        }       
        
        //
        public void HookUpPointPreviewTriggers()
        {
            var pointCollection = MyDataModel.SourcePointRules;

        }

        //public PointPreviewSubViewModel FacilityPointPreviewViewModel
        //{
        //    get { return GetPropertyValue<PointPreviewSubViewModel>(); }
        //    set { SetPropertyValue(value); }
        //}      

        public SqlGeneralViewModelReactive SqlGeneralSubViewModel
        {
            get => GetPropertyValue<SqlGeneralViewModelReactive>();
            set => SetPropertyValue(value);
        }

        public HistorySyncViewModel GeneralHistoryViewModel
        {
            get => GetPropertyValue<HistorySyncViewModel>();
            set => SetPropertyValue(value);
        }
    }
}