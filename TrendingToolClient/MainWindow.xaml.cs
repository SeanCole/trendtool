﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using TrendingToolClient.Forms.Test;
using TrendingToolClient.ViewModels;
using TrendingToolClient.ViewModels.General;
using MahApps.Metro.Controls;
using Serilog;
using Serilog.Events;
using TechneauxReportingDataModel.General;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using System.Collections.Generic;
using TrendingToolClient.ViewModels.CygNet.Sub;
using TrendingToolClient.ViewModels.CygNet.Reactive;
using System.Windows.Forms;

namespace TrendingToolClient
{
    /// <summary>
    /// 
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        //ReportConfigModel CurrentModel = new ReportConfigModel();
        //MainViewModel myViewModel;

        public static MetroWindow ClientParentWindow;

        private ConfigModelFileService _configFile;

        public MainWindow()
        {
            EnsureApplicationResources();

            var path = System.IO.Path.GetDirectoryName((new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath).Replace("%20", " ");
            Assembly.LoadFrom($@"{path}\MahApps.Metro.dll");
            Assembly.LoadFrom($@"{path}\ControlzEx.dll");
            Assembly.LoadFrom($@"{path}\System.Windows.Interactivity.dll");
            
            InitializeComponent();
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += MyHandler;
            ClientParentWindow = this;

            //MessageBox.Show(Assembly.GetExecutingAssembly().GetName().Version.ToString());

            Title = $"Techneaux Trending Tool (Demo {Assembly.GetExecutingAssembly().GetName().Version})";

            try
            {
                var fileName = $"client_Log_" + "{Date}.txt";
                
                Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .WriteTo
                    .RollingFile($@"{AppDomain.CurrentDomain.BaseDirectory}Logs\{fileName}", shared: true)
                    .CreateLogger();
                Log.Debug("Launching client.");

                var myPath = AppDomain.CurrentDomain.BaseDirectory;
                
                //var commandLineParams = Environment.GetCommandLineArgs().ToList();
                _configFile = new ConfigModelFileService(this, myPath);
                _configFile.ResetModified();

                //if (commandLineParams.Count > 0 && commandLineParams.Exists(item => item.ToLower() == @"/autorun"))
                //{
                //    CancellationTokenSource ctSource = new CancellationTokenSource();
                //    AutoOperation.RunAutoOperationAsync(DateTime.Today, Environment.GetCommandLineArgs().ToList(), _configFile, ctSource.Token);

                //    Close();
                //}

                var newConfigModel = new ReportConfigModel();

                FileOpsPanel.DataContext = _configFile;

                if (_configFile.ThisConfigModel != null)
                {
                    var myViewModel = new MainViewModel(_configFile.ThisConfigModel);

                    _configFile.ResetModified();
                    DataContext = myViewModel;
                }
                OpenedOnce = false;
                Closing += MainWindow_Closing;
                BtnNewFile.Click += Button_New;
                BtnOpenFile.Click += Button_Open;
                BtnSaveFile.Click += Button_Save;
                BtnSaveFileAsNew.Click += Button_SaveAsNew;
                SetFacility = SetCurrentFac.None;
                this.Closed += MainWindow_Closed;
                Instance = this;
                //if (!string.IsNullOrWhiteSpace(TrendCombo.SelectedValue as string))
                //{
                //    OpenFile((string)TrendCombo.SelectedValue);

                //}
                //LoadFacility("WMSWIL01", "UIS01", "MTREFM_62340596");
            }
            catch (Exception e)
            {
                Log.Error(e, "Fatal error");
                System.Windows.Forms.MessageBox.Show(e.ToString());
            }
        }
        static void MyHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            if (e.InnerException != null)
                Log.Fatal("Unhandled exception InnerExceptionMessage: " + e.InnerException.Message);
            Log.Fatal("Unhandled exception StackTrace: " + e.StackTrace);
            Log.Fatal("Unhandled exception Message: " + e.Message);
            Log.Fatal("Unhandled exception Source: " + e.Source);

            Console.WriteLine($@"MyHandler caught : {e.Message}");
            Console.WriteLine(@"Runtime terminating: {0}", args.IsTerminating);
            System.Windows.Forms.MessageBox.Show(@"An unhandled exception occurred and the program will not exit. See Log for details.", @"Error", MessageBoxButtons.OK);
        }
        public void LoadFacility(string site, string service, string facility)
        {
            var myViewModel = this.DataContext as MainViewModel;
            if(SetFacility == SetCurrentFac.None && !OpenedOnce)
            {
                SetFacility = SetCurrentFac.OnOpen;
            }
            var facOpt = new FacilityAttributeFilterCondition();
            facOpt.AttributeId = "facility_id";
            facOpt.AttributeDescription = "Facility ID";
            facOpt.ConditionalOperator = SqlConditionalOperators.Like;
            facOpt.ConditionalValue = facility;
            var serviceOpt = new FacilityAttributeFilterCondition();
            serviceOpt.AttributeId = "facility_service";
            serviceOpt.ConditionalOperator = SqlConditionalOperators.Like;
            serviceOpt.ConditionalValue = service;
            serviceOpt.AttributeDescription = "Service";
            var siteOpt = new FacilityAttributeFilterCondition();
            siteOpt.AttributeId = "facility_site";
            siteOpt.AttributeDescription = "Site";
            siteOpt.ConditionalOperator = SqlConditionalOperators.Like;
            siteOpt.ConditionalValue = site;
            myViewModel.ReactiveCygNetViewModel.FacilityFilteringSubViewModel.MyDataModel.BaseRules.Clear();
            myViewModel.ReactiveCygNetViewModel.FacilityFilteringSubViewModel.ConditionList.Clear();
            myViewModel.ReactiveCygNetViewModel.FacilityFilteringSubViewModel.AddExistingCondition(facOpt);
            myViewModel.ReactiveCygNetViewModel.FacilityFilteringSubViewModel.AddExistingCondition(siteOpt);
            myViewModel.ReactiveCygNetViewModel.FacilityFilteringSubViewModel.AddExistingCondition(serviceOpt);
            //myViewModel.LivePreviewVm.SelectedFac = $"{site}.{service}::{facility}";
            //SelectedFacTarget = $"ALL";

        }
      
        public static MainWindow Instance { get; private set; }

        private void MainWindow_Closed(object sender, EventArgs e)
        {

            TrendingToolClient.MainWindow.Instance = null;
            //Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
            //Application.Current.Shutdown();
        }

        public static void EnsureApplicationResources()
        {
            
            if (System.Windows.Application.Current == null)
            {
                // create the Application object
                //if (Application.Current == null)
                //{
                //    MyApplication = new Application
                //    {
                //        ShutdownMode = ShutdownMode.OnExplicitShutdown
                //    };
                //}
                //else
                //    MyApplication = Application.Current;
                                
                new App()
                {
                    ShutdownMode = ShutdownMode.OnExplicitShutdown
                }; 

                var uriStrings = new List<string>()
                {
                    "MahApps.Metro;component/Styles/Controls.xaml" ,
                    //"pack://application:,,,/MahApps.Metro;component/Styles/Brushes.xaml",
                    "MahApps.Metro;component/Styles/Fonts.xaml",
                    "MahApps.Metro;component/Styles/Colors.xaml",
                    "MahApps.Metro;component/Styles/Accents/Blue.xaml",
                    "MahApps.Metro;component/Styles/Accents/Orange.xaml",
                    "MahApps.Metro;component/Styles/Accents/BaseLight.xaml",
                    "MahApps.Metro;component/Styles/Clean/Clean.xaml"
                };

                
                // merge in your application resources
                uriStrings.ForEach(str =>
                {
                    try
                    {
                        System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                            System.Windows.Application.LoadComponent(
                                new Uri(str,
                                UriKind.Relative)) as ResourceDictionary);

                        Log.Debug($"Succeeded on {str}");
                    }
                    catch (Exception)
                    {
                        Log.Debug($"Failed on {str}");
                    }
                });

                //Application.Current.Resources.MergedDictionaries.Add(
                //    Application.LoadComponent(
                //        new Uri("MyLibrary;component/Resources/MyResourceDictionary.xaml",
                //        UriKind.Relative)) as ResourceDictionary);
            }
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
          
        }



        private async void Button_New(object sender, RoutedEventArgs e)
        {
            //InitializeComponent();
            var res = await _configFile.CreateNewConfig();
            if (res.Completed)
            {
                TrendCombo.SelectedIndex = TrendCombo.Items.IndexOf(res.NewName);

                return;

                var myViewModel = new MainViewModel(_configFile.ThisConfigModel);

                DataContext = null;
                DataContext = myViewModel;
                _configFile.ResetModified();
            }
        }

        private async void Button_Open(object sender, RoutedEventArgs e)
        {
            
        }

        private void OpenFile(string name)
        {
            if (_configFile.OpenConfigFile(name))
            {
                try
                {
                    var myViewModel = new MainViewModel(_configFile.ThisConfigModel);

                    DataContext = null;

                    this.DataContext = myViewModel;
                    _configFile.ResetModified();
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Fatal error");
                    System.Windows.Forms.MessageBox.Show(ex.ToString());
                }
            }
        }

        private SetCurrentFac SetFacility { get; set; }

        public enum SetCurrentFac
        {
            None,
            OnOpen,
            NotOnOpen
        }
        private string SelectedFacTarget { get; set; }

        private bool OpenedOnce { get; set; }

        private void Button_Save(object sender, RoutedEventArgs e)
        {
            //_configFile.SaveConfigFile();
        }

        private async void Button_SaveAsNew(object sender, RoutedEventArgs e)
        {
            await _configFile.SaveAsNew();
        }

        private void TrendCombo_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(TrendCombo.SelectedValue as string))
            {
                OpenFile((string) TrendCombo.SelectedValue);
                //if(SetFacility == SetCurrentFac.OnOpen && !OpenedOnce)
                //{
                //    var myViewModel = this.DataContext as MainViewModel;
                //    myViewModel.LivePreviewVm.SelectedFac = SelectedFacTarget;
                  
                //    SetFacility = SetCurrentFac.NotOnOpen;
                //}
                //OpenedOnce = true;
                   
            }
            
        }
    }
}

