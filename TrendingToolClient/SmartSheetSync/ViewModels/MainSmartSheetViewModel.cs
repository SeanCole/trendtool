﻿using TrendingToolClient.SmartSheetSync.View;
using TrendingToolClient.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using TrendingToolClient.ViewModels.CygNet.Reactive;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using TechneauxReportingDataModel.CygNet;
using TechneauxReportingDataModel.CygNet.Utility;
using TechneauxReportingDataModel.SmartSheet;
using TechneauxReportingDataModel.SmartSheet.Enumerable;
using TechneauxReportingDataModel.SmartSheet.SubOptions;
using TechneauxSmartSheetSync;
using TechneauxSmartSheetSync.Resolver;
using TechneauxWpfControls.DataConverters;
using TrendingToolClient.Forms;
using GenericRuleModel.Helper;

namespace TrendingToolClient.SmartSheetSync.ViewModels
{
    public class MainSmartSheetViewModel : ReactiveObject
    {
        private SmartSheetSyncConfigModel SrcConfigModel { get; }
        private CygNetFacilityPointSearchViewModel SrcCygViewModel { get; }

        public MainSmartSheetViewModel(
            SmartSheetSyncConfigModel srcDataModel,
            CygNetFacilityPointSearchViewModel cygVm,
            BindingList<ChildFacilityGroup> childGrps)
        {

            // Init vars
            SrcConfigModel = srcDataModel;

            //DictList = new BindingList<ReportPreviewViewModel>();

            SheetSelectionOptsVm = new SmartSheetSelectionOptionsViewModel(SrcConfigModel.SheetSelectionOpts);
            SrcCygViewModel = cygVm;

            // Init the mapping rule datagrid view model
            RuleList = new RuleGridViewModel<SmartSheetReportElm, SmartSheetReportElmViewModel>(srcDataModel.ColumnRules);

            // Set up evts around child fac groups rules and populate list initially from data model
            ChildFacGroups = new List<ChildFacilityGroup>(childGrps.ToList());

            Observable.FromEventPattern<ListChangedEventHandler, ListChangedEventArgs>(
                    h => childGrps.ListChanged += h,
                    h => childGrps.ListChanged -= h)
                .Select(evt => (evt.Sender as BindingList<ChildFacilityGroup>))
                .Select(x => x.ToList())
                .Subscribe(x => { ChildFacGroups = x; });


            // Set up evts around column rules and init rules initially from data model
            ValidColumnRules = new List<SmartSheetReportElm>(srcDataModel.ColumnRules.Where(rule => rule.IsRuleValid).ToList());
            ColumnRules = new List<SmartSheetReportElm>(srcDataModel.ColumnRules.ToList());

            var sourceRulesChangedObs = Observable.FromEventPattern<ListChangedEventHandler, ListChangedEventArgs>(
                h => RuleList.SourceRules.ListChanged += h,
                h => RuleList.SourceRules.ListChanged -= h)
                .Select(evt => (evt.Sender as BindingList<SmartSheetReportElm>));


            sourceRulesChangedObs.Throttle(TimeSpan.FromSeconds(.5), RxApp.MainThreadScheduler).Subscribe(x => { ValidColumnRules = x.Where(rule => rule.IsRuleValid).ToList(); });
            sourceRulesChangedObs.Throttle(TimeSpan.FromSeconds(.5), RxApp.MainThreadScheduler).Subscribe(x => { ColumnRules = x.ToList(); });

            // Update report preview listview column headers when mapping rules change
            _ruleListItemIds = this.WhenAnyValue(me => me.ValidColumnRules)
                .Select(x =>
                    x.Select(y =>
                        y?.SmartSheetColumn ?? new SmartSheetItemId()).ToList())
                .ToProperty(this, x => x.RuleListItemIds, new List<SmartSheetItemId>());


            // Update report preview
            var selectedSheetChanged = this.WhenAnyValue(x => x.SelectedLiveSheet)
                .Publish()
                .RefCount();

            _dictList = this.WhenAnyValue(
                    me => me.SrcCygViewModel.CachedFacilities,
                    me => me.ChildFacGroups,
                    me => me.ValidColumnRules,
                    me => me._defaultRollupPeriod)
                .Where(parms => parms.Item3.Any())
                .Select(parms => parms.Item1.ToObservable()
                    .Select(fac => Observable.FromAsync(async ct =>
                    {
                        var newRow = await Task.Run(() =>
                            ResolveSmartsheet.ResolveSingleRow(fac, parms.Item2, parms.Item3, parms.Item4, ct), ct);

                        return newRow;
                    }).Delay(TimeSpan.FromMilliseconds(50))).Merge(10))
                .Select(list => list.CreateCollection())
                .ToProperty(this, x => x.DictList, Observable.Empty<SmartSheetHistoryRow>().CreateCollection());

            //var GetReportRows = ReactiveCommand.CreateFromTask(<IEnumerable<ICachedCygNetFacility>, IObservable<)

            // Update row validation errors
            _validationError = this.WhenAnyValue(me => me.ColumnRules)
                .Select(rules =>
                    rules.Any(rule => !rule.IsRuleValid)
                        ? rules.First(rule => !rule.IsRuleValid).ValidationErrorMessage
                        : "")
                .ToProperty(this, x => x.ValidationError, "");


            var getSheetList = ReactiveCommand.CreateFromTask<string, IObservable<SheetWrapper>>(
                SmartSheetModel.GetSheetList);

            this.WhenAnyValue(x => x.AccessToken)
                .Sample(TimeSpan.FromSeconds(.5))
                .InvokeCommand(getSheetList);


            _liveSheetList = getSheetList.Select(x => x.CreateCollection()).ToProperty(this, x => x.LiveSheetList,
                Observable.Empty<SheetWrapper>().CreateCollection());

            var sheetChanged = SrcConfigModel.SheetSelectionOpts.WhenAnyValue(x => x.SheetId).Publish().RefCount();
            sheetChanged.Subscribe(async _ => { SelectedLiveSheet = await SmartSheetModel.TryGetSheet(AccessToken, srcDataModel.SheetSelectionOpts); SmartSheetColumnIdWindow.StaticDataContext = SelectedLiveSheet; });
            //if (LiveSheetList != null)
            //{


            //    foreach (var sht in LiveSheetList)
            //    {
            //        if (sht.SheetId.Equals(srcDataModel.SheetSelectionOpts.SheetId))
            //        {
            //            // SelectedLiveSheet = await TryGetSheet(AccessToken, srcDataModel.SheetSelectionOpts);
            //        }

            //    }
            //}
            // Set data context for popup windows
            SmartSheetIdWindow.StaticDataContext = this;
            
            SmartSheetColumnIdWindow.StaticDataContext = SelectedLiveSheet;
            SmartSheetColumnIdWindow.SheetIdList = RuleList.SourceRules;

        }


        private RollupPeriod _defaultRollupPeriod = new RollupPeriod(
            DateTime.Now.Date.AddHours(8).AddDays(-1),
            DateTime.Now.Date.AddHours(8));

        private bool _helperFlag;

        public bool HelperFlag
        {
            get => _helperFlag;
            set => this.RaiseAndSetIfChanged(ref _helperFlag, value);
        }
        private List<ChildFacilityGroup> _childFacGroups;

        public List<ChildFacilityGroup> ChildFacGroups
        {
            get => _childFacGroups;
            set => this.RaiseAndSetIfChanged(ref _childFacGroups, value);
        }

        public ReactiveCommand WriteToSmartSheet => ReactiveCommand.Create(async () =>
        {
            IsWriteToSmartSheetBusy = true;
            Console.WriteLine($"Cmd worked, there are {DictList.Count} rows available");
            
            //var cachedFacs = SrcCygViewModel.CachedFacilities.ToList();
   
            var cygModel = SrcCygViewModel.CygNetOptsModel.Clone() as CygNetGeneralOptions;
            var myModel = SrcConfigModel.Clone() as SmartSheetSyncConfigModel;
                WriteToSmartSheetPopup.ShowPopup(
                    cygModel, myModel, _defaultRollupPeriod, new CancellationToken());           
            
            //await WriteReportToSmartSheet.UploadReport(cygModel, myModel, _defaultRollupPeriod, CancellationToken.None);
            IsWriteToSmartSheetBusy = false;
        });


        private readonly ObservableAsPropertyHelper<IReactiveDerivedList<SmartSheetHistoryRow>> _dictList;

        public IReactiveDerivedList<SmartSheetHistoryRow> DictList => _dictList.Value;
        //{
        //    get => _DictList;
        //    set => this.RaiseAndSetIfChanged(ref _DictList, value);
        //}

        private List<SmartSheetReportElm> _validColumnRules;


        private bool _isWriteToSmartSheetBusy;
        public bool IsWriteToSmartSheetBusy
        {
            get => _isWriteToSmartSheetBusy;
            set => this.RaiseAndSetIfChanged(ref _isWriteToSmartSheetBusy, value);
        }


        public List<SmartSheetReportElm> ValidColumnRules
        {
            get => _validColumnRules;
            set => this.RaiseAndSetIfChanged(ref _validColumnRules, value);
        }

        private List<SmartSheetReportElm> _columnRules;

        public List<SmartSheetReportElm> ColumnRules
        {
            get => _columnRules;
            set => this.RaiseAndSetIfChanged(ref _columnRules, value);
        }

        private SmartSheetSelectionOptionsViewModel _sheetSelectionOptsVm;

        public SmartSheetSelectionOptionsViewModel SheetSelectionOptsVm
        {
            get => _sheetSelectionOptsVm;
            set => this.RaiseAndSetIfChanged(ref _sheetSelectionOptsVm, value);
        }

        public string AccessToken
        {
            get => SrcConfigModel.SmartSheetConnectOpts.AccessToken;
            set
            {
                SrcConfigModel.SmartSheetConnectOpts.AccessToken = value;
                this.RaisePropertyChanged();
            }
        }

        private RuleGridViewModel<SmartSheetReportElm, SmartSheetReportElmViewModel> _ruleList;

        public RuleGridViewModel<SmartSheetReportElm, SmartSheetReportElmViewModel> RuleList
        {
            get => _ruleList;
            set => this.RaiseAndSetIfChanged(ref _ruleList, value);
        }

        private readonly ObservableAsPropertyHelper<string> _validationError;
        public string ValidationError => _validationError.Value;

        private readonly ObservableAsPropertyHelper<List<SmartSheetItemId>> _ruleListItemIds;
        public List<SmartSheetItemId> RuleListItemIds => _ruleListItemIds.Value;

        private ObservableAsPropertyHelper<IReactiveDerivedList<SheetWrapper>> _liveSheetList;

        public IReactiveDerivedList<SheetWrapper> LiveSheetList => _liveSheetList.Value;
        //{
        //    get => _liveSheetList2;
        //    set => this.RaiseAndSetIfChanged(ref _liveSheetList2, value);
        //}

        private SheetWrapper _selectedLiveSheet;

        public SheetWrapper SelectedLiveSheet
        {
            get => _selectedLiveSheet;
            set => this.RaiseAndSetIfChanged(ref _selectedLiveSheet, value);
        }

        public ICommand OpenColIdWindow => new DelegateCommand(OpenSelectedColIdWindow);

        public void OpenSelectedColIdWindow(object inputObject)
        {
            GenericRuleModel.Helper.ValueDescription result = new ValueDescription();

            var windowInstance = SmartSheetColumnIdWindow.Instance();

            var success = windowInstance.TryGetKeyDescId(out result);
            if (success)
            {
                if (result.Value != null)
                {
                    SrcConfigModel.SheetSelectionOpts.KeyColumn = result.Value as SmartSheetItemId;
                }
            }
        }

        public ICommand OpenSheetIdWindow => new DelegateCommand(OpenSelectedSheetIdWindow);

        public void OpenSelectedSheetIdWindow(object inputObject)
        {
            GenericRuleModel.Helper.ValueDescription result = new ValueDescription();

            var windowInstance = SmartSheetIdWindow.Instance();

            var success = windowInstance.TryGetKeyDescId(out result);
            if (success)
            {
                if (result.Value != null)
                {
                    SelectedLiveSheet = result.Value as SheetWrapper;
                  
                    SrcConfigModel.SheetSelectionOpts.SheetId = (result.Value as SheetWrapper).SheetId;
                    SmartSheetColumnIdWindow.StaticDataContext = SelectedLiveSheet;
                    SmartSheetColumnIdWindow.SheetIdList = RuleList.SourceRules;
                }
            }
        }
    }
}