﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using TrendingToolClient.SmartSheetSync.ViewModels;
using MahApps.Metro.Controls;

namespace TrendingToolClient.SmartSheetSync.View
{
    /// <summary>
    /// Interaction logic for SmartSheetPointSelection.xaml
    /// </summary> 
    public partial class SmartSheetReportDesign : UserControl
    {
        public SmartSheetReportDesign()
        {
            InitializeComponent();
            RulePicker.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(RulePicker_PreviewMouseLeftButtonDown);
            RulePicker.PreviewMouseMove += new MouseEventHandler(RulePicker_PreviewMouseMove);
            RulePicker.Drop += new DragEventHandler(RulesDataGrid_Drop);
        }
        private Point _startPoint;
        private readonly bool _isDragging = false;

        public delegate Point GetPosition(IInputElement element);
        int _rowIndex = -1;
     

        private void RulePicker_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.OriginalSource is Rectangle || e.OriginalSource is Path ||
                e.OriginalSource.GetType() == typeof(MetroThumb) || e.OriginalSource.GetType() == typeof(TextBlock) ||
                e.OriginalSource.GetType() == typeof(TextBox) || e.OriginalSource.GetType() == typeof(Grid) ||
                e.OriginalSource.GetType() == typeof(Button)) return;
            _rowIndex = GetCurrentRowIndex(e.GetPosition);
            if (_rowIndex < 0)
                return;

            _startPoint = e.GetPosition(null);
        }

        private void RulePicker_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            _rowIndex = GetCurrentRowIndex(e.GetPosition);
            if (_rowIndex < 0)
                return;
            if (e.LeftButton == MouseButtonState.Pressed && !_isDragging && !e.OriginalSource.GetType().Equals(typeof(Rectangle)) && !e.OriginalSource.GetType().Equals(typeof(Path)) && !e.OriginalSource.GetType().Equals(typeof(MetroThumb))
                && !e.OriginalSource.GetType().Equals(typeof(TextBlock)) && !e.OriginalSource.GetType().Equals(typeof(TextBox)) && !e.OriginalSource.GetType().Equals(typeof(Grid)) &&
                !e.OriginalSource.GetType().Equals(typeof(Button)))
            {
                Point position = e.GetPosition(null);

                if (Math.Abs(position.X - _startPoint.X) > SystemParameters.MinimumHorizontalDragDistance*5 ||
                        Math.Abs(position.Y - _startPoint.Y) > SystemParameters.MinimumVerticalDragDistance*5)
                {
                    _rowIndex = GetCurrentRowIndex(e.GetPosition);
                    RulePicker.SelectedIndex = _rowIndex;
                    // Console.WriteLine(rowIndex);
                    var selected = ((MainSmartSheetViewModel)DataContext).RuleList.Rows[_rowIndex];
                    if (selected == null)
                        return;
                    var dragdropeffects = DragDropEffects.Move;
                    if (DragDrop.DoDragDrop(RulePicker, selected, dragdropeffects)
                                        != DragDropEffects.None)
                    {
                        RulePicker.SelectedItem = selected;
                    }

                }
            }
        }


        void RulesDataGrid_Drop(object sender, DragEventArgs e)
        {
            if (!e.OriginalSource.GetType().Equals(typeof(Rectangle)) && !e.OriginalSource.GetType().Equals(typeof(Path)) && !e.OriginalSource.GetType().Equals(typeof(MetroThumb)) &&
                !e.OriginalSource.GetType().Equals(typeof(TextBlock)) && !e.OriginalSource.GetType().Equals(typeof(TextBox)) && !e.OriginalSource.GetType().Equals(typeof(Grid)) &&
                !e.OriginalSource.GetType().Equals(typeof(Button)))
            {
                if (_rowIndex < 0)
                    return;
                var index = GetCurrentRowIndex(e.GetPosition);
                if (index < 0)
                    return;
                if (index == _rowIndex)
                    return;
                if (index == RulePicker.Items.Count)
                {
                    return;
                }
            ((MainSmartSheetViewModel)DataContext).RuleList.DragAndDrop(_rowIndex, index);

            }
        }


        private bool GetMouseTargetRow(Visual theTarget, GetPosition position)
        {
            if (theTarget == null)
                return false;
            Rect rect = VisualTreeHelper.GetDescendantBounds(theTarget);
            var point = position((IInputElement)theTarget);
            return rect.Contains(point);
        }
        private DataGridRow GetRowItem(int index)
        {
            if (RulePicker.ItemContainerGenerator.Status
                    != GeneratorStatus.ContainersGenerated)
                return null;
            return RulePicker.ItemContainerGenerator.ContainerFromIndex(index)
                                                            as DataGridRow;
        }

        private int GetCurrentRowIndex(GetPosition pos)
        {
            var curIndex = -1;
            for (var i = 0; i < RulePicker.Items.Count; i++)
            {
                var itm = GetRowItem(i);
                if (GetMouseTargetRow(itm, pos))
                {
                    curIndex = i;
                    break;
                }
            }
            return curIndex;
        }

        private void RulePicker_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            for (var i = 0; i < RulePicker.Columns.Count; i++)
            {
                RulePicker.Columns[i].Width = 0;
            }

            RulePicker.UpdateLayout();

            for (var j = 0; j < RulePicker.Columns.Count; j++)
            {
                RulePicker.Columns[j].Width = new DataGridLength(1, DataGridLengthUnitType.Auto);
            }

        }
    }

}

