﻿using MahApps.Metro.Controls;
using System;
using System.Windows;
using System.Windows.Input;
using TechneauxWpfControls.DataConverters;
using TechneauxSmartSheetSync;
using GenericRuleModel.Helper;

namespace TrendingToolClient.SmartSheetSync.View
{
    /// <summary>
    /// Interaction logic for SmartSheetColumnIdWindow.xaml
    /// </summary>
    public partial class SmartSheetIdWindow : MetroWindow
    {

        public static object StaticDataContext { get; set; }
        public SmartSheetIdWindow()
        {
            InitializeComponent();
            FieldIdListView.MouseDoubleClick += FieldIdListView_MouseDoubleClick;
        }


        private void FieldIdListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var selectedRowItem = FieldIdListView.SelectedItem as SheetWrapper;
            if (selectedRowItem == null)
            {
                return;
            }


            ThisChooserResult.Value = selectedRowItem;
           

            WasCancelled = false;

            Close();
        }

        private void OnCloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Hide();
        }

        public static SmartSheetIdWindow Instance()
        {
            if (StaticDataContext == null)
            {
                throw new InvalidOperationException("Static Data Context must not be null");
            }

            var instance = new SmartSheetIdWindow
            {
                DataContext = StaticDataContext,
                Owner = Application.Current.MainWindow
            };

            return instance;
        }

        public bool TryGetKeyDescId(out ValueDescription result)
        {
            ShowDialog();

            result = ThisChooserResult;
            return !WasCancelled;


        }

        public bool WasCancelled = false;
        public ValueDescription ThisChooserResult { get; private set; } = new ValueDescription();


        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedRowItem = FieldIdListView.SelectedItem as SheetWrapper;
            if (selectedRowItem == null)
            {
                return;
            }
            ThisChooserResult.Value = selectedRowItem;
            WasCancelled = false;
            Close();
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {

            WasCancelled = true;
            Close();
        }
    }
}

