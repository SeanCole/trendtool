﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using TrendingToolClient.SmartSheetSync.ViewModels;
using TrendingToolClient.Utility;
using MahApps.Metro.Controls;
using TechneauxReportingDataModel.SmartSheet.SubOptions;
using TechneauxWpfControls.DataConverters;

namespace TrendingToolClient.SmartSheetSync.View
{
    /// <summary>
    /// Interaction logic for SmartSheetPointSelection.xaml
    /// </summary> 
    public partial class SmartSheetReportPreview : UserControl
    {
        public SmartSheetReportPreview()
        {
            InitializeComponent();
            Loaded += UserControl1_Loaded;

            DataContextChanged += ReportPreview_DataContextChanged;
        }

        private void ReportPreview_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            AddGridColumns();
        }

        void UserControl1_Loaded(object sender, RoutedEventArgs e)
        {
            if (DataContext is INotifyPropertyChanged)
            {
                var myVm = DataContext as INotifyPropertyChanged;
                myVm.PropertyChanged += MyVM_PropertyChanged;
                AddGridColumns();
            }
          
        }

        private void MyVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "RuleListItemIds")
            {
                AddGridColumns();
            }
        }

        protected void AddGridColumns()
        {
            List<GridViewColumn> garbage = new List<GridViewColumn>();
            if (DataContext is MainSmartSheetViewModel)
            {
                var myVm = DataContext as MainSmartSheetViewModel;
                //myVm.IsPointPreviewBusy = true;
                var ruleList = myVm.RuleListItemIds;
                if (!ruleList.IsAny())
                {
                    if (ReportPreviewGridView.Columns.Count > 1)
                    {
                        for (var k = ReportPreviewGridView.Columns.Count - 1; k > 0; k--)
                        {
                            ReportPreviewGridView.Columns.RemoveAt(k);
                        }
                    }
                    return;
                }

                ReportPreviewGridView.AllowsColumnReorder = true;
                ReportPreviewGridView.ColumnHeaderToolTip = "Column Description";
                if (ReportPreviewGridView.Columns.Count > 0)
                {
                    for (var k = ReportPreviewGridView.Columns.Count - 1; k >= 0; k--)
                    {
                        ReportPreviewGridView.Columns.RemoveAt(k);
                    }
                }

                for (var i = 0; i < ruleList.Count; i++)
                {
                    ReportPreviewGridView.Columns.Add(new GridViewColumn());
                }
                for (var i = 0; i < ReportPreviewGridView.Columns.Count; i++)
                {
                    var column = ruleList[i];
                    var thisCol = ReportPreviewGridView.Columns[i];
                    var columnBinding = new Binding("CellValues[" + column.IdString + "].StringValue");
                    columnBinding.FallbackValue = "";
                    thisCol.DisplayMemberBinding = columnBinding;
                    thisCol.Width = thisCol.ActualWidth;
                    thisCol.Width = double.NaN;
                    if (column != null && column.Name!= null && column.Name != "")
                    {

                        thisCol.Header = column.Name;
                    }
                    else
                    {
                        thisCol.Header = "[NoName]";
                    }

                }


                var refreshDataPreviewBinding = new Binding("DictList");
                ReportPreview.ItemsSource = null;
                //DataPreview.ItemsSource = null;
                refreshDataPreviewBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                refreshDataPreviewBinding.FallbackValue = "";
                BindingOperations.SetBinding(ReportPreview, ItemsControl.ItemsSourceProperty, refreshDataPreviewBinding);


            }
            else
            {
                for (var k = ReportPreviewGridView.Columns.Count - 1; k > 0; k--)
                {
                    ReportPreviewGridView.Columns.RemoveAt(k);
                }
            }
        }
    }
}



