﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using TrendingToolClient.ViewModels;
using GenericRuleModel.Helper;
using MahApps.Metro.Controls;
using TechneauxWpfControls.DataConverters;

namespace TrendingToolClient.Forms
{
    /// <summary>
    /// Interaction logic for FacilityLinkingAttributeWindow.xaml
    /// </summary>
    public partial class SqlDatabaseTableChooser : MetroWindow
    {
        public static object StaticDataContext;

        public static readonly DependencyProperty SelectedItemProperty
    = DependencyProperty.Register(
          "SelectedItem",
          typeof(string),
          typeof(SqlDatabaseTableChooser),
          new PropertyMetadata("")
          );

        public static readonly DependencyProperty SelectedParentProperty
    = DependencyProperty.Register(
          "SelectedParent",
          typeof(string),
          typeof(SqlDatabaseTableChooser),
          new PropertyMetadata("")
          );


        public SqlDatabaseTableChooser()
        {
            InitializeComponent();
            SelectionBox.DataContext = this;
            DatabaseTreeView.MouseDoubleClick += DatabaseTreeView_MouseDoubleClick;
            //throw new NotImplementedException("Add event listener for bound list changes by reading the context object at runtime");
        }

        private TreeViewItem _selectedTreeItem;

        private void DatabaseTreeView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (DatabaseTreeView.SelectedItem == null)
            {
                return;
            }

            if (DatabaseTreeView.SelectedItem is SqlTableViewModel)
            {
                var selectedTable = DatabaseTreeView.SelectedItem as SqlTableViewModel;
                _thisChooserResult.Value = selectedTable.Name;
                _thisChooserResult.Description = SelectedParent;
                _wasCancelled = false;
                Close();
            }
        }

      
        public string SelectedParent
        {
            get => (string)GetValue(SelectedParentProperty);
            set => SetValue(SelectedParentProperty, value);
        }
        public string SelectedItem
        {
            get => (string)GetValue(SelectedItemProperty);
            set => SetValue(SelectedItemProperty, value);
        }
        private static SqlDatabaseTableChooser _instance;
        private void OnCloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Hide();
        }

        public static SqlDatabaseTableChooser Instance()
        {
            if (StaticDataContext == null)
            {
                throw new InvalidOperationException("Static Data Context must not be null");
            }

            var instance = new SqlDatabaseTableChooser
            {
                DataContext = StaticDataContext
            };

            return instance;
        }

        public bool TryGetKeyDescId(out ValueDescription result)
        {
            Owner = Application.Current.MainWindow;
            ShowDialog();

            result = _thisChooserResult;
            return !_wasCancelled;
        }

        private bool _wasCancelled = false;
        private readonly ValueDescription _thisChooserResult = new ValueDescription();

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {

            //TreeViewItem selectedRowItem = DatabaseTreeView.SelectedItem as TreeViewItem;
            if (DatabaseTreeView.SelectedItem == null)
            {
                return;
            }

            if (DatabaseTreeView.SelectedItem is SqlTableViewModel)
            {
                var selectedTable = DatabaseTreeView.SelectedItem as SqlTableViewModel;
                _thisChooserResult.Value = ((dynamic)selectedTable).Name;
                _thisChooserResult.Description = SelectedParent;
                _wasCancelled = false;
                Close();
            }
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {             
            _wasCancelled = true;
            Close();
        }

        private void TreeViewItemSelected(object sender, RoutedEventArgs e)
        {
            _selectedTreeItem = e.OriginalSource as TreeViewItem;
        }

        private void DatabaseTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (DatabaseTreeView.SelectedItem == null)
            {
                return;
            }
            if (DatabaseTreeView.SelectedItem is SqlTableViewModel)
            {
                //TreeViewItem item = (TreeViewItem)(DatabaseTreeView.ItemContainerGenerator.ContainerFromItem(DatabaseTreeView.Items.CurrentItem));
                
                var selectedTable = DatabaseTreeView.SelectedItem as SqlTableViewModel;
                SelectedItem = selectedTable.Name;
                //ItemsControl parent = ItemsControl.ItemsControlFromItemContainer(selectedTreeItem);
                var parent = GetSelectedTreeViewItemParent(_selectedTreeItem);
                var selectedDatabase = parent.DataContext as SqlDatabaseViewModel;
                selectedDatabase = parent.DataContext as SqlDatabaseViewModel;
                //TreeViewItem treeitem = parent as TreeViewItem;
                //SelectedParent = treeitem.Header.ToString();//Gets you the immediate parent          
                SelectedParent = selectedDatabase.Name;
                SelectButton.IsEnabled = true ;
                SelectionBox.Visibility = Visibility.Collapsed;        
            }
            else
            {
                SelectedItem = "";
                SelectedParent = "";
                SelectButton.IsEnabled = false;
       

                SelectionBox.Visibility = Visibility.Visible;
            }

        }
        public ItemsControl GetSelectedTreeViewItemParent(TreeViewItem item)
        {
            DependencyObject parent = VisualTreeHelper.GetParent(item);
            while (!(parent is TreeViewItem || parent is TreeView))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }

            return parent as ItemsControl;
        }
    }
}
