﻿namespace TrendingToolClient.Forms.Helper
{
    public class KeyDescChooserResult
    {
        public enum ChooserResults
        {
            ItemSelected,
            Cancelled
        }

        public KeyDescChooserResult() { }

        public KeyDescChooserResult(ChooserResults result, string newKey, string newDesc)
        {
            ChooserResult = result;
            ItemKey = newKey;
            ItemDesc = newDesc;
        }

        public ChooserResults ChooserResult { get; set; }
        public string ItemKey { get; set; }
        public string ItemDesc { get; set; }
    }
}
