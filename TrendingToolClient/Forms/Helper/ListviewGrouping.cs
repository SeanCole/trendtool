﻿using ComponentOwl.BetterListView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CygNetRuleModel.Rules
{
    public interface IGroupedListItem
    {
        ListGroup ItemGroup { get; }
    }


    public interface IListGroup
    {
        string GroupKey { get; }
        string GroupDesc { get; }
        int GroupSortIndex { get; }
    }

    public class ListGroup : IListGroup
    {
        public ListGroup(
            string newGroupKey,
            string newGroupDesc, 
            int newGroupSortIndex = 0)
        {
            GroupKey = newGroupKey;
            GroupDesc = newGroupDesc;
            GroupSortIndex = newGroupSortIndex;
        }

        public string GroupKey { get; }
        public string GroupDesc { get; }
        public int GroupSortIndex { get; }
    }

    public static class ListviewGrouping
    {
        public static void AutoGroupItems(this BetterListView srcListView)
        {
            srcListView.Groups.Clear();

            foreach (var item in srcListView.Items)
            {
                IGroupedListItem thisGrpItem = item.Value as IGroupedListItem;

                if (thisGrpItem != null)
                {
                    if (!srcListView.Groups.ContainsKey(thisGrpItem.ItemGroup.GroupKey))
                    {
                        var NewGroup = srcListView.Groups.Add(thisGrpItem.ItemGroup.GroupKey, thisGrpItem.ItemGroup.GroupDesc);
                        NewGroup.Tag = thisGrpItem.ItemGroup;
                    }

                    var ThisGroup = srcListView.Groups[thisGrpItem.ItemGroup.GroupKey];

                    ThisGroup.Items.Add(item);

                    GroupOrderComparer ThisComparer = new GroupOrderComparer();
                    srcListView.Groups.Sort(ThisComparer);
                }
            }
        }
    }

    public class GroupOrderComparer : IComparer<BetterListViewGroup>
    {
        public int Compare(BetterListViewGroup x, BetterListViewGroup y)
        {
            int xIndex = ((IListGroup)x.Tag).GroupSortIndex;
            int yIndex = ((IListGroup)y.Tag).GroupSortIndex;

            return xIndex.CompareTo(yIndex);
        }
    }


}
