﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using GenericRuleModel.Helper;
using MahApps.Metro.Controls;
using Techneaux.CygNetWrapper.Facilities.Attributes;
using TechneauxWpfControls.DataConverters;

namespace TrendingToolClient.Forms
{
    /// <summary>
    /// Interaction logic for FacilityLinkingAttributeWindow.xaml
    /// </summary>
    public partial class FacilityAttributeChooser : MetroWindow
    {
        public static object StaticDataContext;

        public FacilityAttributeChooser()
        {
            //var path = System.IO.Path.GetDirectoryName((new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath).Replace("%20", " ");
            //Assembly.LoadFrom($@"{path}\MahApps.Metro.dll");
            //Assembly.LoadFrom($@"{path}\System.Windows.Interactivity.dll");

            ////MainWindow.EnsureApplicationResources();

            //this.Resources = Application.Current.Resources;
            //this.Resources.BeginInit();


            InitializeComponent();
            
            AttributeListView.MouseDoubleClick += AttributeListView_MouseDoubleClick;

            //throw new NotImplementedException("Add event listener for bound list changes by reading the context object at runtime");
        }

        private void AttributeListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (!(AttributeListView.SelectedItem is FacilityAttribute selectedAttributeItem))
            {
                return;
            }

            _thisChooserResult.Value = selectedAttributeItem.ColumnName;
            _thisChooserResult.Description = selectedAttributeItem.Description;

            _wasCancelled = false;

            Close();
        }
        
        private void OnCloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Hide();
        }

        public static FacilityAttributeChooser Instance()
        {
            if(StaticDataContext == null)
                {
                    throw new InvalidOperationException("Static Data Context must not be null");
                }

            var instance = new FacilityAttributeChooser
            {
                DataContext = StaticDataContext
            };

            return instance;
        }

        public bool TryGetKeyDescId(out ValueDescription result)
        {
            Owner = MainWindow.ClientParentWindow;
            ShowDialog();

            result = _thisChooserResult;
            return !_wasCancelled;
        }

        private bool _wasCancelled = false;
        private readonly ValueDescription _thisChooserResult = new ValueDescription();

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            if (!(AttributeListView.SelectedItem is FacilityAttribute selectedAttributeItem))
            {
                return;
            }

            _thisChooserResult.Value = selectedAttributeItem.ColumnName;
            _thisChooserResult.Description = selectedAttributeItem.Description;

            _wasCancelled = false;

            Close();
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {

            _wasCancelled = true;        
            Close();
        }
    }
}
