﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;

namespace TrendingToolClient.Forms
{
    /// <summary>
    /// Interaction logic for SinglePointSyncPopup.xaml
    /// </summary>
    public partial class SinglePointSyncPopup : MetroWindow
    {
        CygNetSqlDataCompareModel _sqlCompareModel;
        SimpleCombinedTableSchema _currentTableSchema;
        IEnumerable<MappedSqlRowCompare> _sqlCompare;

        public SinglePointSyncPopup(
            CygNetSqlDataCompareModel srcCompModel, 
            SimpleCombinedTableSchema schema,  
            IEnumerable<MappedSqlRowCompare> compRows)
        {
            InitializeComponent();
            _sqlCompareModel = srcCompModel;
            _currentTableSchema = schema;
            _sqlCompare = compRows;
           
            Loaded += SinglePointPopupWindowSync_Loaded;
        }

        private readonly CancellationTokenSource _cs = new CancellationTokenSource();


        private async void SinglePointPopupWindowSync_Loaded(object sender, RoutedEventArgs e)
        {
            await Task.Run(()=> _sqlCompareModel.UpdateSqlTable(_currentTableSchema, _sqlCompare.ToList(), _cs.Token));

            Close();
        }
        
        public static void ShowPopup(
            CygNetSqlDataCompareModel srcCompModel,
            SimpleCombinedTableSchema schema,
            IEnumerable<MappedSqlRowCompare> compRows)
        {
            var newWindowInstance = new SinglePointSyncPopup(srcCompModel, schema, compRows);

            newWindowInstance.Owner = Application.Current.MainWindow;
            newWindowInstance.ShowDialog();
        }


        private async void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            CancelButton.Content = "Cancelling";
            CancelButton.IsEnabled = false;
            _cs.Cancel();
            //await _compareTask;

            Close();
        }
    }
}
