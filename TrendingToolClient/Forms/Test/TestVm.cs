﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Geared;
using LiveCharts.Wpf;
using ReactiveUI;

namespace TrendingToolClient.Forms.Test
{
    public class TestVm : ReactiveObject
    {
        const int NumItems = 100000;

        public class TestNorm
        {
            public TestNorm(DateTime srcTs, double mins)
            {
                Ts = srcTs;
                MinTs = srcTs.AddMinutes(-mins);
                MaxTs = srcTs.AddMinutes(mins);
            }
            
            public DateTime Ts { get; }
            public DateTime MinTs { get; }
            public DateTime MaxTs { get; }
            public List<DateTimePoint> SrcItems { get; } = new List<DateTimePoint>(); 

            public bool TryAdd(DateTimePoint pnt)
            {
                if(pnt.DateTime >= MinTs && pnt.DateTime <= MaxTs)
                {
                    SrcItems.Add(pnt);
                    return true;
                }
                return false;
            }
        }

        public TestVm()
        {
            var ran = new Random();

            var vals = Enumerable.Range(0, NumItems)
                .Select(num => new DateTimePoint(DateTime.Now.Date.AddMinutes(num + ran.Next(-10, 10) / 12), Math.Sin(num/10) + .5*ran.Next(-1, 1))).ToList();

            var normTimestamps2 = Enumerable.Range(0, NumItems / 5).Select(num => new TestNorm(DateTime.Now.Date.AddMinutes(num * 5), 5)).ToList();

            var startIndex = 0;
            foreach (var pnt in vals)
            {
                var foundFirst = false;
                for (var i = startIndex; i < normTimestamps2.Count; i++)
                {
                    var thisNt = normTimestamps2[i];

                    if(thisNt.TryAdd(pnt))
                    {
                        foundFirst = true; 
                    }
                    else
                    {
                        if(foundFirst)
                        {
                            break;
                        }
                        else
                        {
                            startIndex += 1;
                        }
                    }
                }
            }

            var normValuesTakeNearest2 = normTimestamps2
              .Select(normDate => new DateTimePoint(normDate.Ts,
                  normDate.SrcItems
                      .OrderBy(v => Math.Abs((v.DateTime - normDate.Ts).TotalMinutes))
                      .Select(v => v.Value)
                      .FirstOrDefault())).ToList();



            var normTimestamps = Enumerable.Range(0, NumItems / 5).Select(num => DateTime.Now.Date.AddMinutes(num * 5)).ToList();

            //var NormValuesTakeNearest = NormTimestamps
            //    .Select(normDate => new DateTimePoint(normDate,
            //        Vals.SkipWhile(v => (normDate - v.DateTime).TotalMinutes > 5)
            //            .TakeWhile(v => (v.DateTime - normDate).TotalMinutes <= 5)
            //            .OrderBy(v => Math.Abs((v.DateTime - normDate).TotalMinutes))
            //            .Select(v => v.Value)
            //            .FirstOrDefault())).ToList();

            



            Series = new SeriesCollection
            {
                new GLineSeries
                //new LineSeries
                {
                    //Values = new ChartValues<DateTimePoint>(Vals),
                    Values = vals.AsGearedValues().WithQuality(Quality.Low),
                    Fill = Brushes.Transparent,
                    Title = "Raw History",
                    LineSmoothness = .5
                },
                new GLineSeries
                //new LineSeries
                {
                    //Values = new ChartValues<DateTimePoint>(NormValuesTakeNearest),
                    Values = normValuesTakeNearest2.AsGearedValues().WithQuality(Quality.Low),
                    Fill = Brushes.Transparent,
                    PointGeometry = DefaultGeometries.Triangle,
                    LineSmoothness = .5,
                    Title = "Normalized History"
                }
            };



            XFormatter = val => new DateTime((long)val).ToString("M/dd h:mm t");
            YFormatter = val => val.ToString("#.##");
        }

        private SeriesCollection _series;
        public SeriesCollection Series
        {
            get => _series;
            set => this.RaiseAndSetIfChanged(ref _series, value);
        }

        public ZoomingOptions ZoomingMode { get; } = ZoomingOptions.X;

        public Func<double, string> XFormatter { get; set; }
        public Func<double, string> YFormatter { get; set; }

        public double Unit { get; } = TimeSpan.FromMinutes(5).Ticks;
    }

    public class SimpleData
    {

    }
}
