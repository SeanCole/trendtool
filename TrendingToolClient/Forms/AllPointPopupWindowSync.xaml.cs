﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using TrendingToolClient.ViewModels;
using MahApps.Metro.Controls;
using TechneauxReportingDataModel.Helper;
using TechneauxReportingDataModel.SqlHistory;

namespace TrendingToolClient.Forms
{
    /// <summary>
    /// Interaction logic for AllPointPopupWindowSync.xaml
    /// </summary>
    public partial class AllPointPopupWindowSync : MetroWindow
    {
        private AllPointPopupWindowSync(List<CachedCygNetPointWithRule> srcPoints, SqlHistorySyncConfigModel srcConfigModel)
        {
            InitializeComponent();

            _sourcePoints = srcPoints;
            _sourceConfigModel = srcConfigModel;

            Loaded += AllPointPopupWindowSync_Loaded;
        }

        private readonly CancellationTokenSource _cs = new CancellationTokenSource();
        private Task _compareTask;

        private async void AllPointPopupWindowSync_Loaded(object sender, RoutedEventArgs e)
        {
        //    var vm = new SyncAllPointsViewModel(_sourcePoints, _sourceConfigModel);

           // DataContext = vm;
            //if(Cyg)
           // _compareTask = vm.StartCompareOps(_cs.Token);

            await Task.Run(() =>_compareTask);

            CancelButton.Content = "Ok";
        }

        private readonly List<CachedCygNetPointWithRule> _sourcePoints;
        private readonly SqlHistorySyncConfigModel _sourceConfigModel;


        public static void ShowPopup(List<CachedCygNetPointWithRule> srcPoints, SqlHistorySyncConfigModel srcConfigModel)
        {
            var newWindowInstance = new AllPointPopupWindowSync(srcPoints, srcConfigModel);
       

            newWindowInstance.ShowDialog();
        }
        

        private async void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            CancelButton.Content = "Cancelling";
            CancelButton.IsEnabled = false;
            _cs.Cancel();
            await _compareTask;
                       
            Close();
        }
    }
}
