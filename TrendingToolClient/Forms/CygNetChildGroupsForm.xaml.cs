﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using MahApps.Metro.Controls;
using Techneaux.CygNetWrapper.Facilities.Filtering;

namespace TrendingToolClient.Forms
{
    /// <summary>
    /// Interaction logic for FacilityLinkingAttributeWindow.xaml
    /// </summary>
    public partial class CygNetChildGroupsForm : MetroWindow
    {
        public CygNetChildGroupsForm()
        {
            InitializeComponent();         


            //throw new NotImplementedException("Add event listener for bound list changes by reading the context object at runtime");
        }

      
        private void OnCloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Hide();
        }

        public static object StaticDataContext;
        public static CygNetChildGroupsForm Instance()
        {
            if (StaticDataContext == null)
            {
                throw new InvalidOperationException("Static Data Context must not be null");
            }

            var instance = new CygNetChildGroupsForm
            {
                DataContext = StaticDataContext
            };

            return instance;
        }

        public bool TryGetKeyDescId(out BindingList<ChildFacilityGroup> result)
        {
            ShowDialog();

            result = _thisChooserResult;
            return !_wasCancelled;
        }

        private bool _wasCancelled = false;
        private readonly BindingList<ChildFacilityGroup> _thisChooserResult = new BindingList<ChildFacilityGroup>();

        private void DoneButton_Click(object sender, RoutedEventArgs e)
        {
            //BindingList<ChildFacilityGroup> GroupItems = ChildGroupsListView.ItemsSource as BindingList<ChildFacilityGroup>;
            //if (GroupItems == null)
            //{

            //    return;
            //}

            //ThisChooserResult = GroupItems;

            _wasCancelled = true;

            Close();
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {

            _wasCancelled = true;
            Close();
        }
    }
}
