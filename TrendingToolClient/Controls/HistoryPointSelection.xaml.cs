﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using TrendingToolClient.SqlHistorySync.ViewModels;
using MahApps.Metro.Controls;
using System.Reflection;

namespace TrendingToolClient.Controls
{
    /// <summary>
    /// Interaction logic for RuleSelection.xaml
    /// </summary>
    public partial class HistoryPointSelection : UserControl
    {
        private Point _startPoint;
        private readonly bool _isDragging = false;
    
        public delegate Point GetPosition(IInputElement element);

        private int _rowIndex = -1;
        public HistoryPointSelection()
        {
            var path = System.IO.Path.GetDirectoryName((new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath).Replace("%20", " ");
            Assembly.LoadFrom($@"{path}\MahApps.Metro.dll");

            Assembly.LoadFrom($@"{path}\ControlzEx.dll");


            Assembly.LoadFrom($@"{path}\System.Windows.Interactivity.dll");
            InitializeComponent();


        }

        private void RulePicker_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            foreach (var t in HistPointRulePicker.Columns)
            {
                t.Width = 0;
            }

            HistPointRulePicker.UpdateLayout();

            foreach (var t in HistPointRulePicker.Columns)
            {
                t.Width = new DataGridLength(1, DataGridLengthUnitType.Auto);
            }

        }
    }


}
