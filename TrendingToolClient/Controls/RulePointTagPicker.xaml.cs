﻿using System.Reflection;
using System.Windows.Controls;

namespace TrendingToolClient.Controls
{
    /// <summary>
    /// Interaction logic for RulePicker.xaml
    /// </summary>
    public partial class RulePointTagPicker : UserControl
    {
        public RulePointTagPicker()
        {


            var path = System.IO.Path.GetDirectoryName((new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath).Replace("%20", " ");
            Assembly.LoadFrom($@"{path}\MahApps.Metro.dll");
            Assembly.LoadFrom($@"{path}\ControlzEx.dll");
            Assembly.LoadFrom($@"{path}\System.Windows.Interactivity.dll");
            InitializeComponent();
            
        }
    }
}
