﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using TrendingToolClient.SqlHistorySync.ViewModels;
using MahApps.Metro.Controls;
using System.Reflection;

namespace TrendingToolClient.Controls
{
    /// <summary>
    /// Interaction logic for RulePicker.xaml
    /// </summary>
    public partial class RuleHistoryOptionsPicker : UserControl
    {
        public RuleHistoryOptionsPicker()
        {
            var path = System.IO.Path.GetDirectoryName((new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath).Replace("%20", " ");
            Assembly.LoadFrom($@"{path}\MahApps.Metro.dll");
            Assembly.LoadFrom($@"{path}\ControlzEx.dll");
            Assembly.LoadFrom($@"{path}\System.Windows.Interactivity.dll");
            InitializeComponent();

        }
    }
}
