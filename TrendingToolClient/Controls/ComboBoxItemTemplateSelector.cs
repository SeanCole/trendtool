﻿using System.Windows;
using System.Windows.Controls;

namespace TrendingToolClient.Controls
{
    public class ComboBoxItemTemplateSelector : DataTemplateSelector
    {
        // Can set both templates from XAML
        public DataTemplate SelectedItemTemplate { get; set; }
        public DataTemplate ItemTemplate { get; set; }
        
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var selected = false;

            // container is the ContentPresenter
            if (container is FrameworkElement fe)
            {
                DependencyObject parent = fe.TemplatedParent;
                if (parent != null)
                {
                    if (parent is ComboBox cbo)
                        selected = true;
                }
            }

            if (selected)
                return SelectedItemTemplate;
            else
                return ItemTemplate;
        }
    }
}
