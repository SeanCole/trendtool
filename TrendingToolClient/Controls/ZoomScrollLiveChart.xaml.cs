﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TrendingToolClient.ViewModels;
using LiveCharts.Events;
using LiveCharts.Wpf;

namespace TrendingToolClient.Controls
{
    /// <summary>
    /// Interaction logic for ZoomScrollLiveChart.xaml
    /// </summary>
    public partial class ZoomScrollLiveChart : UserControl
    {
        public ZoomScrollLiveChart()
        {
           
            InitializeComponent();

        }

        private bool SkipEvent = false;

        private void Axis_OnRangeChanged(RangeChangedEventArgs eventargs)
        {
            var vm = (HistoryTrendToolViewModel)DataContext;
            
            var currentRange = eventargs.Range;

            //if (currentRange < TimeSpan.FromDays(7).Ticks)
            //{
            //    (eventargs.Axis as Axis).SetRange(TimeSpan.FromHours(1).Ticks, TimeSpan.FromDays(7).Ticks);
            //}              

            if (currentRange < TimeSpan.TicksPerDay * 2)
            {
                vm.XFormatter = x => new DateTime((long)x).ToString("t");
                return;
            }

            if (currentRange < TimeSpan.TicksPerDay * 60)
            {
                vm.XFormatter = x => new DateTime((long)x).ToString("dd MMM yy");
                return;
            }

            if (currentRange < TimeSpan.TicksPerDay * 540)
            {
                vm.XFormatter = x => new DateTime((long)x).ToString("MMM yy");
                return;
            }

            vm.XFormatter = x => new DateTime((long)x).ToString("yyyy");
        }
    }
}
