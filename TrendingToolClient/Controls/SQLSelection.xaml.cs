﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using TrendingToolClient.SqlHistorySync.ViewModels.Sub;
using TrendingToolClient.ViewModels;
using MahApps.Metro.Controls;

namespace TrendingToolClient.Controls
{
    /// <summary>
    /// Interaction logic for SQLSelection.xaml
    /// </summary>
    public partial class SqlSelection : UserControl
    {
        private Point _startPoint;
        private readonly bool _isDragging = false;
        public delegate Point GetPosition(IInputElement element);
        int _rowIndex = -1;
        public SqlSelection()
        {
            
            InitializeComponent();
            
            SqlPicker.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(SqlFieldPicker_PreviewMouseLeftButtonDown);
            SqlPicker.PreviewMouseMove += new MouseEventHandler(SqlFieldPicker_PreviewMouseMove);
            SqlPicker.Drop += new DragEventHandler(SqlFieldDataGrid_Drop);
        }

        private void SqlFieldPicker_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!e.OriginalSource.GetType().Equals(typeof(Rectangle)) && !e.OriginalSource.GetType().Equals(typeof(Path)) && !e.OriginalSource.GetType().Equals(typeof(MetroThumb)) &&
                !e.OriginalSource.GetType().Equals(typeof(TextBlock)) && !e.OriginalSource.GetType().Equals(typeof(TextBox)) && !e.OriginalSource.GetType().Equals(typeof(Grid)) &&
                !e.OriginalSource.GetType().Equals(typeof(Button)))    
            {
                _rowIndex = GetCurrentRowIndex(e.GetPosition);
                if (_rowIndex < 0)
                    return;

                _startPoint = e.GetPosition(null);
            }
        }

        void SqlFieldPicker_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (_rowIndex < 0)
                return;
            if (e.LeftButton == MouseButtonState.Pressed && !_isDragging && !e.OriginalSource.GetType().Equals(typeof(Rectangle)) && !e.OriginalSource.GetType().Equals(typeof(Path)) && !e.OriginalSource.GetType().Equals(typeof(MetroThumb))
                && !e.OriginalSource.GetType().Equals(typeof(TextBlock)) && !e.OriginalSource.GetType().Equals(typeof(TextBox)) && !e.OriginalSource.GetType().Equals(typeof(Grid)) &&
                !e.OriginalSource.GetType().Equals(typeof(Button)))
            {
                
                Point position = e.GetPosition(null);
                if (Math.Abs(position.X - _startPoint.X) > SystemParameters.MinimumHorizontalDragDistance*5 ||
                        Math.Abs(position.Y - _startPoint.Y) > SystemParameters.MinimumVerticalDragDistance*5)
                {
                    SqlPicker.SelectedIndex = _rowIndex;
                    var selected = ((SqlGeneralViewModelReactive)DataContext).SqlFieldList.Rows[_rowIndex];
                    if (selected == null)
                        return;
                    var dragdropeffects = DragDropEffects.Move;
                    if (DragDrop.DoDragDrop(SqlPicker, selected, dragdropeffects)
                                        != DragDropEffects.None)
                    {
                        SqlPicker.SelectedItem = selected;
                    }
                }
            }
        }

        void SqlFieldDataGrid_Drop(object sender, DragEventArgs e)
        {
            if (!e.OriginalSource.GetType().Equals(typeof(Rectangle)) &&
                !e.OriginalSource.GetType().Equals(typeof(Path)) &&
                !e.OriginalSource.GetType().Equals(typeof(MetroThumb)) &&
                !e.OriginalSource.GetType().Equals(typeof(TextBlock)) &&
                !e.OriginalSource.GetType().Equals(typeof(TextBox)) &&
                !e.OriginalSource.GetType().Equals(typeof(Grid)) &&
                !e.OriginalSource.GetType().Equals(typeof(Button)))
            {

                if (_rowIndex < 0)
                    return;
                var index = GetCurrentRowIndex(e.GetPosition);
                if (index < 0)
                    return;
                if (index == _rowIndex)
                    return;
                if (index == SqlPicker.Items.Count)
                {
                    return;
                }
            ((SqlGeneralViewModelReactive)DataContext).SqlFieldList.DragAndDrop(_rowIndex, index);

            }
        }
        private void SqlFieldPicker_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            foreach (var t in SqlPicker.Columns)
            {
                t.Width = 0;
            }

            SqlPicker.UpdateLayout();

            foreach (var t in SqlPicker.Columns)
            {
                t.Width = new DataGridLength(1, DataGridLengthUnitType.Auto);
            }

        }
        private bool GetMouseTargetRow(Visual theTarget, GetPosition position)
        {
            if (theTarget == null)
                return false;
            Rect rect = VisualTreeHelper.GetDescendantBounds(theTarget);
            var point = position((IInputElement)theTarget);
            return rect.Contains(point);
        }
        private DataGridRow GetRowItem(int index)
        {
            if (SqlPicker.ItemContainerGenerator.Status
                    != GeneratorStatus.ContainersGenerated)
                return null;
            return SqlPicker.ItemContainerGenerator.ContainerFromIndex(index)
                                                            as DataGridRow;
        }
        private int GetCurrentRowIndex(GetPosition pos)
        {
            var curIndex = -1;
            for (var i = 0; i < SqlPicker.Items.Count; i++)
            {
                var itm = GetRowItem(i);
                if (GetMouseTargetRow(itm, pos))
                {
                    curIndex = i;
                    break;
                }
            }
            return curIndex;
        }

        //private void CheckBox_Checked(object sender, RoutedEventArgs e)
        //{
        //    var itemsSource = SqlPicker.ItemsSource as IEnumerable;
        //    foreach( var items in itemsSource)
        //    {
        //        var row = SqlPicker.ItemContainerGenerator.ContainerFromItem(items) as DataGridRow;
        //        RuleViewModel elm = (RuleViewModel)row.Item;
        //        foreach (DataGridColumn column in SqlPicker.Columns)
        //        {
        //            if (column.GetCellContent(row) is DataTemplateColumn)
        //            {
        //                CheckBox cellContent = column.GetCellContent(row) as CheckBox;
        //                if((CheckBox)e.Source != cellContent)
        //                {
        //                    cellContent.IsChecked = false; 
        //                }
        //            }
        //        }
        //    }
           
        //}   
    }
}
