﻿using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;
using System.Reflection;

namespace TrendingToolClient.Controls
{

    /// <summary>
    /// Interaction logic for RuleNormalizationPicker.xaml
    /// </summary>
    public partial class TrendOptionsPicker : UserControl
    {
        public TrendOptionsPicker()
        {
            var path = System.IO.Path.GetDirectoryName((new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath).Replace("%20", " ");
            Assembly.LoadFrom($@"{path}\MahApps.Metro.dll");
            Assembly.LoadFrom($@"{path}\ControlzEx.dll");
            Assembly.LoadFrom($@"{path}\System.Windows.Interactivity.dll");
            InitializeComponent();
        }
        private void NumericUpDown_GotFocus(object sender, RoutedEventArgs e)
        {
            var focused = sender as NumericUpDown;
            focused.HideUpDownButtons = false;
        }

        private void NumericUpDown_LostFocus(object sender, RoutedEventArgs e)
        {
            var lostFocused = sender as NumericUpDown;
            lostFocused.HideUpDownButtons = true;
        }


    }
}

