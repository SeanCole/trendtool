﻿using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using TrendingToolClient.SqlHistorySync.ViewModels;
using MahApps.Metro.Controls;
using TechneauxWpfControls.Simple;
using System.Reflection;

namespace TrendingToolClient.Controls
{

    /// <summary>
    /// Interaction logic for RuPicker.xaml
    /// </summary>
    public partial class RuleNormalizationPicker : UserControl
    {
        public RuleNormalizationPicker()
        {
            var path = System.IO.Path.GetDirectoryName((new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath).Replace("%20", " ");
            Assembly.LoadFrom($@"{path}\MahApps.Metro.dll");
            Assembly.LoadFrom($@"{path}\ControlzEx.dll");
            Assembly.LoadFrom($@"{path}\System.Windows.Interactivity.dll");
            InitializeComponent();
        }
        private void NumericUpDown_GotFocus(object sender, RoutedEventArgs e)
        {
            var focused = sender as NumericUpDown;
            focused.HideUpDownButtons = false;
        }

        private void NumericUpDown_LostFocus(object sender, RoutedEventArgs e)
        {
            var lostFocused = sender as NumericUpDown;
            lostFocused.HideUpDownButtons = true;
        }

        private void IntervalControl_ValueIncreased(object sender, RoutedEventArgs e)
        {
           
            var holderControl = e.OriginalSource as LabeledNumericUpDown;       
            if(holderControl.Value < 24)
            {
                for(var nextValue=holderControl.Value + 1;nextValue <= 24;nextValue++)
                {
                    if(24 % nextValue ==0)
                    {
                        //NextInterval = nextValue - holderControl.Value;              
                        //IntervalControl.IntervalNumber = NextInterval;
                        IntervalControl.Value = nextValue -1;
                        return;
                    }
                }
                //IntervalControl.IntervalNumber = 0;
                IntervalControl.Value = 24;
            }
            else
            {
                IntervalControl.Value = 24;
                //IntervalControl.IntervalNumber = 0;
            }

        }

        private void IntervalControl_ValueDecreased(object sender, RoutedEventArgs e)
        {
            
        
            var holderControl = e.OriginalSource as LabeledNumericUpDown;
            if (holderControl.Value > 0 )
            {
                for (var nextValue = holderControl.Value-1; nextValue >= 1; nextValue--)
                {
                    if (24 % nextValue == 0)
                    {
                        //NextInterval = nextValue - holderControl.Value;
                        //IntervalControl.IntervalNumber = NextInterval;
                        IntervalControl.Value = nextValue + 1;
                        return;
                    }
                }
                IntervalControl.Value= 1;

            }
            else
            {
                IntervalControl.Value = 1;
            }

        }

        //private void LabeledNumericUpDown_SourceUpdated(object sender, DataTransferEventArgs e)
        //{  
        //    object source = e.TargetObject;
        //    LabeledNumericUpDown SourceControl = (LabeledNumericUpDown)source;
        //    double value = SourceControl.Value;
        //    if (1440 % value != 0)
        //    {
        //        IntervalControl.Foreground = Brushes.Red;
        //    }
        //    else
        //    {
        //        IntervalControl.Foreground = Brushes.White;
        //    }
        //}
    }
}
