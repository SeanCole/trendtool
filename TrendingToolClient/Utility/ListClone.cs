﻿using System.Collections.Generic;
using System.Linq;

namespace TrendingToolClient.Utility
{
    public static class ListClone
    {
        public static List<string> DeepCopy(this IEnumerable<string> srcList)
        {
            var newList = srcList.Select(item => string.Copy(item));
            return newList.ToList();
        }
    }
}
