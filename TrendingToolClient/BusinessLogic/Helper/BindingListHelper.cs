﻿using System.ComponentModel;
using System.Linq;
using TechneauxReportingDataModel.SqlHistory.Enumerable;

namespace TrendingToolClient.BusinessLogic.Helper
{
    public static class BindingListHelper
    {
        public static SqlTableMapping AddNewRule(this BindingList<SqlTableMapping> srcList, int index)
        {
            var newRule = new SqlTableMapping();
            srcList.Insert(index, newRule);

            return newRule;
        }

        public static bool DeleteRule(this BindingList<SqlTableMapping> srcList, int index)
        {
            if (srcList.ElementAtOrDefault(index) == null)
                return false;

            srcList.RemoveAt(index);
            return true;
        }

        public static SqlPointSelection AddNewRule(this BindingList<SqlPointSelection> srcList, int index)
        {
            var newRule = new SqlPointSelection();
            srcList.Insert(index, newRule);

            return newRule;
        }

        public static bool DeleteRule(this BindingList<SqlPointSelection> srcList, int index)
        {
            if (srcList.ElementAtOrDefault(index) == null)
                return false;

            srcList.RemoveAt(index);
            return true;
        }
    }
}
