﻿using System;
using System.Windows;
using MahApps.Metro;

namespace TrendingToolClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        public App()
        {
            //InitializeComponent();
            //();
            //testModel.ShowEditorForm();

        }
        protected override void OnStartup(StartupEventArgs e)
        {
            // get the current app style (theme and accent) from the application
            // you can then use the current theme and custom accent instead set a new theme
            Tuple<AppTheme, Accent> appStyle = ThemeManager.DetectAppStyle(Current);

            // now set the Green accent and dark theme
            ThemeManager.ChangeAppStyle(Current,
                                        ThemeManager.GetAccent("Orange"),
                                        ThemeManager.GetAppTheme("BaseLight")); // or appStyle.Item1

            base.OnStartup(e);
        }
    }
}
