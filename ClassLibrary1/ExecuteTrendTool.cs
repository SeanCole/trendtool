﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Serilog;
using System.Reflection;

namespace ComInvoker
{
    [Guid("02810C22-3FF2-4fc2-A7FD-5E1034466248"), ComVisible(true)]
    public class ExecuteTrendTool
    {
       
        public ExecuteTrendTool()
        {
            
        }

        [ComVisible(true)]
        public void LoadTrend()
        {           
            //TrendingToolClient.App app = new TrendingToolClient.App();
            try
            {
                AppDomain currentDomain = AppDomain.CurrentDomain;
                currentDomain.UnhandledException += MyHandler;
                // This call will fail to create an instance of MyType since the
                // assembly resolver is not set
                //InstantiateMyTypeFail(currentDomain);

                //currentDomain.AssemblyResolve += new ResolveEventHandler(MyResolveEventHandler);

                // This call will succeed in creating an instance of MyType since the
                // assembly resolver is now set.
                //InstantiateMyTypeFail(currentDomain);

                // This call will succeed in creating an instance of MyType since the
                // assembly name is valid.
                //InstantiateMyTypeSucceed(currentDomain);

                Log.Logger = new LoggerConfiguration()
               .MinimumLevel.Debug()
               .WriteTo
               .RollingFile($@"{AppDomain.CurrentDomain.BaseDirectory}Logs\{"testlog"}", shared: true)
               .CreateLogger();
      
 

                var path = System.IO.Path.GetDirectoryName((new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath).Replace("%20", " ");
                Assembly.LoadFrom($@"{path}\MahApps.Metro.dll");
                Assembly.LoadFrom($@"{path}\System.Windows.Interactivity.dll");
                Assembly.LoadFrom($@"{path}\TechneauxWpfControls.dll");
                Assembly.LoadFrom($@"{path}\CygNetWrapper.dll");
                Assembly.LoadFrom($@"{path}\TechneauxHistorySynchronization.dll");
                Assembly.LoadFrom($@"{path}\TechneauxUtility.dll");
                Assembly.LoadFrom($@"{path}\ControlzEx.dll");
                if(TrendingToolClient.MainWindow.Instance == null)
                {
                    TrendingToolClient.MainWindow mainwindow = new TrendingToolClient.MainWindow();
                }

                
            }
            catch(Exception ex)
            {

                Log.Debug(ex, "Exception: ");
                
                Log.Debug(ex.InnerException, "Inner exceptionL: ");
            }

 

        }
        //private static Assembly MyResolveEventHandler(object sender, ResolveEventArgs args)
        //{
        //    var path = System.IO.Path.GetDirectoryName((new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath).Replace("%20", " ");
        //    Log.Debug(args.Name);

        //    return Assembly.LoadFrom($@"{path}\{args.Name}");
        //    //return typeof(MyType).Assembly;
        //}
        public void LoadFacility(string site, string service, string facility)
        {
            if (TrendingToolClient.MainWindow.Instance == null )
            {
                return;
            }
    
            TrendingToolClient.MainWindow.Instance.LoadFacility(site, service, facility);

        }
    

        static void MyHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            if (e.InnerException != null)
                Log.Fatal("Unhandled exception InnerExceptionMessage: " + e.InnerException.Message);
            Log.Fatal("Unhandled exception StackTrace: " + e.StackTrace);
            Log.Fatal("Unhandled exception Message: " + e.Message);
            Log.Fatal("Unhandled exception Source: " + e.Source);

            Console.WriteLine($@"MyHandler caught : {e.Message}");
            Console.WriteLine(@"Runtime terminating: {0}", args.IsTerminating);
            MessageBox.Show(@"An unhandled exception occurred and the program will not exit. See Log for details.", @"Error", MessageBoxButtons.OK);
        }
        //    public void LoadVariables(string strSiteService, string strBaseFacility, string strFilterOptions = "")
        //{

        //}



    }
}
