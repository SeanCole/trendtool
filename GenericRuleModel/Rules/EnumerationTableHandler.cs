﻿using Serilog;
using System;
using System.ComponentModel;
using System.Reactive.Linq;
using System.Threading;
using static GenericRuleModel.Rules.EnumerationTable;

namespace GenericRuleModel.Rules
{
    public static class EnumerationTableHandler
    {
        static EnumerationTableHandler()
        {
            // Load enums if available
            var loadedDef = EnumerationTableUtils.LoadEnumTables();

            EnumTableDefinition = loadedDef;

            // hook events for saving enums
            EnumTableDefinition.PropertyChanged += EnumTableDefinition_PropertyChanged;

            //event for automagically reloading enumtables
            Observable.Interval(TimeSpan.FromMinutes(.5))
                .Subscribe(evt =>
                    {                        
                        try
                        {
                            EnumTableLock.Wait();
                            EnumTableDefinition = EnumerationTableUtils.LoadEnumTables();
                        }
                        finally
                        {
                            EnumTableLock.Release();
                        }
                    });
        }
    
        private static SemaphoreSlim EnumTableLock { get; } = new SemaphoreSlim(1, 1);

        private static void EnumTableDefinition_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Log.Debug($"Enum Tables Prop Change Event.");
            Log.Debug($"Enum table change : {e.PropertyName}");
            if((sender is EnumerationTables newTables))
            {
                EnumerationTableUtils.SaveEnumTables(newTables);
                return;
            }
            Log.Debug("Enum tables cast failed for save.");
            //EnumerationTableUtils.SaveEnumTables(EnumTableDefinition);
        }

        public static bool TryLookupValue(string resVal, string tableName, out string value)
        {
            value = "";

            try
            {                
                EnumTableLock.Wait();
                
                var table = GetTable(tableName);
                if (table == null)
                    return false;
                foreach (var pair in table.EnumerationPairs)
                {
                    if (pair.Key.Equals(resVal))
                    {
                        value = pair.Value;
                        return true;

                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Exception looking up enum table value");
                return false;
            }
            finally
            {
                EnumTableLock.Release();
            }
        }

        public static EnumerationTables EnumTableDefinition { get; private set; }

        private static EnumerationTable GetTable(string tableName)
        {

            foreach (var table in EnumTableDefinition.EnumTables)
            {
                if (table.Name.Equals(tableName))
                    return table;
            }

            return null;
        }
    }
}
