﻿using System;
using System.Collections.Generic;
using System.Threading;
using CygNet.Data.Core;
using CygNet.Data.Points;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.VHS;
using CygNet.Data.Historian;
using Serilog;
using Techneaux.CygNetWrapper.Services.PNT;

namespace Techneaux.CygNetWrapper.Points
{
    public abstract class BaseCygNetPoint : ICygNetPoint
    {
        public BaseCygNetPoint(
            PointService newPntService,
            CurrentValueService newCvsService,
            ICygNetFacility newFacility,
            PointTag tag)
        {
            PntService = newPntService;
            CvsService = newCvsService;
            Facility = newFacility;
            Tag = tag;
        }

        public BaseCygNetPoint()
        { }

        public ICygNetFacility Facility { get; }

        public PointService PntService { get; }
        public CurrentValueService CvsService { get; }

        public PointTag Tag { get; }

        public string TagFacilityUdc => Tag.GetTagFacilityUDC();
        public string ShortId => Tag.PointId;
        public string LongId => Tag.LongId;

        //public string Description
        //{
        //    get { return GetAttributeValue("description").ValueString; }
        //}

        private PointConfigRecord _pointConfigRecord;

        public void RefreshPointConfigRecord()
        {
            _pointConfigRecord = null;
        }

        public async Task<PointConfigRecord> GetPointConfigRecord(bool refresh = false)
        {
            if (refresh || _pointConfigRecord == null)
            {
                await Task.Run(() => _pointConfigRecord = PntService.GetPointConfigRecord(Tag));
            }

            return _pointConfigRecord;
        }

        //public virtual async Task<PointAttributeValue> GetAttributeValueAsync(string attrId, bool refresh = false)
        //{
        //    throw new NotImplementedException();
        //}

        private PointAttributeValue _GetAttributeValue(string attributeName, bool refresh = false)
        {
            throw new NotImplementedException();
        }

        public bool TryGetAttributeValue(string attrId, out PointAttributeValue attributeValue, bool refresh = false)
        {
            var validPointAttrIds = PointAttribute.AllPointAttributes;

            if (validPointAttrIds.ContainsKey(attrId))
            {
                attributeValue = _GetAttributeValue(attrId, refresh);
                return true;
            }
            else
            {
                attributeValue = null;
                return false;
            }
        }

        public bool HasCurrentValue => CvsService.PointExists(Tag);

        public PointValue GetCurrentValue()
        {
            var pointValRec = CvsService.GetPointValue(Tag);
            return new PointValue(pointValRec);
        }

        public bool TryGetCurrentValue(out PointValue result)
        {
            try
            {
                result = GetCurrentValue();
                return true;
            }
            catch (Exception ex)
            {
                Log.Debug(ex, "Failed to get point current value");
                result = null;
                return false;
            }
        }

        public void WriteValue(
           object newValue,
           DateTime? newTimestamp
           )
        {
            CvsService.WriteValue(Tag, newValue, newTimestamp);
        }

        public void WriteValue(
            object value,
            DateTime? timeStamp,
            BaseStatusFlags baseStatusMask,
            BaseStatusFlags baseStatus,
            UserStatusFlags userStatusMask,
            UserStatusFlags userStatus
            )
        {
            CvsService.WriteValue(Tag, value, timeStamp, baseStatusMask, baseStatus, userStatusMask, userStatus);
        }

        public async Task<bool> HistoryAvailableInInterval(DateTime earliestDate, DateTime latestDate)
        {
            var vhs = await CvsService.GetAssociatedVhs();

            if (vhs == null)
                throw new InvalidOperationException("Associated VHS service was not found");

            return vhs.HistoryAvailableInInterval(Tag, earliestDate, latestDate);
        }

        public async Task<List<CygNetHistoryEntry>> GetHistory(DateTime earliestDate, DateTime latestDate, CancellationToken ct, int numEntries)
        {
            var vhs = await CvsService.GetAssociatedVhs();

            if (vhs == null)
                throw new InvalidOperationException("Associated VHS service was not found");

            return vhs.GetHistoryForward(Tag, earliestDate, latestDate, ct, numEntries);
        }

        NameStatistics _cachedStats;
        public async Task<NameStatistics> GetHistoryStatistics(bool refresh = false)
        {
            if (refresh || _cachedStats == null)
            {
                var vhs = await CvsService.GetAssociatedVhs();

                _cachedStats = vhs.GetHistStats(Tag);
            }

            return _cachedStats;
        }

        public async Task<bool> HasAnyHistory(bool refresh = false)
        {
            var myStats = await GetHistoryStatistics(refresh);

            if (myStats == null || myStats.EntryCount == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public async Task<DateTime?> GetEarliestHistoryEntryDate(bool refresh)
        {
            var myStats = await GetHistoryStatistics(refresh);

            if (myStats == null)
            {
                return null;
            }
            else
            {
                return myStats.EarliestActiveEntry;
            }
        }

        public async Task<DateTime?> GetLatestHistoryEntryDate(bool refresh)
        {
            var myStats = await GetHistoryStatistics(refresh);

            if (myStats == null)
            {
                return null;
            }
            else
            {
                return myStats.LatestActiveEntry;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is BaseCygNetPoint compPoint)
            {
                return Tag.Equals(compPoint.Tag);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Tag.GetHashCode();
        }

        public override string ToString()
        {
            return Tag.ToString();
        }

        public async Task<List<CygNetHistoryEntry>> GetHistory(DateTime earliestDate, DateTime latestDate, IProgress<int> progress, CancellationToken ct)
        {
            var vhs = await CvsService.GetAssociatedVhs();

            if (vhs == null)
                throw new InvalidOperationException("Associated VHS service was not found");

            return vhs.GetHistoryForward(Tag, earliestDate, latestDate, progress, ct);
        }

        public Task<List<CygNetHistoryEntry>> GetHistoryRollup(DateTime earliestDate, DateTime latestDate, CancellationToken ct)
        {
            throw new NotImplementedException();
        }

        public Task<List<CygNetHistoryEntry>> GetHistoryForward(DateTime earliestDate, DateTime latestDate, CancellationToken ct, int numEntries)
        {
            throw new NotImplementedException();
        }

        public Task<List<CygNetHistoryEntry>> GetHistoryReverse(DateTime earliestDate, DateTime latestDate, CancellationToken ct, int numEntries)
        {
            throw new NotImplementedException();
        }
    }
}