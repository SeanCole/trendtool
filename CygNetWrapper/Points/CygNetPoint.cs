﻿using System.Collections.Generic;
using System.Linq;
using CygNet.Data.Core;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.PNT;

namespace Techneaux.CygNetWrapper.Points
{
    public class CygNetPoint : BaseCygNetPoint
    {
        public CygNetPoint(
            PointService newPntService,
            CurrentValueService newCvsService,
            ICygNetFacility newFacility,
            PointTag tag) : base (newPntService, newCvsService, newFacility, tag)
        {
        }

        public static List<string> GetValidUdcsFromList(IEnumerable<string> udcs)
        {
            return (udcs ?? new List<string>())
                .Where(IsUdcValid)
                .Select(FormatUdc)
                .ToList();
        }

        public static bool IsUdcValid(string udc) => !string.IsNullOrWhiteSpace(udc) 
                                                         && udc.Trim().Length <= 10;

        public static string FormatUdc(string udc) => udc.Trim().ToUpper();
    }
}