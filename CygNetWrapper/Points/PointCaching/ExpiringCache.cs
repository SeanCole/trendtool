﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace Techneaux.CygNetWrapper.Points.PointCaching
{
    public class ExpiringCache<TKey, TCachedObject>
    {
        private readonly Func<TKey, CancellationToken, Task<TCachedObject>> _getNewItem;

        public ExpiringCache(Func<TKey, CancellationToken, Task<TCachedObject>> getNewItem, TimeSpan retentionTime)
        {
            _getNewItem = getNewItem;
            _retentionTime = retentionTime;
        }

        private readonly TimeSpan _retentionTime;

        private readonly ConcurrentDictionary<TKey, (DateTime expiresAt, Lazy<Task<TCachedObject>> cachedObj)> _cacheDict =
            new ConcurrentDictionary<TKey, (DateTime expiresAt, Lazy<Task<TCachedObject>> cachedObj)>();

        public async Task<TCachedObject> GetCachedItem(TKey key, CancellationToken ct)
        {
            var res = _cacheDict.AddOrUpdate(key,
                t => (DateTime.Now + _retentionTime, new Lazy<Task<TCachedObject>>(
                        () => _getNewItem(key, CancellationToken.None)))
                ,
                (k, t) =>
                {
                    if (DeleteIfExpired(k, t))
                    {
                        return (DateTime.Now + _retentionTime, new Lazy<Task<TCachedObject>>(
                                () => _getNewItem(key, CancellationToken.None))
                            );
                    }

                    return t;
                }

            );

            return await res.cachedObj.Value;
        }

        private bool DeleteIfExpired(TKey key, (DateTime expiresAt, Lazy<Task<TCachedObject>> cachedObj) cachedItem)
        {
            if (cachedItem.expiresAt >= DateTime.Now) return false;

            _cacheDict.TryRemove(key, out var _);
            return true;
        }
    }
}