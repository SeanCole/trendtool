﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxReportingDataModel.CygNet.Utility;

namespace Techneaux.CygNetWrapper.Points
{
    public enum HistoryTypes
    {
        [Description("[First] After Contract Begins")]
        FirstAfterContractBegins,

        [Description("[First] Before Contract Ends")]
        FirstBeforeContractEnds,

        [Description("[First] After Contract Ends")]
        FirstAfterContractEnds,

        [Description("[Value] at This Contract Hour")]
        ValueAtContractHour,

        [Description("[Value] at Last Contract Hour")]
        ValueAtLastContractHour,

        [Description("[Value] at Next Contract Hour")]
        ValueAtNextContractHour,

        [Description("[Contract Period] Weighted Average")]
        PeriodCalcWeightedAvg,

        [Description("[Contract Period] Mean")]
        PeriodCalcMean,

        [Description("[Contract Period] Min")] PeriodCalcMin,

        [Description("[Contract Period] Max")] PeriodCalcMax,

        [Description("[Contract Period] Delta")]
        PeriodCalcDelta
    }
    

    public static class PointHistory
    {
        public enum HistDirection
        {
            Forward = 0,
            Reverse = 1,
        }

        public static async Task<CygNetHistoryEntry> GetHistoryRollupValueAsync(
            this ICachedCygNetPoint srcPnt,
            bool allowUnreliable,
            HistoryTypes rollupType,
            RollupPeriod rollupPeriod,
            CancellationToken ct)
        {
            if (srcPnt == null)
                throw new ArgumentNullException(nameof(srcPnt));

            CygNetHistoryEntry thisValue;

            RollupPeriod adjustedPeriod;

            switch (rollupType)
            {
                case HistoryTypes.FirstAfterContractBegins:
                    thisValue = await GetHistVal(srcPnt, allowUnreliable, rollupPeriod, HistDirection.Forward);
                    break;

                case HistoryTypes.FirstBeforeContractEnds:
                    thisValue = await GetHistVal(srcPnt, allowUnreliable, rollupPeriod, HistDirection.Reverse);
                    break;

                case HistoryTypes.FirstAfterContractEnds:
                    adjustedPeriod = new RollupPeriod(rollupPeriod.EarliestTime.AddDays(1), rollupPeriod.LatestTime.AddDays(1));
                    thisValue = await GetHistVal(srcPnt, allowUnreliable, adjustedPeriod, HistDirection.Forward);
                    break;

                case HistoryTypes.ValueAtContractHour:
                    adjustedPeriod = new RollupPeriod(rollupPeriod.EarliestTime, rollupPeriod.EarliestTime.AddSeconds(1));
                    thisValue = await GetHistVal(srcPnt, allowUnreliable, adjustedPeriod, HistDirection.Forward);
                    break;

                case HistoryTypes.ValueAtLastContractHour:
                    adjustedPeriod = new RollupPeriod(rollupPeriod.EarliestTime.AddDays(-1), rollupPeriod.EarliestTime.AddDays(-1).AddSeconds(1));
                    thisValue = await GetHistVal(srcPnt, allowUnreliable, adjustedPeriod, HistDirection.Forward);
                    break;

                case HistoryTypes.ValueAtNextContractHour:
                    adjustedPeriod = new RollupPeriod(rollupPeriod.EarliestTime.AddDays(1), rollupPeriod.EarliestTime.AddDays(1).AddSeconds(1));
                    thisValue = await GetHistVal(srcPnt, allowUnreliable, adjustedPeriod, HistDirection.Forward);
                    break;

                case HistoryTypes.PeriodCalcWeightedAvg:
                case HistoryTypes.PeriodCalcMean:
                case HistoryTypes.PeriodCalcMin:
                case HistoryTypes.PeriodCalcMax:
                case HistoryTypes.PeriodCalcDelta:
                    thisValue = await GetHistRollupValue(srcPnt, allowUnreliable, rollupType, rollupPeriod, ct);
                    break;

                default:
                    thisValue = null;
                    break;
            }

            return thisValue;
        }

        public static async Task<CygNetHistoryEntry> GetHistRollupValue(
            ICachedCygNetPoint srcPoint,
            bool allowUnreliable,
            HistoryTypes rollupType,
            RollupPeriod rollupPer,
            CancellationToken ct)
        {
            double hour20PercentOffset = (rollupPer.LatestTime - rollupPer.EarliestTime).TotalHours * .2;

            DateTime adjEarliestDate = rollupPer.EarliestTime.AddHours(-hour20PercentOffset);
            DateTime adjLatestDate = rollupPer.LatestTime.AddHours(hour20PercentOffset);

            var histValues = await srcPoint.GetHistory(adjEarliestDate, adjLatestDate, null, ct);
            histValues = histValues.Where(hist => hist.IsNumeric).ToList();

            var valuesInRange = histValues.Where(item =>
                item.AdjustedTimestamp >= rollupPer.EarliestTime && item.AdjustedTimestamp <= rollupPer.LatestTime)
                .ToList();

            if (!histValues.Any() || !valuesInRange.Any())
            {
                return null;
            }

            switch (rollupType)
            {
                case HistoryTypes.PeriodCalcWeightedAvg:
                case HistoryTypes.PeriodCalcMean:
                    List<CygNetHistoryEntry> valueSubset = new List<CygNetHistoryEntry>();

                    if (valuesInRange.First().AdjustedTimestamp > rollupPer.EarliestTime &&
                        histValues.Any(item => item.AdjustedTimestamp < rollupPer.EarliestTime))
                    {
                        var preceedingValue = histValues.Last(item => item.AdjustedTimestamp < rollupPer.EarliestTime);

                        if ((valuesInRange.First().AdjustedTimestamp - rollupPer.EarliestTime).TotalMinutes >
                            .5 * (rollupPer.EarliestTime - preceedingValue.AdjustedTimestamp).TotalMinutes)
                        {
                            valueSubset.Add(new 
                                CygNetHistoryEntry
                                {
                                    RawValue = preceedingValue.NumericValue,
                                    RawTimestamp = rollupPer.EarliestTime,
                                    BaseStatus = preceedingValue.BaseStatus,
                                    UserStatus = preceedingValue.UserStatus
                                } );
                        }
                    }

                    valueSubset.AddRange(valuesInRange);

                    var newBaseStatusAvg = valueSubset.Select(hist => hist.BaseStatus).Aggregate((x, y) => x | y);
                    var newUserStatusAvg = valueSubset.Select(hist => hist.UserStatus).Aggregate((x, y) => x | y);

                    if (rollupType == HistoryTypes.PeriodCalcMean)
                    {
                        double avg = valueSubset.Average(item => item.NumericValue);

                        var newAvgEntry = new CygNetHistoryEntry
                        {
                            RawValue = avg,
                            RawTimestamp = rollupPer.LatestTime,
                            BaseStatus = newBaseStatusAvg,
                            UserStatus = newUserStatusAvg
                        };

                        return newAvgEntry;
                    }
                    else
                    {
                        double runningSum = 0;

                        for (int i = 0; i < valueSubset.Count - 1; i++)
                        {
                            runningSum += (valueSubset[i].NumericValue) *
                                          (valueSubset[i + 1].AdjustedTimestamp - valueSubset[i].AdjustedTimestamp).TotalMinutes;
                        }

                        runningSum += (valueSubset.Last().NumericValue) *
                                      (rollupPer.LatestTime - valueSubset.Last().AdjustedTimestamp).TotalMinutes;

                        double avg = runningSum / (rollupPer.LatestTime - valueSubset.First().AdjustedTimestamp).TotalMinutes;

                        var newWavgEntry = new CygNetHistoryEntry
                        {
                            RawValue = avg,
                            RawTimestamp = rollupPer.LatestTime,
                            BaseStatus = newBaseStatusAvg,
                            UserStatus = newUserStatusAvg
                        };

                        return newWavgEntry;
                    }

                case HistoryTypes.PeriodCalcMin:
                    double minValue = valuesInRange.Min(item => item.NumericValue);
                    return histValues.First(item => item.NumericValue == minValue);

                case HistoryTypes.PeriodCalcMax:
                    double maxValue = valuesInRange.Max(item => item.NumericValue);
                    return histValues.First(item => item.NumericValue == maxValue);

                case HistoryTypes.PeriodCalcDelta:
                    maxValue = valuesInRange.Max(item => item.NumericValue);
                    minValue = valuesInRange.Min(item => item.NumericValue);

                    var minPoint = histValues.First(item => item.NumericValue == minValue);
                    var maxPoint = histValues.First(item => item.NumericValue == maxValue);

                    var minMaxList = new List<CygNetHistoryEntry> {maxPoint, minPoint};

                    var newBaseStatusDelta = minMaxList.Select(hist => hist.BaseStatus).Aggregate((x, y) => x | y);
                    var newUserStatusDelta = minMaxList.Select(hist => hist.UserStatus).Aggregate((x, y) => x | y);

                    var newDeltaEntry = new CygNetHistoryEntry()
                    {
                        RawValue = (maxValue-minValue),
                        BaseStatus = newBaseStatusDelta,
                        UserStatus = newUserStatusDelta,
                        RawTimestamp = rollupPer.LatestTime
                    };

                    return newDeltaEntry;

                default:
                    // Invalid rollup type, throw exception
                    throw new Exception("Can't get a rollup value from a non-rollup history type");
            }
        }

        public async static Task<CygNetHistoryEntry> GetHistVal(
            ICachedCygNetPoint srcPoint,
            bool allowUnreliable,
            RollupPeriod rollupPer,
            HistDirection iterDirection)
        {
            // Get value after the latest date (in order to pull in a YDY point and have it aligned with the day it actually occurred)

            switch (iterDirection)
            {
                case HistDirection.Forward:
                    var histf = await srcPoint.GetHistoryForward(rollupPer.EarliestTime, rollupPer.LatestTime, CancellationToken.None, 1);

                    return histf.FirstOrDefault();

                case HistDirection.Reverse:
                    var histr = await srcPoint.GetHistoryForward(rollupPer.EarliestTime, rollupPer.LatestTime, CancellationToken.None, 1);

                    return histr.FirstOrDefault();

                default:
                    throw new ArgumentOutOfRangeException(nameof(iterDirection), iterDirection, null);
            }
        }
    }
}