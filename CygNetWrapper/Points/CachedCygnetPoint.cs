﻿using CygNet.Data.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.PNT;

namespace Techneaux.CygNetWrapper.Points
{
    public class CachedCygNetPoint : BaseCygNetPoint, ICachedCygNetPoint
    {
        public CachedCygNetPoint(
           PointService newPntService,
           CurrentValueService newCvsService,
           ICachedCygNetFacility thisFac,
           PointTag newTag) 
            : base(newPntService, newCvsService, thisFac, newTag)
        {
            Facility = thisFac;
        }
        
        public new ICachedCygNetFacility Facility { get; }

        public async Task<PointAttributeValue> GetAttributeValueAsync(string attrId, CancellationToken ct, bool refresh)
        {
            if(!PointAttribute.AllPointAttributes.TryGetValue(attrId, out var attrObj))
                return null;

            var attrVal = await CvsService.PointAttributeValueCache.GetAttributeValueForPoint(Tag, attrObj, ct);

            return attrVal;
        }

        public async Task<List<PointAttributeValue>> GetAttributeValuesAsync(IEnumerable<string> attrIds, CancellationToken ct, bool refresh)
        {
            var attrVals = await CvsService.PointAttributeValueCache.GetAllAttributeValuesForPoint(Tag,ct);

            var AttrSet = new HashSet<string>(attrIds);

            return attrVals.Where(val => AttrSet.Contains(val.AttributeId)).ToList();
        }

        //public async Task<List<PointAttributeValue>> CacheAllAttributeValues(CancellationToken ct, bool refresh)
        //{
        //    var attrIds = PointAttribute.AllPointAttributes.Keys;
        //    return await MyCache.GetPointAttributeValues(Facility, Tag.UDC, attrIds, ct);
        //}

        public async Task<List<PointAttributeValue>> GetAllAttributeValuesAsync(CancellationToken ct)
        {
            var attrVals = await CvsService.PointAttributeValueCache.GetAllAttributeValuesForPoint(Tag, ct);

            return attrVals;
        }
    }
}
