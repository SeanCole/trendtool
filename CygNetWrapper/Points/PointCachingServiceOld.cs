﻿using CygNet.Data.Core;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper;

namespace Techneaux.CygNetWrapper.Points
{
    public class PointAttributeCachingServiceOld
    {
        HashSet<PointTag> RegisteredPoints = new HashSet<PointTag>();
        HashSet<string> RegisteredUdcs = new HashSet<string>();
        HashSet<SiteService> AllowedCvsServices = new HashSet<SiteService>();
        HashSet<string> CachedAttributeIds = new HashSet<string>();

        private Dictionary<PointTag, Dictionary<string, PointAttributeValue>> InternalCache =
            new Dictionary<PointTag, Dictionary<string, PointAttributeValue>>();

        public PointService MyPointService { get; }
        public PointAttributeCachingServiceOld(PointService thisPntServ, List<CygNetPoint> srcPoints)
        {
            MyPointService = thisPntServ;

            // Get unique UDC list
            var Udcs = srcPoints.Select(pnt => pnt.Tag.UDC);
            RegisteredUdcs.UnionWith(Udcs);

            // Get allowed Cvs Service list
            var CvsSiteServs = srcPoints.Select(pnt => pnt.Tag.SiteService);
            AllowedCvsServices.UnionWith(CvsSiteServs);

            // -- Next check if the allowed services are also associated with the PNT
            var PntAssociatedCvsServs = MyPointService.GetAssociatedCvsServices().Keys;
            AllowedCvsServices.IntersectWith(PntAssociatedCvsServs);

            // Add point tags to cache
            foreach (var pnt in srcPoints)
            {
                RegisteredPoints.Add(pnt.Tag);
                pnt.LinkPointAttributeCache(this);
            }
        }

        public static async Task PopulateCachedPointsForFacilitiesAsync(List<ICygNetFacility> srcFacs, CancellationToken ct, bool forceRefresh = false)
        {
            try
            {
                ct.ThrowIfCancellationRequested();
                // CODE HERE => Need a semaphore or some other async lock so that only one refresh operation can happen at one time
                var FacsByCvsWithInvalid = GetFacsGroupedByCvs(srcFacs);
                var FacsByCvs = FacsByCvsWithInvalid.Item2;
                var CvsServices = FacsByCvs.Keys;

                var FacGroups = GetFacsGroupedByPointService(FacsByCvs);

                // Need exception handling in case of connection issues or no associated PNT service
                foreach (var grp in FacGroups)
                {
                    ct.ThrowIfCancellationRequested();

                    PointService ThisPntServ = grp.PointServ;
                    List<CygNetFacility> Facs = grp.Facs;
                    var AllowedCvsServices = grp.CvsServs.ToDictionary(cvs => cvs.SiteService, cvs => cvs);

                    List<PointTag> AllPointTags = await ThisPntServ.GetAllPointTagsAsync(ct, AllowedCvsServices.Keys);

                    // Eliminate points with duplicate UDCs
                    AllPointTags.GroupBy(item => item.UDC).Select(item => item.First());
                    
                    var NewPoints = AllPointTags.Select(tag => new CygNetPoint(ThisPntServ, AllowedCvsServices[tag.SiteService], tag));

                    // Build a new point cache for all point tags
                    ct.ThrowIfCancellationRequested();
                    PointAttributeCachingServiceOld NewCachingService = new PointAttributeCachingServiceOld(ThisPntServ, NewPoints.ToList());

                    // Link points to facs
                    foreach (var fac in Facs)
                    {
                        var PointsForThisFac = NewPoints.Where(pnt => pnt.Tag.GetFacilityTag() == fac.Tag);
                        fac.PopulatePointCache(PointsForThisFac);
                        ct.ThrowIfCancellationRequested();
                    }
                }
            }
            catch (OperationCanceledException) { throw; }
        }

        private static Tuple<List<CygNetFacility>, Dictionary<CurrentValueService, List<CygNetFacility>>> GetFacsGroupedByCvs(List<CygNetFacility> srcFacs)
        {
            List<CygNetFacility> FacsWithoutValidCvsServ = new List<CygNetFacility>();
            Dictionary<CurrentValueService, List<CygNetFacility>> FacCvsServsGroups = new Dictionary<CurrentValueService, List<CygNetFacility>>();

            foreach (var fac in srcFacs)
            {
                CurrentValueService thisServ;
                if (fac.TryGetValidCvsService(out thisServ))
                {
                    List<CygNetFacility> ThisGroup;
                    if (!FacCvsServsGroups.ContainsKey(thisServ))
                    {
                        ThisGroup = new List<CygNetFacility>();
                        FacCvsServsGroups.Add(thisServ, ThisGroup);
                    }

                    ThisGroup = FacCvsServsGroups[thisServ];
                    ThisGroup.Add(fac);
                }
                else
                {
                    // Couldn't get service object, add facility to list of invalid facs
                    FacsWithoutValidCvsServ.Add(fac);
                }
            }

            return new Tuple<List<CygNetFacility>, Dictionary<CurrentValueService, List<CygNetFacility>>>(FacsWithoutValidCvsServ, FacCvsServsGroups);
        }

        private static List<FacilityGroupByPnt> GetFacsGroupedByPointService(Dictionary<CurrentValueService, List<CygNetFacility>> srcFacs)
        {
            var CvsServices = srcFacs.Keys;
            var PointServices = CvsServices.Select(cvs => cvs.GetAssociatedPnt()).Distinct();

            List<FacilityGroupByPnt> FacGroups = new List<FacilityGroupByPnt>();

            foreach (var PntServ in PointServices)
            {
                FacilityGroupByPnt NewGroup = new FacilityGroupByPnt();

                var CvsServsWithThisPntServ = CvsServices.Where(cvs => cvs.GetAssociatedPnt() == PntServ);
                NewGroup.PointServ = PntServ;

                foreach (var cvsServ in CvsServsWithThisPntServ)
                {
                    NewGroup.CvsServs.Add(cvsServ);
                    NewGroup.Facs.AddRange(srcFacs[cvsServ]);
                }

                FacGroups.Add(NewGroup);
            }

            return FacGroups;
        }

        private class FacilityGroupByPnt
        {
            public PointService PointServ { get; set; } = null;
            public List<CurrentValueService> CvsServs { get; set; } = new List<CurrentValueService>();
            public List<CygNetFacility> Facs { get; set; } = new List<CygNetFacility>();
        }

        SemaphoreSlim PrecacheLock = new SemaphoreSlim(1, 1);
        public async Task PrecachePointAttributes(
            string udc,
            IEnumerable<string> attrIds,
            CancellationToken ct,
            bool forceRefresh = false)
        {
            try
            {
                await PrecacheLock.WaitAsync();
                AttributeUpdateLock.Reset();

                // Check if the UDC is in the registered list, don't allow null UDC
                if (!string.IsNullOrWhiteSpace(udc) && !RegisteredUdcs.Contains(udc))
                    return;

                // Check if attrId is valid
                var AllValidAttributes = PointAttribute.ReadAllPointAttributesAsync();
                HashSet<string> ValidAttributes = new HashSet<string>();
                ValidAttributes.UnionWith(attrIds.Distinct().Intersect(AllValidAttributes.Keys));

                if (!ValidAttributes.Any())
                    return;

                // Check if refresh is forced, otherwise check if attrId is already cached
                if (forceRefresh)
                {
                    CachedAttributeIds.ExceptWith(ValidAttributes);
                }

                // Find attributes not in cache
                ValidAttributes.ExceptWith(CachedAttributeIds);

                if (!ValidAttributes.Any())
                    return;

                // Get full attribute definitions (excluding tags)
                List<PointAttribute> FullValidAttrs = ValidAttributes
                    .Select(attrId => AllValidAttributes[attrId])
                    .Where(fullattr => fullattr.Category != PointAttribute.Categories.Tag)
                    .ToList();

                // Get attributes here
                await CachePointAttributes(udc, FullValidAttrs, ct);

                CachedAttributeIds.UnionWith(ValidAttributes);
            }
            finally
            {
                AttributeUpdateLock.Set();
                PrecacheLock.Release();
            }
            // if udc is null, get for all udcs
        }

        private async Task CachePointAttributes(string udcToCache, List<PointAttribute> attrsToCache, CancellationToken ct)
        {
            try
            {
                string DSN = "DSN=CygNet;ServiceFilter=" + MyPointService.SiteService.ToString();
                using (OdbcConnection DbConnection = new OdbcConnection(DSN))
                {
                    await DbConnection.OpenAsync(ct);

                    using (OdbcCommand DbCommand = DbConnection.CreateCommand())
                    {
                        DbCommand.CommandTimeout = 300;

                        DbCommand.CommandText = GetAttrCachingSqlCmd(udcToCache, attrsToCache);
                        DbDataReader DbReader = await DbCommand.ExecuteReaderAsync(ct);

                        while (DbReader.Read())
                        {
                            ct.ThrowIfCancellationRequested();

                            string site = Convert.IsDBNull(DbReader["site"]) ? string.Empty : DbReader["site"].ToString();
                            string service = Convert.IsDBNull(DbReader["service"]) ? string.Empty : DbReader["service"].ToString();
                            string facId = Convert.IsDBNull(DbReader["facilityid"]) ? string.Empty : DbReader["facilityid"].ToString();
                            string udc = Convert.IsDBNull(DbReader["uniformdatacode"]) ? string.Empty : DbReader["uniformdatacode"].ToString();
                            string pointId = Convert.IsDBNull(DbReader["pointid"]) ? string.Empty : DbReader["pointid"].ToString();
                            string pointIdLong = Convert.IsDBNull(DbReader["pointidlong"]) ? string.Empty : DbReader["pointidlong"].ToString();

                            if (string.IsNullOrEmpty(site) ||
                                string.IsNullOrEmpty(service) ||
                                string.IsNullOrEmpty(facId) ||
                                string.IsNullOrEmpty(udc))
                            {
                                continue;
                            }

                            SiteService ThisSiteServ = new SiteService(site, service);
                            PointTag ThisTag = new PointTag(ThisSiteServ, pointId, pointIdLong, facId, udc);

                            Dictionary<string, PointAttributeValue> ThisAttrCache;
                            if (InternalCache.TryGetValue(ThisTag, out ThisAttrCache))
                            {
                                if (ThisAttrCache == null)
                                {
                                    ThisAttrCache = new Dictionary<string, PointAttributeValue>();
                                }

                                foreach (var attr in attrsToCache)
                                {
                                    object sqlVal = DbReader[attr.SqlColumnName];
                                    PointAttributeValue ThisAttrVal;

                                    if (Convert.IsDBNull(sqlVal))
                                    {
                                        ThisAttrVal = new PointAttributeValue(attr, null);
                                    }
                                    else
                                    {
                                        ThisAttrVal = PointAttribute.ConvertSqlToStandardValue(attr.SqlColumnName, sqlVal);
                                    }

                                    ThisAttrCache[attr.Id] = ThisAttrVal;
                                }
                            }
                            else
                            {
                                // Point tag not in cache, can't do anything
                            }
                        }
                    }
                }
            }
            catch (TaskCanceledException) { }
            catch (OperationCanceledException) { throw; }
            catch (OdbcException ex) when (ex.HResult == -2146232009) { } // Odbc cancellation exception
            catch (Exception ex)
            {
                Log.Error(ex, "General exception caching point attrs");
                // General exception
            }
        }

        private string GetAttrCachingSqlCmd(string udc, List<PointAttribute> attrsToCache)
        {
            var sqlAttrIds = attrsToCache.Select(attr => attr.SqlColumnName);
            var PntService = MyPointService.SiteService.ToString().Replace('.', '_');

            string sql = $@"SELECT site, service, facilityid, uniformdatacode, pointid, pointidlong, {string.Join(",", sqlAttrIds)} 
                            FROM {PntService}.pnt_header_record 
                            WHERE ";

            string UdcConditionAnd = "";
            if (!string.IsNullOrWhiteSpace(udc))
            {
                UdcConditionAnd = " AND ";
                sql += " udc = '{udc}' ";
            }

            if (AllowedCvsServices.Any())
            {
                var Sites = AllowedCvsServices.Select(siteserv => $"'{siteserv.Site}'");
                var Services = AllowedCvsServices.Select(siteserv => $"'{siteserv.Service}'");

                sql += $@" {UdcConditionAnd}' 
                           site IN ({string.Join(",", Sites)}) AND
                           service IN ({string.Join(",", Sites)})";
            }

            sql += ";";

            return sql;
        }


        //public async Task<object> GetAttributeValueForPointAsync(PointTag thisTag, string attrId, bool forceRefresh = false)
        //{
        //    // PreCache attributes for all points with this UDC or just for the facility (think more on this)
        //    throw new NotImplementedException();
        //}

        //private async Task GetAttributesForUdcAsync(bool forceRefresh = false)
        //{
        //}

        ManualResetEventSlim AttributeUpdateLock = new ManualResetEventSlim(true);

        public PointAttributeValue GetCachedAttributeValue(PointTag pointTag, string attrId)
        {
            AttributeUpdateLock?.Wait();

            // Check if attr is valid (must be recognized id, non-tag attribute)
            var AllValidAttrs = PointAttribute.ReadAllPointAttributesAsync();
            PointAttribute ThisAttr;
            if (!AllValidAttrs.TryGetValue(attrId, out ThisAttr))
            {
                throw new ArgumentException("Unrecognized attribute Id");
            }

            Dictionary<string, PointAttributeValue> ThisAttrCache;
            if (InternalCache.TryGetValue(pointTag, out ThisAttrCache))
            {
                PointAttributeValue ThisVal;
                if (!ThisAttrCache.TryGetValue(attrId, out ThisVal))
                {
                    ThisVal = new PointAttributeValue(ThisAttr, AttributeValueResults.AttributeNotCached);
                }

                return ThisVal;
            }
            else
            {
                // Facility is not in cache, need to handle this appropriately here
                throw new NotImplementedException();
            }
        }

        public List<string> GetCachedAttributeIds()
        {
            AttributeUpdateLock?.Wait();

            return CachedAttributeIds.ToList();
        }

    }
}
