﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CygNet.Data.Core;
using CygNet.Data.Points;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Services;
using System;
using CygNet.Data.Historian;
using Techneaux.CygNetWrapper.Services.PNT;
using Techneaux.CygNetWrapper.Services.VHS;

namespace Techneaux.CygNetWrapper.Points
{
    public interface ICygNetPoint
    {
        CurrentValueService CvsService { get; }
        ICygNetFacility Facility { get; }
        PointService PntService { get; }
        string ShortId { get; }
        string LongId { get; }
        PointTag Tag { get; }
        string TagFacilityUdc { get; }
        Task<PointConfigRecord> GetPointConfigRecord(bool refresh = false);

        bool TryGetAttributeValue(string attrId, out PointAttributeValue attributeValue, bool refresh = false);

        Task<bool> HistoryAvailableInInterval(DateTime earliestDate, DateTime latestDate);

        Task<List<CygNetHistoryEntry>> GetHistory(
            DateTime earliestDate, 
            DateTime latestDate,
            IProgress<int> progress,
            CancellationToken ct);

        Task<List<CygNetHistoryEntry>> GetHistoryRollup(
            DateTime earliestDate, 
            DateTime latestDate,
            CancellationToken ct);

        Task<List<CygNetHistoryEntry>> GetHistoryForward(
            DateTime earliestDate, 
            DateTime latestDate,
            CancellationToken ct, 
            int numEntries);

        Task<List<CygNetHistoryEntry>> GetHistoryReverse(
            DateTime earliestDate, 
            DateTime latestDate,
            CancellationToken ct, 
            int numEntries);

        Task<NameStatistics> GetHistoryStatistics(bool refresh = false);

        Task<bool> HasAnyHistory(bool refresh = false);

        Task<DateTime?> GetEarliestHistoryEntryDate(bool refresh);

        Task<DateTime?> GetLatestHistoryEntryDate(bool refresh);
    }

    public interface ICachedCygNetPoint : ICygNetPoint
    {

        Task<List<PointAttributeValue>> GetAllAttributeValuesAsync(CancellationToken ct);

        Task<PointAttributeValue> GetAttributeValueAsync(string attrId, CancellationToken ct, bool refresh);

        Task<List<PointAttributeValue>> GetAttributeValuesAsync(IEnumerable<string> attrIds, CancellationToken ct,
            bool refresh);

        new ICachedCygNetFacility Facility { get; }
    }
}