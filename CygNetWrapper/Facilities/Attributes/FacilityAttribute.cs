using CygNet.Data.Facilities;

namespace Techneaux.CygNetWrapper.Facilities.Attributes
{
    public class FacilityAttribute : AttributeDefinition
    {
        public FacilityAttribute()
        {

        }

        public FacilityAttribute(AttributeDefinition newAttribute)
        {
            ColumnId = newAttribute.ColumnId;
            ColumnName = newAttribute.ColumnName;
            Description = newAttribute.Description;
            Category = newAttribute.Category;
            InUse = newAttribute.InUse;
            IsBoolean = newAttribute.IsBoolean;

            AttributeType = newAttribute.AttributeType();
        }

        public FacilityAttributeTypes AttributeType { get; set; }
        
    }
}
