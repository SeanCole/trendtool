﻿using CygNet.Data.Core;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper.Facilities.Attributes;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.FAC;
using Techneaux.CygNetWrapper.Services.FMS;
using TechneauxUtility;

namespace Techneaux.CygNetWrapper.Facilities
{
    public class CachedCygNetFacility : BaseCygNetFacility, ICachedCygNetFacility
    {
        public CachedCygNetFacility(
            FacilityService newFacService,
            CurrentValueService newCvsService,
            FacilityTag newTag)
            : base(newFacService, newCvsService, newTag)
        {
            CvsService = newCvsService;
        }

        public CurrentValueService CvsService { get; }
        
        public override bool TryGetAttributeValue(string attributeName, out FacilityAttributeValue attributeValue,
            bool refresh = false)
        {
            if (HasAttribute(attributeName, refresh))
            {
                attributeValue = FacService.GetCachedAttributeValue(Tag, attributeName);
                return true;
            }
            else
            {
                attributeValue = null;
                return false;
            }
        }

        public async Task<FmsMeter> TryGetLinkedFmsMeter() => await TryGetLinkedFmsMeter(false);

        public async Task<FmsMeter> TryGetLinkedFmsMeter(bool forceRefresh)
        {
            var myFmsServs = (await Task.Run(() => CvsService.ParentDomain.FindServicesByType(ServiceType.FMS)
                .Select(serv => serv as FmsService))).ToList();

            if (!myFmsServs.Any()) return null;

            foreach (var fms in myFmsServs)
            {
                var myFmsMeter = await Task.Run(() => fms.TryGetFmsMeterForFacility(Tag, forceRefresh));

                if (myFmsMeter != null)
                    return myFmsMeter;
            }

            return null;
        }

        public async Task<bool> IsLinkedToFmsMeter()
        {
            var myMeter = await TryGetLinkedFmsMeter();

            return myMeter != null;
        }

        public async Task<bool> IsMeterInGroup(string groupId)
        {
            var myMtr = await TryGetLinkedFmsMeter();

            return myMtr != null && myMtr.AssociatedGroupIds.Contains(groupId);
        }

        public async Task<bool> IsMatchingFmsFilters(IEnumerable<IFacilityFilterCondition> filters)
        {
            var validFilters = filters.Where(filter =>
                    FacilityFmsFilterCondition.FmsFilteringAttrs.Any(attr => attr.ColumnName == filter.AttributeId))
                .ToList();

            if (!validFilters.Any()) return true;


            // get meter
            var meter = await TryGetLinkedFmsMeter();

            var hasMeterValue = meter != null ? "Y" : "N";
            var GroupNames = meter?.AssociatedGroupIds.ToList() ?? new List<string>();

            foreach (var filter in validFilters)
            {
                if (filter.AttributeId !=
                    FacilityFmsFilterCondition.FmsFilterTypes.facility_has_fms_meter_YN.ToString()) continue;

                switch (filter.ConditionalOperator)
                {
                    case SqlConditionalOperators.Like:
                        if (!hasMeterValue.IsLikePattern(filter.ConditionalValue))
                            return false;
                        break;

                    case SqlConditionalOperators.NotLike:
                        if (hasMeterValue.IsLikePattern(filter.ConditionalValue))
                            return false;
                        break;

                    case SqlConditionalOperators.In:
                        if (!hasMeterValue.IsLikePatterns(filter.InItemList))
                            return false;
                        break;

                    case SqlConditionalOperators.NotIn:
                        if (hasMeterValue.IsLikePatterns(filter.InItemList))
                            return false;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(filter.ConditionalOperator), "Operator not supported");
                }
            }
            
            return true;
        }

        public override FacilityAttributeValue GetAttributeValue(string attributeId, bool refresh = false)
        {
            return FacService.GetCachedAttributeValue(Tag, attributeId);
        }

        public List<string> GetPreCachedAttrIds()
        {
            return FacService.GetCachedAttributeIds();
        }

        public List<FacilityAttributeValue> GetAllCachedAttributeValues()
        {
            var allAttrVals = new List<FacilityAttributeValue>();
            foreach (var attrId in GetPreCachedAttrIds())
            {
                if (TryGetAttributeValue(attrId, out var thisVal))
                {
                    allAttrVals.Add(thisVal);
                }
            }

            return allAttrVals;
        }

        public async Task<FacilityAttributeValue> TryGetAttributeValueAsync(string attributeName, bool refresh = false)
        {
            FacilityAttributeValue attrValue = null;

            if (HasAttribute(attributeName, refresh))
            {
                attrValue = await Task.Run(() => FacService.GetCachedAttributeValue(Tag, attributeName));
            }

            return attrValue;
        }

        public string LinkedFacilityAttributeId { get; private set; }
        public ICachedCygNetFacility ParentFacility { get; private set; }
        public List<ICachedCygNetFacility> ChildFacilities { get; private set; } = new List<ICachedCygNetFacility>();

        public void SetLinkedFacilities(string facLinkingAttr, ICachedCygNetFacility newParentFac,
            List<ICachedCygNetFacility> newChildFacs)
        {
            LinkedFacilityAttributeId = facLinkingAttr;
            ParentFacility = newParentFac;

            if (newChildFacs.IsAny())
            {
                ChildFacilities = newChildFacs;
            }
            else
            {
                ChildFacilities = new List<ICachedCygNetFacility>();
            }
        }


        public override Task<FacilityAttributeValue> GetAttributeValueAsync(string attributeName, bool refresh = false)
        {
            throw new NotImplementedException();
        }

        public override string Description => GetAttributeValue("facility_desc").ToString(CultureInfo.InvariantCulture);

        public override string Type => GetAttributeValue("facility_type").ToString();

        public override string Category => GetAttributeValue("facility_category").ToString();

        public override bool IsActive => GetAttributeValue("facility_is_active").StringValue == "Y";


        #region Points

        private readonly ConcurrentDictionary<string, ICachedCygNetPoint> _localPointCache = new ConcurrentDictionary<string, ICachedCygNetPoint>();

        //public async Task<ICachedCygNetPoint> GetPoint(string udc)
        //{
        //    return await GetPoint(udc, CancellationToken.None);
        //}

        //private readonly SemaphoreSlim _pointTagLock = new SemaphoreSlim(1, 1);

        public async Task<ICachedCygNetPoint> GetPoint(string udc, CancellationToken ct)
        {
            //await _pointTagLock.WaitAsync(ct);

            try
            {
                var cachedTag = await CvsService.PointTagCache.GetPointForFacility(Tag, udc, ct);

                if (cachedTag != null)
                {
                    if (_localPointCache.TryGetValue(cachedTag.UDC, out var cachedPoint)) return cachedPoint;

                    var pntServ = await CvsService.GetAssociatedPnt();
                    cachedPoint = new CachedCygNetPoint(
                        pntServ,
                        CvsService,
                        this,
                        cachedTag);

                    _localPointCache.TryAdd(udc, cachedPoint);

                    return cachedPoint;
                }
                else
                {
                    if (!_localPointCache.ContainsKey(udc))
                        _localPointCache.TryRemove(udc, out var _);

                    return null;
                }
            }
            finally
            {
                //_pointTagLock.Release();
            }
        }

        public async Task<List<ICachedCygNetPoint>> GetPoints(IEnumerable<string> udcs)
        {
            return await GetPoints(udcs, CancellationToken.None);
        }

        public async Task<List<ICachedCygNetPoint>> GetPoints(IEnumerable<string> udcs, CancellationToken ct)
        {
            var udcFilterList = udcs.ToList();

            var allPoints = await GetPoints(ct);

            if (!udcFilterList.IsAny())
                return allPoints;

            var udcSet = new HashSet<string>(udcFilterList);

            var filteredPoints = allPoints.Where(pnt => udcSet.Contains(pnt.Tag.UDC)).ToList();

            return filteredPoints;
        }

        public async Task<List<ICachedCygNetPoint>> GetPoints()
        {
            return await GetPoints(CancellationToken.None);
        }

        public async Task<List<ICachedCygNetPoint>> GetPoints(CancellationToken ct)
        {
            //await _pointTagLock.WaitAsync(ct);

            try
            {
                var cachedTags = await CvsService.PointTagCache.GetPointsForFacility(Tag, ct);

                if (cachedTags != null)
                {
                    var udcSet = new HashSet<string>(cachedTags.Select(tag => tag.UDC));
                    var newUdcs = cachedTags.Where(tag => !_localPointCache.ContainsKey(tag.UDC)).ToList();

                    if (newUdcs.Any())
                    {
                        var pntServ = await CvsService.GetAssociatedPnt();

                        foreach (var tag in newUdcs)
                        {
                            var newCachedPoint = new CachedCygNetPoint(
                                pntServ,
                                CvsService,
                                this,
                                tag);

                            _localPointCache.TryAdd(tag.UDC, newCachedPoint);
                        }
                    }

                    foreach (var udc in _localPointCache.Keys)
                    {
                        if (!udcSet.Contains(udc))
                            _localPointCache.TryRemove(udc, out var _);
                    }

                    return _localPointCache.Values.ToList();
                }
                else
                {
                    _localPointCache.Clear();

                    return null;
                }
            }
            finally
            {
                //_pointTagLock.Release();
            }
        }

        #endregion
    }
}