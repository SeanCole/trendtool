﻿using System.Threading.Tasks;
using CygNet.Data.Core;
using CygNet.Data.Facilities;
using Techneaux.CygNetWrapper.Facilities.Attributes;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.FAC;

namespace Techneaux.CygNetWrapper.Facilities
{
    public interface ICygNetFacility
    {
        string Category { get; }
        bool IsActive { get; }

        DomainSiteService CvsDomainSiteService { get; }
        //CurrentValueService CvsService { get; }
        SiteService CvsSiteService { get; }

        Task<CurrentValueService> GetCvsService();
        string Description { get; }
        FacilityService FacService { get; }
        string Id { get; }
        FacilityTag Tag { get; }
        string TagString { get; }
        string Type { get; }
        
        Task<FacilityRecord> GetFacRecord(bool refresh = false);

        ICygNetFacility GetParentFacility(string attrName);

        bool HasAttribute(string attributeName, bool refresh = false);

        Task<FacilityAttributeValue> GetAttributeValueAsync(string attributeName, bool refresh = false);
        FacilityAttributeValue GetAttributeValue(string attrId, bool refresh = false);

        bool TryGetAttributeValue(string attributeName, out FacilityAttributeValue attributeValue, bool refresh = false);

        //bool TryGetValidCvsService(out CurrentValueService thisCvsServ);
    }
}