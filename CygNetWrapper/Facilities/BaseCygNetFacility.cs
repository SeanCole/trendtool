﻿using CygNet.Data.Core;
using CygNet.Data.Facilities;
using System;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper.Facilities.Attributes;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.FAC;

namespace Techneaux.CygNetWrapper.Facilities
{
    public abstract class BaseCygNetFacility : ICygNetFacility
    {
        protected BaseCygNetFacility(
            FacilityService newFacService,
            CurrentValueService newCvsService,
            FacilityTag newTag)
        {
            FacService = newFacService;
            CvsDomainSiteService = newCvsService.DomainSiteService;
            _cvsService = newCvsService;
            Tag = newTag;
        }

        protected BaseCygNetFacility(
           FacilityService newFacService,
           FacilityTag newTag)
        {
            FacService = newFacService;
            CvsDomainSiteService = new DomainSiteService(newFacService.DomainSiteService.DomainId, newTag.Site, newTag.Service);

            Tag = newTag;
        }

        public FacilityService FacService { get; }
        public DomainSiteService CvsDomainSiteService { get; }

        public SiteService CvsSiteService => CvsDomainSiteService.SiteService;

        private CurrentValueService _cvsService;
        public async Task<CurrentValueService> GetCvsService() =>
            _cvsService ?? (_cvsService = (await FacService.GetAssociatedCvsServices())[CvsSiteService]);

        //public bool TryGetValidCvsService(out CurrentValueService thisCvsServ)
        //{
        //    var CvsDict = await FacService.GetAssociatedCvsServices();
        //    return CvsDict.TryGetValue(CvsSiteService, out thisCvsServ);
        //}
        public FacilityTag Tag { get; }

        public string TagString => Tag.GetFacilityTag();

        public string Id => Tag?.FacilityId;

        public abstract string Description { get; }
        public abstract string Type { get; }
        public abstract string Category { get; }
        public abstract bool IsActive { get; }


        internal FacilityRecord FacRecord;

        public async Task<FacilityRecord> GetFacRecord(bool refresh = false)
        {
            if (refresh || FacRecord == null)
            {
                FacRecord = await Task.Run(() => FacService.GetFacilityRecord(Tag));
            }

            return FacRecord;
        }

        public abstract Task<FacilityAttributeValue> GetAttributeValueAsync(string attributeName, bool refresh = false);

        public abstract FacilityAttributeValue GetAttributeValue(string attributeId, bool refresh = false);

        public bool HasAttribute(string attributeName, bool refresh = false)
        {
            return FacService.IsValidActiveAttributeName(attributeName);
        }

        public abstract bool TryGetAttributeValue(string attributeName, out FacilityAttributeValue attributeValue, bool refresh = false);






        public ICygNetFacility GetDirectChildFacility(string attrName)
        {
            throw new NotImplementedException();
        }
        public ICygNetFacility GetChildFacilities(string filterName)
        {
            throw new NotImplementedException();
        }

        public ICygNetFacility GetParentFacility(string attrName)
        {
            throw new NotImplementedException();
        }

        public sealed override bool Equals(object obj)
        {
            if (obj is BaseCygNetFacility thisFac)
            {
                return Tag.Equals(thisFac.Tag);
            }

            return false;
        }

        public static bool operator ==(BaseCygNetFacility leftFacility, BaseCygNetFacility rightFacility)
        {
            if (leftFacility is null || rightFacility is null)
            {
                return ReferenceEquals(leftFacility, rightFacility);
            }
            return (leftFacility.Equals(rightFacility));
        }

        public static bool operator !=(BaseCygNetFacility leftFacility, BaseCygNetFacility rightFacility)
        {
            if (leftFacility is null || rightFacility is null)
            {
                return !ReferenceEquals(leftFacility, rightFacility);
            }
            return (!leftFacility.Equals(rightFacility));
        }

        public sealed override int GetHashCode()
        {
            return Tag.GetHashCode();
        }

        public sealed override string ToString()
        {
            return Tag.ToString();
        }
    }
}