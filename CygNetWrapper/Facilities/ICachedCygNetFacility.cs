﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper.Facilities.Attributes;
using Techneaux.CygNetWrapper.Services;

namespace Techneaux.CygNetWrapper.Facilities
{
    public interface ICachedCygNetFacility : ICygNetFacility
    {
        List<FacilityAttributeValue> GetAllCachedAttributeValues();

        List<string> GetPreCachedAttrIds();
        Task<FacilityAttributeValue> TryGetAttributeValueAsync(string attributeName, bool refresh = false);

        CurrentValueService CvsService { get; }

        string LinkedFacilityAttributeId { get; }
        ICachedCygNetFacility ParentFacility { get; }
        List<ICachedCygNetFacility> ChildFacilities { get; }
        void SetLinkedFacilities(string facLinkingAttr, ICachedCygNetFacility newParentFac, List<ICachedCygNetFacility> newChildFacs);
    }
}
