using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Techneaux.CygNetWrapper.Facilities.Filtering
{
   [Serializable]
    public class FacilityAttributeFilterCondition : FacilityFilterConditionBase
    { 
        public FacilityAttributeFilterCondition() : base()
        {
            
        }
        
        public override bool IsValid => !string.IsNullOrEmpty(AttributeId);

        public override string InvalidReason => IsValid ? "Rule is valid" : "Attribute or condition ID is blank";
    }
}
