﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Microsoft.VisualBasic.CompilerServices;
using Microsoft.VisualBasic.FileIO;
using TechneauxUtility;
using XmlDataModelUtility;

namespace Techneaux.CygNetWrapper.Facilities.Filtering
{
    public abstract class FacilityFilterConditionBase : NotifyCopyDataModel, IFacilityFilterCondition
    {
        protected FacilityFilterConditionBase()
        {
            AttributeId = "";
            AttributeDescription = "";
            ConditionalOperator = SqlConditionalOperators.Like;
            ConditionalValue = "";
        }

        public string AttributeDescription
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public string AttributeId
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public SqlConditionalOperators ConditionalOperator
        {
            get => GetPropertyValue<SqlConditionalOperators>();
            set => SetPropertyValue(value);
        }

        public string ConditionalValue
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }


        public List<string> InItemList => SplitCsvWithQuotes(ConditionalValue);

        [XmlIgnore]
        public abstract bool IsValid { get; }

        [XmlIgnore]
        public abstract string InvalidReason { get; }

        public static bool DoAttributesMatchFilters(
                  ICachedCygNetFacility fac,
                  List<IFacilityFilterCondition> facFilters)
        {
            var attrVals = fac.GetAllCachedAttributeValues();
            var attrDict = attrVals.ToDictionary(attr => attr.AttributeId, attr => attr.StringValue);

            return DoAttributesMatchFilters(attrDict, facFilters);
        }

        public static bool DoAttributesMatchFilters(
               SimpleCygnetFacility fac,
               List<IFacilityFilterCondition> facFilters)
        {
            var attrDict = fac.AttributeValues;

            return DoAttributesMatchFilters(attrDict, facFilters);
        }

        public static bool DoAttributesMatchFilters(Dictionary<string, string> attrVals, List<IFacilityFilterCondition> facFilters)
        {
            if (!facFilters.IsAny())
                return true;

            foreach (var filter in facFilters)
            {
                switch (filter.ConditionalOperator)
                {
                    case SqlConditionalOperators.In:
                        var filterInList = filter.InItemList;
                        if (filterInList.Any(subCondValue =>
                            LikeOperator.LikeString(
                                attrVals[filter.AttributeId],
                                subCondValue, 
                                Microsoft.VisualBasic.CompareMethod.Text)))
                            //objAttr == attrVals[filter.AttributeId]))
                        {
                            break;
                        }
                        else
                        {
                            return false;
                        }
                    case SqlConditionalOperators.NotIn:
                        var filterNotInList = filter.InItemList;
                        if (filterNotInList.Any(subCondValue =>
                            LikeOperator.LikeString(
                                attrVals[filter.AttributeId],
                                subCondValue,
                                Microsoft.VisualBasic.CompareMethod.Text)))
                        {
                            return false;
                        }
                        else
                        {
                            break;
                        }
                    case SqlConditionalOperators.Like:
                        if (LikeOperator.LikeString(attrVals[filter.AttributeId], filter.ConditionalValue, Microsoft.VisualBasic.CompareMethod.Text))
                        {
                            break;
                        }
                        else
                        {
                            return false;
                        }
                    case SqlConditionalOperators.NotLike:
                        if (LikeOperator.LikeString(attrVals[filter.AttributeId], filter.ConditionalValue, Microsoft.VisualBasic.CompareMethod.Text))
                        {
                            return false;
                        }
                        else
                        {
                            break;
                        }
                    default:

                        break;
                }
            }

            return true;
        }


        private static List<string> SplitCsvWithQuotes(string csvString)
        {
            using (var parser = new TextFieldParser(new StringReader(csvString)))
            {
                parser.HasFieldsEnclosedInQuotes = true;
                parser.SetDelimiters(",");

                var csvItems = new List<string>();
                while (!parser.EndOfData)
                {
                    var fields = parser.ReadFields();

                    if (fields.IsAny())
                        csvItems.AddRange(fields);
                }

                return csvItems;
            }
        }

        public override string ToString()
        {
            return $"[{AttributeId}] {ConditionalOperator} [{ConditionalValue}]";
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, null))
            {
                return false;
            }

            if (!(obj is FacilityAttributeFilterCondition objCompare))
                return false;
            return AttributeId == objCompare.AttributeId &&
                   ConditionalOperator == objCompare.ConditionalOperator &&
                   ConditionalValue == objCompare.ConditionalValue;
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
    }
}
