using System;
using System.ComponentModel;
using TechneauxUtility;
using XmlDataModelUtility;

namespace Techneaux.CygNetWrapper.Facilities.Filtering
{
    [Serializable]
    public class FacilityFilteringOptions : NotifyCopyDataModel
    {
        public FacilityFilteringOptions()
        {
            BaseRules = new BindingList<FacilityAttributeFilterCondition>();
            ChildGroups = new BindingList<ChildFacilityGroup>();
            ChildReferenceAttrKey = new IdDescKey();
            ChildOrdinalAttrKey = new IdDescKey();
        }

        public BindingList<FacilityAttributeFilterCondition> BaseRules
        {
            get => GetPropertyValue<BindingList<FacilityAttributeFilterCondition>>();
            set => SetPropertyValue(value);
        }

        public IdDescKey ChildReferenceAttrKey
        {
            get => GetPropertyValue<IdDescKey>();
            set => SetPropertyValue(value);
        }

        public IdDescKey ChildOrdinalAttrKey
        {
            get => GetPropertyValue<IdDescKey>();
            set => SetPropertyValue(value);
        }

        public BindingList<ChildFacilityGroup> ChildGroups
        {
            get => GetPropertyValue<BindingList<ChildFacilityGroup>>();
            set => SetPropertyValue(value);
        }
  
        public FacilityAttributeFilterCondition AddNewCondition(int index)
        {
            var NewCondition = new FacilityAttributeFilterCondition();
            BaseRules.Insert(index, NewCondition);

            return NewCondition;
        }

        public void DeleteCondition(int index)
        {
            if (index < 0)
                return;
            BaseRules.RemoveAt(index);
        }

    }
}
