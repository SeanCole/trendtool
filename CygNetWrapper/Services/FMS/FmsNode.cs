﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XmlDataModelUtility;

namespace Techneaux.CygNetWrapper.Services.FMS
{
    public class FmsNode : NotifyCopyDataModel
    {
        public long NodeId
        {
            get => GetPropertyValue<long>();
            set => SetPropertyValue(value);
        }

        public string NodeName
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
    }
}
