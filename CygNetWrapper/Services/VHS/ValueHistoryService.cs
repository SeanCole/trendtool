﻿using CygNet.Data.Core;
using CygNet.Data.Historian;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Techneaux.CygNetWrapper.Services.VHS
{
    public class ValueHistoryService : CygNetService
    {
        const ServiceType ThisServiceType = ServiceType.VHS;

        public ValueHistoryService(CygNetDomain parentDomain, ServiceDefinition vhsServiceDef)
            : base(parentDomain, vhsServiceDef)
        {
            if (ServiceDefinition == null)
                throw new ArgumentException("Service cannot be null");
            if (ServiceDefinition.ServiceType != ThisServiceType)
                throw new ArgumentException("Service is wrong type");

            //Task.Run(() => ReadFacServiceAttributesAsync());
        }

        public ValueHistoryService(CygNetDomain parentDomain, DomainSiteService vhsServiceDef)
            : base(parentDomain, vhsServiceDef, ServiceType.FAC)
        {
            if (vhsServiceDef == null)
                throw new ArgumentException("Service cannot be null");

            // CODE HERE => Check service type
            if (ServiceDefinition.ServiceType != ServiceType.FAC)
                throw new ArgumentException("Service is wrong type");

            //Task.Run(() => ReadFacServiceAttributesAsync());
        }

        private CygNet.API.Historian.Client _serviceClient;

        public CygNet.API.Historian.Client ServiceClient
        {
            get
            {
                if (_serviceClient == null)
                    _serviceClient = new CygNet.API.Historian.Client(DomainSiteService);

                return _serviceClient;
            }
        }

        public List<CygNetHistoryEntry> GetHistoryForward(
            PointTag tag,
            DateTime earliestDate,
            DateTime latestDate,
            IProgress<int> progress,
            CancellationToken ct)
        {
            return GetHistoryForward(
                tag,
                earliestDate,
                latestDate,
                progress,
                ct,
                -1);
        }

        public List<CygNetHistoryEntry> GetHistoryForward(
            PointTag tag,
            DateTime earliestDate,
            DateTime latestDate,
            CancellationToken ct)
        {
            return GetHistoryForward(
                tag,
                earliestDate,
                latestDate,
                null,
                ct,
                -1);
        }

        public List<CygNetHistoryEntry> GetHistoryForward(
            PointTag tag,
            DateTime earliestDate,
            DateTime latestDate,
            CancellationToken ct,
            int numEntries)
        {
            return GetHistoryForward(
                tag,
                earliestDate,
                latestDate,
                null,
                ct,
                numEntries);
        }

        private List<CygNetHistoryEntry> GetHistoryForward(
            PointTag tag,
            DateTime earliestDate,
            DateTime latestDate,
            IProgress<int> progress,
            CancellationToken ct,
            int numEntries)
        {
            earliestDate = earliestDate.ToUniversalTime();
            latestDate = latestDate.ToUniversalTime();

            var timeOffset = GetServiceTimezoneOffset();

            var maxEntries = numEntries < 1 ? int.MaxValue : numEntries;
            var counter = 0;

            var currentIntProgress = 0;
            var totalMinutes = (latestDate - earliestDate).TotalMinutes;

            progress?.Report(0);

            var histList = new List<CygNetHistoryEntry>();

            try
            {
                var hist = GetHistEnumForward(tag, earliestDate, latestDate);

                foreach (var entry in hist)
                {
                    var newHistEntry = new CygNetHistoryEntry(entry)
                    {
                        ServerUtcOffset = timeOffset
                    };

                    if (entry.TimeOrdinal > 0)
                        continue;

                    if (entry.Timestamp < earliestDate)
                        continue;

                    //if (entry.Timestamp > latestDate)
                    //    break;

                    histList.Add(newHistEntry);

                    counter++;

                    if (counter >= maxEntries)
                        break;

                    ct.ThrowIfCancellationRequested();

                    var currentProgress = Math.Floor(100 * Math.Max((double)counter / maxEntries,
                        (entry.Timestamp - earliestDate).TotalMinutes) / totalMinutes);

                    if (currentProgress > currentIntProgress)
                    {
                        currentIntProgress = (int)currentProgress;
                        currentIntProgress = currentIntProgress < 0 ? 0 : currentIntProgress;
                        currentIntProgress = currentIntProgress > 100 ? 100 : currentIntProgress;

                        //Console.WriteLine($"Progress={currentIntProgress}");

                        progress?.Report(currentIntProgress);
                    }
                }
            }
            catch (MessagingException ex) when (ex.Code == 11)
            {
                // Eat exceptions where not a history point
            }
            finally
            {
                progress?.Report(100);
            }

            return histList;
        }

        public List<CygNetHistoryEntry> GetHistoryReverse(
            PointTag tag,
            DateTime earliestDate,
            DateTime latestDate,
            CancellationToken ct,
            int? numEntries = null)
        {
            var hist = GetHistEnumReverse(tag, earliestDate, latestDate);

            var timeOffset = GetServiceTimezoneOffset();

            var maxEntries = numEntries ?? int.MaxValue;
            var counter = 0;

            var histList = new List<CygNetHistoryEntry>();
            foreach (var entry in hist)
            {
                var newHistEntry = new CygNetHistoryEntry(entry)
                {
                    ServerUtcOffset = timeOffset
                };

                histList.Add(newHistEntry);

                counter++;

                if (counter >= maxEntries)
                    break;

                ct.ThrowIfCancellationRequested();
            }

            return histList;
        }

        public IEnumerable<HistoricalEntry> GetHistEnumForward(
            PointTag tag,
            DateTime earliestDate,
            DateTime latestDate)
        {
            var thisName = new Name { ID = tag.GetTagPointIdFull() };

            var hist = ServiceClient.GetHistoricalEntries(
                thisName,
                earliestDate.ToUniversalTime(),
                latestDate.ToUniversalTime().AddSeconds(10),
                false);

            return hist.SkipWhile(entry => entry.Timestamp < earliestDate.ToUniversalTime());
        }

        public IEnumerable<HistoricalEntry> GetHistEnumReverse(
            PointTag tag,
            DateTime earliestDate,
            DateTime latestDate)
        {
            var thisName = new Name { ID = tag.GetTagPointIdFull() };

            var hist = ServiceClient.GetHistoricalEntries(
                thisName,
                earliestDate.ToUniversalTime(),
                latestDate.ToUniversalTime(),
                false);

            return hist;
        }

        public bool HistoryAvailableInInterval(PointTag pointTag, DateTime? earliestDate = null,
            DateTime? latestDate = null)
        {
            var thisEarliestDate = earliestDate ?? DateTime.Now.AddYears(-10);
            var thisLatestDate = latestDate ?? DateTime.Now;

            var theHist = GetHistoryForward(pointTag, thisEarliestDate, thisLatestDate, CancellationToken.None, 1);
            if (theHist != null && theHist.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public NameStatistics GetHistStats(PointTag pointTag)
        {
            var thisName = new Name { ID = pointTag.GetTagPointIdFull() };

            var histStats = ServiceClient.GetNameStatistics(thisName);

            return histStats;
        }


        //Public Function DeleteHistoryByDate(pointTag As String, earliestDate As Date, latestDate As Date) As Boolean
        //    ' Test if the vhs is ready
        //    If Not Me.IsReady Then Return False

        //    If HistoryAvailableInInterval(pointTag, earliestDate, latestDate) Then
        //        Dim myDelValReq As New CxVhsLib.DeleteHistoryValueExReq
        //        Dim myResp As New CxVhsLib.DeleteHistoryValueExResp

        //        myDelValReq.TagStringEx = New CxVhsLib.HistoryTagStringEx With {.TagString = pointTag}
        //myDelValReq.operations = 1

        //        Dim numRetryAttempts As Integer = 0
        //        Dim deletionSuccess As Boolean = False

        //        Do Until deletionSuccess OrElse numRetryAttempts = 4
        //            For Each entry As CxVhsLib.ValueEntryEx In GetPointHistory(pointTag, earliestDate, latestDate)
        //                myDelValReq.ValueEntryEx = entry
        //                _vhsClient.DeleteHistoryValueEx(myDelValReq, myResp)
        //            Next
        //            deletionSuccess = Not HistoryAvailableInInterval(pointTag, earliestDate, latestDate)
        //            numRetryAttempts += 1
        //        Loop
        //        If deletionSuccess Then
        //            Return True
        //        Else
        //            Return False
        //        End If
        //    Else
        //        Return True
        //    End If
        //End Function

        //Public Function DeleteAllHistory(pointTag As String) As Boolean
        //    ' Test if the vhs is ready
        //    If Not Me.IsReady Then Return False

        //    Dim myPointDelReq As New CxVhsLib.DeleteHistoryPointReq
        //    Dim myResp As New CxVhsLib.DeleteHistoryPointResp

        //    myPointDelReq.TagString = New CxVhsLib.HistoryTagString With {.TagString = pointTag}

        //If _vhsClient.DeleteHistoryPoint(myPointDelReq, myResp) = 0 Then
        //    Return True
        //Else
        //        Return False
        //    End If
        //End Function
    }
}