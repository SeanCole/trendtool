﻿using System;
using System.Threading.Tasks;
using CygNet.Data.Core;
using CygNet.API.Points;
using Techneaux.CygNetWrapper.Services.FAC;
using Techneaux.CygNetWrapper.Services.VHS;
using Nito.AsyncEx;
using Serilog;
using Techneaux.CygNetWrapper.Points.PointCaching;
using Techneaux.CygNetWrapper.Services.FMS;
using Techneaux.CygNetWrapper.Services.PNT;

namespace Techneaux.CygNetWrapper.Services
{
    public class CurrentValueService : CygNetService
    {
        private RealtimeClient _serviceClient;

        public RealtimeClient ServiceClient
        {
            get
            {
                if (_serviceClient == null)
                {
                    try
                    {
                        _serviceClient = new RealtimeClient(DomainSiteService);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, $"Failed to create service client for {DomainSiteService}");
                    }
                }

                return _serviceClient;
            }
        }



        public override bool IsServiceAvailable => ServiceClient != null;

        public CurrentValueService(CygNetDomain myDomain, ServiceDefinition newCvsServiceDef) : base(myDomain,
            newCvsServiceDef)
        {
            if (ServiceDefinition == null)
                throw new ArgumentException("Service cannot be null");

            _assocPnt = new AsyncLazy<PointService>(async () =>
                await Task.Run(() => ParentDomain.GetAssociatedService(this, ServiceType.PNT)) as PointService);
            _assocFac = new AsyncLazy<FacilityService>(async () =>
                await Task.Run(() => ParentDomain.GetAssociatedService(this, ServiceType.FAC)) as FacilityService);
            _assocVhs = new AsyncLazy<ValueHistoryService>(async () =>
                await Task.Run(() => ParentDomain.GetAssociatedService(this, ServiceType.VHS)) as ValueHistoryService);

            PointTagCache = new PointTagCache(this);
            PointAttributeValueCache = new PointAttributeCache(this);
        }

        public PointTagCache PointTagCache { get; }
        public PointAttributeCache PointAttributeValueCache { get; }

        private readonly AsyncLazy<PointService> _assocPnt;

        public async Task<PointService> GetAssociatedPnt()
        {
            return await _assocPnt;
        }

        private readonly AsyncLazy<FacilityService> _assocFac;

        public async Task<FacilityService> GetAssociatedFac()
        {
            return await _assocFac;
        }

        private readonly AsyncLazy<ValueHistoryService> _assocVhs;

        public async Task<ValueHistoryService> GetAssociatedVhs()
        {
            return await _assocVhs;
        }


        public bool PointExists(PointTag tag)
        {
            return ServiceClient.PointExists(tag);
        }

        public PointValueRecord GetPointValue(PointTag tag)
        {
            return ServiceClient.GetPointValueRecord(tag);
        }

        public PointValueRecord TryGetPointValue(PointTag tag)
        {
            throw new NotImplementedException();
        }

        public double GetPointVersion(PointTag tag)
        {
            return ServiceClient.GetPointVersion(tag);
        }

        public void WriteValue(
            PointTag tag,
            object newValue,
            DateTime? newTimestamp
        )
        {
            ServiceClient.SetPointValue(tag, newValue, newTimestamp);
        }

        public void WriteValue(
            PointTag tag,
            object value,
            DateTime? timeStamp,
            BaseStatusFlags baseStatusMask,
            BaseStatusFlags baseStatus,
            UserStatusFlags userStatusMask,
            UserStatusFlags userStatus
        )
        {
            ServiceClient.SetPointValue(tag, value, timeStamp, baseStatusMask, baseStatus, userStatusMask, userStatus);
        }
    }
}