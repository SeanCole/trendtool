﻿using Serilog;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Techneaux.CygNetWrapper
{
    public static class LibraryLogging
    {
        const string ThisLoggingLibraryName = "CygNetWrapper";

        public static LoggingLevelSwitch LogLevelSwitch { get; set; } = new LoggingLevelSwitch();

        public static void InitLogging(
            string parentApplicationId,
            LogEventLevel loggingLevel)
        {
            var fileName = $"{ThisLoggingLibraryName}_Log_({parentApplicationId}_" + "{Date}.txt";

            LogLevelSwitch = new LoggingLevelSwitch(loggingLevel);

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(LogLevelSwitch)
                .WriteTo
                .RollingFile($@"{AppDomain.CurrentDomain.BaseDirectory}\Logs\{fileName}", shared: true)
                .CreateLogger();
        }

        //private static readonly ConcurrentDictionary<string, Stopwatch> ObjStopWatches = new ConcurrentDictionary<string, Stopwatch>();

        //private static string GetStopWatchKey(string desc, string callerName)
        //{
        //    var currentThreadId = Thread.CurrentThread.ManagedThreadId;
        //    var currentProcessId = Process.GetCurrentProcess().Id;

        //    var newKey = $"{callerName}.{desc}.{currentProcessId}.{currentThreadId}";

        //    return newKey;
        //}

        //public static void StartTimingProcess(string desc = "", ILogger localLog = null, [CallerMemberName] string callerName = "")
        //{
        //    var thisKey = GetStopWatchKey(desc, callerName);

        //    if (!ObjStopWatches.ContainsKey(thisKey))
        //    {
        //        ObjStopWatches[thisKey] = new Stopwatch();
        //    }
        //    var objStopWatch = ObjStopWatches[thisKey];

        //    objStopWatch.Start();

        //    var logMessage = $"Stopwatch [Method={callerName}, Description={desc}] Started";

        //    localLog?.Verbose(logMessage);
        //    Log.Verbose(logMessage);
        //}

        //public static TimeSpan EndTimingProcess(string desc = "", ILogger localLog = null, [CallerMemberName] string callerName = "")
        //{
        //    var thisKey = GetStopWatchKey(desc, callerName);

        //    if (!ObjStopWatches.ContainsKey(thisKey))
        //    {
        //        ObjStopWatches[thisKey] = new Stopwatch();
        //    }

        //    var objStopWatch = ObjStopWatches[thisKey];

        //    if (objStopWatch.IsRunning)
        //        objStopWatch.Stop();

        //    var elapsed = objStopWatch.Elapsed;

        //    objStopWatch.Reset();

        //    var logMessage = $"Stopwatch [Method={callerName}, Description={desc}] Stopped: Elapsed time (sec): {objStopWatch.Elapsed.TotalSeconds}";

        //    localLog?.Verbose(logMessage);
        //    Log.Verbose(logMessage);

        //    return elapsed;
        //}

        //static Logger() {
        //    // Get calling exe name            


        //    // Init log

        //}

        //public static Logger Instance
        //{
        //    get
        //    {
        //        if (instance == null)
        //        {
        //            instance = new Logger();
        //        }
        //        return instance;
        //    }
        //}

        //public static void LogError(Exception ex, [CallerMemberName]string memberName = "")
        //{
        //    StackFrame frame = new StackFrame(1);
        //    var method = frame.GetMethod();
        //    var type = method.DeclaringType;
        //    var name = method.Name;

        //    Log.Error(ex, "CallerMemberName=" + memberName + "[" + "method=" + method + "," + "type=" + type + "]");
        //}

        //public static void LogInfo(string info, [CallerMemberName]string memberName = "")
        //{
        //    StackFrame frame = new StackFrame(1);
        //    var method = frame.GetMethod();
        //    var type = method.DeclaringType;
        //    var name = method.Name;

        //    Log.Information(info, "CallerMemberName=" + memberName + "[" + "method=" + method + "," + "type=" + type + "]");
        //}

        //public static void LogInfo(string messageTemplate, params object[] logParams)
        //{
        //    StackFrame frame = new StackFrame(1);
        //    var method = frame.GetMethod();
        //    var type = method.DeclaringType;
        //    var name = method.Name;

        //    Log.Information(messageTemplate, logParams);
        //}
    }
}
