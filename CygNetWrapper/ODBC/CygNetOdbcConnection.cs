﻿using System;
using System.Data.Odbc;
using System.Threading.Tasks;
using Serilog;

namespace Techneaux.CygNetWrapper.ODBC
{
    public class CygNetOdbcConnection
    {
        private const string Dsn = "DSN=CygNet;";

        public static async Task<bool> TestOdbcConnection()
        {
            try
            {
                using (var dbConnection = new OdbcConnection(Dsn))
                {
                    await dbConnection.OpenAsync();
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Warning(ex, "Odbc connection failed");
                return false;
            }
        }

        public const string DsnString = "DSN=CygNet;Uid=<username>;Pwd=<password>;";
    }
}
