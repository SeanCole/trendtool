﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Reflection;
using System.Dynamic;

namespace XmlDataModelUtility
{
    public class NotifyDynamicBase<T> : DynamicObject, INotifyPropertyChanged where T : INotifyPropertyChanged
    {
        private readonly Guid _id = Guid.NewGuid();
        public Guid Id => _id;

        #region "Interface Implementation"
        private readonly Dictionary<string, object> _propertyBackingDictionary = new Dictionary<string, object>();

        private readonly Dictionary<IBindingList, string> _listNames = new Dictionary<IBindingList, string>();

        protected T GetPropertyValue<T>([CallerMemberName] string propertyName = null)
        {
            if (propertyName == null) throw new ArgumentNullException("propertyName");

            if (_propertyBackingDictionary.TryGetValue(propertyName, out var value))
            {

                return (T)value;
            }

            return default;
        }

        protected bool SetPropertyValue<T>(T newValue, [CallerMemberName] string propertyName = null)
        {
            if (newValue is INotifyPropertyChanged &&
                newValue != null &&
                newValue is IBindingList thisBindingList)
            {
                _listNames[thisBindingList] = propertyName;
                thisBindingList.ListChanged += PropObj_ListChanged;
            }

            if (propertyName == null) throw new ArgumentNullException("propertyName");

            if (EqualityComparer<T>.Default.Equals(newValue, GetPropertyValue<T>(propertyName))) return false;

            _propertyBackingDictionary[propertyName] = newValue;
            OnPropertyChanged(propertyName);
            RaiseEventsForDependentProperties(propertyName);
            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        private void PropObj_ListChanged(object sender, ListChangedEventArgs e)
        {

            if (!_listNames.TryGetValue(sender as IBindingList, out var propName))
                propName = e.ListChangedType.ToString();

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        private void PropObj_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(e.PropertyName));
        }

        public T MyDataModel { get; }

        private readonly bool _modelRaisesPropertyChangedEvents = false;
        public NotifyDynamicBase(T sourceDataModel)
        {
            MyDataModel = sourceDataModel;

            // Raises its own property changed events
            if (MyDataModel is INotifyPropertyChanged)
            {
                _modelRaisesPropertyChangedEvents = true;
                var raisesPropChangedEvents =
                  MyDataModel as INotifyPropertyChanged;

                raisesPropChangedEvents.PropertyChanged +=
                  (sender, args) =>
                  OnPropertyChanged(args.PropertyName);

                raisesPropChangedEvents.PropertyChanged +=
                  (sender, args) =>
                  RaiseEventsForDependentProperties(args.PropertyName);
            }
        }

        #endregion

        #region "DynamicObject Overrides"
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            var propertyName = binder.Name;
            //Console.WriteLine($"TryGet:{propertyName}");


            PropertyInfo property = null;
            if (MyDataModel != null)
            {

                property = MyDataModel.GetType().GetProperty(propertyName);
            }

            if (property == null || property.CanRead == false)
            {
                result = null;
                return false;
            }

            result = property.GetValue(MyDataModel, null);
            //RaiseEventsForDependentProperties(propertyName);
            return true;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            var propertyName = binder.Name;
            var property = MyDataModel.GetType().GetProperty(propertyName);

            //Console.WriteLine($"TrySet:{propertyName}");

            if (property == null || property.CanWrite == false)
                return false;

            property.SetValue(MyDataModel, value, null);

            if (_modelRaisesPropertyChangedEvents == false)
                RaiseEventsForDependentProperties(propertyName);

            return true;
        }

        private void RaiseEventsForDependentProperties(string nameofPropertyChanged)
        {
            var propertyNames = GetType().GetProperties();

            foreach (var property in propertyNames)
            {
                var customAttributes = property.GetCustomAttributes(typeof(AffectedByOtherPropertyChangeAttribute), true);
                //     if(property.GetType().IsSubclassOf(this))
                foreach (AffectedByOtherPropertyChangeAttribute propertyAtttribute in customAttributes)
                {
                    if (propertyAtttribute.TriggerProperty == nameofPropertyChanged)
                    {
                        OnPropertyChanged(property.Name);
                    }
                }

            }
        }
    }
    #endregion
}
