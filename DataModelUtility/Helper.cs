﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace XmlDataModelUtility
{
    internal static class Helper
    {
        public static List<Type> FindAllDerivedTypes<T>()
        {
            var listOfBs = (from domainAssembly in AppDomain.CurrentDomain.GetAssemblies()
                            from assemblyType in domainAssembly.GetTypes()
                            where typeof(T).IsAssignableFrom(assemblyType)
                            select assemblyType).ToArray();

            return listOfBs.ToList();
        }
    }
}
