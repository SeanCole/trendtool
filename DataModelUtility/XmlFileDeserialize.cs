﻿using Serilog;
using System;
using System.IO;
using System.Security.Policy;
using System.Xml.Serialization;

namespace XmlDataModelUtility
{
    public static class XmlFileSerialization<T>
    {
        public static T LoadModelFromFile(string strPath)
        {
            try
            {
                using (var sr = new StreamReader(strPath))
                {
                    var xmlSerial = new XmlSerializer(typeof(T), NotifyCopyDataModel.DerivedTypes);
                    var readConfig = (T)xmlSerial.Deserialize(sr);

                    return readConfig;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Deserialization failed");
                return default;
            }
        }
        
        public static bool SaveModelToFile(T srcModel, string strPath)
        {
            try
            {
                using (var sw = new StreamWriter(strPath))
                {                    
                    var derivedTypeList = NotifyCopyDataModel.DerivedTypes;
                    var xmlSerial = new XmlSerializer(typeof(T), derivedTypeList);
                    xmlSerial.Serialize(sw, srcModel);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Error saving file to path [{strPath}]");
                return false;
            }
        }

        public static bool TryWriteObjectToFile(T src, string fileName)
        {
            var path = $@"{Environment.CurrentDirectory}\{fileName}";

            try
            {
                using (var sw = new StreamWriter(path))
                {
                    var xmlSerial = new XmlSerializer(typeof(T));
                    xmlSerial.Serialize(sw, src);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Error saving object to path [{path}]");
                return false;
            }
        }


        public enum FileOperationResult
        {
            Successful
        }
    }
}
