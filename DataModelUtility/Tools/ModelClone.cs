﻿namespace XmlDataModelUtility.Tools
{
    public class ModelClone<T> where T : NotifyCopyDataModel, new()
    {
        private T _copy = null; 
        private readonly T _sourceConfig;

        public ModelClone(T sourceConfig)
        {
            _sourceConfig = sourceConfig;
        }

        public T Copy
        {
            get
            {
                if (_copy == null)
                    _copy = _sourceConfig.Clone() as T;

                return _copy;
            }
        }
    }
}