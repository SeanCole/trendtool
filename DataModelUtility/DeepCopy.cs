﻿using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace XmlDataModelUtility
{
    public interface IDeepCopyable
    {
        object DeepCopy();
    }



    public static class DeepCopyObject
    {
        public static Dictionary<Type, XmlSerializer> CloningSerializerSet = new Dictionary<Type, XmlSerializer>();

        public static T Copy<T>(T inputObject)
        {
            // Serialize the source config to an xml string

            object destObject;

            try
            {
                //XmlSerializer XmlSerial = new XmlSerializer(typeof(T));
                if (!CloningSerializerSet.ContainsKey(typeof(T)))
                {
                    CloningSerializerSet[typeof(T)] = new XmlSerializer(typeof(T), NotifyCopyDataModel.DerivedTypes);
                }

                var xmlSerial = CloningSerializerSet[typeof(T)];
                                

                //XmlSerializer XmlSerial = new XmlSerializer(typeof(T), NotifyCopyDataModel.DerivedTypes);

                string configXmlString;

                using (var sw = new StringWriter())
                {
                    xmlSerial.Serialize(sw, inputObject);
                    configXmlString = sw.ToString();
                }

                using (var sr = new StringReader(configXmlString))
                {
                    destObject = xmlSerial.Deserialize(sr);
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex, "Failed to copy object");
                destObject = null;
            }

            return (T)destObject;
        }
    }
}
