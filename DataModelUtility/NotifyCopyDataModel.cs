﻿using System;

namespace XmlDataModelUtility
{
    public class NotifyCopyDataModel : NotifyModelBase, ICloneable
    {
        public object Clone()
        {
            return DeepCopyObject.Copy(this);
        }

        static NotifyCopyDataModel()
        {
            DerivedTypes = Helper.FindAllDerivedTypes<NotifyCopyDataModel>().ToArray();
        }

        public static Type[] DerivedTypes { get; private set; }
    }
}
