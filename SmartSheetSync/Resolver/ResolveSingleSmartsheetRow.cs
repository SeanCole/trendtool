﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CygNetRuleModel.Resolvers;
using GenericRuleModel.Resolvers;
using Techneaux.CygNetWrapper;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using TechneauxReportingDataModel.CygNet.Utility;
using TechneauxReportingDataModel.SmartSheet.Enumerable;
using TechneauxUtility;

namespace TechneauxSmartSheetSync.Resolver
{
    public static class ResolveSmartsheet
    {
        public static async Task<SmartSheetHistoryRow> ResolveSingleRow(
            ICachedCygNetFacility srcFac,
            IEnumerable<ChildFacilityGroup> childGrps,
            IEnumerable<SmartSheetReportElm> rules,
            RollupPeriod rollupPer,
            CancellationToken ct)
        {
            var ruleList = rules.ToList();
            var childGroupList = childGrps.ToList();

            // Check parameters
            if (!ruleList.IsAny())
                throw new ArgumentNullException(nameof(rules), "Rules must not be null or empty");

            if(srcFac == null)
                throw new ArgumentNullException(nameof(srcFac), "Facility must not be null");

            var newRow = new SmartSheetHistoryRow();

            foreach (var rule in ruleList)
            {
                var val = await CygNetResolver.ResolveRule(srcFac, childGroupList, rule, rollupPer, ct);

                if (rule.FormatConfig.EnableFormat)
                {
                    if (val.HasError)
                    {
                        if (!string.IsNullOrEmpty(rule.DefaultIfError))
                        {
                            val = FormattingResolver.Resolve(new CygNetConvertableValue(rule.DefaultIfError), rule.FormatConfig);
                        }
                        else
                        {
                            val = FormattingResolver.Resolve(val, rule.FormatConfig);
                        }
                    }
                    else
                    {
                        val = FormattingResolver.Resolve(val, rule.FormatConfig);
                    }
                }

                newRow.CellValues.Add(rule.SmartSheetColumn.IdString, val);
            }


            return newRow;
        }

    }
}
