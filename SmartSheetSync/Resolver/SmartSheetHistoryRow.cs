﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechneauxReportingDataModel.SmartSheet.SubOptions;
using TechneauxUtility;

namespace TechneauxSmartSheetSync.Resolver
{
    public class SmartSheetHistoryRow : IValueSet
    {
        public Dictionary<string, IConvertibleValue> CellValues { get; set; } = new Dictionary<string, IConvertibleValue>();

     

        public SmartSheetItemId KeyColumnId { get; set; }

        public IConvertibleValue KeyValue
        {
            get
            {
                if (KeyColumnId == null)
                    return null;

                if (!CellValues.TryGetValue(KeyColumnId.IdString, out var val))
                    throw new InvalidOperationException("The key column value isn't present in the value dictionary");

                return val;
            }
        }

        public IConvertibleValue this[string index]
        {
            get => CellValues[index];
            set => CellValues[index]=value;
        }
    }
}
