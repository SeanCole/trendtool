﻿using Smartsheet.Api.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TechneauxSmartSheetSync
{
    public class RowWrapper
    {
        //private static RowWrapper GetNewRow()
        //{
        //    new Row.AddRowBuilder(true, null, null, null, null);
        //}

        public bool IsNew { get; }

        private Row ExistingRow { get; set; }

        private Row.AddRowBuilder NewRowBuilder { get; set; }

        //private readonly Dictionary<string, Cell> _cellDict = new Dictionary<string, Cell>();
        private readonly  Dictionary<long, Cell> _cellDictId = new Dictionary<long, Cell>();

        private readonly SheetWrapper _parentSheet;
        
        public RowWrapper(Row srcRow, SheetWrapper parentSheet)
        {
            ExistingRow = srcRow;
            _parentSheet = parentSheet;
            IsNew = false;

            if (srcRow.Cells != null && srcRow.Cells.Any())
            {
                foreach (Cell cell in srcRow.Cells)
                {
                    var cellColName = _parentSheet.ColIdToName[cell.ColumnId.Value];
                    //_cellDict[cellColName] = cell;
                    _cellDictId[cell.ColumnId.Value] = cell;
                }
            }
        }

        public RowWrapper(SheetWrapper parentSheet)
        {
            IsNew = true;
            NewRowBuilder = new Row.AddRowBuilder(true, null, null, null, null);
            _parentSheet = parentSheet;
        }

        public object GetCellValue(long colName)
        {
            // Check if cell exists
            if (_cellDictId.TryGetValue(colName, out var thisCell))
            {
                return thisCell.Value;
            }
            else
            {
                return null;
            }
        }

        //public void SetCellValue(string colName, object value)
        //{
        //    // If cell doesn't exist, create one
        //    if (_cellDict.TryGetValue(colName, out var thisCell))
        //    {
        //        var updater = new Cell.UpdateCellBuilder(thisCell.ColumnId.Value, value);

        //        thisCell = updater.Build();
        //    }
        //    else
        //    {
        //        var newId = _parentSheet.ColNameToId[colName];
        //        var newCellBuild = new Cell.AddCellBuilder(newId, value);

        //        var newCell = newCellBuild.Build();

        //        _cellDict[colName] = newCell;
        //    }
        //}

        private HashSet<long> _updatedCells = new HashSet<long>();


        public void SetCellValue(long colId, object value)
        {
            if (value==null)
                return;
            

            _updatedCells.Add(colId);
            
            // If cell doesn't exist, create one
            if (_cellDictId.TryGetValue(colId, out var thisCell))
            {
                thisCell.Value = value;

                //var updater = new Cell.UpdateCellBuilder(thisCell.ColumnId.Value, value);

                //updater.Build();
            }
            else
            {
                var newId = colId;
                var newCellBuild = new Cell.AddCellBuilder(newId, value);
                
                var newCell = newCellBuild.Build();

                _cellDictId[colId] = newCell;
            }
        }

        public Row GetFinalRow()
        {
            if (IsNew)
            {
                NewRowBuilder.Build();
                NewRowBuilder.SetCells(_cellDictId.Values.ToList());
                var newRow = NewRowBuilder.Build();
                
                return newRow;
            }
            else
            {
                var updateBuilder = new Row.UpdateRowBuilder(ExistingRow.Id.Value);
                updateBuilder.SetCells(_updatedCells.Select(key => _cellDictId[key]).Where(c => c.Value != null).ToList());

                var updatedRow = updateBuilder.Build();

                return updatedRow;
            }
        }
    }
}
