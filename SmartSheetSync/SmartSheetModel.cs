﻿using Serilog;
using Smartsheet.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using TechneauxReportingDataModel.SmartSheet.SubOptions;

namespace TechneauxSmartSheetSync
{
    public static class SmartSheetModel
    {
        private static readonly Dictionary<string, SmartsheetClient> ConnectionCache
            = new Dictionary<string, SmartsheetClient>();

        private static readonly SemaphoreSlim GetConnectionLock = new SemaphoreSlim(1, 1);

        private static async Task<SmartsheetClient> GetConnectedClient(string accessToken)
        {
            try
            {
                await GetConnectionLock.WaitAsync();

                if (!ConnectionCache.TryGetValue(accessToken, out var ss))
                {
                    ss = new SmartsheetBuilder().SetAccessToken(accessToken).Build();
                    ConnectionCache[accessToken] = ss;
                }

                return ss;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error getting SmartSheet client");
                throw;
            }
            finally
            {
                GetConnectionLock.Release();
            }
        }

        public static async Task<IObservable<SheetWrapper>> GetSheetList(string srcAuthenticationId)
        {
            SmartsheetClient ss;

            try
            {
                ss = await GetConnectedClient(srcAuthenticationId);

                var sheetList = ss.SheetResources
                    .ListSheets(null, null, null)
                    .Data;

                var obsSheets = sheetList.Where(sht => sht.Id.HasValue)
                    .ToObservable()
                    .Select(sht => Observable.FromAsync(async () =>
                    {
                        var sheet = await Task.Run(() =>
                            ss.SheetResources.GetSheet(sht.Id.Value, null, null, null, null, null, null, null));

                        return new SheetWrapper(ss, sheet);
                    }))
                    .Merge(1);

                return obsSheets;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Exception getting smartsheet client");
                return Observable.Empty<SheetWrapper>();
            }
        }

        public static async Task<List<SheetWrapper>> GetSheetWrapperList(string srcConnOpts)
        {
            SmartsheetClient ss;

            try
            {
                ss = await GetConnectedClient(srcConnOpts);

                var sheetList = ss.SheetResources
                    .ListSheets(null, null, null)
                    .Data;

                var wrappedList = sheetList
                    .Where(sht => sht.Id.HasValue)
                    .Select(sht => new SheetWrapper(ss, sht))
                    //.Select(sht => new SheetWrapper(ss, ss.SheetResources.GetSheet(sht.Id.Value, null, null, null, null, null, null, null)))
                    .ToList();

                return wrappedList;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Exception getting smartsheet client");
                return new List<SheetWrapper>();
            }
        }

        public static async Task<SheetWrapper> TryGetSheet(
            string accessToken,
            SheetSelectionOptions sheetOpts)
        {
            try
            {
                var ss = await GetConnectedClient(accessToken);
                if (sheetOpts.SheetId == null)
                    return new SheetWrapper();
                var thisSmartSheet = await Task.Run(() =>
                    ss.SheetResources.GetSheet(sheetOpts.SheetId.Id, null, null, null, null, null, null, null));

                var newSheet = new SheetWrapper(ss, thisSmartSheet);

                return newSheet;
            }
            catch (Exception e)
            {
                Log.Error(e, "Failed to get smartsheet");
                return new SheetWrapper();
            }
        }
    }
}

